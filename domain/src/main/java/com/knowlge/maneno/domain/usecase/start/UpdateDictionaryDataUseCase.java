package com.knowlge.maneno.domain.usecase.start;

import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Country;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.School;
import com.knowlge.maneno.domain.model.Tag;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 */

public class UpdateDictionaryDataUseCase extends UseCase<Void, Void> {

    @Inject
    public UpdateDictionaryDataUseCase() {}

    @Override
    protected Observable<Void> createUseCaseObservable(Void params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Void, Void>() {
            @Override
            public Void execute(Void params) throws Exception {
                // This will be called during application start and when network connection is achieved, so:
                // todo: make sure that it is executed successfully only once per whole application single run - use in-memory global flag?

                if (Util.isOnline()) {
                    // todo - define retries count for each following rest API calls.

                    // todo - think about moving following operations into different usecases, so single dictionary operation is retries if failure occurs?
                    List<Tag> tags = restApi.getTags();
                    localRepository.updateTags(tags);

                    List<Language> languages = restApi.getLanguages();
                    localRepository.updateLanguages(languages);

                    List<ColorCode> colorCodes = restApi.getColorCodes();
                    localRepository.updateColorCodes(colorCodes);

                    List<Country> countries = restApi.getCountries();
                    localRepository.updateCountries(countries);

                    List<School> schools = restApi.getSchools();
                    localRepository.updateSchools(schools);

                    // todo - mark dictionary data as updated, so no second run is performed during this application run.
                }

                return null;
            }
        });
    }
}
