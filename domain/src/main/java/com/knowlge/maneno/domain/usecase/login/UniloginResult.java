package com.knowlge.maneno.domain.usecase.login;

/**
 * Created by Bogumił Sikora.
 */

public class UniloginResult {
    public static final int SUCCESS = 1;
    public static final int TEMP_LICENSE = -1;
    public static final int FAILED = 0;
}
