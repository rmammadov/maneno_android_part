package com.knowlge.maneno.domain.parser.epub;

import com.knowlge.maneno.domain.model.Chapter;

import java.util.List;

/**
 * Created by Bogumił Sikora.
 */

public class EpubParseResult {
    private String bookTitle;
    private String authors;
    private List<Chapter> chapters;

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }
}
