package com.knowlge.maneno.domain.parser.epub.model.toc;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by Bogumił Sikora.
 */
@Root(strict = false)
public class NavPoint {

    @Element(name = "text")
    @Path("navLabel")
    private String label;

    @Attribute(name = "src")
    @Path("content")
    private String href;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
