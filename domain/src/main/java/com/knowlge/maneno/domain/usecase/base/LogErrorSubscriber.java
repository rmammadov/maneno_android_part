package com.knowlge.maneno.domain.usecase.base;

import com.knowlge.maneno.domain.log.Log;

/**
 * Created by Bogumił Sikora.
 */

public class LogErrorSubscriber extends rx.Subscriber {

    private Log log;

    public LogErrorSubscriber(Log log) {
        this.log = log;
    }

    @Override
    public void onCompleted() {}

    @Override
    public void onError(Throwable e) {
        if (log != null) {
            log.e(e);
        }
    }

    @Override
    public void onNext(Object o) {}
}
