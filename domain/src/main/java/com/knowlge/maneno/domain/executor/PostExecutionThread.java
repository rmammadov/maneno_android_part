package com.knowlge.maneno.domain.executor;

import rx.Scheduler;

/**
 * Created by Bogumił Sikora.
 */
public interface PostExecutionThread {
    Scheduler getScheduler();
}

