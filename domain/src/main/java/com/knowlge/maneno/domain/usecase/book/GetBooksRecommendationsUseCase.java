package com.knowlge.maneno.domain.usecase.book;

import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Bogumił Sikora.
 */

public class GetBooksRecommendationsUseCase extends GetBooksDataUseCase<GetBooksRecommendationsUseCase.Params,List<BookInfo>> {

    @Inject
    public GetBooksRecommendationsUseCase() {}

    @Override
    protected Observable<List<BookInfo>> createUseCaseObservable(Params params) {
        return ObservableCreator.createMultipleTimeObservable(params, true, new ObservableCreator.MultipleTime<Params, List<BookInfo>>() {
            @Override
            public void execute(Params params, Subscriber<? super List<BookInfo>> subscriber) throws Exception {
                // todo - Question (Improvement): Would it be possible that "api/books/recommendations" returns ids with book "timestamp" for each book?
                // Then, we could check "timestamp" in local cache before calling api/books/{id}/info.

                List<Integer> recommendedBookIds = restApi.getBookRecommendations();

                Map<Integer, BookInfo> localBookInfos = localRepository.getBookInfosMap(recommendedBookIds);

                // For each book id call: api/books/{id}/info.
                List<BookInfo> result = new ArrayList<>();
                if (recommendedBookIds != null && recommendedBookIds.size() > 0) {
                    for (Integer bookId : recommendedBookIds) {
                        if (localBookInfos != null && localBookInfos.containsKey(bookId)) {
                            result.add(localBookInfos.get(bookId));
                        } else {
                            BookInfo bookInfo = restApi.getBookInfo(bookId);

                            if (bookInfo != null) {
                                result.add(bookInfo);

                                setProperties(bookInfo);

                                localRepository.saveBookInfo(bookInfo);
                            }
                        }
                    }
                }

                // Send result to UI - it contains combination of local repository and rest API books list.
                subscriber.onNext(result);

                if (recommendedBookIds != null) {
                    // For books that are already in local repository, call getBookInfo rest API later and update data if necessary.
                    for (Integer id : recommendedBookIds) {
                        if (localBookInfos != null && localBookInfos.containsKey(id)) {
                            BookInfo bookInfo = restApi.getBookInfo(id);

                            if (bookInfo != null) {
                                localRepository.updateBookInfo(mergeBookInfoData(localBookInfos.get(id), bookInfo));
                            }
                        }
                    }
                }
            }
        });
    }

    public static class Params { } // todo - needed?
}
