package com.knowlge.maneno.domain.repository;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Bogumił Sikora.
 */

public interface FileRepository {
    void saveFile(String filePath, InputStream inputStream) throws IOException;

    void unzipFileInto(String zipFileName, String destinationDirectoryName) throws IOException;

    void unzipFileInto(File zipFile, String destinationDirectoryName) throws IOException;

    boolean delete(String fileName) throws IOException;
    boolean delete(File epubFile) throws IOException;

    File[] getFiles(String folderName);

    boolean exists(String filePath);

    File getFile(String filePath);

    String readFile(String filePath) throws IOException;

    String getParentAbsolutePath(String filePath);
}
