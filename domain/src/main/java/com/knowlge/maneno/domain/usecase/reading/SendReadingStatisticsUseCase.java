package com.knowlge.maneno.domain.usecase.reading;

import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.ReadingProgress;
import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Use case called in following situations:
 *     - at application start if any user is already logged in,
 *     - when network connection is established,
 *     - after user logged in successfully.
 * It sends all reading statistics data which was not sent yet, because of no network connection or application kill.
 *
 * Created by Bogumił Sikora.
 */

public class SendReadingStatisticsUseCase extends ReadingStatisticsSendableUseCase<Void, Void> {

    @Inject
    public SendReadingStatisticsUseCase() {}

    @Override
    protected Observable<Void> createUseCaseObservable(Void params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Void, Void>() {
            @Override
            public Void execute(Void params) throws Exception {
                Integer currentUserId = localRepository.getCurrentUserId();

                if (currentUserId != null) {
                    // Send all locally cached ReadingSessions and ReadingPageStatistics which belongs to currently logged in user.
                    List<ReadingSession> readingSessions = localRepository.getReadingSessions(currentUserId);
                    if (readingSessions != null) {
                        for (ReadingSession session : readingSessions) {
                            sentSessionStatistics(session);
                        }
                    }
                }

                return null;
            }
        });
    }

    private void sentSessionStatistics(ReadingSession session) throws IOException {
        if (Util.isOnline() && session != null) {

            // TODO - uncomment and correct if timeInterval for session is updated every time BookPageOpenedUseCase is called - check todo.
            // Otherwise following does not make, because endReadingDateMillis is set always with timeInterval in StopReadingSessionUseCase.

//            List<ReadingPageStatistics> readingPageStatisticsList = localRepository.getReadingPagesStatistics(session.getId());
//
//            if (session.getEndReadingDateMillis() == null) { // It is possible when app was killed during reading for example.
//                // Set endReadingDateMillis based on latest ReadingPageStatistics pageStartTimeMillis/pageEndTimeMillis.
//                if (readingPageStatisticsList != null && readingPageStatisticsList.size() > 0) {
//                    Long endReadingDateMillis = 0L;
//                    for (ReadingPageStatistics rps : readingPageStatisticsList) {
//                        if (rps.getPageEndTimeMillis() != null) {
//                            endReadingDateMillis = Math.max(endReadingDateMillis, rps.getPageEndTimeMillis());
//                        }
//
//                        if (rps.getPageStartTimeMillis() != null) {
//                            endReadingDateMillis = Math.max(endReadingDateMillis, rps.getPageStartTimeMillis());
//                        }
//                    }
//
//                    if (endReadingDateMillis > 0) {
//                        session.setEndReadingDateMillis(endReadingDateMillis);
//                    }
//                } else {
//                    // Invalid reading session: no end date, no any page statistics, ignore and delete session.
//                    localRepository.deleteReadingSession(session);
//                    return;
//                }
//            }

            sentSessionStatistics(session, true, true);
        }
    }

    @Override
    protected Void createSentResult(ReadingProgress readingProgress) {
        return null;
    }
}
