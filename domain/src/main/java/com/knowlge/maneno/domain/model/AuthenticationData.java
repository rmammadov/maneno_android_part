package com.knowlge.maneno.domain.model;

/**
 * Created by Bogumił Sikora.
 */

public class AuthenticationData {
    private Integer userId;
    private String accessToken;
    private String tokenType;
    private Integer expiresIn;

    public AuthenticationData() {}

    public AuthenticationData(AuthenticationResult authenticationResult) {
        if (authenticationResult != null) {
            if (authenticationResult.getUser() != null) {
                this.userId = authenticationResult.getUser().getId();
            }
            this.accessToken = authenticationResult.getAccessToken();
            this.tokenType = authenticationResult.getTokenType();
            this.expiresIn = authenticationResult.getExpiresIn();
        }
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Integer getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }
}
