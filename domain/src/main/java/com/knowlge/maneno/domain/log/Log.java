package com.knowlge.maneno.domain.log;

/**
 * Created by Bogumił Sikora.
 */
public interface Log {
    void d(String message);
    void e(String message);
    void e(String message, Throwable t);
    void e(Throwable t);
}
