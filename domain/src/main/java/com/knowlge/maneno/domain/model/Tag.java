package com.knowlge.maneno.domain.model;

/**
 * Created by Bogumił Sikora.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tag {

    public static final Integer TYPE_ID_CATEGORY = 1;
    public static final Integer TYPE_ID_DIFFICULTY = 2;
    // public static final Integer Type_Id_Language = 3; // doesn't exist on the server
    public static final Integer TYPE_ID_LENGTH = 4;
    public static final Integer TYPE_ID_SERIES = 7;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("tagTypeId")
    @Expose
    private Integer tagTypeId;

    @SerializedName("iconUrl")
    @Expose
    private String iconUrl;

    @SerializedName("colorCode")
    @Expose
    private Integer colorCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTagTypeId() {
        return tagTypeId;
    }

    public void setTagTypeId(Integer tagTypeId) {
        this.tagTypeId = tagTypeId;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Integer getColorCode() {
        return colorCode;
    }

    public void setColorCode(Integer colorCode) {
        this.colorCode = colorCode;
    }

}