package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bogumił Sikora.
 */

public class ReadingProgress {

    @SerializedName("readingSessionId")
    @Expose
    private Integer readingSessionId;
    @SerializedName("userReadingHistoryId")
    @Expose
    private String userReadingHistoryId;
    @SerializedName("unknownWords")
    @Expose
    private List<String> unknownWords = null;
    @SerializedName("achievements")
    @Expose
    private List<Achievement> achievements = null;
    @SerializedName("statPoints")
    @Expose
    private Integer statPoints;
    @SerializedName("equipPoints")
    @Expose
    private Integer equipPoints;
    @SerializedName("isLevelCompleted")
    @Expose
    private Boolean isLevelCompleted;

    public Integer getReadingSessionId() {
        return readingSessionId;
    }

    public void setReadingSessionId(Integer readingSessionId) {
        this.readingSessionId = readingSessionId;
    }

    public String getUserReadingHistoryId() {
        return userReadingHistoryId;
    }

    public void setUserReadingHistoryId(String userReadingHistoryId) {
        this.userReadingHistoryId = userReadingHistoryId;
    }

    public List<String> getUnknownWords() {
        return unknownWords;
    }

    public void setUnknownWords(List<String> unknownWords) {
        this.unknownWords = unknownWords;
    }

    public List<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<Achievement> achievements) {
        this.achievements = achievements;
    }

    public Integer getStatPoints() {
        return statPoints;
    }

    public void setStatPoints(Integer statPoints) {
        this.statPoints = statPoints;
    }

    public Integer getEquipPoints() {
        return equipPoints;
    }

    public void setEquipPoints(Integer equipPoints) {
        this.equipPoints = equipPoints;
    }

    public Boolean getLevelCompleted() {
        return isLevelCompleted;
    }

    public void setLevelCompleted(Boolean levelCompleted) {
        isLevelCompleted = levelCompleted;
    }
}
