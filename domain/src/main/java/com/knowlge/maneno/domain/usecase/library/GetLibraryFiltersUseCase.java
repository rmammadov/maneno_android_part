package com.knowlge.maneno.domain.usecase.library;

import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.Tag;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 */

public class GetLibraryFiltersUseCase extends UseCase<Void, GetLibraryFiltersUseCase.Result> {

    @Inject
    public GetLibraryFiltersUseCase() {}

    @Override
    protected Observable<Result> createUseCaseObservable(Void params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Void, Result>() {
            @Override
            public Result execute(Void params) throws Exception {
                Result filters = new Result();

                filters.colorCodes = localRepository.getColorCodes();
                filters.categories = localRepository.getTagsByType(Tag.TYPE_ID_CATEGORY);
                filters.languages = localRepository.getLanguages();
                filters.lengths = localRepository.getTagsByType(Tag.TYPE_ID_LENGTH);
                filters.difficulties = localRepository.getTagsByType(Tag.TYPE_ID_DIFFICULTY);

                return filters;
            }
        });
    }

    public static class Result {
        private List<ColorCode> colorCodes;
        private List<Tag> categories;
        private List<Language> languages;
        private List<Tag> lengths;
        private List<Tag> difficulties;

        public List<ColorCode> getColorCodes() {
            return colorCodes;
        }

        public List<Tag> getCategories() {
            return categories;
        }

        public List<Language> getLanguages() {
            return languages;
        }

        public List<Tag> getLengths() {
            return lengths;
        }

        public List<Tag> getDifficulties() {
            return difficulties;
        }
    }
}
