package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bogumił Sikora.
 */
public class Level {

    @SerializedName("lastUpgradeDate")
    @Expose
    private String lastUpgradeDate;
    @SerializedName("daySinceUpgrade")
    @Expose
    private Integer daySinceUpgrade;
    @SerializedName("downgradeDates")
    @Expose
    private List<String> downgradeDates = null;
    @SerializedName("dailyReadings")
    @Expose
    private List<DailyReading> dailyReadings = null;
    @SerializedName("freeStatPoints")
    @Expose
    private Integer freeStatPoints;
    @SerializedName("freeEquipPoints")
    @Expose
    private Integer freeEquipPoints;
    @SerializedName("daysActiveInLevel")
    @Expose
    private Integer daysActiveInLevel;
    @SerializedName("daysInLevel")
    @Expose
    private Integer daysInLevel;
    @SerializedName("levelCompletePercent")
    @Expose
    private Integer levelCompletePercent;
    @SerializedName("minutesRemaining")
    @Expose
    private Integer minutesRemaining;
    @SerializedName("minutesRemainingToday")
    @Expose
    private Integer minutesRemainingToday;
    @SerializedName("daysRemaining")
    @Expose
    private Integer daysRemaining;
    @SerializedName("canAddNewAbility")
    @Expose
    private Boolean canAddNewAbility;
    @SerializedName("canImproveAbility")
    @Expose
    private Boolean canImproveAbility;
    @SerializedName("statPointsLimitReached")
    @Expose
    private Boolean statPointsLimitReached;
    @SerializedName("mood")
    @Expose
    private Integer mood;
    @SerializedName("moodScore")
    @Expose
    private Integer moodScore;
    @SerializedName("dragonColor")
    @Expose
    private Integer dragonColor;
    @SerializedName("progressWasReseted")
    @Expose
    private Boolean progressWasReseted;
    @SerializedName("stats")
    @Expose
    private List<Stat> stats = null;
    @SerializedName("dragonLevel")
    @Expose
    private Integer dragonLevel;

    public String getLastUpgradeDate() {
        return lastUpgradeDate;
    }

    public void setLastUpgradeDate(String lastUpgradeDate) {
        this.lastUpgradeDate = lastUpgradeDate;
    }

    public Integer getDaySinceUpgrade() {
        return daySinceUpgrade;
    }

    public void setDaySinceUpgrade(Integer daySinceUpgrade) {
        this.daySinceUpgrade = daySinceUpgrade;
    }

    public List<String> getDowngradeDates() {
        return downgradeDates;
    }

    public void setDowngradeDates(List<String> downgradeDates) {
        this.downgradeDates = downgradeDates;
    }

    public List<DailyReading> getDailyReadings() {
        return dailyReadings;
    }

    public void setDailyReadings(List<DailyReading> dailyReadings) {
        this.dailyReadings = dailyReadings;
    }

    public Integer getFreeStatPoints() {
        return freeStatPoints;
    }

    public void setFreeStatPoints(Integer freeStatPoints) {
        this.freeStatPoints = freeStatPoints;
    }

    public Integer getFreeEquipPoints() {
        return freeEquipPoints;
    }

    public void setFreeEquipPoints(Integer freeEquipPoints) {
        this.freeEquipPoints = freeEquipPoints;
    }

    public Integer getDaysActiveInLevel() {
        return daysActiveInLevel;
    }

    public void setDaysActiveInLevel(Integer daysActiveInLevel) {
        this.daysActiveInLevel = daysActiveInLevel;
    }

    public Integer getDaysInLevel() {
        return daysInLevel;
    }

    public void setDaysInLevel(Integer daysInLevel) {
        this.daysInLevel = daysInLevel;
    }

    public Integer getLevelCompletePercent() {
        return levelCompletePercent;
    }

    public void setLevelCompletePercent(Integer levelCompletePercent) {
        this.levelCompletePercent = levelCompletePercent;
    }

    public Integer getMinutesRemaining() {
        return minutesRemaining;
    }

    public void setMinutesRemaining(Integer minutesRemaining) {
        this.minutesRemaining = minutesRemaining;
    }

    public Integer getMinutesRemainingToday() {
        return minutesRemainingToday;
    }

    public void setMinutesRemainingToday(Integer minutesRemainingToday) {
        this.minutesRemainingToday = minutesRemainingToday;
    }

    public Integer getDaysRemaining() {
        return daysRemaining;
    }

    public void setDaysRemaining(Integer daysRemaining) {
        this.daysRemaining = daysRemaining;
    }

    public Boolean getCanAddNewAbility() {
        return canAddNewAbility;
    }

    public void setCanAddNewAbility(Boolean canAddNewAbility) {
        this.canAddNewAbility = canAddNewAbility;
    }

    public Boolean getCanImproveAbility() {
        return canImproveAbility;
    }

    public void setCanImproveAbility(Boolean canImproveAbility) {
        this.canImproveAbility = canImproveAbility;
    }

    public Boolean getStatPointsLimitReached() {
        return statPointsLimitReached;
    }

    public void setStatPointsLimitReached(Boolean statPointsLimitReached) {
        this.statPointsLimitReached = statPointsLimitReached;
    }

    public Integer getMood() {
        return mood;
    }

    public void setMood(Integer mood) {
        this.mood = mood;
    }

    public Integer getMoodScore() {
        return moodScore;
    }

    public void setMoodScore(Integer moodScore) {
        this.moodScore = moodScore;
    }

    public Integer getDragonColor() {
        return dragonColor;
    }

    public void setDragonColor(Integer dragonColor) {
        this.dragonColor = dragonColor;
    }

    public Boolean getProgressWasReseted() {
        return progressWasReseted;
    }

    public void setProgressWasReseted(Boolean progressWasReseted) {
        this.progressWasReseted = progressWasReseted;
    }

    public List<Stat> getStats() {
        return stats;
    }

    public void setStats(List<Stat> stats) {
        this.stats = stats;
    }

    public Integer getDragonLevel() {
        return dragonLevel;
    }

    public void setDragonLevel(Integer dragonLevel) {
        this.dragonLevel = dragonLevel;
    }

}
