package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Bogumił Sikora.
 */

public class Achievement {
    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("isAchieved")
    @Expose
    public Boolean isAchieved;

    @SerializedName("iconUrl")
    @Expose
    public String iconUrl;

    @SerializedName("equipPoints")
    @Expose
    public String equipPoints;

    @SerializedName("statPoints")
    @Expose
    public String statPoints;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getAchieved() {
        return isAchieved;
    }

    public void setAchieved(Boolean achieved) {
        isAchieved = achieved;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getEquipPoints() {
        return equipPoints;
    }

    public void setEquipPoints(String equipPoints) {
        this.equipPoints = equipPoints;
    }

    public String getStatPoints() {
        return statPoints;
    }

    public void setStatPoints(String statPoints) {
        this.statPoints = statPoints;
    }
}
