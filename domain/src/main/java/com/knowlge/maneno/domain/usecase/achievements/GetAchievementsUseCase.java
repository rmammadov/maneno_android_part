package com.knowlge.maneno.domain.usecase.achievements;

import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 */

public class GetAchievementsUseCase extends UseCase<GetAchievementsUseCase.Params,List<Achievement>> {

    @Inject
    public GetAchievementsUseCase() {}

    @Override
    protected Observable<List<Achievement>> createUseCaseObservable(Params params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Params, List<Achievement>>() {
            @Override
            public List<Achievement> execute(Params params) throws Exception {
                // todo - save achievement in local memory cache. It is cleared when readingstatistics request is sent and we get new achievement in response.
                return restApi.getAchievements(params.onlyAchieved);
            }
        });
    }

    public static class Params {
        private boolean onlyAchieved;

        public Params(boolean onlyAchieved) {
            this.onlyAchieved = onlyAchieved;
        }
    }
}
