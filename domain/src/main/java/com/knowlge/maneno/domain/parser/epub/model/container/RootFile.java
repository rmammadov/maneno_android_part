package com.knowlge.maneno.domain.parser.epub.model.container;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Bogumił Sikora.
 */
@Root
public class RootFile {
    @Attribute(name = "full-path")
    private String fullPath;

    @Attribute(name = "media-type")
    private String mediaType;

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
}
