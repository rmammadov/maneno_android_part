package com.knowlge.maneno.domain.repository;

import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.ReadingProgress;

/**
 * Created by Bogumił Sikora.
 */

public interface MemoryCache {
    void putLatestReadingProgress(Long sessionId, ReadingProgress readingProgress);
    ReadingProgress getLatestReadingProgress(Long sessionId);

    BookInfo getBookInfo(Integer bookId, boolean withChapters);

    void put(BookInfo bookInfo);
}
