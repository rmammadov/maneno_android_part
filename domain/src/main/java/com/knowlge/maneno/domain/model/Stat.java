package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bogumił Sikora.
 */
public class Stat {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("abilities")
    @Expose
    private List<Ability> abilities = null;
    @SerializedName("points")
    @Expose
    private Integer points;
    @SerializedName("statCapReached")
    @Expose
    private Boolean statCapReached;
    @SerializedName("canAddNewAbility")
    @Expose
    private Boolean canAddNewAbility;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ability> getAbilities() {
        return abilities;
    }

    public void setAbilities(List<Ability> abilities) {
        this.abilities = abilities;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Boolean getStatCapReached() {
        return statCapReached;
    }

    public void setStatCapReached(Boolean statCapReached) {
        this.statCapReached = statCapReached;
    }

    public Boolean getCanAddNewAbility() {
        return canAddNewAbility;
    }

    public void setCanAddNewAbility(Boolean canAddNewAbility) {
        this.canAddNewAbility = canAddNewAbility;
    }

}
