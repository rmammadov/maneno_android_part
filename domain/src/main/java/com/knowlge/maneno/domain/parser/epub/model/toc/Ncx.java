package com.knowlge.maneno.domain.parser.epub.model.toc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Bogumił Sikora.
 */
@Root(name = "ncx", strict = false)
public class Ncx {

    @Element(name = "text")
    @Path("docTitle")
    String title;

    @ElementList(name = "navMap")
    List<NavPoint> navPoints;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<NavPoint> getNavPoints() {
        return navPoints;
    }

    public void setNavPoints(List<NavPoint> navPoints) {
        this.navPoints = navPoints;
    }
}
