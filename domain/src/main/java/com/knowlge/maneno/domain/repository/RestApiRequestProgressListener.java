package com.knowlge.maneno.domain.repository;

/**
 * Created by Bogumił Sikora.
 */

public interface RestApiRequestProgressListener {
    void onProgress(int percent, String progressId);
    String getProgressId();
}
