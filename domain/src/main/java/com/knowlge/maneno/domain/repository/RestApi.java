package com.knowlge.maneno.domain.repository;

import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.domain.model.AuthenticationData;
import com.knowlge.maneno.domain.model.AuthenticationResult;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.BookInfoList;
import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Country;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.Level;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingProgress;
import com.knowlge.maneno.domain.model.ReadingStatisticsParams;
import com.knowlge.maneno.domain.model.School;
import com.knowlge.maneno.domain.model.Tag;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Bogumił Sikora.
 */

public interface RestApi {
    void setAuthenticationData(AuthenticationData authData);

    AuthenticationResult getToken(String username, String password) throws IOException;
    Boolean checkForUnilogin(String username) throws IOException;
    AuthenticationResult getTokenForUniloginUser(String username) throws IOException;
    int authenticateWithUniloginServer(String username, String password) throws IOException;
    boolean assignTempLicense(String username)throws IOException;

    List<Tag> getTags() throws IOException;
    List<Language> getLanguages() throws IOException;
    List<ColorCode> getColorCodes() throws IOException;
    List<Country> getCountries() throws IOException;
    List<School> getSchools() throws IOException;

    // Methods using current logged in user authorization data. User id parameter can be omitted.
    List<Integer> getBookRecommendations() throws IOException;
    BookInfo getBookInfo(Integer bookId) throws IOException;

    InputStream getBookContent(String encr, String hash, boolean encrypt, RestApiRequestProgressListener progressListener) throws IOException;

    ReadingProgress sendReadingStatistics(Integer bookId, ReadingStatisticsParams readingStatisticsParams) throws IOException;

    boolean sendReadingPagesStatistics(Integer bookId, String userReadingHistoryId, List<ReadingPageStatistics> readingPageStatisticsList) throws IOException;

    Level getUserLevel(Integer userId) throws IOException;

    List<Achievement> getAchievements(boolean onlyAchieved) throws IOException;

    BookInfoList getBooks(Integer authorId, String colorCodes, String tags, String languages, Boolean liked, Integer offset, Integer limit) throws IOException;
}
