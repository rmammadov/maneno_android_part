package com.knowlge.maneno.domain.usecase.reading;

import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.repository.LocalRepository;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import java.util.Date;
import java.util.UUID;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 */

public class StartReadingSessionUseCase extends UseCase<StartReadingSessionUseCase.Params, StartReadingSessionUseCase.Result> {

    @Inject
    public StartReadingSessionUseCase() {}

    @Override
    protected Observable<Result> createUseCaseObservable(Params params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Params, Result>() {
            @Override
            public Result execute(Params params) throws Exception {
                BookInfo bookInfo = localRepository.getBookInfo(params.bookId, false);
                Integer currentUserId = localRepository.getCurrentUserId();

                if (bookInfo != null && currentUserId != null) {
                    Integer latestTextOffset = bookInfo.getLatestTextOffset();
                    if (latestTextOffset == null) {
                        latestTextOffset = 0; // Set 0 as default offset.
                    }
                    Result result = new Result(latestTextOffset);

                    synchronized (LocalRepository.class) {
                        ReadingSession readingSession = getCurrentReadingSession();

                        if (readingSession != null) {
                            if (ReadingSession.STATUS_STOPPED == readingSession.getStatus()
                                    && params.bookId.equals(readingSession.getBookId())
                                    && currentUserId.equals(readingSession.getUserId())) {
                                continueReadingSession(readingSession);

                                return result;
                            } else {
                                readingSession.setStatus(ReadingSession.STATUS_END);
                                localRepository.updateReadingSession(readingSession);

                                // todo - call SendReadingStatisticsUseCase ???
                            }
                        }

                        // If no session to continue and can start new one.
                        if (params.startNew) {
                            startNewReadingSession(currentUserId, params.bookId);
                            return result;
                        }
                    }
                }

                return null;
            }
        });
    }

    private void continueReadingSession(ReadingSession session) {
        session.setStatus(ReadingSession.STATUS_RUNNING);
        session.setReadStartTimeMillis(new Date().getTime());

        localRepository.updateReadingSession(session);
    }

    private void startNewReadingSession(Integer userId, Integer bookId) {
        // Create new session local record.
        ReadingSession readingSession = new ReadingSession();
        readingSession.setReadingStatisticsId(UUID.randomUUID().toString());
        readingSession.setStatus(ReadingSession.STATUS_RUNNING);
        readingSession.setReadStartTimeMillis(new Date().getTime());
        readingSession.setBookId(bookId);
        readingSession.setUserId(userId);

        long sessionId = localRepository.saveReadingSession(readingSession);

        localRepository.setCurrentReadingSessionId(sessionId);
    }

    public static class Params {
        private Integer bookId;
        private boolean startNew;

        public Params(Integer bookId, boolean startNew) {
            this.bookId = bookId;
            this.startNew = startNew;
        }
    }

    public static class Result {
        private Integer offsetInBook;

        public Result(Integer offsetInBook) {
            this.offsetInBook = offsetInBook;
        }

        public Integer getOffsetInBook() {
            return offsetInBook;
        }
    }
}
