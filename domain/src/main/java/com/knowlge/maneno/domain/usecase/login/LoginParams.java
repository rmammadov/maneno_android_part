package com.knowlge.maneno.domain.usecase.login;

import com.knowlge.maneno.domain.usecase.base.UseCase;

/**
 * Created by Bogumił Sikora.
 */

public class LoginParams {

    private final String email;
    private final String password;
    private final boolean rememberMe;

    public LoginParams(String email, String password, boolean rememberMe) {
        this.email = email;
        this.password = password;
        this.rememberMe = rememberMe;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public boolean isRememberMe() {
        return rememberMe;
    }
}
