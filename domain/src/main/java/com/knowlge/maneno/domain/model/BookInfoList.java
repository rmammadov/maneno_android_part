package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bogumił Sikora.
 */

public class BookInfoList {

    @SerializedName("rows")
    @Expose
    private List<BookInfo> rows = null;

    @SerializedName("totalRows")
    @Expose
    private Integer totalRows;

    public List<BookInfo> getRows() {
        return rows;
    }

    public void setRows(List<BookInfo> rows) {
        this.rows = rows;
    }

    public Integer getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Integer totalRows) {
        this.totalRows = totalRows;
    }
}
