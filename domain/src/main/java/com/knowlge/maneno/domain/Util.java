package com.knowlge.maneno.domain;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by Bogumił Sikora.
 */

public class Util {
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat(Consts.DATE_TIME_FORMAT, Locale.getDefault());

    public static boolean isSet(String str) {
        return str != null && str.length() > 0;
    }

    public static boolean isOnline() {
        try {
            Runtime runtime = Runtime.getRuntime();
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (Throwable t) {
            t.printStackTrace();
        }

        return false;
    }

    public static String sha1(String str) {
        byte[] sha1Bytes = Hashing.sha1()
                .newHasher()
                .putString(str, Charset.forName("UTF-8"))
                .hash().asBytes();

        return BaseEncoding.base64().encode(sha1Bytes);
    }

    public static String sha256(String str) {
        return Hashing.sha256()
                .newHasher()
                .putString(str, Charset.forName("UTF-8"))
                .hash().toString();
    }

    public static String hmacsha1(String value, String key) {
        return Hashing.hmacSha1(key.getBytes())
                .newHasher()
                .putString(value, Charset.forName("UTF-8"))
                .hash().toString();
    }

    public static void unzip(File zipFile, File targetDirectory) throws IOException {
        ZipInputStream zis = new ZipInputStream(
                new BufferedInputStream(new FileInputStream(zipFile)));
        try {
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                File file = new File(targetDirectory, ze.getName());
                File dir = ze.isDirectory() ? file : file.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " +
                            dir.getAbsolutePath());
                if (ze.isDirectory())
                    continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }
            /* if time should be restored as well
            long time = ze.getTime();
            if (time > 0)
                file.setLastModified(time);
            */
            }
        } finally {
            zis.close();
        }
    }

    public static String getFileExtension(File file) {
        if (file != null) {
            return getFileExtension(file.getName());
        }

        return "";
    }

    public static String getFileExtension(String fileName) {
        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i > 0) {
            extension = fileName.substring(i+1);
        }

        return extension.toLowerCase();
    }

    public static String formatDate(Date date) {
        if (date != null) {
            synchronized (Util.class) {
                return SIMPLE_DATE_FORMAT.format(date);
            }
        }
        return null;
    }

    public static String formatDate(Date date, String format) throws ParseException {
        if (isSet(format) && date != null) {
            return new SimpleDateFormat(format, Locale.getDefault()).format(date);
        }
        return null;
    }

    public static Date parseDate(String timestamp) throws ParseException {
        if (isSet(timestamp)) {
            synchronized (Util.class) {
                return SIMPLE_DATE_FORMAT.parse(timestamp);
            }
        }
        return null;
    }

    public static Date parseDate(String date, String format) throws ParseException {
        if (isSet(date)) {
            return new SimpleDateFormat(format, Locale.getDefault()).parse(date);
        }
        return null;
    }

    public static String toString(Integer [] array) {
        if (array != null) {
            String result = "[";
            for (int i = 0; i < array.length; i++) {
                if (i > 0) {
                    result += ",";
                }
                result += String.valueOf(array[i]);
            }
            result += "]";

            return result;
        }

        return null;
    }
}