package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Bogumił Sikora.
 */
public class Ability {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("degree")
    @Expose
    private Integer degree;
    @SerializedName("canImproveAbility")
    @Expose
    private Boolean canImproveAbility;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(Integer degree) {
        this.degree = degree;
    }

    public Boolean getCanImproveAbility() {
        return canImproveAbility;
    }

    public void setCanImproveAbility(Boolean canImproveAbility) {
        this.canImproveAbility = canImproveAbility;
    }

}