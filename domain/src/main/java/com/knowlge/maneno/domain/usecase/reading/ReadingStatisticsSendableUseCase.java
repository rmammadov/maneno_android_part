package com.knowlge.maneno.domain.usecase.reading;

import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingProgress;
import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.model.ReadingStatisticsParams;
import com.knowlge.maneno.domain.repository.LocalRepository;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import rx.Subscriber;

/**
 * Created by Bogumił Sikora.
 */

public abstract class ReadingStatisticsSendableUseCase<Q, R> extends UseCase<Q, R> {

    protected abstract R createSentResult(ReadingProgress readingProgress);

    void sentSessionStatistics(ReadingSession session, boolean deleteReadingSession, boolean sendPagesStatistics) throws IOException {
        sentSessionStatistics(session, deleteReadingSession, sendPagesStatistics, null);
    }

    private void returnNoProgress(Subscriber<? super R> subscriber) {
        if (subscriber != null) {
            ReadingProgress noProgress = new ReadingProgress();
            noProgress.setStatPoints(0);
            noProgress.setEquipPoints(0);
            noProgress.setLevelCompleted(false);
            subscriber.onNext(createSentResult(noProgress));
        }
    }

    synchronized void sentSessionStatistics(ReadingSession session, boolean deleteReadingSession, boolean sendPagesStatistics, Subscriber<? super R> subscriber) throws IOException {
        if (Util.isOnline() && session != null) {

            // If session still have reading time interval which was not sent to server yet.
            if (session.getTimeInterval() > Consts.MIN_READ_TIME_SECONDS) {
                // Save timeIntervalToSend before sending request, because during long request user can resume reading and increase the interval.
                int timeIntervalToSend = session.getTimeInterval();
                synchronized (LocalRepository.class) {
                    if (!Util.isSet(session.getReadingStatisticsId())) {
                        session.setReadingStatisticsId(UUID.randomUUID().toString());
                        localRepository.updateReadingSession(session);
                    }
                }

                ReadingProgress readingProgress = sendReadingStatistics(session);

                if (readingProgress != null && readingProgress.getReadingSessionId() != null) {
                    synchronized (LocalRepository.class) {
                        // Get most recent session data from local repository within synchronized block.
                        session = localRepository.getReadingSession(session.getId());
                        session.setReadingSessionId(readingProgress.getReadingSessionId());
                        session.setReadingStatisticsId(null);
                        session.setUserReadingHistoryId(readingProgress.getUserReadingHistoryId());
                        session.decreaseTimeInterval(timeIntervalToSend);
                        localRepository.updateReadingSession(session);
                        memoryCache.putLatestReadingProgress(session.getId(), readingProgress);
                    }

                    if (subscriber != null) {
                        subscriber.onNext(createSentResult(readingProgress));
                    }
                } else {
                    returnNoProgress(subscriber);
                }
            } else if (subscriber != null) {
                ReadingProgress readingProgress = memoryCache.getLatestReadingProgress(session.getId());
                if (readingProgress != null) {
                    subscriber.onNext(createSentResult(readingProgress));
                } else {
                    returnNoProgress(subscriber);
                }

            }

            if (sendPagesStatistics) {
                List<ReadingPageStatistics> readingPageStatisticsList = localRepository.getReadingPagesStatistics(session.getId());

                // If session was already sent properly, sent reading page statistics to server.
                if (session.getTimeInterval() <= Consts.MIN_READ_TIME_SECONDS) {
                    if (session.getReadingSessionId() != null && session.getUserReadingHistoryId() != null
                            && session.getBookId() != null
                            && readingPageStatisticsList != null && readingPageStatisticsList.size() > 0) {
                        if (restApi.sendReadingPagesStatistics(session.getBookId(), session.getUserReadingHistoryId(), readingPageStatisticsList)) {
                            localRepository.deleteReadingPagesStatistics(readingPageStatisticsList);

                            // Everything was sent, we can delete session record.
                            if (deleteReadingSession) {
                                localRepository.deleteReadingSession(session);
                            }
                        }
                    } else {
                        if (deleteReadingSession) {
                            localRepository.deleteReadingSession(session);
                        }
                    }
                }
            }
        }
    }

    private ReadingProgress sendReadingStatistics(ReadingSession session) {
        try {
            return restApi.sendReadingStatistics(session.getBookId(), new ReadingStatisticsParams(session));
        } catch (IOException e) {
            // Try second time.
            try {
                return restApi.sendReadingStatistics(session.getBookId(), new ReadingStatisticsParams(session));
            } catch (IOException e2) {
                // Try third time.
                try {
                    return restApi.sendReadingStatistics(session.getBookId(), new ReadingStatisticsParams(session));
                } catch (IOException e3) {
                    log.e("Call sendReadingStatistics unsuccessful for the 3rd time!", e3);
                }
            }
        }

        return null;
    }
}
