package com.knowlge.maneno.domain.usecase.reading;

import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.repository.LocalRepository;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import java.util.Date;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 */

public class BookPageOpenedUseCase extends UseCase<BookPageOpenedUseCase.Params, Void> {

    @Inject
    public BookPageOpenedUseCase() {}

    @Override
    protected Observable<Void> createUseCaseObservable(Params params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Params, Void>() {
            @Override
            public Void execute(Params params) throws Exception {
                synchronized (LocalRepository.class) {
                    ReadingSession readingSession = getCurrentReadingSession();

                    if (readingSession != null
                            && ReadingSession.STATUS_RUNNING == readingSession.getStatus()) {
                        Date now = new Date();

                        int wordsRead = 0;
                        int wordIndexEnd = params.offsetInBook;
                        if (params.pageText != null) {
                            wordIndexEnd += params.pageText.length();
                            // Split page text by every non word or number character substring.
                            wordsRead = params.pageText.split("[^\\w&&[^0-9]]+").length;
                        }

                        // Update current reading session charOffset property.
                        readingSession.setCharOffset(params.offsetInBook);
                        // todo - update readingSession timeInterval, so time will be logged even if app is killed before StopReadingSessionUseCase is executed.
                        localRepository.updateReadingSession(readingSession);

                        ReadingPageStatistics newReadingPage = new ReadingPageStatistics();
                        newReadingPage.setSessionId(readingSession.getId());
                        newReadingPage.setPageNumber(params.pageNumber);
                        newReadingPage.setWordIndexStart(params.offsetInBook);
                        newReadingPage.setWordIndexEnd(wordIndexEnd);
                        newReadingPage.setPageStartTime(Util.formatDate(now));
                        newReadingPage.setWordsRead(wordsRead);
                        long newReadingPageId = localRepository.saveReadingPageStatistics(newReadingPage);

                        ReadingPageStatistics currentReadingPage = getCurrentReadingPage();
                        localRepository.setCurrentReadingPageId(newReadingPageId);

                        if (currentReadingPage != null && currentReadingPage.getPageEndTime() == null) {
                            currentReadingPage.setPageEndTime(Util.formatDate(now));
                            localRepository.updateReadingPageStatistics(currentReadingPage);
                        }
                    } else {
                        // todo - no current reading session - error? how to handle? Just log?
                    }
                }

                return null;
            }
        });
    }

    public static class Params {
        private Integer pageNumber;
        private Integer offsetInBook;
        private String pageText;

        public Params(Integer pageNumber, Integer offsetInBook, String pageText) {
            this.pageNumber = pageNumber;
            this.offsetInBook = offsetInBook;
            this.pageText = pageText;
        }
    }
}
