package com.knowlge.maneno.domain.parser.epub;

import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.log.Log;
import com.knowlge.maneno.domain.model.Chapter;
import com.knowlge.maneno.domain.parser.epub.model.container.Container;
import com.knowlge.maneno.domain.parser.epub.model.container.RootFile;
import com.knowlge.maneno.domain.parser.epub.model.opf.Creator;
import com.knowlge.maneno.domain.parser.epub.model.opf.Item;
import com.knowlge.maneno.domain.parser.epub.model.opf.ItemRef;
import com.knowlge.maneno.domain.parser.epub.model.opf.OpfPackage;
import com.knowlge.maneno.domain.parser.epub.model.toc.NavPoint;
import com.knowlge.maneno.domain.parser.epub.model.toc.Ncx;
import com.knowlge.maneno.domain.repository.FileRepository;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Bogumił Sikora.
 */
@Singleton
public class EpubParser {

    private static final String MANIFEST_PATH = "/META-INF/container.xml";
    private static final String OEBPS_PACKAGE_MEDIA_TYPE = "application/oebps-package+xml";
    private static final String NCX_MEDIA_TYPE = "application/x-dtbncx+xml";
    private static final String ROLE_AUTHOR = "aut";

    @Inject
    FileRepository fileRepository;

    @Inject
    Log log;

    @Inject
    public EpubParser() {}

    public synchronized EpubParseResult parse(String bookUniqueFolderName, Integer bookId) throws Exception {
        // Assuming that under "[context_app_files_path]/[bookUniqueFolderName]/" unzipped epub content exists.
        // Implementation does not have null checks. Any NullPointerException means some content errors - proper parsing is not possible.

        Serializer serializer = new Persister();
        File manifestFile = fileRepository.getFile(bookUniqueFolderName + MANIFEST_PATH);
        Container container = serializer.read(Container.class, manifestFile);

        String opfPath = null;
        for (RootFile rootFile : container.getRootFiles()) {
            if (OEBPS_PACKAGE_MEDIA_TYPE.equals(rootFile.getMediaType())) {
                opfPath = rootFile.getFullPath();
                break;
            }
        }

        // If no oebps package, then according to the specification, just take first root file.
        if (opfPath == null) {
            opfPath = container.getRootFiles().get(0).getFullPath();
        }

        String opfFolder = opfPath.substring(0, opfPath.lastIndexOf("/")+1); // Include latest path separator.

        // Parse OPF file.
        File opfFile = fileRepository.getFile(bookUniqueFolderName + "/" + opfPath);
        OpfPackage opfPackage = serializer.read(OpfPackage.class, opfFile);

        Map<String, String> idHrefMap = new HashMap<>();
        String ncxPath = null;
        for (Item item: opfPackage.getItems()) {
            idHrefMap.put(item.getId(), item.getHref());
            if (NCX_MEDIA_TYPE.equals(item.getMediaType())) {
                ncxPath = opfFolder + item.getHref();
            }
        }

        // Parse TOC file.
        File ncxFile = fileRepository.getFile(bookUniqueFolderName + "/" + ncxPath);
        Ncx toc = serializer.read(Ncx.class, ncxFile);
        Map<String, String> hrefTitle = new HashMap<>();
        for (NavPoint navPoint: toc.getNavPoints()) {
            hrefTitle.put(navPoint.getHref(), navPoint.getLabel());
        }

        // Orders of chapters is defined in spine list.
        List<Chapter> chapters = new ArrayList<>();
        int index = 0;
        for (ItemRef itemRef: opfPackage.getSpine()) {
            String href = idHrefMap.get(itemRef.getIdRef());
            Chapter ch = new Chapter();
            ch.setBookId(bookId);
            ch.setIndex(index);
            ch.setContentPath(bookUniqueFolderName + "/" + opfFolder + href);
            ch.setTitle(hrefTitle.get(href));
            chapters.add(ch);
            index++;
        }

        String authorsName = null;
        if (opfPackage.getCreators() != null)
        for (Creator creator: opfPackage.getCreators()) {
            if (ROLE_AUTHOR.equals(creator.getRole())) {
                if (authorsName == null) {
                    authorsName = creator.getName();
                } else {
                    authorsName += ", " + creator.getName();
                }
            }
        }

        EpubParseResult result = new EpubParseResult();
        result.setChapters(chapters);
        result.setAuthors(authorsName);

        if (Util.isSet(toc.getTitle())) {
            result.setBookTitle(toc.getTitle());
        } else {
            result.setBookTitle(opfPackage.getTitle());
        }

        return result;
    }
}
