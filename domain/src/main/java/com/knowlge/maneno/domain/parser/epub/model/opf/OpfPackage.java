package com.knowlge.maneno.domain.parser.epub.model.opf;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Bogumił Sikora.
 */
@Root(name = "package", strict = false)
public class OpfPackage {

    @Element(name = "title")
    @Namespace(reference="http://purl.org/dc/elements/1.1/", prefix="dc")
    @Path("metadata")
    private String title;

    @ElementList(entry = "creator", inline = true)
    @Namespace(reference="http://purl.org/dc/elements/1.1/", prefix="dc")
    @Path("metadata")
    private List<Creator> creators;

    @ElementList(name = "manifest")
    private List<Item> items;

    @ElementList(name = "spine")
    private List<ItemRef> spine;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Creator> getCreators() {
        return creators;
    }

    public void setCreators(List<Creator> creators) {
        this.creators = creators;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<ItemRef> getSpine() {
        return spine;
    }

    public void setSpine(List<ItemRef> spine) {
        this.spine = spine;
    }
}
