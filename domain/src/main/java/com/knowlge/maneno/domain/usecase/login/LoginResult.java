package com.knowlge.maneno.domain.usecase.login;

import com.knowlge.maneno.domain.usecase.base.UseCase;

/**
 * Created by Bogumił Sikora.
 */
public class LoginResult {

    public static final int ERR_CODE_INCORRECT_USERNAME_PASSWORD = 1;
    public static final int ERR_CODE_UNILOGIN_USER_LOGIN_ERROR = 2;

    private final boolean success;
    private final Integer errCode;

    public LoginResult(boolean success, Integer errCode) {
        this.success = success;
        this.errCode = errCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public Integer getErrCode() {
        return errCode;
    }
}
