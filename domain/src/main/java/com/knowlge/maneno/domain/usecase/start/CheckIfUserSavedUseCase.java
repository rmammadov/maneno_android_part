package com.knowlge.maneno.domain.usecase.start;

import com.knowlge.maneno.domain.model.User;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 */

public class CheckIfUserSavedUseCase extends UseCase<Void, Boolean> {

    @Inject
    public CheckIfUserSavedUseCase() {}

    @Override
    protected Observable<Boolean> createUseCaseObservable(Void params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Void, Boolean>() {
            @Override
            public Boolean execute(Void params) throws Exception {
                User u = localRepository.getCurrentUser();

                return u != null && u.isRememberMe();

                // todo - if remember me is false, then "clear current user"?
            }
        });
    }
}
