package com.knowlge.maneno.domain.parser.epub.model.opf;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

/**
 * Created by Bogumił Sikora.
 */
@Root
public class Creator {

    @Text
    private String name;

    @Attribute(name = "role")
    @Namespace(reference="http://www.idpf.org/2007/opf", prefix="opf")
    private String role;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
