package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Bogumił Sikora.
 */

public class ReadingStatisticsParams {

    @SerializedName("ReadingSessionId")
    @Expose
    private Integer readingSessionId;

    @SerializedName("EndReadingDate")
    @Expose
    private String endReadingDate;

    @SerializedName("ReadingStatisticsId")
    @Expose
    private String readingStatisticsId;

    @SerializedName("TimeInterval")
    @Expose
    private Integer timeInterval;

    @SerializedName("CharOffset")
    @Expose
    private Integer charOffset;

    @SerializedName("UnknownWords")
    @Expose
    private String[] unknownWords;

    @SerializedName("NumberOfWords")
    @Expose
    private Integer numberOfWords;

    public ReadingStatisticsParams(ReadingSession readingSession) {
        this.readingSessionId = readingSession.getReadingSessionId();
        this.readingStatisticsId = readingSession.getReadingStatisticsId();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ", Locale.getDefault());
        this.endReadingDate = sdf.format(new Date(readingSession.getEndReadingDateMillis()));

        this.timeInterval = readingSession.getTimeInterval();
        this.charOffset = readingSession.getCharOffset();

        if (readingSession.getUnknownWords() != null) {
            this.unknownWords = readingSession.getUnknownWords().split(",");
        } else {
            this.unknownWords = new String[0];
        }

        this.numberOfWords = readingSession.getNumberOfWords();
    }

    public Integer getReadingSessionId() {
        return readingSessionId;
    }

    public void setReadingSessionId(Integer readingSessionId) {
        this.readingSessionId = readingSessionId;
    }

    public String getEndReadingDate() {
        return endReadingDate;
    }

    public void setEndReadingDate(String endReadingDate) {
        this.endReadingDate = endReadingDate;
    }

    public String getReadingStatisticsId() {
        return readingStatisticsId;
    }

    public void setReadingStatisticsId(String readingStatisticsId) {
        this.readingStatisticsId = readingStatisticsId;
    }

    public Integer getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(Integer timeInterval) {
        this.timeInterval = timeInterval;
    }

    public Integer getCharOffset() {
        return charOffset;
    }

    public void setCharOffset(Integer charOffset) {
        this.charOffset = charOffset;
    }

    public String[] getUnknownWords() {
        return unknownWords;
    }

    public void setUnknownWords(String[] unknownWords) {
        this.unknownWords = unknownWords;
    }

    public Integer getNumberOfWords() {
        return numberOfWords;
    }

    public void setNumberOfWords(Integer numberOfWords) {
        this.numberOfWords = numberOfWords;
    }
}
