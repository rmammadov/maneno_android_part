package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.Util;

import java.text.ParseException;
import java.util.Date;

/**
 * Created by Bogumił Sikora.
 */
public class DailyReading {

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("minutes")
    @Expose
    private Integer minutes;

    public DailyReading(Date date, int minutes) {
        try {
            this.date = Util.formatDate(date, Consts.DATE_TIME_FORMAT_SHORT);
        } catch (ParseException e) {
            // todo - just log warning.
        }
        this.minutes = minutes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getMinutes() {
        return minutes;
    }

    public void setMinutes(Integer minutes) {
        this.minutes = minutes;
    }

}
