package com.knowlge.maneno.domain.parser.epub.model.opf;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Bogumił Sikora.
 *
 * Example element: <item id="id-4" href="ch04.xhtml" media-type="application/xhtml+xml"/>
 */

@Root
public class Item {

    @Attribute
    private String id;

    @Attribute
    private String href;

    @Attribute(name="media-type")
    private String mediaType;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }
}
