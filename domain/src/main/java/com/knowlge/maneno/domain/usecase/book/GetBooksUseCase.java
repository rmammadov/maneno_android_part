package com.knowlge.maneno.domain.usecase.book;

import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.BookInfoList;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 */

public class GetBooksUseCase extends GetBooksDataUseCase<GetBooksUseCase.Params, GetBooksUseCase.Result> {

    @Inject
    public GetBooksUseCase() {}

    @Override
    protected Observable<GetBooksUseCase.Result> createUseCaseObservable(Params params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Params, GetBooksUseCase.Result>() {
            @Override
            public GetBooksUseCase.Result execute(Params params) throws Exception {
                if (params == null) {
                    params = new Params();
                }

                BookInfoList booksList = restApi.getBooks(params.authorId, Util.toString(params.colorCodes), Util.toString(params.tags),
                        Util.toString(params.languages), params.liked, params.offset, params.limit);

                if (booksList == null || booksList.getRows() == null || booksList.getRows().size() == 0) {
                    return new Result(null, new ArrayList<BookInfo>(), 0);
                }

                List<Integer> resultBookIds = new ArrayList<>();
                for (BookInfo bookInfo: booksList.getRows()) {
                    resultBookIds.add(bookInfo.getId());
                }

                // Check if we already have some local data for result books.
                Map<Integer, BookInfo> localBookInfos = localRepository.getBookInfosMap(resultBookIds);

                List<BookInfo> manenosChoiceBooksList = new ArrayList<>();
                List<BookInfo> resultBooksList = new ArrayList<>();
                for (BookInfo bookInfo: booksList.getRows()) {
                    if (localBookInfos != null &&
                            localBookInfos.get(bookInfo.getId()) != null) {
                        BookInfo book = mergeBookInfoData(localBookInfos.get(bookInfo.getId()), bookInfo);
                        if (book != null) {
                            // Newer data was received, update local repository.
                            localRepository.updateBookInfo(book);
                        } else {
                            // Add data from local repository, because no new data was get.
                            book = localBookInfos.get(bookInfo.getId());
                        }

                        if (book.getManenosChoice()) {
                            manenosChoiceBooksList.add(book);
                        } else {
                            resultBooksList.add(book);
                        }
                    } else {
                        setProperties(bookInfo);
                        memoryCache.put(bookInfo);

                        if (bookInfo.getManenosChoice()) {
                            manenosChoiceBooksList.add(bookInfo);
                        } else {
                            resultBooksList.add(bookInfo);
                        }
                    }
                }

                return new Result(manenosChoiceBooksList, resultBooksList, booksList.getTotalRows());
            }
        });
    }

    public static class Params {
        private Integer authorId;
        private Integer[] colorCodes;
        private Integer[] tags;
        private Integer[] languages;
        private Boolean liked;
        private Integer offset = 0;
        private Integer limit = Consts.DEFAULT_BOOKS_FETCHING_LIMIT;

        public Params() {}

        public Params(Set<Integer> colorCodes, Set<Integer> tags, Set<Integer> languages, Integer offset) {
            this.colorCodes = toArray(colorCodes);
            this.tags = toArray(tags);
            this.languages = toArray(languages);
            this.offset = offset;
        }

        private Integer[] toArray(Set<Integer> set) {
            if (set != null && set.size() > 0) {
                return set.toArray(new Integer[set.size()]);
            }

            return  null;
        }
    }

    public static class Result {
        private List<BookInfo> manenosChoiceBooks;
        private List<BookInfo> books;
        private Integer totalRows;

        public Result(List<BookInfo> manenosChoiceBooks, List<BookInfo> books, Integer totalRows) {
            this.manenosChoiceBooks = manenosChoiceBooks;
            this.books = books;
            this.totalRows = totalRows;
        }

        public List<BookInfo> getManenosChoiceBooks() {
            return manenosChoiceBooks;
        }

        public List<BookInfo> getBooks() {
            return books;
        }

        public Integer getTotalRows() {
            return totalRows;
        }
    }
}
