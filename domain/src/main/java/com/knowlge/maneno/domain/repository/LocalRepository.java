package com.knowlge.maneno.domain.repository;

import com.knowlge.maneno.domain.model.AuthenticationData;
import com.knowlge.maneno.domain.model.AuthenticationResult;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.Chapter;
import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Country;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.Level;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.model.School;
import com.knowlge.maneno.domain.model.Tag;
import com.knowlge.maneno.domain.model.User;

import java.util.List;
import java.util.Map;

/**
 * Created by Bogumił Sikora.
 */

public interface LocalRepository {
    Integer getCurrentUserId();
    User getCurrentUser();

    User findUser(String username);

    void updateUser(User user);

    boolean saveAuthenticationData(AuthenticationResult authResult);

    Level getUserLevel(Integer userId);

    AuthenticationData getAuthenticationData();

    boolean updateDictionaryData(List<Tag> tags, List<Language> languages, List<Country> countries, List<School> schools);

    Tag getTag(Integer tagId);
    List<Tag> getTags(List<Integer> tagIds);
    List<Tag> getTagsByType(Integer typeId);

    BookInfo getBookInfo(Integer bookId, boolean withChapters);

    ColorCode getColorCode(Integer colorCodeId);

    List<ColorCode> getColorCodes();

    void saveBookInfo(BookInfo bookInfo);
    void updateBookInfo(BookInfo bookInfo);
    Map<Integer,BookInfo> getBookInfosMap(List<Integer> ids);

    void setCurrentReadingSessionId(Long sessionId);
    Long getCurrentReadingSessionId();
    ReadingSession getReadingSession(Long sessionId);
    long saveReadingSession(ReadingSession readingSession);
    void updateReadingSession(ReadingSession readingSession);
    void deleteReadingSession(ReadingSession session);
    List<ReadingSession> getReadingSessions(Integer userId);

    void setCurrentReadingPageId(Long readingPageId);
    Long getCurrentReadingPageId();
    ReadingPageStatistics getReadingPageStatistics(Long readingPageStatisticsId);
    List<ReadingPageStatistics> getReadingPagesStatistics(Long readingSessionId);
    long saveReadingPageStatistics(ReadingPageStatistics readingPageStatistics);
    void updateReadingPageStatistics(ReadingPageStatistics readingPageStatistics);

    int getDistinctPagesCount(Long readingSessionId);
    int getDistinctWordsReadCount(Long readingSessionId);

    void deleteReadingPagesStatistics(List<ReadingPageStatistics> readingPagesStatistics);

    void saveBookChapters(Integer bookId, List<Chapter> chapters);
    void deleteBookChapters(Integer bookInfoId);

    void updateUserLevel(Integer userId, Level level);

    void updateTags(List<Tag> tags);
    void updateLanguages(List<Language> languages);
    void updateColorCodes(List<ColorCode> colorCodes);
    void updateCountries(List<Country> countries);
    void updateSchools(List<School> schools);

    List<Language> getLanguages();
}
