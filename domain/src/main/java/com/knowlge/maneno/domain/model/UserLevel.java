package com.knowlge.maneno.domain.model;

/**
 * Created by Bogumił Sikora.
 */

public class UserLevel {
    private Integer userId;
    private Level level;

    public UserLevel() {}

    public UserLevel(Integer userId, Level level) {
        this.userId = userId;
        this.level = level;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }
}
