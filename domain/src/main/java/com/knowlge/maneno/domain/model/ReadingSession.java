package com.knowlge.maneno.domain.model;

/**
 * Created by Bogumił Sikora.
 */

public class ReadingSession {

    /**
     * Session is running - book is opened and user is reading the book.
     */
    public static final int STATUS_RUNNING = 1;

    /**
     * Session is stopped - user clicked back button or app goes to background.
     */
    public static final int STATUS_STOPPED = 2;

    /**
     * Session is end - user clicked "End Reading" button.
     */
    public static final int STATUS_END = 3;


    private Long id; // Local storage id. Used to map proper ReadingPageStatistics records.
    private Integer userId; // Many users could have different reading session on the same device.

    private Integer status;

    private Long endReadingDateMillis; // Should be sent in format: "2017-02-07T13:26:01+02:00"
    private Integer timeInterval = 0; // In seconds. Only latest interval from last successful readingstatistics request call.
    private Integer sessionTimeInterval = 0; // Whole session time interval in seconds.
    private Integer charOffset = 0; //
    private String unknownWords; // Unknown words coma separated. Could have duplicates, which should be removed before sent to the server.
    private Integer numberOfWords = 0; // Number of words read during this session.

    private Long readStartTimeMillis; // Used to calculate timeInterval during session end.
    private Integer bookId;

    private Integer readingSessionId;
    private String userReadingHistoryId;
    private String readingStatisticsId; // GUID generated on app side.

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getEndReadingDateMillis() {
        return endReadingDateMillis;
    }

    public void setEndReadingDateMillis(Long endReadingDateMillis) {
        this.endReadingDateMillis = endReadingDateMillis;
    }

    public Integer getTimeInterval() {
        return timeInterval;
    }

    public void setTimeInterval(Integer timeInterval) {
        this.timeInterval = timeInterval;
    }

    public Integer getSessionTimeInterval() {
        return sessionTimeInterval;
    }

    public void setSessionTimeInterval(Integer sessionTimeInterval) {
        this.sessionTimeInterval = sessionTimeInterval;
    }

    public Integer getCharOffset() {
        return charOffset;
    }

    public void setCharOffset(Integer charOffset) {
        this.charOffset = charOffset;
    }

    public String getUnknownWords() {
        return unknownWords;
    }

    public void setUnknownWords(String unknownWords) {
        this.unknownWords = unknownWords;
    }

    public Integer getNumberOfWords() {
        return numberOfWords;
    }

    public void setNumberOfWords(Integer numberOfWords) {
        this.numberOfWords = numberOfWords;
    }

    public Long getReadStartTimeMillis() {
        return readStartTimeMillis;
    }

    public void setReadStartTimeMillis(Long readStartTimeMillis) {
        this.readStartTimeMillis = readStartTimeMillis;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Integer getReadingSessionId() {
        return readingSessionId;
    }

    public void setReadingSessionId(Integer readingSessionId) {
        this.readingSessionId = readingSessionId;
    }

    public String getUserReadingHistoryId() {
        return userReadingHistoryId;
    }

    public void setUserReadingHistoryId(String userReadingHistoryId) {
        this.userReadingHistoryId = userReadingHistoryId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void increaseReadingIntervals(int readingIntervalSeconds) {
        timeInterval += readingIntervalSeconds;
        sessionTimeInterval += readingIntervalSeconds;
    }

    public void decreaseTimeInterval(int interval) {
        timeInterval -= interval;
    }

    public String getReadingStatisticsId() {
        return readingStatisticsId;
    }

    public void setReadingStatisticsId(String readingStatisticsId) {
        this.readingStatisticsId = readingStatisticsId;
    }
}
