package com.knowlge.maneno.domain.usecase.login;

import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.AuthenticationResult;
import com.knowlge.maneno.domain.model.User;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import java.io.IOException;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 */

public class LoginUseCase extends UseCase<LoginParams, LoginResult> {

    @Inject
    public LoginUseCase() {}

    @Override
    protected Observable<LoginResult> createUseCaseObservable(LoginParams params) {

        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<LoginParams, LoginResult>() {
            @Override
            public LoginResult execute(LoginParams params) throws Exception {
                String username = params.getEmail();
                String password = params.getPassword();
                boolean rememberMe = params.isRememberMe();

                boolean result = false;
                Integer errCode = null;

                if (!Util.isOnline()) {
                    result = authenticate(false, username, password, rememberMe);
                } else {
                    Boolean isUnilogin = restApi.checkForUnilogin(username);

                    if (isUnilogin != null) {
                        if (isUnilogin) {
                            // Authenticate unilogin user.
                            int uniloginResult = restApi.authenticateWithUniloginServer(username, password);
                            if (uniloginResult == UniloginResult.SUCCESS) {
                                result = authenticate(true, username, password, rememberMe);
                            } else if (uniloginResult == UniloginResult.TEMP_LICENSE) {
                                if (restApi.assignTempLicense(username)) {
                                    result = authenticate(true, username, password, rememberMe);
                                } // else failed to assign temp license.
                            } // else UniloginResult.FAILED.

                            // Set errCode if unilogin authentication failed.
                            if (!result) {
                                errCode = LoginResult.ERR_CODE_UNILOGIN_USER_LOGIN_ERROR;
                            }
                        } else {
                            result = authenticate(false, username, password, rememberMe);
                        }
                    } else {
                        result = false;
                        errCode = LoginResult.ERR_CODE_UNILOGIN_USER_LOGIN_ERROR;
                    }
                }

                // Set default errCode if authentication failed.
                if (!result && errCode == null) {
                    errCode = LoginResult.ERR_CODE_INCORRECT_USERNAME_PASSWORD;
                }

                // Set current authentication data for Web API calls.
                if (result) {
                    restApi.setAuthenticationData(localRepository.getAuthenticationData());
                }

                return new LoginResult(result, errCode);
            }
        });
    }

    private boolean authenticate(boolean isUniloginUser, String username, String password, boolean rememberMe) {
        if (!Util.isOnline()) {
            User user = localRepository.findUser(username);
            if (user != null) {
                // Check if password hash is the same.
                boolean result = calculatePasswordHash(password, username).equals(user.getPasswordHash());

                if (result && rememberMe != user.isRememberMe()) {
                    user.setRememberMe(rememberMe);
                    localRepository.updateUser(user);
                }

                return result;
            }
        } else {
            AuthenticationResult authResult = null;
            try {
                if (isUniloginUser) {
                    authResult = restApi.getTokenForUniloginUser(username);
                } else {
                    authResult = restApi.getToken(username, password);
                }
            } catch (IOException e) {
                // todo - log exception properly if debug.
                e.printStackTrace();
            }

            if (authResult != null) {
                // Property isUniloginUser will be used in settings (not change password functionality available) and in sing up process - email/password form is skipped.
                authResult.getUser().setIsUniloginUser(isUniloginUser);

                // Save password hash, so offline authentication is possible.
                authResult.getUser().setPasswordHash(calculatePasswordHash(password, username));

                authResult.getUser().setRememberMe(rememberMe);

                // Save or update authentication tokens and current user data.
                return localRepository.saveAuthenticationData(authResult);
            }
        }

        return false;
    }

    private String calculatePasswordHash(String password, String username) {
        return Util.hmacsha1(password, username);
    }
}
