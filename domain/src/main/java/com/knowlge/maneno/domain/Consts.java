package com.knowlge.maneno.domain;

/**
 * Created by Bogumił Sikora.
 */

public class Consts {
    public static final String SECRET_KEY = "VseX4VtP4O=";
    public static final int MIN_READ_TIME_SECONDS = 60;
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZZZZZ";
    public static final String DATE_TIME_FORMAT_SHORT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_YEAR_MONTH_DAY_FORMAT = "yyyy-MM-dd";
    public static final String DATE_DAY_MONTH_YEAR_FORMAT = "dd-MM-yy";
    public static final String DATE_DAY_MONTH_FORMAT = "dd/MM";
    public static final String PROGRESS_ID_HEADER = "ProgressId";
    public static final Integer MIN_DAILY_READING_MINUTES = 15;

    public static final int DEFAULT_BOOKS_FETCHING_LIMIT = 30;

    public static final int FULL_SCREEN_LAYOUT_TYPE = 1;
    public static final int PART_SCREEN_LAYOUT_TYPE = 2;

    public static class LangCode {
        public static final String DANISH = "da";
        public static final String BRITISH_ENGLISH = "en";
        public static final String TYSK = "ge";
    }
}
