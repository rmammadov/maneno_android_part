package com.knowlge.maneno.domain.usecase.book;

import com.google.common.io.BaseEncoding;
import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.Chapter;
import com.knowlge.maneno.domain.parser.epub.EpubParseResult;
import com.knowlge.maneno.domain.parser.epub.EpubParser;
import com.knowlge.maneno.domain.repository.RestApiRequestProgressListener;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

import static com.knowlge.maneno.domain.Util.isSet;

/**
 * Created by Bogumił Sikora.
 */

public class GetBookContentUseCase extends UseCase<GetBookContentUseCase.Params, GetBookContentUseCase.Result> {

    private final static int DOWNLOAD_BOOK_PERCENT_PART = 90;
    private final static int UNZIP_BOOK_PERCENT_PART = 92;
    private final static int PARSED_BOOK_PERCENT_PART = 95;

    @Inject
    EpubParser epubParser;

    @Inject
    public GetBookContentUseCase() {}

    @Override
    protected Observable<Result> createUseCaseObservable(Params params) {
        return ObservableCreator.createMultipleTimeObservable(params, true, new ObservableCreator.MultipleTime<Params, Result>() {
            @Override
            public void execute(Params params, Subscriber<? super Result> subscriber) throws Exception {
                Integer currentUserId = localRepository.getCurrentUserId();
                BookInfo bookInfo = localRepository.getBookInfo(params.bookId, true);

                if (bookInfo == null) {
                    bookInfo = memoryCache.getBookInfo(params.bookId, false);
                }

                if (currentUserId == null || bookInfo == null) {
                    // todo - handle error.
                    return;
                }

                verifyBookContent(bookInfo);

                if (bookInfo.getChapters() == null) {
                    // todo - Make sure that it works properly even if it is not per user folder.
                    String uniqueFolderName = params.bookId + "_" + new Date().getTime();
                    String zipFileName = uniqueFolderName + ".zip";

                    InputStream bookContentInputStream = downloadBook(params.bookId, currentUserId, subscriber);
                    fileRepository.saveFile(zipFileName, bookContentInputStream);

                    fileRepository.unzipFileInto(zipFileName, uniqueFolderName);
                    subscriber.onNext(new Result(UNZIP_BOOK_PERCENT_PART));

                    // Iterate through all unzipped files and save file names.
                    File epubFile = null;
                    File pdfFile = null;
                    for (File file: fileRepository.getFiles(uniqueFolderName)) {
                        String extension = Util.getFileExtension(file);

                        if ("epub".equals(extension)) {
                            epubFile = file;
                        } else if ("pdf".equals(extension)) {
                            pdfFile = file;
                        } else if ("json".equals(extension)) {
                            bookInfo.setDictionaryFilePath(file.getName());
                        }
                        // todo - save paths to narration files (narrationFileName, narrationMetadataFileName),
                        // but probably no action is required if we have relative paths
                    }

                    if (epubFile != null) {
                        fileRepository.unzipFileInto(epubFile, uniqueFolderName);
                        fileRepository.delete(epubFile);

                        EpubParseResult parseResult = epubParser.parse(uniqueFolderName, bookInfo.getId());
                        bookInfo.setChapters(parseResult.getChapters());
                        bookInfo.setName(parseResult.getBookTitle());
                        if (Util.isSet(parseResult.getAuthors())) {
                            bookInfo.setAuthorName(parseResult.getAuthors());
                        }

                        parseChaptersXhtmlFiles(bookInfo);

                        localRepository.saveBookChapters(bookInfo.getId(), bookInfo.getChapters());

                        // todo - xhtml chapters files could be deleted here.
                    } else if (pdfFile != null) {
                        // todo - parse pdf content.
                    }

                    subscriber.onNext(new Result(PARSED_BOOK_PERCENT_PART));

                    // todo - set proper paths in BookInfo object
                    bookInfo.setUniqueFolderName(uniqueFolderName);
                    bookInfo.setBookLoadDate(new Date().getTime());

                    fileRepository.delete(zipFileName);
                }

                bookInfo.setLastTimeOpened(new Date().getTime());
                localRepository.updateBookInfo(bookInfo);

                subscriber.onNext(new Result(bookInfo));
            }
        });
    }

    private void parseChaptersXhtmlFiles(BookInfo bookInfo) throws IOException {
        // Get chapter content from xhtml file
        for (Chapter ch: bookInfo.getChapters()) {
            String content = fileRepository.readFile(ch.getContentPath());
            Document parsedContent = Jsoup.parse(content);

            // Get images files.
            String parentAbsolutePath = fileRepository.getParentAbsolutePath(ch.getContentPath());
            Elements images = parsedContent.getElementsByTag("img");
            for (Element img: images) {
                String src = img.attr("src");
                File imgFile = new File(parentAbsolutePath + "/" + src);
                if (imgFile.exists()) {
                    ch.putImage(src, imgFile.getAbsolutePath());
                }
            }

            ch.setBodyHtml(parsedContent.body().html());
        }
    }

    public static class Params {
        private Integer bookId;

        public Params(Integer bookId) {
            this.bookId = bookId;
        }
    }

    public static class Result {
        private final Integer progressPercent; // Downloading book content progressPercent percent from 1 to 100.
        private final BookInfo bookInfo; // Not null only when progressPercent is 100.

        public Result(int progressPercent) {
            this.progressPercent = progressPercent;
            this.bookInfo = null;
        }

        public Result(BookInfo bookInfo) {
            this.progressPercent = 99; // 1% for rest UI tasks (splitting into pages).
            this.bookInfo = bookInfo;
        }

        public Integer getProgressPercent() {
            return progressPercent;
        }

        public BookInfo getBookInfo() {
            return bookInfo;
        }
    }

    private void verifyBookContent(BookInfo bookInfo) {
        // todo - add checking 30 days book content expiration.

        if (bookInfo.getChapters() != null) {
            // Check if chapters content xhtml files exists. It is possible, that user deleted files.
            boolean filesExists = true;
            for (Chapter ch: bookInfo.getChapters()) {
                if (!fileRepository.exists(ch.getContentPath())) {
                    filesExists = false;
                    break;
                }
            }

            if (!filesExists) {
                if (isSet(bookInfo.getUniqueFolderName())) {
                    // todo - delete whole content under bookInfo.getUniqueFolderName().
                }
                bookInfo.setChapters(null);
                localRepository.deleteBookChapters(bookInfo.getId());
                bookInfo.setUniqueFolderName(null);
            }
        }
    }

    private InputStream downloadBook(Integer bookId, Integer currentUserId, Subscriber<? super Result> subscriber) throws IOException {
        String userIdAndBookIdStr = String.valueOf(bookId) + "|" + String.valueOf(currentUserId);
        String withSecretKey = userIdAndBookIdStr + "|" + Consts.SECRET_KEY;

        String encr = BaseEncoding.base64().encode(userIdAndBookIdStr.getBytes());
        String hash = Util.sha256(withSecretKey).toUpperCase();

        return restApi.getBookContent(encr, hash, false, new DownloadBookProgressListener(subscriber, bookId));
    }

    private class DownloadBookProgressListener implements RestApiRequestProgressListener {
        private Subscriber<? super Result> subscriber;
        private String progressId;

        public DownloadBookProgressListener(Subscriber<? super Result> subscriber, Integer bookId) {
            this.subscriber = subscriber;
            this.progressId = "get_book_content_" + bookId;
        }

        @Override
        public void onProgress(int percent, String progressId) {
            if (progressId != null && progressId.equals(this.progressId)) {
                // Download part is from 0% to 90% of whole part, so rescale before sending.
                subscriber.onNext(new Result(percent * DOWNLOAD_BOOK_PERCENT_PART / 100));
            }
        }

        @Override
        public String getProgressId() {
            return progressId;
        }
    }
}
