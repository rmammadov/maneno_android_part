package com.knowlge.maneno.domain.usecase.reading;

import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.usecase.base.LogErrorSubscriber;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 *
 * Executed when user stopped reading and clicked "End Reading" button on reading session summary popup view.
 *
 */

public class EndReadingSessionUseCase extends UseCase<Void, Void> {

    @Inject
    SendReadingStatisticsUseCase sendReadingStatisticsUseCase;

    @Inject
    public EndReadingSessionUseCase() {}

    @Override
    protected Observable<Void> createUseCaseObservable(Void params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Void, Void>() {
            @Override
            public Void execute(Void params) throws Exception {
                ReadingSession session = getCurrentReadingSession();
                if (session != null && session.getStatus() == ReadingSession.STATUS_STOPPED) {
                    session.setStatus(ReadingSession.STATUS_END);
                    localRepository.updateReadingSession(session);
                }

                localRepository.setCurrentReadingSessionId(null);

                // Initiate sending statistics - here reading session should be already sent,
                // but now we can send pages statistics and then remove session record.
                sendReadingStatisticsUseCase.execute(new LogErrorSubscriber(log));

                return null;
            }
        });
    }
}
