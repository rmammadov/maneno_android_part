package com.knowlge.maneno.domain.usecase.book;

import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.Tag;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import java.util.Date;
import java.util.List;

/**
 * Created by Bogumił Sikora.
 */

abstract class GetBooksDataUseCase<Q, R> extends UseCase<Q, R> {

    BookInfo mergeBookInfoData(BookInfo localBookInfo, BookInfo remoteBookInfo) {
        try {
            if (localBookInfo == null || remoteBookInfo == null) {
                return null;
            }

            Date localTimestamp = Util.parseDate(localBookInfo.getTimestamp());
            Date remoteTimestamp = Util.parseDate(remoteBookInfo.getTimestamp());

            // Update local only if timestamp is newer.
            if (localTimestamp == null || remoteTimestamp == null
                    || remoteTimestamp.getTime() > localTimestamp.getTime()) {
                // Update only remote data, do not touch already set local data.

                //localBookInfo.setName(remoteBookInfo.getName()); // Do not update, value already updated from ePub.
                localBookInfo.setTotalWordCount(remoteBookInfo.getTotalWordCount());
                localBookInfo.setTotalChapters(remoteBookInfo.getTotalChapters());
                localBookInfo.setTotalPages(remoteBookInfo.getTotalPages());
                localBookInfo.setAuthorId(remoteBookInfo.getAuthorId());
                localBookInfo.setReleaseDate(remoteBookInfo.getReleaseDate());
                localBookInfo.setFiles(remoteBookInfo.getFiles());
                localBookInfo.setTagList(remoteBookInfo.getTagList());
                localBookInfo.setLike(remoteBookInfo.getLike());
                localBookInfo.setLikesCount(remoteBookInfo.getLikesCount());
                //localBookInfo.setAuthorName(remoteBookInfo.getAuthorName()); // Do not update, value already updated from ePub.
                localBookInfo.setSummary(remoteBookInfo.getSummary());
                localBookInfo.setHasOriginalNarration(remoteBookInfo.getHasOriginalNarration());
                localBookInfo.setNarrationFileName(remoteBookInfo.getNarrationFileName());
                localBookInfo.setNarrationMetadataFileName(remoteBookInfo.getNarrationMetadataFileName());
                localBookInfo.setThumbnailUrl(remoteBookInfo.getThumbnailUrl());
                localBookInfo.setIsPrivate(remoteBookInfo.getIsPrivate());
                localBookInfo.setHasTasks(remoteBookInfo.getHasTasks());
                localBookInfo.setFriendsReading(remoteBookInfo.getFriendsReading());
                localBookInfo.setFriendsRecommend(remoteBookInfo.getFriendsRecommend());
                localBookInfo.setLanguageId(remoteBookInfo.getLanguageId());
                localBookInfo.setTotalCharacters(remoteBookInfo.getTotalCharacters());
                localBookInfo.setManenosChoice(remoteBookInfo.getManenosChoice());
                localBookInfo.setBackgroundColor(remoteBookInfo.getBackgroundColor());
                localBookInfo.setTimestamp(remoteBookInfo.getTimestamp());
                localBookInfo.setPurgeDate(remoteBookInfo.getPurgeDate());
                localBookInfo.setNumberOfReads(remoteBookInfo.getNumberOfReads());
                localBookInfo.setIsAvailable(remoteBookInfo.getIsAvailable());
                localBookInfo.setColorCode(remoteBookInfo.getColorCode());
                localBookInfo.setPackages(remoteBookInfo.getPackages());

                setProperties(localBookInfo);

                return localBookInfo;
            }
        } catch (Exception e) {
            log.e(e);
        }

        return null;
    }

    void setProperties(BookInfo bookInfo) {
        setBookInfoRelations(bookInfo);
        setTagsRelatedProperties(bookInfo);
    }

    private void setBookInfoRelations(BookInfo bookInfo) {
        if (bookInfo != null) {
            bookInfo.setColorCodeObj(localRepository.getColorCode(bookInfo.getColorCode()));

            // todo - others?
        }
    }

    private void setTagsRelatedProperties(BookInfo bookInfo) {
        if (bookInfo != null && bookInfo.getTagList() != null) {
            List<Tag> tags = localRepository.getTags(bookInfo.getTagList());

            String lix = null;
            for (Tag tag: tags) {
                if (Tag.TYPE_ID_DIFFICULTY.equals(tag.getTagTypeId())) {
                    if (tag.getName() != null) {
                        // If it is group tag (LIX 27-21, LIX31+), skip it.
                        if (tag.getName().contains("-") || tag.getName().contains("+")) {
                            continue;
                        }
                        if (lix == null) {
                            lix = tag.getName();
                            bookInfo.setLevel(tag.getName());
                        } else {
                            lix = lix + ", " + tag.getName();
                            // For 2 or more level tags we show firs LIX only.
                            if (tag.getName().contains("LIX")
                                    && !bookInfo.getLevel().contains("LIX")) {
                                bookInfo.setLevel(tag.getName());
                            }
                        }
                    }
                } else if (Tag.TYPE_ID_SERIES.equals(tag.getTagTypeId())) {
                    bookInfo.setSeriesName(tag.getName());
                    bookInfo.setSeriesTagId(tag.getId());
                }
            }

            bookInfo.setLix(lix);
            bookInfo.setLixValue(null);
            if (bookInfo.getLevel() != null) {
                String lixValue = bookInfo.getLevel().replaceAll("LIX", "").replaceAll("LET", "");
                try {
                    bookInfo.setLixValue(Integer.parseInt(lixValue));
                } catch (NumberFormatException nfe) {
                    log.e(nfe);
                }
            }
        }
    }
}
