package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Bogumił Sikora.
 */

public class ReadingPageStatistics {
    private transient Long id; // Local storage id.
    private transient Long sessionId; // Local storage reading session id.
    private transient Integer wordsRead;


    @SerializedName("pageNumber")
    @Expose
    private Integer pageNumber;

    @SerializedName("wordIndexStart")
    @Expose
    private Integer wordIndexStart;

    @SerializedName("wordIndexEnd")
    @Expose
    private Integer wordIndexEnd;

    @SerializedName("pageStartTime")
    @Expose
    private String pageStartTime; // In format: yyyy-MM-dd'T'HH:mm:ssZZZZZ

    @SerializedName("pageEndTime")
    @Expose
    private String pageEndTime; // In format: yyyy-MM-dd'T'HH:mm:ssZZZZZ


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getWordIndexStart() {
        return wordIndexStart;
    }

    public void setWordIndexStart(Integer wordIndexStart) {
        this.wordIndexStart = wordIndexStart;
    }

    public Integer getWordIndexEnd() {
        return wordIndexEnd;
    }

    public void setWordIndexEnd(Integer wordIndexEnd) {
        this.wordIndexEnd = wordIndexEnd;
    }

    public String getPageStartTime() {
        return pageStartTime;
    }

    public void setPageStartTime(String pageStartTime) {
        this.pageStartTime = pageStartTime;
    }

    public String getPageEndTime() {
        return pageEndTime;
    }

    public void setPageEndTime(String pageEndTime) {
        this.pageEndTime = pageEndTime;
    }

    public Integer getWordsRead() {
        return wordsRead;
    }

    public void setWordsRead(Integer wordsRead) {
        this.wordsRead = wordsRead;
    }
}
