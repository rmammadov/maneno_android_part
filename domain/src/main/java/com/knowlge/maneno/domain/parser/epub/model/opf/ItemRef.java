package com.knowlge.maneno.domain.parser.epub.model.opf;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by Bogumił Sikora.
 *
 * Example element: <itemref idref="id-1"/>
 */
@Root(name = "itemref")
public class ItemRef {

    @Attribute(name = "idref")
    private String idRef;

    public String getIdRef() {
        return idRef;
    }

    public void setIdRef(String idRef) {
        this.idRef = idRef;
    }
}
