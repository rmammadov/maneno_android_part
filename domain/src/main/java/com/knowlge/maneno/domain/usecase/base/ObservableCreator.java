package com.knowlge.maneno.domain.usecase.base;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Bogumił Sikora.
 */
public class ObservableCreator {

    public interface OneTime<P, R> {
        R execute(final P params) throws Exception;
    }

    public interface MultipleTime<P, R> {
        void execute(final P params, Subscriber<? super R> subscriber) throws Exception;
    }

    public static<P, R> Observable<R> createOneTimeObservable(final P params, final OneTime<P, R> oneTime) {
        return Observable.create(new Observable.OnSubscribe<R>() {
            @Override
            public void call(Subscriber<? super R> subscriber) {
                try {
                    subscriber.onNext(oneTime.execute(params));
                    subscriber.onCompleted();
                } catch (Throwable t) {
                    subscriber.onError(t);
                }
            }
        });
    }

    public static<P, R> Observable<R> createMultipleTimeObservable(final P params, final boolean autoComplete, final MultipleTime<P, R> multipleTime) {
        return Observable.create(new Observable.OnSubscribe<R>() {
            @Override
            public void call(Subscriber<? super R> subscriber) {
                try {
                    multipleTime.execute(params, subscriber);
                    if (autoComplete) {
                        subscriber.onCompleted();
                    }
                } catch (Throwable t) {
                    subscriber.onError(t);
                }
            }
        });
    }
}
