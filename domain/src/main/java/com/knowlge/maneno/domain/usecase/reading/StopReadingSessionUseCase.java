package com.knowlge.maneno.domain.usecase.reading;

import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingProgress;
import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.model.User;
import com.knowlge.maneno.domain.repository.LocalRepository;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Bogumił Sikora.
 *
 * Executed every time when reading session is stopped:
 * - when user click back button,
 * - when user click device back button,
 * - when application goes to background during reading.
 *
 */

public class StopReadingSessionUseCase extends ReadingStatisticsSendableUseCase<StopReadingSessionUseCase.Params, StopReadingSessionUseCase.Result>{

    @Inject
    public StopReadingSessionUseCase() {}

    @Override
    protected Observable<Result> createUseCaseObservable(Params params) {
        return ObservableCreator.createMultipleTimeObservable(params, false, new ObservableCreator.MultipleTime<Params, Result>() {
            @Override
            public void execute(Params params, Subscriber<? super Result> subscriber) throws Exception {
                ReadingSession readingSession = null;
                Date now = new Date();

                synchronized (LocalRepository.class) {
                    readingSession = getCurrentReadingSession();

                    if (readingSession != null
                            && ReadingSession.STATUS_RUNNING == readingSession.getStatus()) {
                        readingSession.setStatus(ReadingSession.STATUS_STOPPED);
                        readingSession.setEndReadingDateMillis(now.getTime());
                        readingSession.increaseReadingIntervals(getLastReadingIntervalSeconds(readingSession));

                        int words = localRepository.getDistinctWordsReadCount(readingSession.getId());
                        readingSession.setNumberOfWords(words);

                        localRepository.updateReadingSession(readingSession);

                        // Get current reading page and set its pageEndTimeMillis.
                        ReadingPageStatistics currentReadingPage = getCurrentReadingPage();
                        if (currentReadingPage != null && currentReadingPage.getPageEndTime() == null) {
                            currentReadingPage.setPageEndTime(Util.formatDate(now));
                            localRepository.updateReadingPageStatistics(currentReadingPage);
                        }

                        localRepository.setCurrentReadingPageId(null);

                        if (params.sendResult) {
                            int minutes = readingSession.getSessionTimeInterval() / 60; // Session time interval is in seconds.
                            int pages = localRepository.getDistinctPagesCount(readingSession.getId());

                            User currentUser = localRepository.getCurrentUser();
                            subscriber.onNext(new Result(currentUser.getName(), minutes, pages, words));
                        }

                        // Update bookInfo properties.
                        BookInfo bookInfo = localRepository.getBookInfo(readingSession.getBookId(), false);
                        if (bookInfo != null) {
                            bookInfo.setLatestTextOffset(params.currentTextLocation);
                            if (bookInfo.getPagesRead() == null) {
                                bookInfo.setPagesRead(params.currentPageNumber);
                            } else {
                                bookInfo.setPagesRead(Math.max(params.currentPageNumber, bookInfo.getPagesRead()));
                            }
                            bookInfo.setPagesTotal(params.pagesCount);
                            localRepository.updateBookInfo(bookInfo);
                        }
                    }
                }

                if (readingSession != null && ReadingSession.STATUS_STOPPED == readingSession.getStatus()) {
                    if (params.sendResult) {
                        sentSessionStatistics(readingSession, false, false, subscriber);
                    } else {
                        sentSessionStatistics(readingSession, false, false);
                    }
                }
            }
        });
    }

    @Override
    protected Result createSentResult(ReadingProgress readingProgress) {
        return new StopReadingSessionUseCase.Result(readingProgress.getStatPoints(), readingProgress.getEquipPoints(),
                readingProgress.getAchievements(), readingProgress.getLevelCompleted());
    }

    private int getLastReadingIntervalSeconds(ReadingSession readingSession) {
        long readingIntervalSeconds = new Date().getTime() - readingSession.getReadStartTimeMillis();
        readingIntervalSeconds = readingIntervalSeconds/1000;

        return (int) readingIntervalSeconds;
    }

    public static class Params {
        private Integer currentTextLocation; // Current text offset in book.
        private Integer currentPageNumber;
        private Integer pagesCount;
        private boolean sendResult;

        public Params(Integer currentTextLocation, Integer currentPageNumber, Integer pagesCount, boolean sendResult) {
            this.currentTextLocation = currentTextLocation;
            this.currentPageNumber = currentPageNumber;
            this.pagesCount = pagesCount;
            this.sendResult = sendResult;
        }
    }

    public static class Result {
        private Integer minutes;
        private Integer pages;
        private Integer words;

        private Integer statPoints;
        private Integer equipPoints;
        private List<Achievement> achievements;
        private Boolean isLevelCompleted;
        private String userName;

        public Result(String userName, Integer minutes, Integer pages, Integer words) {
            this.userName = userName;
            this.minutes = minutes;
            this.pages = pages;
            this.words = words;
        }

        public Result(Integer statPoints, Integer equipPoints, List<Achievement> achievements, Boolean isLevelCompleted) {
            this.statPoints = statPoints;
            this.equipPoints = equipPoints;
            this.achievements = achievements;
            this.isLevelCompleted = isLevelCompleted;
        }

        public Integer getMinutes() {
            return minutes;
        }

        public Integer getPages() {
            return pages;
        }

        public Integer getWords() {
            return words;
        }

        public Integer getStatPoints() {
            return statPoints;
        }

        public void setStatPoints(Integer statPoints) {
            this.statPoints = statPoints;
        }

        public Integer getEquipPoints() {
            return equipPoints;
        }

        public void setEquipPoints(Integer equipPoints) {
            this.equipPoints = equipPoints;
        }

        public List<Achievement> getAchievements() {
            return achievements;
        }

        public void setAchievements(List<Achievement> achievements) {
            this.achievements = achievements;
        }

        public Boolean getLevelCompleted() {
            return isLevelCompleted;
        }

        public void setLevelCompleted(Boolean levelCompleted) {
            isLevelCompleted = levelCompleted;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }
    }
}
