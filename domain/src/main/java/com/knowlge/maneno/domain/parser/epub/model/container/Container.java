package com.knowlge.maneno.domain.parser.epub.model.container;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Bogumił Sikora.
 */
@Root(name = "container", strict = false)
public class Container {

    @ElementList(name = "rootfiles")
    private List<RootFile> rootFiles;

    public List<RootFile> getRootFiles() {
        return rootFiles;
    }

    public void setRootFiles(List<RootFile> rootFiles) {
        this.rootFiles = rootFiles;
    }
}
