package com.knowlge.maneno.domain.usecase.base;

import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.executor.PostExecutionThread;
import com.knowlge.maneno.domain.log.Log;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.repository.FileRepository;
import com.knowlge.maneno.domain.repository.LocalRepository;
import com.knowlge.maneno.domain.repository.MemoryCache;
import com.knowlge.maneno.domain.repository.RestApi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by Bogumił Sikora.
 */

public abstract class UseCase<Q, R> {

    @Inject
    protected Executor executor;

    @Inject
    protected PostExecutionThread postExecutionThread;

    @Inject
    protected RestApi restApi;

    @Inject
    protected LocalRepository localRepository;

    @Inject
    protected MemoryCache memoryCache;

    @Inject
    protected FileRepository fileRepository;

    @Inject
    protected Log log;

    private Subscription subscription = Subscriptions.empty();

    protected abstract Observable<R> createUseCaseObservable(final Q params);

    public void execute(Subscriber subscriber) {
        execute(subscriber, null);
    }

    @SuppressWarnings("unchecked")
    public void execute(Subscriber subscriber, final Q params) {
        subscription = createUseCaseObservable(params)
                .subscribeOn(Schedulers.from(executor))
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(subscriber);
    }

    protected ReadingSession getCurrentReadingSession() {
        Long currentReadingSessionId = localRepository.getCurrentReadingSessionId();

        if (currentReadingSessionId != null) {
            return localRepository.getReadingSession(currentReadingSessionId);
        }

        return null;
    }

    protected ReadingPageStatistics getCurrentReadingPage() {
        Long currentReadingPageId = localRepository.getCurrentReadingPageId();

        if (currentReadingPageId != null) {
            return localRepository.getReadingPageStatistics(currentReadingPageId);
        }

        return null;
    }
}
