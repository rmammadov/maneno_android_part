package com.knowlge.maneno.domain.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bogumił Sikora.
 */

public class BookInfo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name; // kBookTitleKey = @"title";
    @SerializedName("totalWordCount")
    @Expose
    private Integer totalWordCount;
    @SerializedName("totalChapters")
    @Expose
    private Integer totalChapters;
    @SerializedName("totalPages")
    @Expose
    private Integer totalPages;
    @SerializedName("authorId")
    @Expose
    private Integer authorId;
    @SerializedName("releaseDate")
    @Expose
    private String releaseDate;
    @SerializedName("files")
    @Expose
    private List<BookFile> files = null;
    @SerializedName("tagList")
    @Expose
    private List<Integer> tagList = null;
    @SerializedName("like")
    @Expose
    private Boolean like;
    @SerializedName("likesCount")
    @Expose
    private Integer likesCount;
    @SerializedName("authorName")
    @Expose
    private String authorName;
    @SerializedName("summary")
    @Expose
    private String summary; // kSummaryDescKey = @"summaryDescription";
    @SerializedName("hasOriginalNarration")
    @Expose
    private Boolean hasOriginalNarration;
    @SerializedName("narrationFileName")
    @Expose
    private String narrationFileName;
    @SerializedName("narrationMetadataFileName")
    @Expose
    private String narrationMetadataFileName;
    @SerializedName("thumbnailUrl")
    @Expose
    private String thumbnailUrl; //  kBookCoverURLKey = @"bookCoverURLKey";
    @SerializedName("isPrivate")
    @Expose
    private Boolean isPrivate;
    @SerializedName("hasTasks")
    @Expose
    private Boolean hasTasks;
    @SerializedName("friendsReading")
    @Expose
    private List<BaseUser> friendsReading = null;
    @SerializedName("friendsRecomend")
    @Expose
    private List<BaseUser> friendsRecommend = null;
    @SerializedName("languageId")
    @Expose
    private Integer languageId;
    @SerializedName("totalCharacters")
    @Expose
    private Integer totalCharacters;
    @SerializedName("manenosChoice")
    @Expose
    private Boolean manenosChoice; // kManenoChoiceKey = @"isManenoChoice";
    @SerializedName("backgroundColor")
    @Expose
    private String backgroundColor; // Hex format: "#FFFFFF"
    @SerializedName("timeStamp")
    @Expose
    private String timeStamp;
    @SerializedName("purgeDate")
    @Expose
    private String purgeDate;
    @SerializedName("numberOfReads")
    @Expose
    private Integer numberOfReads;
    @SerializedName("isAvailable")
    @Expose
    private Boolean isAvailable;
    @SerializedName("packages")
    @Expose
    private List<BookPackage> packages = null;

    @SerializedName("colorCode")
    @Expose
    private Integer colorCodeId;

    private ColorCode colorCodeObj;

    // No json properties:

    // Tags related:
    private String seriesName; // kSeriesNameKey = @"SeriesName"; - set based on tag with typeId = "7" (MNNSeriesTypeId)
    private Integer seriesTagId; // kSeriesTagKey = @"SeriesTag"; - id of series tag.
    private String lix; // kLixKey = @"lix"; - comma separated tags names with typeId = "2" (MNNDifficultyTypeId), but without "Group tags", which contains "-" or "+"
    private String level;
    private Integer lixValue; // Used to set proper level circle color on books list.

    // Date (in milliseconds from 1970) when book was last time opened.
    private Long lastTimeOpened;

    // Statics values updated at the end of every reading session and stored locally:
    private Integer pagesRead; // = MAX(self.book.pagesRead, self.currentPageNumber);
    private Integer pagesTotal; // = [self.pagesArray count];
    private Integer latestTextOffset; // = [self currentTextLocation];

    // Properties set during book decryption.
    private Long bookLoadDate; // Date (in milliseconds from 1970) when book was downloaded and decrypted first time. Used to check if book is more than 30 days. If so, content will be cleared.
    private String uniqueFolderName; // Set during book decryption. It is "$book.id_$timeIntervalSinceReferenceDate". Will contain decrypted book content.
    private String dictionaryFilePath;
    private String narrationFilePath;
    private String narrationJsonPath;
    private List<Chapter> chapters;

    // todo - to find usage and understand:
    // defaultFontSize: kFontSizeKey = @"fontSize";

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalWordCount() {
        return totalWordCount;
    }

    public void setTotalWordCount(Integer totalWordCount) {
        this.totalWordCount = totalWordCount;
    }

    public Integer getTotalChapters() {
        return totalChapters;
    }

    public void setTotalChapters(Integer totalChapters) {
        this.totalChapters = totalChapters;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<BookFile> getFiles() {
        return files;
    }

    public void setFiles(List<BookFile> files) {
        this.files = files;
    }

    public List<Integer> getTagList() {
        return tagList;
    }

    public void setTagList(List<Integer> tagList) {
        this.tagList = tagList;
    }

    public Boolean getLike() {
        return like;
    }

    public void setLike(Boolean like) {
        this.like = like;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Boolean getHasOriginalNarration() {
        return hasOriginalNarration;
    }

    public void setHasOriginalNarration(Boolean hasOriginalNarration) {
        this.hasOriginalNarration = hasOriginalNarration;
    }

    public String getNarrationFileName() {
        return narrationFileName;
    }

    public void setNarrationFileName(String narrationFileName) {
        this.narrationFileName = narrationFileName;
    }

    public String getNarrationMetadataFileName() {
        return narrationMetadataFileName;
    }

    public void setNarrationMetadataFileName(String narrationMetadataFileName) {
        this.narrationMetadataFileName = narrationMetadataFileName;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Boolean getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(Boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public Boolean getHasTasks() {
        return hasTasks;
    }

    public void setHasTasks(Boolean hasTasks) {
        this.hasTasks = hasTasks;
    }

    public List<BaseUser> getFriendsReading() {
        return friendsReading;
    }

    public void setFriendsReading(List<BaseUser> friendsReading) {
        this.friendsReading = friendsReading;
    }

    public List<BaseUser> getFriendsRecommend() {
        return friendsRecommend;
    }

    public void setFriendsRecommend(List<BaseUser> friendsRecommend) {
        this.friendsRecommend = friendsRecommend;
    }

    public Integer getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Integer languageId) {
        this.languageId = languageId;
    }

    public Integer getTotalCharacters() {
        return totalCharacters;
    }

    public void setTotalCharacters(Integer totalCharacters) {
        this.totalCharacters = totalCharacters;
    }

    public Boolean getManenosChoice() {
        return manenosChoice;
    }

    public void setManenosChoice(Boolean manenosChoice) {
        this.manenosChoice = manenosChoice;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getTimestamp() {
        return timeStamp;
    }

    public void setTimestamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getPurgeDate() {
        return purgeDate;
    }

    public void setPurgeDate(String purgeDate) {
        this.purgeDate = purgeDate;
    }

    public Integer getNumberOfReads() {
        return numberOfReads;
    }

    public void setNumberOfReads(Integer numberOfReads) {
        this.numberOfReads = numberOfReads;
    }

    public Boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(Boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public List<BookPackage> getPackages() {
        return packages;
    }

    public void setPackages(List<BookPackage> packages) {
        this.packages = packages;
    }

    public Integer getColorCode() {
        return colorCodeId;
    }

    public void setColorCode(Integer colorCodeId) {
        this.colorCodeId = colorCodeId;
    }

    public ColorCode getColorCodeObj() {
        return colorCodeObj;
    }

    public void setColorCodeObj(ColorCode colorCodeObj) {
        this.colorCodeObj = colorCodeObj;
    }

    public Integer getPagesRead() {
        return pagesRead;
    }

    public void setPagesRead(Integer pagesRead) {
        this.pagesRead = pagesRead;
    }

    public Integer getPagesTotal() {
        return pagesTotal;
    }

    public void setPagesTotal(Integer pagesTotal) {
        this.pagesTotal = pagesTotal;
    }

    public Integer getLatestTextOffset() {
        return latestTextOffset;
    }

    public void setLatestTextOffset(Integer latestTextOffset) {
        this.latestTextOffset = latestTextOffset;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    public Integer getSeriesTagId() {
        return seriesTagId;
    }

    public void setSeriesTagId(Integer seriesTagId) {
        this.seriesTagId = seriesTagId;
    }

    public String getLix() {
        return lix;
    }

    public void setLix(String lix) {
        this.lix = lix;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Integer getLixValue() {
        return lixValue;
    }

    public void setLixValue(Integer lixValue) {
        this.lixValue = lixValue;
    }

    public Long getLastTimeOpened() {
        return lastTimeOpened;
    }

    public void setLastTimeOpened(Long lastTimeOpened) {
        this.lastTimeOpened = lastTimeOpened;
    }

    public Long getBookLoadDate() {
        return bookLoadDate;
    }

    public void setBookLoadDate(Long bookLoadDate) {
        this.bookLoadDate = bookLoadDate;
    }

    public String getUniqueFolderName() {
        return uniqueFolderName;
    }

    public void setUniqueFolderName(String uniqueFolderName) {
        this.uniqueFolderName = uniqueFolderName;
    }

    public String getDictionaryFilePath() {
        return dictionaryFilePath;
    }

    public void setDictionaryFilePath(String dictionaryFilePath) {
        this.dictionaryFilePath = dictionaryFilePath;
    }

    public String getNarrationFilePath() {
        return narrationFilePath;
    }

    public void setNarrationFilePath(String narrationFilePath) {
        this.narrationFilePath = narrationFilePath;
    }

    public String getNarrationJsonPath() {
        return narrationJsonPath;
    }

    public void setNarrationJsonPath(String narrationJsonPath) {
        this.narrationJsonPath = narrationJsonPath;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void setChapters(List<Chapter> chapters) {
        this.chapters = chapters;
    }

    public Integer getPagesCount() {
        if (totalPages != null && totalPages > 0) {
            return totalPages;
        } else {
            return pagesTotal; // Return local statistics updated at every reading session end.
        }
    }
}
