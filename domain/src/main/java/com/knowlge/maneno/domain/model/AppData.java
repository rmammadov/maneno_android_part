package com.knowlge.maneno.domain.model;

/**
 * Created by Bogumił Sikora.
 */

public class AppData {
    public static final String KEY_CURRENT_USER_ID = "current_user_id";
    public static final String KEY_CURRENT_READING_SESSION_ID = "current_reading_session_id";

    private String key;
    private String value;

    public AppData() {}

    public AppData(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
