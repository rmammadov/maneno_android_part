package com.knowlge.maneno.domain.usecase;

import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.DailyReading;
import com.knowlge.maneno.domain.model.Level;
import com.knowlge.maneno.domain.model.User;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Bogumił Sikora.
 */

public class GetUserUseCase extends UseCase<GetUserUseCase.Params, User> {

    @Inject
    public GetUserUseCase() {}

    @Override
    protected Observable<User> createUseCaseObservable(Params params) {
        return ObservableCreator.createMultipleTimeObservable(params, true, new ObservableCreator.MultipleTime<Params, User>() {
            @Override
            public void execute(Params params, Subscriber<? super User> subscriber) throws Exception {
                // Get existing user from local repository.
                User user = localRepository.getCurrentUser();
                if (user != null) {
                    // todo - get level with daily readings.
                    user.setLevel(localRepository.getUserLevel(user.getId()));
                    subscriber.onNext(user);

                    if (Util.isOnline()) {
                        Level level = restApi.getUserLevel(user.getId());
                        if (level != null) {
                            level = setupDailyReadings(level);
                            user.setLevel(level);
                            subscriber.onNext(user);

                            localRepository.updateUserLevel(user.getId(), level);
                        }

                        // todo - 1. call web API to get most recent data.

                        // todo - before step 2 and 3, check if anything changed????

                        // todo - 2. return retrieved most recent data, so UI is refreshed.

                        // todo - 3. update data in local repository.
                    }
                }
            }
        });
    }

    public class Params {
        // todo - define params which user data needs to be retrived: level, stats, ability, etc...
    }

    private Level setupDailyReadings(Level level) {
        // Fill empty days in daily readings.
        Date now = new Date();
        Date lastUpgraded = parseDate(level.getLastUpgradeDate());

        if (lastUpgraded == null && (level.getDailyReadings() == null || level.getDailyReadings().size() == 0)) {
            List<DailyReading> dailyReadings = new ArrayList<>();
            dailyReadings.add(new DailyReading(now, 0));
            level.setDailyReadings(dailyReadings);
            return level;
        }

        if (lastUpgraded == null) { // Here level.getDailyReadings() is not empty for sure if lastUpgraded == null.
            lastUpgraded = parseDate(level.getDailyReadings().get(0).getDate());
        }

        if (lastUpgraded != null) {
            List<DailyReading> newDailyReadings = new ArrayList<>();

            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(lastUpgraded);

            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(now);

            // Create map of daily readings: dayCode -> daily reading
            Map<String, DailyReading> dailyReadingsMap = new HashMap<>();
            Calendar tmpCal = Calendar.getInstance();
            for (DailyReading dailyReading: level.getDailyReadings()) {
                Date date = parseDate(dailyReading.getDate());
                if (date != null) {
                    tmpCal.setTime(date);
                    dailyReadingsMap.put(getDayCode(tmpCal), dailyReading);
                }
            }

            DailyReading dr;
            while (!isSameDay(cal1, cal2)) {
                dr = dailyReadingsMap.get(getDayCode(cal1));
                if (dr == null) {
                    dr = new DailyReading(cal1.getTime(), 0);
                }

                newDailyReadings.add(dr);

                cal1.add(Calendar.DATE, 1);
            }

            dr = dailyReadingsMap.get(getDayCode(cal1));
            if (dr == null) {
                dr = new DailyReading(cal1.getTime(), 0);
            }

            newDailyReadings.add(dr);

            level.setDailyReadings(newDailyReadings);
        }

        return level;
    }

    private boolean isSameDay(Calendar cal1, Calendar cal2) {
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) && cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    private String getDayCode(Calendar calendar) {
        return String.valueOf(calendar.get(Calendar.DAY_OF_YEAR)) + "_" + calendar.get(Calendar.YEAR);
    }

    private Date parseDate(String date) {
        if (Util.isSet(date)) {
            try {
                return Util.parseDate(date, Consts.DATE_TIME_FORMAT_SHORT);
            } catch (ParseException e) {
                // Try another format.
                try {
                    return Util.parseDate(date, Consts.DATE_YEAR_MONTH_DAY_FORMAT);
                } catch (ParseException e1) {
                    // Do nothing.
                }
            }
        }

        return null;
    }
}
