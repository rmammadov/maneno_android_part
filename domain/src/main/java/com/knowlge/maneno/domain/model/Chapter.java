package com.knowlge.maneno.domain.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Bogumił Sikora.
 */

public class Chapter {

    private Integer bookId; // Used for local storage mapping.

    // Path to xhtml content of the chapter.
    // It is relative path under app files directory. App directory depends on context.
    // For internal storage it is under "context.getFilesDir()", for external storage it is under predefined path.
    // todo - change to URI?
    private String contentPath;

    private String title;
    private Integer index; // Used to order chapters in local storage. It is 0-based index.

    // Contains html of body from xhtml file under contentPath. For now do not saved in local storage. todo - maybe it should be saved?
    private String bodyHtml;
    private Map<String, String> images; // Map with relative image paths (from xhtml files) to absolute image paths.

    // Set during pages generation.
    private Integer offsetInBook;
    private Integer firstPageIndex;

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getBodyHtml() {
        return bodyHtml;
    }

    public void setBodyHtml(String bodyHtml) {
        this.bodyHtml = bodyHtml;
    }

    public Map<String, String> getImages() {
        return images;
    }

    public void setImages(Map<String, String> images) {
        this.images = images;
    }

    public void putImage(String relative, String absolute) {
        if (images == null) {
            images = new HashMap<>();
        }

        images.put(relative, absolute);
    }

    public String getImage(String relative) {
        if (images != null) {
            return images.get(relative);
        }

        return null;
    }

    public Integer getOffsetInBook() {
        return offsetInBook;
    }

    public void setOffsetInBook(Integer offsetInBook) {
        this.offsetInBook = offsetInBook;
    }

    public Integer getFirstPageIndex() {
        return firstPageIndex;
    }

    public void setFirstPageIndex(Integer firstPageIndex) {
        this.firstPageIndex = firstPageIndex;
    }
}
