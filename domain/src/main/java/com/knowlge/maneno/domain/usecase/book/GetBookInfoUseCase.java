package com.knowlge.maneno.domain.usecase.book;

import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.usecase.base.ObservableCreator;
import com.knowlge.maneno.domain.usecase.base.UseCase;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by Bogumił Sikora.
 */

public class GetBookInfoUseCase extends GetBooksDataUseCase<GetBookInfoUseCase.Params, BookInfo> {

    @Inject
    public GetBookInfoUseCase() {}

    @Override
    protected Observable<BookInfo> createUseCaseObservable(Params params) {
        return ObservableCreator.createOneTimeObservable(params, new ObservableCreator.OneTime<Params, BookInfo>() {
            @Override
            public BookInfo execute(Params params) throws Exception {
                // Always check local repository first, because it has the most accurate data.
                // Some of data is set only locally (like pagesRead, pagesTotal).
                BookInfo book = localRepository.getBookInfo(params.bookId, false); // todo - Move "withChapters" to use case Params?
                if (book == null) {
                    book = memoryCache.getBookInfo(params.bookId, false);
                }

                if (book == null) {
                    // todo - if there is not books locally, call restApi directly?
                }

                return book;
            }
        });
    }

    public static class Params {
        private final Integer bookId;

        public Params(Integer bookId) {
            this.bookId = bookId;
        }
    }
}
