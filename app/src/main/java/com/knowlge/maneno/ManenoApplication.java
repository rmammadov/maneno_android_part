package com.knowlge.maneno;

import android.app.Application;

import com.knowlge.maneno.di.components.ApplicationComponent;
import com.knowlge.maneno.di.components.DaggerApplicationComponent;
import com.knowlge.maneno.di.modules.ApplicationModule;
import com.knowlge.maneno.domain.repository.RestApiRequestProgressListener;

import java.util.HashMap;
import java.util.Map;


public class ManenoApplication extends Application {

    private ApplicationComponent applicationComponent;
    private Map<String, RestApiRequestProgressListener> requestProgressListeners = new HashMap<>();

    @Override
    public void onCreate() {
        super.onCreate();

        this.initApplicationComponent();
    }

    public ApplicationComponent getApplicationComponent() {
        return this.applicationComponent;
    }

    private void initApplicationComponent() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
    }

    public RestApiRequestProgressListener getRequestProgressListener(String progressId) {
        return requestProgressListeners.get(progressId);
    }

    public void setRequestProgressListener(String progressId, RestApiRequestProgressListener listener) {
        requestProgressListeners.put(progressId, listener);
    }
}
