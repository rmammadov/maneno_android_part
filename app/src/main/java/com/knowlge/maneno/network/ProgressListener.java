package com.knowlge.maneno.network;


public interface ProgressListener {
    void update(String progressId, long bytesRead, long contentLength, boolean done);
}
