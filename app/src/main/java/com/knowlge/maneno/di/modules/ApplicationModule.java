package com.knowlge.maneno.di.modules;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.knowlge.maneno.ManenoApplication;
import com.knowlge.maneno.backend.AuthenticationEndpoint;
import com.knowlge.maneno.backend.BooksEndpoint;
import com.knowlge.maneno.backend.DictionariesEndpoint;
import com.knowlge.maneno.backend.ManenoRestApi;
import com.knowlge.maneno.backend.UniloginAuthenticationEndpoint;
import com.knowlge.maneno.backend.UsersEndpoint;
import com.knowlge.maneno.data.AppFileRepository;
import com.knowlge.maneno.data.AppMemoryCache;
import com.knowlge.maneno.data.LocalDataRepository;
import com.knowlge.maneno.data.db.AppDBOpenHelper;
import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.executor.PostExecutionThread;
import com.knowlge.maneno.domain.log.Log;
import com.knowlge.maneno.domain.repository.FileRepository;
import com.knowlge.maneno.domain.repository.LocalRepository;
import com.knowlge.maneno.domain.repository.MemoryCache;
import com.knowlge.maneno.domain.repository.RestApi;
import com.knowlge.maneno.domain.repository.RestApiRequestProgressListener;
import com.knowlge.maneno.executor.AppExecutor;
import com.knowlge.maneno.executor.MainUIThread;
import com.knowlge.maneno.log.AppLog;
import com.knowlge.maneno.network.ProgressListener;
import com.knowlge.maneno.network.ProgressResponseBody;

import java.io.IOException;
import java.util.concurrent.Executor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


@Module
public class ApplicationModule {
    private static final String BASE_WEB_API_URL = "http://maneno-dev.azurewebsites.net/api/";
    //private static final String BASE_WEB_API_URL = "http://maneno-v3.azurewebsites.net/api/";
    private static final String UNILOGIN_AUTH_SERVER_URL = "https://auth.emu.dk/";

    private final ManenoApplication application;

    public ApplicationModule(ManenoApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return application;
    }

    @Provides
    @Singleton
    public ManenoApplication provideManenoApplication() {
        return application;
    }

    @Provides
    @Singleton
    public PostExecutionThread providePostExecutionThread(MainUIThread mainUIThread) {
        return mainUIThread;
    }

    @Provides @Singleton
    public Executor provideExecutor(AppExecutor appExecutor) {
        return appExecutor;
    }

    @Provides
    @Singleton
    public SQLiteDatabase provideSQLiteDatabase(AppDBOpenHelper appDbOpenHelper) {
        return appDbOpenHelper.getWritableDatabase();
    }

    @Provides
    @Singleton
    public LocalRepository provideRepository(LocalDataRepository localDataRepository) {
        return localDataRepository;
    }

    @Provides
    @Singleton
    public MemoryCache provideMemoryCache(AppMemoryCache appMemoryCache) {
        return appMemoryCache;
    }

    @Provides
    @Singleton
    public FileRepository provideFileRepository(AppFileRepository appFileRepository) {
        return appFileRepository;
    }

    @Provides
    @Singleton
    public Log provideLog(AppLog log) {
        return log;
    }

    @Provides @Singleton
    AuthenticationEndpoint provideAuthenticationEndpoint(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_WEB_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(AuthenticationEndpoint.class);
    }

    @Provides @Singleton
    UniloginAuthenticationEndpoint provideUniloginAuthenticationEndpoint(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UNILOGIN_AUTH_SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(UniloginAuthenticationEndpoint.class);
    }

    @Provides @Singleton
    BooksEndpoint provideBooksEndpoint(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_WEB_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(BooksEndpoint.class);
    }

    @Provides @Singleton
    DictionariesEndpoint provideDictionariesEndpoint(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_WEB_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(DictionariesEndpoint.class);
    }

    @Provides @Singleton
    UsersEndpoint provideUsersEndpoint(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_WEB_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(UsersEndpoint.class);
    }

    @Provides @Singleton
    ProgressListener provideProgressListener() {
        return new ProgressListener() {
            Long prevBytes = 0L;
            @Override
            public void update(String progressId, long bytesRead, long contentLength, boolean done) {
                if (prevBytes + contentLength/100L < bytesRead) {
                    prevBytes = bytesRead;

                    // Get any per request progress listener.
                    RestApiRequestProgressListener listener = application.getRequestProgressListener(progressId);
                    if (listener != null) {
                        if (!done) {
                            listener.onProgress((int) (bytesRead*100L/contentLength), progressId);
                        } else {
                            listener.onProgress(100, progressId);
                        }
                    }
                }
                if (done) {
                    prevBytes = 0L;
                }
            }
        };
    }

    @Provides @Singleton
    OkHttpClient provideOkHttpClient(final ProgressListener progressListener) {
        return new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        String progressId = chain.request().header(Consts.PROGRESS_ID_HEADER);
                        if (Util.isSet(progressId)) {
                            Request request = chain.request().newBuilder().removeHeader(Consts.PROGRESS_ID_HEADER).build();

                            Response originalResponse = chain.proceed(request);
                            return originalResponse.newBuilder()
                                    .body(new ProgressResponseBody(originalResponse.body(), progressId, progressListener))
                                    .build();
                        } else {
                            return chain.proceed(chain.request());
                        }
                    }
                })
                .build();
    }

    @Provides @Singleton
    RestApi provideRestApi(ManenoApplication application, LocalRepository localRepository, AuthenticationEndpoint authenticationEndpoint,
                           UniloginAuthenticationEndpoint uniloginAuthenticationEndpoint, BooksEndpoint booksEndpoint,
                           DictionariesEndpoint dictionariesEndpoint, UsersEndpoint usersEndpoint) {
        return new ManenoRestApi(application, localRepository.getAuthenticationData(), authenticationEndpoint, uniloginAuthenticationEndpoint,
                booksEndpoint, dictionariesEndpoint, usersEndpoint);
    }
}
