package com.knowlge.maneno.di.components;

import com.knowlge.maneno.ManenoApplication;
import com.knowlge.maneno.di.modules.ApplicationModule;
import com.knowlge.maneno.ui.achievements.di.AchievementsComponent;
import com.knowlge.maneno.ui.achievements.di.AchievementsModule;
import com.knowlge.maneno.ui.bookdetails.di.BookDetailsComponent;
import com.knowlge.maneno.ui.bookdetails.di.BookDetailsModule;
import com.knowlge.maneno.ui.introduction.di.IntroductionComponent;
import com.knowlge.maneno.ui.introduction.di.IntroductionModule;
import com.knowlge.maneno.ui.library.di.LibraryComponent;
import com.knowlge.maneno.ui.library.di.LibraryModule;
import com.knowlge.maneno.ui.login.di.LoginComponent;
import com.knowlge.maneno.ui.login.di.LoginModule;
import com.knowlge.maneno.ui.main.di.MainComponent;
import com.knowlge.maneno.ui.main.di.MainModule;
import com.knowlge.maneno.ui.reading.di.ReadingComponent;
import com.knowlge.maneno.ui.reading.di.ReadingModule;
import com.knowlge.maneno.ui.start.di.StartComponent;
import com.knowlge.maneno.ui.start.di.StartModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(ManenoApplication manenoApplication);

    LoginComponent createLoginComponent(LoginModule loginModule);
    StartComponent createStartComponent(StartModule startModule);
    AchievementsComponent createAchievementsComponent(AchievementsModule achievementsModule);
    IntroductionComponent createIntroductionComponent(IntroductionModule introductionModule);
    LibraryComponent createLibraryComponent(LibraryModule libraryModule);
    MainComponent createMainComponent(MainModule mainModule);
    BookDetailsComponent createBookDetailsComponent(BookDetailsModule bookDetailsModule);
    ReadingComponent createReadingComponent(ReadingModule readingModule);

}
