package com.knowlge.maneno.ui.common.subscriber;

import com.knowlge.maneno.domain.log.Log;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.ui.base.BaseSubscriber;


public class BookInfoResultSubscriber extends BaseSubscriber<BookInfo> {

    public interface BookReceivedListener {
        void onBookReceived(BookInfo book);
    }

    private final BookReceivedListener bookReceivedListener;


    public BookInfoResultSubscriber(BookReceivedListener bookReceivedListener, Log log) {
        super(log);
        this.bookReceivedListener = bookReceivedListener;
    }

    @Override
    public void onNext(BookInfo book) {
        super.onNext(book);

        if (book != null) {
            bookReceivedListener.onBookReceived(book);
        }
    }
}
