package com.knowlge.maneno.ui.reading.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.reading.ReadingActivity;
import com.knowlge.maneno.ui.reading.ReadingView;

import dagger.Module;
import dagger.Provides;


@Module
public class ReadingModule {
    private final ReadingActivity readingActivity;

    public ReadingModule(ReadingActivity readingActivity) {
        this.readingActivity = readingActivity;
    }

    @Provides
    @ActivityScope
    ReadingView provideReadingView() {
        return this.readingActivity;
    }
}
