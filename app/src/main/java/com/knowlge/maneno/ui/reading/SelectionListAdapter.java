package com.knowlge.maneno.ui.reading;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Util;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;



public class SelectionListAdapter extends RecyclerView.Adapter<SelectionListAdapter.ViewHolder> {

    public interface OnItemClickedListener {
        void onItemClicked(int position, ElementData element);
    }

    private Context context;
    private List<ElementData> elements;
    private boolean multipleSelection;
    private final LayoutInflater layoutInflater;

    private int selectedPosition;

    private OnItemClickedListener onItemClickedListener;

    public SelectionListAdapter(Context context, List<ElementData> elements, boolean multipleSelection) {
        this.context = context;
        this.elements = elements;
        this.multipleSelection = multipleSelection;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public SelectionListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_selection_element, parent, false);
        return new SelectionListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SelectionListAdapter.ViewHolder holder, int position) {
        holder.setElement(context, elements.get(position), onItemClickedListener, multipleSelection);
    }

    @Override
    public int getItemCount() {
        if (elements != null) {
            return elements.size();
        }

        return 0;
    }

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public void toggleSelection(int position) {
        if (position >= 0 && position < elements.size()) {
            elements.get(position).isSelected = !elements.get(position).isSelected;
            notifyItemChanged(position);
        }
    }

    public void setSelected(int newSelectedPosition) {
        if (multipleSelection) {
            if (newSelectedPosition >= 0 && newSelectedPosition < elements.size()) {
                elements.get(newSelectedPosition).isSelected = true;
                notifyItemChanged(newSelectedPosition);
            }
        } else if (newSelectedPosition >= 0 && newSelectedPosition != selectedPosition
                    && newSelectedPosition < elements.size()) {
            int oldSelectedPosition = selectedPosition;
            selectedPosition = newSelectedPosition;

            elements.get(oldSelectedPosition).isSelected = false;
            notifyItemChanged(oldSelectedPosition);

            elements.get(selectedPosition).isSelected = true;
            notifyItemChanged(selectedPosition);
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.nameTextView) TextView nameTextView;
        @BindView(R.id.iconImageView) ImageView iconImageView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setElement(Context context, final ElementData element, final OnItemClickedListener onItemClickedListener,
                               boolean multipleSelection) {
            final int position = getAdapterPosition();

            if (Util.isSet(element.imageUrl)) {
                iconImageView.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(element.imageUrl)
                        .into(iconImageView);
            } else if (element.imageResId != null) {
                iconImageView.setVisibility(View.VISIBLE);
                iconImageView.setImageResource(element.imageResId);
            } else {
                iconImageView.setVisibility(View.INVISIBLE);
            }

            if (element.isSelected) {
                nameTextView.setTextColor(ContextCompat.getColor(context, R.color.blueFontColor));
            } else {
                nameTextView.setTextColor(ContextCompat.getColor(context, R.color.defaultFontColor));
            }
            nameTextView.setText(element.name);

            if (multipleSelection && element.isSelected) {
                // todo - show tick icon.
            } else {
                // todo - hide tick icon.
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickedListener != null && position != RecyclerView.NO_POSITION) {
                        onItemClickedListener.onItemClicked(position, element);
                    }
                }
            });
        }
    }

    public static class ElementData {
        private Integer id;
        private String imageUrl;
        private Integer imageResId;
        private String name;
        private boolean isSelected = false;

        public ElementData(String name) {
            this.name = name;
        }

        public ElementData(Integer id, String imageUrl, String name) {
            this.id = id;
            this.imageUrl = imageUrl;
            this.name = name;
        }

        public ElementData(Integer id, String name) {
            this.id = id;
            this.name = name;
        }

        public void setImageResId(Integer imageResId) {
            this.imageResId = imageResId;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public Integer getId() {
            return id;
        }
    }
}
