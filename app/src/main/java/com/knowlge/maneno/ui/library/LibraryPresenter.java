package com.knowlge.maneno.ui.library;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.usecase.book.GetBooksUseCase;
import com.knowlge.maneno.domain.usecase.library.GetLibraryFiltersUseCase;
import com.knowlge.maneno.ui.base.BasePresenter;
import com.knowlge.maneno.ui.base.BaseSubscriber;
import com.knowlge.maneno.ui.library.subscriber.GetBooksResultSubscriber;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import static com.knowlge.maneno.ui.library.LibraryView.FILTER_CATEGORIES;
import static com.knowlge.maneno.ui.library.LibraryView.FILTER_COLORS;
import static com.knowlge.maneno.ui.library.LibraryView.FILTER_DIFFICULTIES;
import static com.knowlge.maneno.ui.library.LibraryView.FILTER_LANGUAGES;
import static com.knowlge.maneno.ui.library.LibraryView.FILTER_LENGTHS;
import static com.knowlge.maneno.ui.library.LibraryView.FULL_VIEW_TYPE;
import static com.knowlge.maneno.ui.library.LibraryView.GRID_VIEW_TYPE;


@ActivityScope
public class LibraryPresenter extends BasePresenter<LibraryView> {

    @Inject GetBooksUseCase getBooksUseCase;

    @Inject GetLibraryFiltersUseCase getLibraryFiltersUseCase;

    private ArrayList<Integer> manenosChoiceBookIdsList;
    private ArrayList<Integer> resultBookIdsList;

    private boolean isLoading = false;
    private int totalRows = 0;

    private int nextOffset = 0;
    private Set<Integer> colorCodes = new HashSet<>();
    private Set<Integer> tags = new HashSet<>();
    private Set<Integer> languages = new HashSet<>();

    private boolean filterChanged = false;

    private int currentViewType = GRID_VIEW_TYPE;

    @Inject
    public LibraryPresenter(LibraryView view) {
        super(view);
    }

    public void open() {
        getView().setViewType(GRID_VIEW_TYPE);

        getLibraryFiltersUseCase.execute(new BaseSubscriber<GetLibraryFiltersUseCase.Result>(getLog()) {
            @Override
            public void onNext(GetLibraryFiltersUseCase.Result filters) {
                super.onNext(filters);

                getView().setFiltersData(filters.getColorCodes(), filters.getCategories(),
                        filters.getLanguages(), filters.getLengths(), filters.getDifficulties());

                // todo - preselect user language.
                getBooks();
            }
        });
    }

    public void onBookSelected(int position) {
        if (currentViewType == GRID_VIEW_TYPE) {
            getView().setBookSelected(position);

            if (manenosChoiceBookIdsList != null &&
                    position <= manenosChoiceBookIdsList.size()) {
                getView().openBookDetails(manenosChoiceBookIdsList, position - 1); // Remove manenos choice header from position.
            } else {
                int prevElements = 2; // Two section headers.
                if (manenosChoiceBookIdsList != null) {
                    prevElements += manenosChoiceBookIdsList.size();
                }
                getView().openBookDetails(resultBookIdsList, position - prevElements);
            }
        } else {
            int visibleThreshold = 4;
            int itemsCount = 0;
            if (manenosChoiceBookIdsList != null) {
                itemsCount += manenosChoiceBookIdsList.size();
            }
            if (resultBookIdsList != null) {
                itemsCount += resultBookIdsList.size();
            }

            if (!isLoading && itemsCount <= (position + visibleThreshold)) {
                if (nextOffset < totalRows) {
                    getBooks();
                }
            }
        }
    }

    public void onBooksResultListScrolled(int itemCount, int lastVisibleItemPosition) {
        int visibleThreshold = 3;

        if (!isLoading && itemCount <= (lastVisibleItemPosition + visibleThreshold)) {
            if (nextOffset < totalRows) {
                getBooks();
            }
        }
    }

    public void onBooksReceived(List<BookInfo> manenosChoiceBooks, List<BookInfo> books, Integer totalRows) {
        isLoading = false;
        getView().hideActivityIndicator();

        if (books != null && books.size() > 0 && totalRows != null) {
            boolean isLoadingNextElements = resultBookIdsList != null;

            if (!isLoadingNextElements) {
                manenosChoiceBookIdsList = new ArrayList<>();
                resultBookIdsList = new ArrayList<>();
            }

            for (BookInfo bookInfo: books) {
                resultBookIdsList.add(bookInfo.getId());
            }

            if (manenosChoiceBooks != null) {
                for (BookInfo bookInfo: manenosChoiceBooks) {
                    manenosChoiceBookIdsList.add(bookInfo.getId());
                }
                nextOffset += manenosChoiceBooks.size();
            }

            this.totalRows = totalRows;
            nextOffset += books.size();

            if (isLoadingNextElements) {
                // If manenos choice books was received in next (not first) chunk, add them normally to the end.
                if (manenosChoiceBooks != null && manenosChoiceBooks.size() > 0) {
                    books.addAll(manenosChoiceBooks);
                }
                getView().insertNextBooksToList(books, currentViewType);
            } else {
                getView().setResultBooksList(manenosChoiceBooks, books, currentViewType);
            }
        }
    }

    public void onFullViewSelected() {
        if (currentViewType != FULL_VIEW_TYPE) {
            currentViewType = FULL_VIEW_TYPE;
            getView().setViewType(FULL_VIEW_TYPE);

            reloadBooksAfterViewTypeChange();
        }
    }

    public void onGridViewSelected() {
        if (currentViewType != GRID_VIEW_TYPE) {
            currentViewType = GRID_VIEW_TYPE;
            getView().setViewType(GRID_VIEW_TYPE);

            reloadBooksAfterViewTypeChange();
        }
    }

    private void reloadBooksAfterViewTypeChange() {
        manenosChoiceBookIdsList = null;
        resultBookIdsList = null;

        nextOffset = 0;
        totalRows = 0;

        getBooks();
    }

    private void getBooks() {
        isLoading = true;

        getView().showLoadingNextBooks();

        getBooksUseCase.execute(new GetBooksResultSubscriber(getLog(), this),
                new GetBooksUseCase.Params(colorCodes, tags, languages, nextOffset));
    }

    public void onFilterButtonClicked(int filterId) {
        getView().openFilterSelectionList(filterId);
    }

    public void onHideFiltersSelectionListButtonClicked() {
        if (filterChanged) {
            reloadBooksAfterFilterSelection();
            filterChanged = false;
        }

        getView().hideFilterSelectionList();
    }

    public void onToggleBottomNavigationButtonClicked() {
        getView().toggleBottomNavigation();
    }

    private void reloadBooksAfterFilterSelection() {
        getView().clearResultBooksList();
        getView().showActivityIndicator();

        manenosChoiceBookIdsList = null;
        resultBookIdsList = null;

        nextOffset = 0;
        totalRows = 0;

        getBooks();
    }

    public void onFilterItemClicked(int filterId, int position, boolean isItemSelected, Integer itemId) {
        filterChanged = true;

        getView().toggleSelection(filterId, position);

        Set<Integer> filterSet = null;

        if (FILTER_COLORS == filterId) {
            filterSet = colorCodes;
        } else if (FILTER_LANGUAGES == filterId) {
            filterSet = languages;
        } else if (FILTER_CATEGORIES == filterId || FILTER_LENGTHS == filterId || FILTER_DIFFICULTIES == filterId) {
            filterSet = tags;
        }

        if (filterSet != null) {
            if (isItemSelected) {
                filterSet.remove(itemId);
            } else {
                filterSet.add(itemId);
            }
        }
    }

    public void onBottomNavigationItemClicked(int position, boolean isItemSelected, int itemKey) {
        getView().setBottomNavigationSelection(position, itemKey);
    }
}
