package com.knowlge.maneno.ui.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.knowlge.maneno.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MNNDialogFragment extends DialogFragment {

    private static final String PARAM_TITLE_RES_ID = "PARAM_TITLE_RES_ID";
    private static final String PARAM_TEXT_RES_ID = "PARAM_TEXT_RES_ID";

    private static final String PARAM_BUTTON_1_TEXT_RES_ID = "PARAM_BUTTON_1_TEXT_RES_ID";
    private static final String PARAM_BUTTON_2_TEXT_RES_ID = "PARAM_BUTTON_2_TEXT_RES_ID";

    private static final String PARAM_BUTTON_1_TYPE_ID = "PARAM_BUTTON_1_TYPE_ID";
    private static final String PARAM_BUTTON_2_TYPE_ID = "PARAM_BUTTON_2_TYPE_ID";

    private int titleResId;
    private int textResId;
    private int button1TextResId;
    private int button2TextResId;
    private String button1TypeId;
    private String button2TypeId;

    private View.OnClickListener button1OnClickListener;
    private View.OnClickListener button2OnClickListener;

    @BindView(R.id.title) TextView title;
    @BindView(R.id.text) TextView text;
    @BindView(R.id.button1) MNNButton button1;
    @BindView(R.id.button2) MNNButton button2;

    public static MNNDialogFragment newInstance(int titleResId, int textResId, int button1TextResId) {
        return newInstance(titleResId, textResId, button1TextResId, 0);
    }

    public static MNNDialogFragment newInstance(int titleResId, int textResId, int button1TextResId, int button2TextResId) {
        return newInstance(titleResId, textResId, button1TextResId, button2TextResId, MNNButton.FILLED_WITH_GREEN, MNNButton.GREEN_BORDER);
    }

    public static MNNDialogFragment newInstance(int titleResId, int textResId, int button1TextResId, int button2TextResId,
                                                String button1TypeId, String button2TypeId) {
        MNNDialogFragment mnnDialogFragment = new MNNDialogFragment();

        Bundle args = new Bundle();
        args.putInt(PARAM_TITLE_RES_ID, titleResId);
        args.putInt(PARAM_TEXT_RES_ID, textResId);
        args.putInt(PARAM_BUTTON_1_TEXT_RES_ID, button1TextResId);
        args.putInt(PARAM_BUTTON_2_TEXT_RES_ID, button2TextResId);
        args.putString(PARAM_BUTTON_1_TYPE_ID, button1TypeId);
        args.putString(PARAM_BUTTON_2_TYPE_ID, button2TypeId);

        mnnDialogFragment.setArguments(args);

        return mnnDialogFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);

        titleResId = getArguments().getInt(PARAM_TITLE_RES_ID);
        textResId = getArguments().getInt(PARAM_TEXT_RES_ID);
        button1TextResId = getArguments().getInt(PARAM_BUTTON_1_TEXT_RES_ID);
        button2TextResId = getArguments().getInt(PARAM_BUTTON_2_TEXT_RES_ID);
        button1TypeId = getArguments().getString(PARAM_BUTTON_1_TYPE_ID);
        button2TypeId = getArguments().getString(PARAM_BUTTON_2_TYPE_ID);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mnn_dialog_fragment, container, false);

        ButterKnife.bind(this, view);
        setupUI();

        return view;
    }

    public void setButton1OnClickListener(View.OnClickListener button1OnClickListener) {
        this.button1OnClickListener = button1OnClickListener;
    }

    public void setButton2OnClickListener(View.OnClickListener button2OnClickListener) {
        this.button2OnClickListener = button2OnClickListener;
    }

    // todo - implement onBack click handling???

    private void setupUI() {
        title.setText(titleResId);
        text.setText(textResId);

        setupButton(button1, button1TextResId, button1TypeId, button1OnClickListener);
        setupButton(button2, button2TextResId, button2TypeId, button2OnClickListener);
    }

    private void setupButton(MNNButton button, int textResId, String typeId, View.OnClickListener onClickListener) {
        if (textResId > 0) {
            button.setText(textResId);
            button.setType(getContext(), typeId);
            button.setVisibility(View.VISIBLE);
            if (onClickListener != null) {
                button.setOnClickListener(onClickListener);
            }
        } else {
            button.setVisibility(View.GONE);
        }
    }
}
