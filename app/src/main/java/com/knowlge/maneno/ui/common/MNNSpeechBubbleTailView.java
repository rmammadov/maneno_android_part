package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Util;


public class MNNSpeechBubbleTailView extends View {

    private static final int BOTTOM_LEFT = 1;
    private static final int BOTTOM_RIGHT = 2;
    private static final int LEFT_UP = 3;
    private static final int LEFT_DOWN = 4;
    private static final int RIGHT_UP = 5;
    private static final int RIGHT_DOWN = 6;
    private static final int TOP_LEFT = 7;
    private static final int TOP_RIGHT = 8;
    
    Paint paint;
    Path path;

    private int kind = BOTTOM_LEFT;
    private int color = Color.parseColor("#ffffff"); // White color as default.

    public MNNSpeechBubbleTailView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MNNSpeechBubbleTailView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.MNNSpeechBubbleTailView);
        String kindAttr = typedArray.getString(R.styleable.MNNSpeechBubbleTailView_kind);
        typedArray.recycle();

        if (Util.isSet(kindAttr)) {
            kind = Integer.parseInt(kindAttr);
        }

        // todo - add color attribute.


        path = new Path();
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setStrokeWidth(1);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // todo - handle other kinds.

        switch (kind) {
            case LEFT_UP: path.moveTo(canvas.getWidth(), 0);
                path.quadTo(-0.5f * canvas.getWidth(), 0.1f * canvas.getHeight(), canvas.getWidth(), canvas.getHeight());
                path.close();
                canvas.drawPath(path, paint);
                break;
        }
    }

}