package com.knowlge.maneno.ui.reading;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ClipDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.common.base.CharMatcher;
import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.domain.model.Chapter;
import com.knowlge.maneno.ui.base.BaseActivity;
import com.knowlge.maneno.ui.common.MNNSelectionList;
import com.knowlge.maneno.ui.reading.di.ReadingModule;
import com.knowlge.maneno.ui.reading.model.PageData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.google.common.base.CharMatcher.whitespace;



public class ReadingActivity extends BaseActivity implements ReadingView {
    private static final String PARAM_BOOK_ID = "PARAM_BOOK_ID";

    @Inject
    ReadingPresenter readingPresenter;

    @BindView(R.id.downloadProgressLayout)
    ViewGroup downloadProgressLayout;

    @BindView(R.id.bookFullClipImageView)
    ImageView bookFullClipImageView;

    @BindView(R.id.readingLayout)
    ViewGroup readingLayout;

    @BindView(R.id.backButton)
    ImageView backButton;

    @BindView(R.id.lockRotationButton)
    ImageView lockRotationButton;

    @BindView(R.id.tipButton)
    ImageView tipButton;

    @BindView(R.id.showChaptersMenuImageView)
    ImageView showChaptersMenuImageView;

    @BindView(R.id.titleTextView)
    TextView titleTextView;

    @BindView(R.id.pagesViewPager)
    ViewPager pagesViewPager;

    @BindView(R.id.fullPageContentTextView)
    TextView fullPageContentTextView;

    @BindView(R.id.withImagePageContentTextView)
    TextView withImagePageContentTextView;

    @BindView(R.id.chapterTitleTextView)
    TextView chapterTitleTextView;

    @BindView(R.id.pageNumberTextView)
    TextView pageNumberTextView;

    @BindView(R.id.prevPageImageView)
    ImageView prevPageImageView;

    @BindView(R.id.nextPageImageView)
    ImageView nextPageImageView;

    @BindView(R.id.titleBar)
    ViewGroup titleBar;

    @BindView(R.id.chaptersSelectionList)
    MNNSelectionList chaptersSelectionList;

    @BindView(R.id.chapterTitleLayout)
    ViewGroup chapterTitleLayout;

    private ClipDrawable bookFullClipDrawable;

    private Integer bookId;

    private ReadingSessionSummaryDialogFragment readingSessionSummaryDialogFragment;

    private SelectionListAdapter chaptersMenuAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bookId = getIntent().getIntExtra(PARAM_BOOK_ID, 0);

        setContentView(R.layout.activity_reading);
        ButterKnife.bind(this);
        setupUI();
    }

    @Override
    protected void onStart() {
        super.onStart();

        readingPresenter.open(bookId);
    }

    @Override
    protected void onStop() {
        super.onStop();

        readingPresenter.onStopReading();
    }

    private void setupUI() {
        chaptersSelectionList.setVisibility(View.INVISIBLE);
        titleBar.bringToFront();

        if (bookFullClipImageView != null) {
            bookFullClipDrawable = (ClipDrawable) bookFullClipImageView.getDrawable();
            bookFullClipDrawable.setLevel(0); // Set initial download progress to 0.
        }

        downloadProgressLayout.setVisibility(View.VISIBLE);
        readingLayout.setVisibility(View.INVISIBLE);

        backButton.setClickable(true);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readingPresenter.onBackButtonClicked();
            }
        });

        prevPageImageView.setClickable(true);
        prevPageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readingPresenter.onPrevPageButtonClicked();
            }
        });

        nextPageImageView.setClickable(true);
        nextPageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readingPresenter.onNextPageButtonClicked();
            }
        });
        showChaptersMenuImageView.setClickable(true);
        showChaptersMenuImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readingPresenter.onShowChaptersButtonClicked();
            }
        });

        chaptersSelectionList.setOnHideClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                readingPresenter.onHideChaptersButtonClicked();
            }
        });

        pagesViewPager.addOnPageChangeListener(onPageChangeListener);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            readingPresenter.onBookPageSelected(position);
        }
    };

    @Override
    protected void setupActivityComponent() {
        getApplicationComponent()
                .createReadingComponent(new ReadingModule(this))
                .inject(this);
    }

    public static Intent getCallingIntent(Context context, Integer bookId) {
        Intent intent = new Intent(context, ReadingActivity.class);

        intent.putExtra(PARAM_BOOK_ID, bookId);

        return intent;
    }

    @Override
    public void setTitle(String title) {
        titleTextView.setText(title);
    }

    @Override
    public List<PageData> loadPages(List<Chapter> chapters) {
        // todo - set proper font size, alignment and line leading for measure text views.
        int pageTextOffset;
        int currentImageIdx;
        int chapterTextOffset;
        int offsetInBook = 0;

        // Split chapters into pages.
        List<PageData> pages = new ArrayList<>();
        if (chapters != null) {
            for (Chapter ch : chapters) {
                ch.setOffsetInBook(offsetInBook);
                ch.setFirstPageIndex(null); // Clear, so we can properly set new one.

                Spanned spannedChapterContent = Html.fromHtml(ch.getBodyHtml());
                SpannableStringBuilder chapterSsb = new SpannableStringBuilder(spannedChapterContent);
                chapterSsb = trimSpannable(chapterSsb);
                ImageSpan[] imageSpans = chapterSsb.getSpans(0, chapterSsb.length(), ImageSpan.class);

                List<Integer> imagesOffset = new ArrayList<>();
                List<ImageSpan> images = new ArrayList<>();

                // Built arrays with images spans and offsets. Remove images spans from content.
                for (ImageSpan is : imageSpans) {
                    int start = chapterSsb.getSpanStart(is);
                    chapterSsb.delete(start, chapterSsb.getSpanEnd(is));
                    imagesOffset.add(start);
                    images.add(is);
                }

                String chapterText = chapterSsb.toString();

                String tempChapterText = whitespace().trimLeadingFrom(chapterText);
                int trimStart = chapterText.length() - tempChapterText.length();

                chapterText = whitespace().trimTrailingFrom(tempChapterText);
                int trimEnd = tempChapterText.length() - chapterText.length();

                // Reindex images offsets. Only in situation if after first image there were whitespace at the chapter beginning.
                if (trimStart > 0 && imagesOffset.size() > 0) {
                    List<Integer> newImagesOffset = new ArrayList<>();
                    for (Integer offset : imagesOffset) {
                        newImagesOffset.add(offset - trimStart);
                    }

                    imagesOffset = newImagesOffset;
                }

                // Reindex images offsets. Only in situation if directly before last images there were whitespaces at the chapter end.
                if (trimEnd > 0 && imagesOffset.size() > 0) {
                    List<Integer> newImagesOffset = new ArrayList<>();
                    for (Integer offset : imagesOffset) {
                        if (offset > chapterText.length()) {
                            newImagesOffset.add(offset - trimEnd);
                        } else {
                            newImagesOffset.add(offset);
                        }
                    }

                    imagesOffset = newImagesOffset;
                }

                currentImageIdx = 0;
                chapterTextOffset = 0;

                while (chapterText.length() > 0) {
                    pageTextOffset = getPageTextOffset(false, chapterText);

                    PageData pageData = new PageData();

                    // Check if we still have possible images to place and if it should be placed in current page.
                    if (currentImageIdx < imagesOffset.size()
                            && imagesOffset.get(currentImageIdx) < chapterTextOffset + pageTextOffset) {

                        pageData.setImagePath(ch.getImage(images.get(currentImageIdx).getSource()));
                        currentImageIdx++;

                        // Remeasure pageTextOffset for page with image.
                        pageTextOffset = getPageTextOffset(true, chapterText);
                    }

                    pageData.setChapterIndex(ch.getIndex());
                    // todo change text to spanned?
                    pageData.setText(chapterText.substring(0, pageTextOffset).trim());
                    pageData.setOffsetInBook(offsetInBook);

                    if (ch.getFirstPageIndex() == null) {
                        ch.setFirstPageIndex(pages.size());
                    }
                    pages.add(pageData);

                    chapterTextOffset += pageTextOffset;
                    offsetInBook += pageTextOffset;
                    tempChapterText = chapterText.substring(pageTextOffset);

                    // Remove leading whitespaces and increase offset.
                    chapterText = CharMatcher.whitespace().trimLeadingFrom(tempChapterText);
                    chapterTextOffset += tempChapterText.length() - chapterText.length();
                }

                // If we still have images, add page with image only.
                if (currentImageIdx < imagesOffset.size()) {
                    PageData pageData = new PageData();
                    pageData.setImagePath(ch.getImage(images.get(currentImageIdx).getSource()));
                    pageData.setOffsetInBook(offsetInBook);

                    pageData.setChapterIndex(ch.getIndex());

                    if (ch.getFirstPageIndex() == null) {
                        ch.setFirstPageIndex(pages.size());
                    }
                    pages.add(pageData);
                }
            }

            List<SelectionListAdapter.ElementData> chaptersList = new ArrayList<>();

            for (Chapter chapter: chapters) {
                if (Util.isSet(chapter.getTitle())) {
                    chaptersList.add(new SelectionListAdapter.ElementData(chapter.getTitle()));
                } else {
                    chaptersList.add(new SelectionListAdapter.ElementData(String.valueOf(chapter.getIndex() + 1)));
                }
            }

            chaptersMenuAdapter = new SelectionListAdapter(this, chaptersList, false);
            chaptersMenuAdapter.setOnItemClickedListener(new SelectionListAdapter.OnItemClickedListener() {
                @Override
                public void onItemClicked(int position, SelectionListAdapter.ElementData element) {
                    readingPresenter.onChaptersMenuItemClicked(position);
                }
            });
            chaptersSelectionList.setAdapter(chaptersMenuAdapter);
        }

        pagesViewPager.setAdapter(new ReadingPageAdapter(getSupportFragmentManager(), pages));

        return pages;
    }

    private int getPageTextOffset(boolean withImage, String chapterText) {
        TextView measureTextView = fullPageContentTextView;
        if (withImage) {
            measureTextView = withImagePageContentTextView;
        }

        int maxLines = (measureTextView.getHeight()/measureTextView.getLineHeight()) - 1;

        measureTextView.setText(chapterText); // todo measureText for maxWidth = maxLines*measureTextView.getWidth() and get subString from chapterText.
        int lastPageLineIdx = Math.min(measureTextView.getLineCount(), maxLines);
        return measureTextView.getLayout().getLineVisibleEnd(lastPageLineIdx - 1);
    }

    private SpannableStringBuilder trimSpannable(SpannableStringBuilder spannable) {
        String text = spannable.toString();

        String newText = whitespace().trimLeadingFrom(text);
        int trimStart = text.length() - newText.length();

        newText = whitespace().trimTrailingFrom(newText);
        int trimEnd = text.length() - newText.length() - trimStart;

        return spannable.delete(0, trimStart).delete(spannable.length() - trimEnd, spannable.length());
    }

    @Override
    public void showPage(Integer num) {
        if (num != null) {
            pagesViewPager.setCurrentItem(num);
        }
    }

    @Override
    public void setChapterTitle(String chapterTitle) {
        chapterTitleTextView.setText(chapterTitle);
    }

    @Override
    public void setPageNum(int currentPageNum, int pagesCount) {
        pageNumberTextView.setText(getString(R.string.page_number, currentPageNum, pagesCount));
    }

    @Override
    public void openReadingSessionSummaryView(String userName, Integer minutes, Integer pages, Integer words) {
        readingSessionSummaryDialogFragment = ReadingSessionSummaryDialogFragment.newInstance(userName, minutes, pages, words);

        readingSessionSummaryDialogFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void closeReadingSessionSummaryView() {
        if (readingSessionSummaryDialogFragment != null) {
            readingSessionSummaryDialogFragment.dismiss();
            readingSessionSummaryDialogFragment = null;
        }
    }

    @Override
    public void updateReadingSessionSummaryView(Integer statPoints, Integer equipPoints, List<Achievement> achievements, Boolean levelCompleted) {
        if (readingSessionSummaryDialogFragment != null) {
            readingSessionSummaryDialogFragment.setEarnedPoints(statPoints, equipPoints);

            // todo - update summary view - achievements get.
        }
    }

    @Override
    public void updateBookDownloadProgress(int progressPercent) {
        if (bookFullClipDrawable != null) {
            bookFullClipDrawable.setLevel(100 * progressPercent); // For clip drawable level is from 0 to 10000;
        }
    }

    @Override
    public void showBookContent() {
        downloadProgressLayout.setVisibility(View.INVISIBLE);
        readingLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideChaptersMenu() {
        int startTop = titleBar.getBottom();
        int newTop = startTop - chaptersSelectionList.getHeight();

        chaptersSelectionList.moveUpHide(startTop, newTop);
    }

    @Override
    public void setChapterSelected(int position) {
        chaptersMenuAdapter.setSelected(position);
    }

    @Override
    public void showChaptersMenu() {
        int newTop = titleBar.getBottom();
        int startTop = newTop - chaptersSelectionList.getHeight();

        chaptersSelectionList.moveDownShow(startTop, newTop);
    }

    public void onEndReadingButtonClicked() {
        readingPresenter.onEndReadingButtonClicked();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            readingPresenter.onBackButtonClicked(); // Device back button works in the same way as top left back button.
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onReadingSessionSummaryDismissed() {
        readingPresenter.onReadingSessionSummaryDismissed(bookId);
    }
}
