package com.knowlge.maneno.ui.bookdetails.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.bookdetails.BookDetailsFragment;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = BookDetailsModule.class)
public interface BookDetailsComponent {
    void inject(BookDetailsFragment bookDetailsFragment);
}
