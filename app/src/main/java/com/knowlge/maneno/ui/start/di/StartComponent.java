package com.knowlge.maneno.ui.start.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.start.StartActivity;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = StartModule.class)
public interface StartComponent {
    void inject(StartActivity activity);
}