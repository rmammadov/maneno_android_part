package com.knowlge.maneno.ui.common.subscriber;

import com.knowlge.maneno.domain.log.Log;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.ui.base.BaseSubscriber;

import java.util.List;


public class BooksListResultSubscriber extends BaseSubscriber<List<BookInfo>> {

    public interface BooksReceivedListener {
        void onBooksReceived(List<BookInfo> books);
    }

    private final BooksReceivedListener booksReceivedListener;


    public BooksListResultSubscriber(BooksReceivedListener booksReceivedListener, Log log) {
        super(log);
        this.booksReceivedListener = booksReceivedListener;
    }

    @Override
    public void onNext(List<BookInfo> books) {
        super.onNext(books);

        if (books != null) {
            booksReceivedListener.onBooksReceived(books);
        }
    }
}
