package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.knowlge.maneno.R;
import com.knowlge.maneno.ui.common.model.BottomNavigationItemsModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rmammadov on 3/31/17.
 */
public class BottomNavigationItemsAdapter extends RecyclerView.Adapter<BottomNavigationItemsAdapter.ViewHolder> {

    private int itemHeight = 0;
    private int itemWidth = 0;

    public interface OnItemClickedListener {
        void onItemClicked(int position, BottomNavigationItemsModel item);
    }

    private List<BottomNavigationItemsModel> itemsList;
    private Context context;

    private int selectedPosition;

    private OnItemClickedListener onItemClickedListener;

    public BottomNavigationItemsAdapter(Context context, List<BottomNavigationItemsModel> itemsList) {
        this.context = context;
        this.itemsList = itemsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bottom_navigation_item, parent, false);

        //Set width and height of the item
        if (itemWidth != 0)
            itemView.getLayoutParams().width = itemWidth / getItemCount();
        if (itemHeight != 0)
            itemView.getLayoutParams().height = itemHeight;

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(context, itemsList.get(position), onItemClickedListener);
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setOnItemClickedListener(BottomNavigationItemsAdapter.OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    public void setSelected(int newPosition) {
        if (newPosition >= 0 && newPosition < itemsList.size() && newPosition != selectedPosition) {
            int oldSelectedPosition = selectedPosition;
            selectedPosition = newPosition;

            itemsList.get(oldSelectedPosition).setSelected(false);
            notifyItemChanged(oldSelectedPosition);

            itemsList.get(selectedPosition).setSelected(true);
            notifyItemChanged(selectedPosition);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemTitleTextView)
        CustomFontTextView titleTextView;
        @BindView(R.id.itemIconImageView)
        ImageView iconImageView;
        @BindView(R.id.rlTabItmesSelector)
        RelativeLayout rlTabItemsSelector;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setItem(Context context, final BottomNavigationItemsModel item, final BottomNavigationItemsAdapter.OnItemClickedListener onItemClickedListener) {
            final int position = getAdapterPosition();

            // load values for the components
            titleTextView.setText(item.getTitle());
            Picasso.with(context).load(item.getImageId()).into(iconImageView);

            // todo - should replace with new version of the method to fit  with api version.
            if (item.isSelected()) {
                rlTabItemsSelector.setSelected(true);
                selectedPosition = position;
            } else {
                rlTabItemsSelector.setSelected(false);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemClickedListener != null && position != RecyclerView.NO_POSITION) {
                        onItemClickedListener.onItemClicked(position, item);
                    }
                }
            });
        }
    }

    public void setItemHeight(int itemHeight) {
        this.itemHeight = itemHeight;
    }

    public void setItemWidth(int itemWidth) {
         this.itemWidth = itemWidth;
    }
}
