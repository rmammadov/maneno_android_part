package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.graphics.Typeface;

import com.knowlge.maneno.domain.Util;

import java.util.Hashtable;


/**
 * Used due to memory leak: https://code.google.com/p/android/issues/detail?id=9904
 */

class Typefaces {
    private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

    // Directory name in assets where font files are.
    private static final String DIR_NAME = "fonts";

    /*
        <enum name="aleoBold" value="1" />
        <enum name="aleoItalic" value="2" />
        <enum name="aleoLight" value="3" />
        <enum name="aleoLightItalic" value="4" />
        <enum name="aleoRegular" value="5" />
     */
    private static final String ALEO_BOLD_ID = "1";
    private static final String ALEO_ITALIC_ID = "2";
    private static final String ALEO_LIGHT_ID = "3";
    private static final String ALEO_LIGHT_ITALIC_ID = "4";
    private static final String ALEO_REGULAR_ID = "5";

    public static Typeface get(Context c, String fontId) {
        Typeface t = null;
        if (Util.isSet(fontId)) {
            t = getFromCache(c, fontId);
        }

        if (t == null) {
            // Return Aleo Bold by default.
            t = getFromCache(c, ALEO_BOLD_ID);
        }

        return t;
    }

    private static Typeface getFromCache(Context c, String fontId) {
        synchronized(cache){
            if(!cache.containsKey(fontId)){
                String fileName = getFontFileName(fontId);
                if (!Util.isSet(fileName)) {
                    return null;
                }

                Typeface t = Typeface.createFromAsset(c.getAssets(), DIR_NAME + "/" + fileName);
                cache.put(fontId, t);

            }
            return cache.get(fontId);
        }
    }

    private static String getFontFileName(String fontId) {
        if (ALEO_BOLD_ID.equals(fontId)) {
            return "aleoBold.ttf";
        } else if (ALEO_ITALIC_ID.equals(fontId)) {
            return "aleoItalic.ttf";
        } else if (ALEO_LIGHT_ID.equals(fontId)) {
            return "aleoLight.ttf";
        } else if (ALEO_LIGHT_ITALIC_ID.equals(fontId)) {
            return "aleoLightItalic.ttf";
        } else if (ALEO_REGULAR_ID.equals(fontId)) {
            return "aleoRegular.ttf";
        }

        return null;
    }

}

