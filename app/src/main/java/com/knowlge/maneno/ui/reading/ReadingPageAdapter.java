package com.knowlge.maneno.ui.reading;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.knowlge.maneno.ui.reading.model.PageData;

import java.util.List;



public class ReadingPageAdapter extends FragmentPagerAdapter {

    private List<PageData> pages;

    public ReadingPageAdapter(FragmentManager fm, List<PageData> pages) {
        super(fm);
        this.pages = pages;
    }

    @Override
    public Fragment getItem(int position) {
        if (position >= 0 && position < pages.size()) {
            PageData pageData = pages.get(position);
            return ReadingPageFragment.newInstance(pageData.getText(), pageData.getImagePath());
        }

        return null;
    }

    @Override
    public int getCount() {
        return pages.size();
    }
}
