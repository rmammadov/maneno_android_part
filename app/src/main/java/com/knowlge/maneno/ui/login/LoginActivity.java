package com.knowlge.maneno.ui.login;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.VideoView;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.usecase.login.LoginResult;
import com.knowlge.maneno.ui.animations.PauseVideoRunnable;
import com.knowlge.maneno.ui.animations.Player;
import com.knowlge.maneno.ui.base.BaseActivity;
import com.knowlge.maneno.ui.common.MNNCheckBox;
import com.knowlge.maneno.ui.common.MNNDialogFragment;
import com.knowlge.maneno.ui.login.di.LoginModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginView, View.OnClickListener {

    @Inject
    LoginPresenter loginPresenter;

    @BindView(R.id.loginButton)
    Button loginButton;

    @BindView(R.id.signupButton)
    Button signupButton;

    @BindView(R.id.emailEditText)
    EditText emailEditText;

    @BindView(R.id.passwordEditText)
    EditText passEditText;

    @BindView(R.id.videoView)
    VideoView videoView;

    @BindView(R.id.videoPlaceholder)
    View videoPlaceholder;

    @BindView(R.id.rememberMeCheckBox)
    MNNCheckBox rememberMeCheckBox;

    @BindView(R.id.checkMarkOn)
    View checkMarkOn;

    @BindView(R.id.emailLayout)
    View emailLayout;

    @BindView(R.id.passwordLayout)
    View passwordLayout;

    private MNNDialogFragment loginErrorDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setupUI();
    }

    @Override
    protected void onDestroy() {
        videoView.stopPlayback();
        super.onDestroy();
    }

    @Override
    protected void setupActivityComponent() {
        getApplicationComponent()
                .createLoginComponent(new LoginModule(this))
                .inject(this);
    }

    private void setupUI() {
        loginButton.setOnClickListener(this);
        signupButton.setOnClickListener(this);

        passEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (focus) {
                    loginPresenter.onPasswordFieldFocusGained();
                } else {
                    loginPresenter.onPasswordFieldFocusLost();
                }
            }
        });

        pauseVideoRunnable = new PauseVideoRunnable(videoView);
        Player.playAnimation(this, videoView, videoPlaceholder, R.raw.logoanimation, false, pauseVideoRunnable, 3000);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.loginButton) {
            loginPresenter.onLoginButtonClicked(emailEditText.getText().toString(), passEditText.getText().toString(), rememberMeCheckBox.isChecked());
        } else if (view.getId() == R.id.signupButton) {
            loginPresenter.onSignupButtonClicked();
        }
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    private Handler dragonEyesClosedHandler;
    private PauseVideoRunnable pauseVideoRunnable;

    @Override
    public void startClosingEyesAnimation() {
        videoView.seekTo(3000);
        videoView.start();
        dragonEyesClosedHandler = new Handler();
        dragonEyesClosedHandler.postDelayed(pauseVideoRunnable, 1500);
    }

    @Override
    public void startOpenEyesAnimation() {
        videoView.start();
        if (dragonEyesClosedHandler != null) {
            dragonEyesClosedHandler.removeCallbacks(pauseVideoRunnable);
        }
    }

    @Override
    public void animateEmailFieldError() {
        animateViewShake(emailLayout);
    }

    @Override
    public void animatePasswordFieldError() {
        animateViewShake(passwordLayout);
    }

    @Override
    public void openLoginErrorDialog(Integer errCode) {
        closeLoginErrorDialog();

        if (errCode != null) {
            if (errCode == LoginResult.ERR_CODE_UNILOGIN_USER_LOGIN_ERROR) {
                loginErrorDialogFragment = MNNDialogFragment.newInstance(R.string.error, R.string.unilogin_error_dialog_text,
                        R.string.ok);

                loginErrorDialogFragment.setButton1OnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loginPresenter.onUniloginErrorOkButtonClicked();
                    }
                });
            } else if (errCode == LoginResult.ERR_CODE_INCORRECT_USERNAME_PASSWORD) {
                loginErrorDialogFragment = MNNDialogFragment.newInstance(R.string.incorrect_password, R.string.incorrect_password_dialog_text,
                        R.string.try_again, R.string.recovery);

                loginErrorDialogFragment.setButton1OnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loginPresenter.onTryAgainButtonClicked();
                    }
                });
                loginErrorDialogFragment.setButton2OnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loginPresenter.onPasswordRecoveryButtonClicked();
                    }
                });
            }

            if (loginErrorDialogFragment != null) {
                loginErrorDialogFragment.show(getSupportFragmentManager(), "dialog");
            }
        }
    }

    @Override
    public void closeLoginErrorDialog() {
        if (loginErrorDialogFragment != null) {
            loginErrorDialogFragment.dismiss();
            loginErrorDialogFragment = null;
        }
    }
}
