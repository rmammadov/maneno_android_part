package com.knowlge.maneno.ui.main;

import com.knowlge.maneno.ui.base.BaseActivityView;


public interface MainView extends BaseActivityView {
    void setItemSelected(int menuItemId);

    void openPage(int menuItemId);

    void closeNavigation();
}
