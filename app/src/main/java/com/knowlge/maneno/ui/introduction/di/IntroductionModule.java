package com.knowlge.maneno.ui.introduction.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.introduction.IntroductionFragment;
import com.knowlge.maneno.ui.introduction.IntroductionView;

import dagger.Module;
import dagger.Provides;


@Module
public class IntroductionModule {

    private final IntroductionFragment introductionFragment;

    public IntroductionModule(IntroductionFragment introductionFragment) {
        this.introductionFragment = introductionFragment;
    }

    @Provides
    @ActivityScope
    IntroductionView provideIntroductionView() {
        return this.introductionFragment;
    }
}
