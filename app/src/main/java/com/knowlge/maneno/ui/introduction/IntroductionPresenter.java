package com.knowlge.maneno.ui.introduction;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.User;
import com.knowlge.maneno.domain.usecase.GetUserUseCase;
import com.knowlge.maneno.domain.usecase.book.GetBooksRecommendationsUseCase;
import com.knowlge.maneno.ui.base.BasePresenter;
import com.knowlge.maneno.ui.base.BaseSubscriber;
import com.knowlge.maneno.ui.common.subscriber.BooksListResultSubscriber;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


@ActivityScope
public class IntroductionPresenter extends BasePresenter<IntroductionView> {

    @Inject
    GetUserUseCase getUserUseCase;

    @Inject
    GetBooksRecommendationsUseCase getBooksRecommendationsUseCase;

    private ArrayList<Integer> recommendedBooksListIds;

    private User user;

    @Inject
    public IntroductionPresenter(IntroductionView view) {
        super(view);
    }

    public void open(final Boolean levelUpSavedState, final Boolean dragonStagesSavedState, final Integer selectedTabSavedId) {
        // todo - move subscriber to separate class, so it could be reused.

        getUserUseCase.execute(new BaseSubscriber<User>(getLog()) {
            @Override
            public void onNext(User user) {
                IntroductionPresenter.this.user = user;
                getView().setUserData(user);

                if(levelUpSavedState != null && levelUpSavedState) {
                    getView().showLevelUp(user);
                }

                if(dragonStagesSavedState != null && dragonStagesSavedState) {
                    getView().showDragonStages(user);
                }

                if (selectedTabSavedId != null) {
                    onTabSelected(selectedTabSavedId);
                }
            }
        });
    }

    public void onTabSelected(int selectedTabId) {
        getView().setTabSelected(selectedTabId);

        if (selectedTabId == IntroductionView.NEW_BOOKS_ARRIVALS_TAB_ID) {
            // todo - check if tab is already loaded with content and then do not execute getBooksRecommendationsUseCase.
            // todo - show activity indicator within proper tab.
            getBooksRecommendationsUseCase.execute(new BooksListResultSubscriber(recomBooksReceivedListener, getLog()));
        } else if (selectedTabId == IntroductionView.SUGGESTED_FRIENDS_TAB_ID) {
            // todo - handle proper user case.
        } else if (selectedTabId == IntroductionView.READING_TAB_ID) {
            // todo - handle proper user case.
        }
    }

    private final BooksListResultSubscriber.BooksReceivedListener recomBooksReceivedListener = new BooksListResultSubscriber.BooksReceivedListener() {
        @Override
        public void onBooksReceived(List<BookInfo> books) {
            recommendedBooksListIds = new ArrayList<>();
            for (BookInfo bookInfo: books) {
                recommendedBooksListIds.add(bookInfo.getId());
            }

            getView().setBooksRecommendations(books);
        }
    };

    public void onRecommendedBookSelected(int position) {
        getView().setRecommendedBookSelected(position);
        getView().openBookDetails(recommendedBooksListIds, position);
    }

    public void onLevelUpButtonClicked() {
        getView().hideDragonStages();
        getView().toggleLevelUpVisibility(user);
    }

    public void onDragonStagesButtonClicked() {
        getView().hideLevelUp();
        getView().toggleDragonStagesVisibility(user);
    }

    public void onDailyReadingLeftArrowClicked() {
        getView().scrollDailyReadingsLeft(1);
    }

    public void onDailyReadingRightArrowClicked() {
        getView().scrollDailyReadingsRight(1);
    }
}
