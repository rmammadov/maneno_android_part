package com.knowlge.maneno.ui.reading;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.Chapter;
import com.knowlge.maneno.domain.usecase.base.LogErrorSubscriber;
import com.knowlge.maneno.domain.usecase.book.GetBookContentUseCase;
import com.knowlge.maneno.domain.usecase.reading.BookPageOpenedUseCase;
import com.knowlge.maneno.domain.usecase.reading.EndReadingSessionUseCase;
import com.knowlge.maneno.domain.usecase.reading.StartReadingSessionUseCase;
import com.knowlge.maneno.domain.usecase.reading.StopReadingSessionUseCase;
import com.knowlge.maneno.ui.base.BasePresenter;
import com.knowlge.maneno.ui.base.BaseSubscriber;
import com.knowlge.maneno.ui.reading.model.PageData;
import com.knowlge.maneno.ui.reading.subscriber.GetBookContentResultSubscriber;
import com.knowlge.maneno.ui.reading.subscriber.StartReadingSessionResultSubscriber;
import com.knowlge.maneno.ui.reading.subscriber.StopReadingSessionResultSubscriber;

import java.util.List;

import javax.inject.Inject;

import rx.observers.Subscribers;


@ActivityScope
public class ReadingPresenter extends BasePresenter<ReadingView> {

    @Inject
    GetBookContentUseCase getBookContentUseCase;

    @Inject
    StartReadingSessionUseCase startReadingSessionUseCase;

    @Inject
    BookPageOpenedUseCase bookPageOpenedUseCase;

    @Inject
    StopReadingSessionUseCase stopReadingSessionUseCase;

    @Inject
    EndReadingSessionUseCase endReadingSessionUseCase;

    private List<Chapter> chapters;
    private List<PageData> pages;
    private Integer currentPageIndex;

    @Inject
    public ReadingPresenter(ReadingView view) {
        super(view);
    }

    public void open(Integer bookId) {
        // todo - check if we have chapters already
        // todo - if not, get book data.
        getBookContentUseCase.execute(new GetBookContentResultSubscriber(this), new GetBookContentUseCase.Params(bookId));
    }

    public void onBookContentReceived(BookInfo bookInfo) {
        this.chapters = bookInfo.getChapters();

        getView().setTitle(bookInfo.getName());
        pages = getView().loadPages(bookInfo.getChapters());

        startReadingSession(bookInfo.getId(), true);
    }

    private void startReadingSession(Integer bookId, boolean startNew) {
        startReadingSessionUseCase.execute(new StartReadingSessionResultSubscriber(this), new StartReadingSessionUseCase.Params(bookId, startNew));
    }

    public void onReadingSessionStarted(Integer offsetInBook) {
        if (currentPageIndex != null) {
            getView().showPage(currentPageIndex);
        } else {
            int startingPageIdx = getPageIndexForOffset(offsetInBook);

            getView().showPage(startingPageIdx);

            // We have to call this for first item, because listener is not fired.
            if (startingPageIdx == 0) {
                onBookPageSelected(0);
            }
        }

        getView().showBookContent();
    }

    public void onGetBookContentProgress(int progressPercent) {
        getView().updateBookDownloadProgress(progressPercent);
    }

    public void onWordTapped(String word) {
        // todo - call WordTappedUseCase with params: (In use case word should be added to unknownWords for current session.)
            // - word
            // - singleTap (boolean)

        // todo - on result handle this on UI.
    }

    public void onBookPageSelected(int newPageIndex) {
        this.currentPageIndex = newPageIndex;

        if (pages != null && pages.size() > currentPageIndex) {
            PageData page = pages.get(currentPageIndex);
            String currentChapterTitle = chapters.get(page.getChapterIndex()).getTitle();

            getView().setChapterTitle(currentChapterTitle);
            getView().setPageNum(currentPageIndex + 1, pages.size());
            getView().setChapterSelected(page.getChapterIndex());

            bookPageOpenedUseCase.execute(new LogErrorSubscriber(log), new BookPageOpenedUseCase.Params(currentPageIndex + 1,
                    page.getOffsetInBook(), page.getText()));
        }
    }

    public void onPrevPageButtonClicked() {
        if (currentPageIndex > 0) {
            currentPageIndex--;
            getView().showPage(currentPageIndex);
        }
    }

    public void onNextPageButtonClicked() {
        if (pages != null) {
            if (currentPageIndex < pages.size() - 1) {
                currentPageIndex++;
                getView().showPage(currentPageIndex);
            } else {
                stopReading(true);
            }
        }
    }

    // Called when app goes to background.
    public void onStopReading() {
        if (pages != null) {
            stopReading(false);
        }
    }

    public void onBackButtonClicked() {
        if (pages != null) {
            stopReading(true);
        } else {
            getView().finish();
        }
    }

    public void onReadingSessionStopped(String userName, Integer minutes, Integer pages, Integer words) {
        getView().openReadingSessionSummaryView(userName, minutes, pages, words);
    }

    public void onEndReadingButtonClicked() {
        endReadingSessionUseCase.execute(new BaseSubscriber(getLog()) {
            @Override
            public void onNext(Object event) {
                super.onNext(event);

                getView().closeReadingSessionSummaryView();
                getView().finish();
            }
        });
    }

    public void onReadingSessionSummaryDismissed(Integer bookId) {
        // Just try to continue existing session.
        startReadingSession(bookId, false);
    }

    public void onReadingSessionPointsGet(Integer statPoints, Integer equipPoints, List<Achievement> achievements, Boolean levelCompleted) {
        getView().updateReadingSessionSummaryView(statPoints, equipPoints, achievements, levelCompleted);
    }

    public void onShowChaptersButtonClicked() {
        getView().showChaptersMenu();
    }

    public void onHideChaptersButtonClicked() {
        getView().hideChaptersMenu();
    }

    public void onChaptersMenuItemClicked(int position) {
        getView().setChapterSelected(position);

        getView().showPage(chapters.get(position).getFirstPageIndex());

        getView().hideChaptersMenu();
    }

    private void stopReading(boolean sendResult) {
        stopReadingSessionUseCase.execute(new StopReadingSessionResultSubscriber(this),
                new StopReadingSessionUseCase.Params(getCurrentBookOffset(), currentPageIndex + 1, pages.size(), sendResult));
    }

    private int getCurrentBookOffset() {
        if (getCurrentPage() != null) {
            return getCurrentPage().getOffsetInBook() + getCurrentLineOffset();
        } else {
            return 0;
        }
    }

    private int getCurrentLineOffset() {
        // todo - calculate based on latest selected line.
        return 0;
    }

    private PageData getCurrentPage() {
        if (pages != null) {
            return pages.get(currentPageIndex);
        }

        return null;
    }

    private int getPageIndexForOffset(Integer offset) {
        int result = 0;

        if (offset != null && offset > 0 && pages != null) {
            // Find proper page index for given offset.
            for (int idx = 1; idx < pages.size(); idx++) {
                // If beginning offset of "idx" page is larger than given offset, then given offset is on previous page.
                if (pages.get(idx).getOffsetInBook() > offset) {
                    result = idx - 1;
                    break;
                }
            }
        }

        return result;
    }
}
