package com.knowlge.maneno.ui.reading.subscriber;

import com.knowlge.maneno.domain.usecase.reading.StopReadingSessionUseCase;
import com.knowlge.maneno.ui.base.BaseSubscriber;
import com.knowlge.maneno.ui.reading.ReadingPresenter;


public class StopReadingSessionResultSubscriber extends BaseSubscriber<StopReadingSessionUseCase.Result> {
    private final ReadingPresenter readingPresenter;

    public StopReadingSessionResultSubscriber(ReadingPresenter readingPresenter) {
        super(readingPresenter.getLog());
        this.readingPresenter = readingPresenter;
    }

    @Override
    public void onNext(StopReadingSessionUseCase.Result r) {
        super.onNext(r);

        if (r.getMinutes() != null && r.getPages() != null && r.getWords() != null) {
            readingPresenter.onReadingSessionStopped(r.getUserName(), r.getMinutes(), r.getPages(), r.getWords());
        }

        if (r.getStatPoints() != null && r.getEquipPoints() != null) {
            readingPresenter.onReadingSessionPointsGet(r.getStatPoints(), r.getEquipPoints(), r.getAchievements(), r.getLevelCompleted());
        }
    }
}
