package com.knowlge.maneno.ui.introduction;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.DailyReading;
import com.knowlge.maneno.ui.common.MNNProgress;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;



public class DailyReadingsAdapter extends RecyclerView.Adapter<DailyReadingsAdapter.ViewHolder> {

    private Context context;
    private List<DailyReading> dailyReadings;
    private final LayoutInflater layoutInflater;

    public DailyReadingsAdapter(Context context, List<DailyReading> dailyReadings) {
        this.context = context;
        this.dailyReadings = dailyReadings;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_daily_reading, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setDailyReading(context, dailyReadings.get(position));
    }

    @Override
    public int getItemCount() {
        if (dailyReadings != null) {
            return dailyReadings.size();
        }

        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.readingMinTextView) TextView readingMinTextView;
        @BindView(R.id.readingDateTextView) TextView readingDateTextView;
        @BindView(R.id.readingProgress) MNNProgress readingProgress;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setDailyReading(Context context, DailyReading dailyReading) {
            if (dailyReading != null) {
                readingMinTextView.setText(context.getString(R.string.x_min, dailyReading.getMinutes()));
                try {
                    Date date = Util.parseDate(dailyReading.getDate(), Consts.DATE_TIME_FORMAT_SHORT);
                    readingDateTextView.setText(Util.formatDate(date, Consts.DATE_DAY_MONTH_FORMAT));
                } catch (ParseException e) {
                    // todo - log warning
                    readingDateTextView.setText(""); // Clear text if error.
                }

                if (dailyReading.getMinutes() >= Consts.MIN_DAILY_READING_MINUTES) {
                    readingProgress.setFillColor(MNNProgress.FILL_COLOR_GREEN);
                    readingProgress.setLevel(100);
                } else if (dailyReading.getMinutes() > 0) {
                    // At least 10%, so blue progress is higher than red empty progress.
                    readingProgress.setFillColor(MNNProgress.FILL_COLOR_BLUE);
                    readingProgress.setLevel(10 + (dailyReading.getMinutes() * 90 / Consts.MIN_DAILY_READING_MINUTES));
                } else {
                    readingProgress.setFillColor(MNNProgress.FILL_COLOR_RED);
                    readingProgress.setLevel(10); // Red empty progress has 10%.
                }
            }
        }
    }
}
