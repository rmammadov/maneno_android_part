package com.knowlge.maneno.ui.achievements;

import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.ui.base.BaseFragmentView;

import java.util.List;


public interface AchievementsView extends BaseFragmentView {
    void setAchievements(List<Achievement> achievements);
}
