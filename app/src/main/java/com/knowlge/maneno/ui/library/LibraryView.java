package com.knowlge.maneno.ui.library;

import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.Tag;
import com.knowlge.maneno.ui.base.BaseFragmentView;

import java.util.List;


public interface LibraryView extends BaseFragmentView {

    int FULL_VIEW_TYPE = 1;
    int GRID_VIEW_TYPE = 2;

    int FILTER_COLORS = 1;
    int FILTER_CATEGORIES = 2;
    int FILTER_LANGUAGES = 3;
    int FILTER_LENGTHS = 4;
    int FILTER_DIFFICULTIES = 5;

    int OFFLINE_LIBRARY = 1;
    int ONLINE_LIBRARY = 2;
    int CURENTLY_READING = 3;

    void setResultBooksList(List<BookInfo> manenosChoiceBooks, List<BookInfo> books, int currentViewType);

    void setBookSelected(int position);

    void showLoadingNextBooks();

    void insertNextBooksToList(List<BookInfo> books, int currentViewType);

    void setViewType(int typeId);

    void setFiltersData(List<ColorCode> colorCodes, List<Tag> categories, List<Language> languages, List<Tag> lengths, List<Tag> difficulties);

    void openFilterSelectionList(int filterId);

    void hideFilterSelectionList();

    void toggleSelection(int filterId, int position);

    void clearResultBooksList();

    void toggleBottomNavigation();

    void setBottomNavigationSelection(int position, int itemKey);
}
