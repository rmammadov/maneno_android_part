package com.knowlge.maneno.ui.library.subscriber;

import com.knowlge.maneno.domain.log.Log;
import com.knowlge.maneno.domain.usecase.book.GetBooksUseCase;
import com.knowlge.maneno.ui.base.BaseSubscriber;
import com.knowlge.maneno.ui.library.LibraryPresenter;


public class GetBooksResultSubscriber extends BaseSubscriber<GetBooksUseCase.Result> {

    private LibraryPresenter libraryPresenter;

    public GetBooksResultSubscriber(Log log, LibraryPresenter libraryPresenter) {
        super(log);
        this.libraryPresenter = libraryPresenter;
    }

    @Override
    public void onNext(GetBooksUseCase.Result result) {
        super.onNext(result);

        libraryPresenter.onBooksReceived(result.getManenosChoiceBooks(), result.getBooks(), result.getTotalRows());
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);

        libraryPresenter.onBooksReceived(null, null, null);
    }
}
