package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.knowlge.maneno.R;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MNNCheckBox extends LinearLayout implements View.OnClickListener {

    @BindView(R.id.checkMarkOn)
    ImageView checkMarkOn;

    private boolean isChecked = false;

    public MNNCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);

        setOrientation(LinearLayout.HORIZONTAL);

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.mnn_check_box, this, true);

        ButterKnife.bind(this, view);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MNNCheckBox);
        Integer textId = typedArray.getResourceId(R.styleable.MNNCheckBox_text, 0);
        if (textId > 0) {
            ((CustomFontTextView)view.findViewById(R.id.text)).setText(textId);
        }
        typedArray.recycle();

        View checkMark = view.findViewById(R.id.checkMark);
        checkMark.setClickable(true);
        checkMark.setOnClickListener(this);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        return new SavedState(super.onSaveInstanceState(), isChecked);
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof SavedState) {
            SavedState s = (SavedState) state;
            this.isChecked = s.isChecked();
            animateCheckMark();

            super.onRestoreInstanceState(s.getSuperState());
        } else {
            super.onRestoreInstanceState(state);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.checkMark) {
            isChecked = !isChecked;
            animateCheckMark();
        }
    }

    public boolean isChecked() {
        return isChecked;
    }

    private void animateCheckMark() {
        float from = 1.0f;
        float to = 0.0f;

        if (isChecked) {
            from = 0.0f;
            to = 1.0f;
        }

        Animation scale = new ScaleAnimation(from, to, from, to, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scale.setDuration(400);
        scale.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (isChecked) {
                    checkMarkOn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (!isChecked) {
                    checkMarkOn.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });

        checkMarkOn.startAnimation(scale);
    }

    private static class SavedState extends BaseSavedState {

        private static final int TRUE_VALUE = 1;
        private static final int FALSE_VALUE = -1;

        private int isChecked;

        SavedState(Parcelable p, boolean isChecked) {
            super(p);
            if (isChecked) {
                this.isChecked = TRUE_VALUE;
            } else {
                this.isChecked = FALSE_VALUE;
            }
        }

        private SavedState(Parcel source) {
            super(source);
            this.isChecked = source.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeInt(this.isChecked);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {

            @Override
            public SavedState createFromParcel(Parcel source) {
                return new SavedState(source);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };

        boolean isChecked() {
            return isChecked == TRUE_VALUE;
        }
    }
}
