package com.knowlge.maneno.ui.animations;

import android.widget.VideoView;


public class PauseVideoRunnable implements Runnable {
    private VideoView videoView;
    private boolean isOneTime = false;

    public PauseVideoRunnable(VideoView videoView) {
        this.videoView = videoView;
    }

    public PauseVideoRunnable(VideoView videoView, boolean isOneTime) {
        this.videoView = videoView;
        this.isOneTime = isOneTime;
    }

    @Override
    public void run() {
        if (videoView != null && videoView.isPlaying()) {
            videoView.pause();
        }

        if (isOneTime) {
            videoView = null;
        }
    }
}