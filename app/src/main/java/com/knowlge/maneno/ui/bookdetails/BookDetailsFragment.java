package com.knowlge.maneno.ui.bookdetails;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.ui.base.BaseFragment;
import com.knowlge.maneno.ui.bookdetails.di.BookDetailsModule;
import com.knowlge.maneno.ui.common.CustomFontTextView;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BookDetailsFragment extends BaseFragment implements BookDetailsView, View.OnClickListener {

    private static final String PARAM_BOOK_ID = "PARAM_BOOK_ID";
    private static final String PARAM_LAYOUT_TYPE = "PARAM_LAYOUT_TYPE";

    @Inject
    BookDetailsPresenter bookDetailsPresenter;

    @BindView(R.id.mainLayout)
    ViewGroup mainLayout;

    @BindView(R.id.coverImageView)
    ImageView coverImageView;

    @BindView(R.id.tasksImageView)
    ImageView tasksImageView;

    @BindView(R.id.favouriteImageView)
    ImageView favouriteImageView;

    @BindView(R.id.narrationImageView)
    ImageView narrationImageView;

    @BindView(R.id.titleTextView)
    CustomFontTextView titleTextView;

    @BindView(R.id.authorTextView)
    CustomFontTextView authorTextView;

    @BindView(R.id.icPagesImageView)
    ImageView icPagesImageView;

    @BindView(R.id.totalPagesTextView)
    CustomFontTextView totalPagesTextView;

    @BindView(R.id.levelTextView)
    CustomFontTextView levelTextView;

    @BindView(R.id.summaryTextView)
    CustomFontTextView summaryTextView;

    @BindView(R.id.seriesLayout)
    ViewGroup seriesLayout;

    @BindView(R.id.seriesName)
    CustomFontTextView seriesName;

    @BindView(R.id.seriesFilterButton)
    ImageView seriesFilterButton;

    @BindView(R.id.goToReadingButton)
    Button goToReadingButton;

    private Integer bookId;
    private int layoutType;

    public static BookDetailsFragment newInstance(Integer bookId, int layoutType) {
        BookDetailsFragment bookDetailsFragment = new BookDetailsFragment();

        Bundle args = new Bundle();
        args.putInt(PARAM_BOOK_ID, bookId);
        args.putInt(PARAM_LAYOUT_TYPE, layoutType);

        bookDetailsFragment.setArguments(args);

        return bookDetailsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        bookId = getArguments().getInt(PARAM_BOOK_ID);
        layoutType = getArguments().getInt(PARAM_LAYOUT_TYPE);
    }

    @Override
    protected void setupFragmentComponent() {
        getApplicationComponent()
                .createBookDetailsComponent(new BookDetailsModule(this))
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;

        if (layoutType == Consts.FULL_SCREEN_LAYOUT_TYPE) {
            view = inflater.inflate(R.layout.fragment_book_details_full_screen, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_book_details_part_screen, container, false);
        }

        ButterKnife.bind(this, view);
        setupUI();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        bookDetailsPresenter.open(bookId);
    }

    private void setupUI() {
        if (layoutType == Consts.FULL_SCREEN_LAYOUT_TYPE) {
            mainLayout.setBackgroundResource(R.drawable.book_details_background);
        }

        goToReadingButton.setOnClickListener(this);
    }

    @Override
    public void showBook(BookInfo book) {
        Picasso.with(getContext())
                .load(book.getThumbnailUrl())
                .into(coverImageView);
        titleTextView.setText(book.getName());
        authorTextView.setText(book.getAuthorName());

        Integer totalPages = book.getPagesCount();
        if (totalPages != null) {
            totalPagesTextView.setVisibility(View.VISIBLE);
            icPagesImageView.setVisibility(View.VISIBLE);
            totalPagesTextView.setText(String.valueOf(totalPages));
        } else {
            totalPagesTextView.setVisibility(View.GONE);
            icPagesImageView.setVisibility(View.GONE);
        }
        levelTextView.setText(book.getLix());
        summaryTextView.setText(book.getSummary());
        if (book.getSeriesName() != null) {
            seriesLayout.setVisibility(View.VISIBLE);
            seriesName.setText(getString(R.string.series_name, book.getSeriesName()));
        } else {
            seriesLayout.setVisibility(View.GONE);
        }

        if (book.getHasTasks()) { // todo - verify if this is proper flag.
            tasksImageView.setImageResource(R.drawable.book_tasks_on);
        } else {
            tasksImageView.setImageResource(R.drawable.book_tasks_off);
        }

        if (book.getLike()) {
            favouriteImageView.setImageResource(R.drawable.book_favourite_on);
        } else {
            favouriteImageView.setImageResource(R.drawable.book_favourite_off);
        }

        if (book.getHasOriginalNarration()) { // todo - verify if this is proper flag.
            narrationImageView.setImageResource(R.drawable.book_narration_on);
        } else {
            narrationImageView.setImageResource(R.drawable.book_narration_off);
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.goToReadingButton) {
            bookDetailsPresenter.onGotToReadingButtonClicked(bookId);
        }
    }
}
