package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.knowlge.maneno.R;



public class MNNButton extends AppCompatButton {
    public static final String FILLED_WITH_GREEN = "1";
    public static final String GREEN_BORDER = "2";
    public static final String WHITE_BORDER = "3";

    public MNNButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Get attributes and set default values if attribute does not exist:
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MNNButton);

        setTypeface(Typefaces.get(context, typedArray.getString(R.styleable.MNNButton_font)));
        String buttonType = typedArray.getString(R.styleable.MNNButton_type);
        int textSizeDimenResId = typedArray.getResourceId(R.styleable.MNNButton_textSize, 0);

        typedArray.recycle();

        // Set default value.
        if (textSizeDimenResId <= 0) {
            textSizeDimenResId = R.dimen._10sdp;
        }

        setType(context, buttonType);
        setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(textSizeDimenResId)); // getDimension() returns value implicitly converted to px.
    }

    public void setType(Context context, String typeId) {
        if (FILLED_WITH_GREEN.equals(typeId)) {
            setBackgroundResource(R.drawable.btn_green_selector);
            setTextColor(ContextCompat.getColor(context, R.color.white));
        } else if (GREEN_BORDER.equals(typeId)) {
            setBackgroundResource(R.drawable.btn_green_border_selector);
            setTextColor(ContextCompat.getColor(context, R.color.lightGreen));
        } else if (WHITE_BORDER.equals(typeId)) {
            setBackgroundResource(R.drawable.btn_white_border_selector);
            setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            setType(context, FILLED_WITH_GREEN); // Default type.
        }

        // todo - define other button types.

        // Clear padding, because after "setBackgroundResource()" it is set to non zero values.
        setPadding(0, 0, 0, 0);

        // So button text is not automatically capitalized.
        setTransformationMethod(null);
    }
}
