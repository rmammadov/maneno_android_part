package com.knowlge.maneno.ui.start.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.start.StartActivity;
import com.knowlge.maneno.ui.start.StartView;

import dagger.Module;
import dagger.Provides;


@Module
public class StartModule {
    private final StartActivity startActivity;

    public StartModule(StartActivity activity) {
        this.startActivity = activity;
    }

    @Provides
    @ActivityScope
    StartView provideStartView() {
        return this.startActivity;
    }
}
