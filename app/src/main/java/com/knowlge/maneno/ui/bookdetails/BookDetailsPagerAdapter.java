package com.knowlge.maneno.ui.bookdetails;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.knowlge.maneno.domain.model.BookInfo;

import java.util.ArrayList;
import java.util.List;




public class BookDetailsPagerAdapter extends FragmentPagerAdapter {

    private List<Integer> bookIds;
    private int layoutType;

    public BookDetailsPagerAdapter(FragmentManager fm, List<Integer> bookIds, int layoutType) {
        super(fm);
        this.bookIds = bookIds;
        this.layoutType = layoutType;
    }

    @Override
    public Fragment getItem(int position) {
        if (position >= 0 && position < bookIds.size()) {
            return BookDetailsFragment.newInstance(bookIds.get(position), layoutType);
        }

        return null;
    }

    @Override
    public int getCount() {
        return bookIds.size();
    }

    public void insertMoreBooks(List<BookInfo> books) {
        if (bookIds == null) {
            bookIds = new ArrayList<>();
        }

        if (books != null) {
            for (BookInfo bookInfo: books) {
                bookIds.add(bookInfo.getId());
            }
        }

        notifyDataSetChanged();
    }
}
