package com.knowlge.maneno.ui.main.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.main.MainActivity;
import com.knowlge.maneno.ui.main.MainView;

import dagger.Module;
import dagger.Provides;


@Module
public class MainModule {
    private final MainActivity mainActivity;

    public MainModule(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Provides
    @ActivityScope
    MainView provideMainView() {
        return this.mainActivity;
    }
}
