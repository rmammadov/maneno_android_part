package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.knowlge.maneno.R;
import com.knowlge.maneno.ui.reading.SelectionListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MNNSelectionList extends LinearLayout implements View.OnClickListener {

    @BindView(R.id.hideImageView) ImageView hideImageView;
    @BindView(R.id.elementsRecyclerView) RecyclerView elementsRecyclerView;

    private View.OnClickListener onHideClickListener;

    public MNNSelectionList(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        setOrientation(LinearLayout.VERTICAL);

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.selection_list, this, true);

        ButterKnife.bind(this, view);

        hideImageView.setClickable(true);
        hideImageView.setOnClickListener(this);

        elementsRecyclerView.setHasFixedSize(true);
        elementsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
    }

    @Override
    public void onClick(View view) {
        if (R.id.hideImageView == view.getId()) {
            if (onHideClickListener != null) {
                onHideClickListener.onClick(this);
            }
        }
    }

    public void setOnHideClickListener(OnClickListener onHideClickListener) {
        this.onHideClickListener = onHideClickListener;
    }

    public void setAdapter(SelectionListAdapter selectionListAdapter) {
        setAdapter(selectionListAdapter, null);
    }

    public void setAdapter(SelectionListAdapter selectionListAdapter, final Runnable onAdapterUpdatedRunnable) {
        elementsRecyclerView.setAdapter(selectionListAdapter);

        if (onAdapterUpdatedRunnable != null) {
            getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    onAdapterUpdatedRunnable.run();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                    else {
                        getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                }
            });
        }
    }

    public void moveDownShow(final int startTop, final int newTop) {
        layout(getLeft(), startTop, getLeft() + getMeasuredWidth(), startTop + getMeasuredHeight() );

        setVisibility(View.VISIBLE);

        TranslateAnimation moveDown = new TranslateAnimation(0, 0, 0, getHeight());
        moveDown.setDuration(500);
        moveDown.setFillEnabled(true);
        moveDown.setAnimationListener(
                new Animation.AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {}

                    @Override
                    public void onAnimationRepeat(Animation animation) {}

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        layout(getLeft(), newTop, getLeft() + getMeasuredWidth(), newTop + getMeasuredHeight() );
                    }
                }
        );

        startAnimation(moveDown);
    }

    public void moveUpHide(final int startTop, final int newTop) {
        layout(getLeft(), startTop, getLeft() + getMeasuredWidth(), startTop + getMeasuredHeight() );

        TranslateAnimation moveUp = new TranslateAnimation(0, 0, 0, -getHeight());
        moveUp.setDuration(500);
        moveUp.setFillEnabled(true);
        moveUp.setAnimationListener(
                new Animation.AnimationListener() {

                    @Override
                    public void onAnimationStart(Animation animation) {}

                    @Override
                    public void onAnimationRepeat(Animation animation) {}

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        layout(getLeft(), newTop, getLeft() + getMeasuredWidth(), newTop + getMeasuredHeight() );
                    }
                }
        );

        startAnimation(moveUp);

        setVisibility(View.INVISIBLE);
    }
}
