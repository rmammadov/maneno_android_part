package com.knowlge.maneno.ui.login;

import com.knowlge.maneno.ui.base.BaseActivityView;


public interface LoginView extends BaseActivityView {
    void startClosingEyesAnimation();
    void startOpenEyesAnimation();

    void animateEmailFieldError();
    void animatePasswordFieldError();

    void openLoginErrorDialog(Integer errCode);

    void closeLoginErrorDialog();
}
