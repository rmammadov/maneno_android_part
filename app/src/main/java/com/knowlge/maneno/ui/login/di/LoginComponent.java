package com.knowlge.maneno.ui.login.di;

import dagger.Subcomponent;
import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.login.LoginActivity;


@ActivityScope
@Subcomponent(modules = LoginModule.class)
public interface LoginComponent {
    void inject(LoginActivity loginActivity);
}
