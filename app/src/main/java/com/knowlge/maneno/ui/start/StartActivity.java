package com.knowlge.maneno.ui.start;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.knowlge.maneno.ui.base.BaseActivity;
import com.knowlge.maneno.ui.start.di.StartModule;

import javax.inject.Inject;


public class StartActivity extends BaseActivity implements StartView {

    @Inject
    StartPresenter startPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // todo - load simple UI with Maneno log.

        startPresenter.open();
    }

    @Override
    protected void setupActivityComponent() {
        getApplicationComponent()
                .createStartComponent(new StartModule(this))
                .inject(this);
    }
}
