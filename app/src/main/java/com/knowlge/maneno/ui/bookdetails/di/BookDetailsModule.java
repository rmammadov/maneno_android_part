package com.knowlge.maneno.ui.bookdetails.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.bookdetails.BookDetailsFragment;
import com.knowlge.maneno.ui.bookdetails.BookDetailsView;

import dagger.Module;
import dagger.Provides;


@Module
public class BookDetailsModule {
    private final BookDetailsFragment bookDetailsFragment;

    public BookDetailsModule(BookDetailsFragment bookDetailsFragment) {
        this.bookDetailsFragment = bookDetailsFragment;
    }

    @Provides
    @ActivityScope
    BookDetailsView provideBookDetailsView() {
        return this.bookDetailsFragment;
    }
}
