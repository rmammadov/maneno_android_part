package com.knowlge.maneno.ui.common.model;

/**
 * Created by rmammadov on 3/31/17.
 */
public class BottomNavigationItemsModel {

    private String title;
    private int imgResourceId;
    private boolean isSelected = false;
    private int itemKey;

    public BottomNavigationItemsModel(String title, int imgResourceId, boolean isSelected, int itemKey) {
        this.title = title;
        this.imgResourceId = imgResourceId;
        this.isSelected = isSelected;
        this.itemKey = itemKey;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImageId() {
        return imgResourceId;
    }

    public void setImageId(int imgResourceId) {
        this.imgResourceId = imgResourceId;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public int getItemKey() {
        return itemKey;
    }

    public void setItemKey(int itemKey) {
        this.itemKey = itemKey;
    }
}
