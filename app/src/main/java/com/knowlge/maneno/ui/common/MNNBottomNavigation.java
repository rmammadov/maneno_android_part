package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.knowlge.maneno.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rmammadov on 3/29/17.
 */

public class MNNBottomNavigation extends LinearLayout implements View.OnClickListener {

    @BindView(R.id.toggleImageView) ImageView toggleImageView;
    @BindView(R.id.elementsRecyclerView) RecyclerView elementsRecyclerView;
    @BindView(R.id.viewTopPanel) View viewTopPanel;

    private View.OnClickListener onToggleClickListener;

    private int navigationHeight = 0;
    private int navigationToggleButtonHeight = (int) getResources().getDimension(R.dimen.bottom_navigation_toggle_image_height);
    private int navigationItemBottomPadding = (int) getResources().getDimension(R.dimen.bottom_navigation_items_bottom_padding);

    private int navigationItemHeight = 0;
    public static int screenWidth = 0;

    private Boolean isUp = true;
    private Boolean isHidden = false;

    private ResizeAnimation resizeAnimation;

    private Context context;

    public MNNBottomNavigation(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        setOrientation(LinearLayout.HORIZONTAL);

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.bottom_navigation, this, true);

        ButterKnife.bind(this, view);

        toggleImageView.setClickable(true);
        toggleImageView.setOnClickListener(this);

        elementsRecyclerView.setHasFixedSize(true);
        elementsRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
    }

    @Override
    public void onClick(View view) {
        if (R.id.toggleImageView == view.getId()) {
            if (onToggleClickListener != null) {
                onToggleClickListener.onClick(this);
            }
        }
    }

    private void setBottomNavigationViewParameters() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
        if (isHidden)
            params.height = getNavigationToggleButtonHeight();
        else
            params.height = getNavigationHeight();
        setLayoutParams(params);
    }

    public void setNavigationItemHeight(int navigationItemHeight) {
        this.navigationItemHeight = navigationItemHeight;
    }

    public void setToggleImageAnimation(Animation anim) {
        toggleImageView.setAnimation(anim);
    }

    public void setAdapter(BottomNavigationItemsAdapter itmesListAdapter) {
        itmesListAdapter.setItemHeight(navigationItemHeight);
        itmesListAdapter.setItemWidth(getScreenWidth(context));
        elementsRecyclerView.setAdapter(itmesListAdapter);
    }

    public void setOnToggleClickListener(OnClickListener onToggleClickListener) {
        this.onToggleClickListener = onToggleClickListener;
    }

    public void setStartAsHidden(boolean isHidden) {
        this.isHidden = isHidden;
        setBottomNavigationViewParameters();
        toggleImageView.setImageResource(R.drawable.arrow_up_no_bg);
        isUp = false;
    }

    public void toggle() {
        if (isVisible()) {
            moveDownHide();
        } else {
            moveUpShow();
        }
    }

    public void moveDownHide() {
        navigationHeight = getNavigationHeight();
        resizeAnimation = new ResizeAnimation(this, navigationHeight, navigationToggleButtonHeight);
        this.startAnimation(resizeAnimation);
        toggleImageView.setImageResource(R.drawable.arrow_up_no_bg);
        isUp = false;
    }

    public void moveUpShow() {
        navigationHeight = getNavigationHeight();
        resizeAnimation = new ResizeAnimation(this, navigationToggleButtonHeight, navigationHeight);
        this.startAnimation(resizeAnimation);
        toggleImageView.setImageResource(R.drawable.arrow_down_no_bg);
        isUp = true;
    }


    public int getNavigationHeight() {
        return navigationItemHeight + navigationToggleButtonHeight + navigationItemBottomPadding;
    }

    public int getNavigationToggleButtonHeight() {
        return navigationToggleButtonHeight;
    }

    public Boolean isVisible() {
        return isUp;
    }

    /**
     * an animation for resizing the view.
     */
    public class ResizeAnimation extends Animation {
        private View mView;
        private float mToHeight;
        private float mFromHeight;

        public ResizeAnimation(View v, float fromHeight, float toHeight) {
            mToHeight = toHeight;
            mFromHeight = fromHeight;
            mView = v;
            setDuration(300);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float height =
                    (mToHeight - mFromHeight) * interpolatedTime + mFromHeight;
            ViewGroup.LayoutParams p = mView.getLayoutParams();
            p.height = (int) height;
            mView.requestLayout();
        }
    }

    // todo - move this method to proper util class.

    /**
     * calculate item width based on the screen, should move this method to utils class
     *
     * @param context
     * @return
     */
    public static int getScreenWidth(Context context) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }
        return screenWidth;
    }
}

