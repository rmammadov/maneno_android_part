package com.knowlge.maneno.ui.introduction.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.introduction.IntroductionFragment;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = IntroductionModule.class)
public interface IntroductionComponent {
    void inject(IntroductionFragment introductionFragment);
}
