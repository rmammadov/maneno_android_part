package com.knowlge.maneno.ui.start;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.domain.usecase.base.LogErrorSubscriber;
import com.knowlge.maneno.domain.usecase.reading.SendReadingStatisticsUseCase;
import com.knowlge.maneno.domain.usecase.start.CheckIfUserSavedUseCase;
import com.knowlge.maneno.domain.usecase.start.UpdateDictionaryDataUseCase;
import com.knowlge.maneno.ui.base.BasePresenter;
import com.knowlge.maneno.ui.start.subscriber.CheckIfUserSavedSubscriber;

import javax.inject.Inject;


@ActivityScope
public class StartPresenter extends BasePresenter<StartView> {

    @Inject
    CheckIfUserSavedUseCase checkIfUserSavedUseCase;

    @Inject
    UpdateDictionaryDataUseCase updateDictionaryDataUseCase;

    @Inject
    SendReadingStatisticsUseCase sendReadingStatisticsUseCase;

    @Inject
    StartPresenter(StartView view) {
        super(view);
    }

    public void open() {
        checkIfUserSavedUseCase.execute(new CheckIfUserSavedSubscriber(this));

        // todo - move following execution to different place - just after local repository initialization.
        updateDictionaryDataUseCase.execute(new LogErrorSubscriber(log));
    }

    public void onCheckedIfUserSaved(Boolean saved) {
        if (saved) {
            getView().openIntroductionScreen();

            // Initiate old unset reading statistics send for logged in user.
            sendReadingStatisticsUseCase.execute(new LogErrorSubscriber(log));
        } else {
            getView().openLoginScreen();
        }
        getView().finish();
    }
}
