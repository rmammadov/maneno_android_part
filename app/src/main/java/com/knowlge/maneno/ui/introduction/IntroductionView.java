package com.knowlge.maneno.ui.introduction;

import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.User;
import com.knowlge.maneno.ui.base.BaseFragmentView;

import java.util.List;



public interface IntroductionView extends BaseFragmentView {
    int NEW_BOOKS_ARRIVALS_TAB_ID = 1;
    int SUGGESTED_FRIENDS_TAB_ID = 2;
    int READING_TAB_ID = 3;

    void setUserData(User user);

    void setTabSelected(int selectedTabId);

    void setBooksRecommendations(List<BookInfo> books);

    void setRecommendedBookSelected(int position);

    void hideLevelUp();
    void showLevelUp(User user);
    void toggleLevelUpVisibility(User user);

    void hideDragonStages();
    void showDragonStages(User user);
    void toggleDragonStagesVisibility(User user);


    void scrollDailyReadingsLeft(int positionsCount);

    void scrollDailyReadingsRight(int positionsCount);
}
