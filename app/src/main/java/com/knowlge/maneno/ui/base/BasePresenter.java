package com.knowlge.maneno.ui.base;

import com.knowlge.maneno.domain.log.Log;

import javax.inject.Inject;


public class BasePresenter<T extends BaseView> implements Presenter<T> {

    private T view;

    @Inject
    protected Log log;

    public BasePresenter(T view) {
        attachView(view);
    }

    @Override
    public void attachView(T view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    public T getView() {
        return view;
    }

    public Log getLog() {
        return log;
    }
}
