package com.knowlge.maneno.ui.start.subscriber;

import com.knowlge.maneno.ui.base.BaseSubscriber;
import com.knowlge.maneno.ui.start.StartPresenter;


public class CheckIfUserSavedSubscriber extends BaseSubscriber<Boolean> {

    private StartPresenter startPresenter;

    public CheckIfUserSavedSubscriber(StartPresenter startPresenter) {
        super(startPresenter.getLog());
        this.startPresenter = startPresenter;
    }

    @Override
    public void onNext(Boolean result) {
        super.onNext(result);
        startPresenter.onCheckedIfUserSaved(result);
    }
}
