package com.knowlge.maneno.ui.base;

import com.knowlge.maneno.domain.log.Log;

public abstract class BaseSubscriber<T> extends rx.Subscriber<T> {

    private Log log;

    public BaseSubscriber(Log log) {
        this.log = log;
    }

    @Override
    public void onCompleted() {}

    @Override
    public void onNext(T event) {
        // todo - log result.
    }

    @Override
    public void onError(Throwable e) {
        if (log != null) {
            log.e(e);
        }
    }
}
