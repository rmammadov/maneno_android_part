package com.knowlge.maneno.ui.bookdetails;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.usecase.book.GetBookInfoUseCase;
import com.knowlge.maneno.ui.base.BasePresenter;
import com.knowlge.maneno.ui.common.subscriber.BookInfoResultSubscriber;

import javax.inject.Inject;


@ActivityScope
public class BookDetailsPresenter extends BasePresenter<BookDetailsView> implements BookInfoResultSubscriber.BookReceivedListener {

    @Inject
    GetBookInfoUseCase getBookInfoUseCase;

    @Inject
    public BookDetailsPresenter(BookDetailsView view) {
        super(view);
    }

    public void open(Integer bookId) {
        getBookInfoUseCase.execute(new BookInfoResultSubscriber(this, getLog()), new GetBookInfoUseCase.Params(bookId));
    }

    @Override
    public void onBookReceived(BookInfo book) {
        getView().showBook(book);
    }

    public void onGotToReadingButtonClicked(Integer bookId) {
        getView().openReadingScreen(bookId);
    }
}
