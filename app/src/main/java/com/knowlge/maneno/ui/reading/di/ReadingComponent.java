package com.knowlge.maneno.ui.reading.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.reading.ReadingActivity;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = ReadingModule.class)
public interface ReadingComponent {
    void inject(ReadingActivity readingActivity);
}
