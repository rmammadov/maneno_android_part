package com.knowlge.maneno.ui.reading;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.knowlge.maneno.R;

import butterknife.BindView;
import butterknife.ButterKnife;



public class ReadingSessionSummaryDialogFragment extends DialogFragment {
    private static final String PARAM_USER_NAME = "PARAM_USER_NAME";
    private static final String PARAM_MINUTES = "PARAM_MINUTES";
    private static final String PARAM_PAGES = "PARAM_PAGES";
    private static final String PARAM_WORDS = "PARAM_WORDS";

    private String userName;
    private Integer minutes;
    private Integer pages;
    private Integer words;
    private Integer statPoints;
    private Integer equipPoints;

    @BindView(R.id.endReadingButton)
    Button endReadingButton;

    @BindView(R.id.dialogTitle)
    TextView dialogTitle;

    @BindView(R.id.minutesValueTextView)
    TextView minutesValueTextView;

    @BindView(R.id.pagesValueTextView)
    TextView pagesValueTextView;

    @BindView(R.id.wordsValueTextView)
    TextView wordsValueTextView;

    @BindView(R.id.earnedCoinsTensValue)
    TextView earnedCoinsTensValue;

    @BindView(R.id.earnedCoinsUnityValue)
    TextView earnedCoinsUnityValue;

    @BindView(R.id.earnedDiamondsTensValue)
    TextView earnedDiamondsTensValue;

    @BindView(R.id.earnedDiamondsUnityValue)
    TextView earnedDiamondsUnityValue;

    public static ReadingSessionSummaryDialogFragment newInstance(String userName, Integer minutes, Integer pages, Integer words) {
        ReadingSessionSummaryDialogFragment fragment = new ReadingSessionSummaryDialogFragment();

        Bundle args = new Bundle();
        args.putString(PARAM_USER_NAME, userName);
        args.putInt(PARAM_MINUTES, minutes);
        args.putInt(PARAM_PAGES, pages);
        args.putInt(PARAM_WORDS, words);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen);

        userName = getArguments().getString(PARAM_USER_NAME);
        minutes = getArguments().getInt(PARAM_MINUTES);
        pages = getArguments().getInt(PARAM_PAGES);
        words = getArguments().getInt(PARAM_WORDS);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_reading_session_summary, container, false);

        ButterKnife.bind(this, view);
        setupUI();

        return view;
    }

    private void setupUI() {
        dialogTitle.setText(getString(R.string.hi_user_during_this_session, userName));
        if (minutes != null) {
            minutesValueTextView.setText(String.valueOf(minutes));
        }
        if (pages != null) {
            pagesValueTextView.setText(String.valueOf(pages));
        }
        if (words != null) {
            wordsValueTextView.setText(String.valueOf(words));
        }

        endReadingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ReadingActivity)getActivity()).onEndReadingButtonClicked();
            }
        });

        showEarnedPoints();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);

        ((ReadingActivity)getActivity()).onReadingSessionSummaryDismissed();
    }

    public void setEarnedPoints(Integer statPoints, Integer equipPoints) {
        this.statPoints = statPoints;
        this.equipPoints = equipPoints;

        showEarnedPoints();
    }

    private void showEarnedPoints() {
        if (statPoints != null && earnedCoinsTensValue != null
                && earnedCoinsUnityValue != null) {
            earnedCoinsTensValue.setText(String.valueOf(statPoints / 10));
            earnedCoinsUnityValue.setText(String.valueOf(statPoints % 10));
        }

        if (equipPoints != null && earnedDiamondsTensValue != null
                && earnedDiamondsUnityValue != null) {
            earnedDiamondsTensValue.setText(String.valueOf(equipPoints / 10));
            earnedDiamondsUnityValue.setText(String.valueOf(equipPoints % 10));
        }
    }
}
