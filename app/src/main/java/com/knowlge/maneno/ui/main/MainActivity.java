package com.knowlge.maneno.ui.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.knowlge.maneno.R;
import com.knowlge.maneno.ui.achievements.AchievementsFragment;
import com.knowlge.maneno.ui.base.BaseActivity;
import com.knowlge.maneno.ui.base.BaseFragment;
import com.knowlge.maneno.ui.introduction.IntroductionFragment;
import com.knowlge.maneno.ui.library.LibraryFragment;
import com.knowlge.maneno.ui.main.di.MainModule;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends BaseActivity implements MainView {

    @Inject
    MainPresenter mainPresenter;

    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;

    @BindView(R.id.navigationView)
    NavigationView navigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupUI();
    }

    @Override
    protected void setupActivityComponent() {
        getApplicationComponent()
                .createMainComponent(new MainModule(this))
                .inject(this);
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    private void setupUI() {
        setSupportActionBar(toolbar);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);

//        drawerToggle.setDrawerIndicatorEnabled(false);
//        drawerToggle.setHomeAsUpIndicator(R.drawable.ic_menu);

        // Disable all state based tinting, so icon are shown properly.
        navigationView.setItemIconTintList(null);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.nav_logout) {
                    mainPresenter.onLogoutClicked();
                } else {
                    mainPresenter.onMenuItemSelected(menuItem.getItemId());
                }
                return true;
            }
        });
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                drawerLayout.openDrawer(GravityCompat.START);
//                return true;
//        }
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void setItemSelected(int menuItemId) {
        navigationView.getMenu().findItem(menuItemId).setChecked(true);
    }

    @Override
    public void openPage(int menuItemId) {
        BaseFragment fragment = null;

        switch (menuItemId) {
            case R.id.nav_your_dragon_fragment:
                break;
            case R.id.nav_achievements_fragment:
                fragment = new AchievementsFragment();
                break;
            case R.id.nav_introduction_fragment:
                fragment = new IntroductionFragment();
                break;
            case R.id.nav_library_fragment:
                fragment = new LibraryFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.contentLayout, fragment).commit();
        }

        setTitle(navigationView.getMenu().findItem(menuItemId).getTitle());
    }

    @Override
    public void closeNavigation() {
        drawerLayout.closeDrawers();
    }
}
