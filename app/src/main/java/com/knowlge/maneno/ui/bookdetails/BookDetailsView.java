package com.knowlge.maneno.ui.bookdetails;

import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.ui.base.BaseFragmentView;


public interface BookDetailsView extends BaseFragmentView {
    void showBook(BookInfo book);
}
