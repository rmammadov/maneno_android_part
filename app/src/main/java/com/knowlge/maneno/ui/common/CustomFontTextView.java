package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.knowlge.maneno.R;


public class CustomFontTextView extends AppCompatTextView {

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);

        setTypeface(Typefaces.get(context, typedArray.getString(R.styleable.CustomFontTextView_font)));

        typedArray.recycle();
    }
}
