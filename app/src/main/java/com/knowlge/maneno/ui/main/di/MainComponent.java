package com.knowlge.maneno.ui.main.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.main.MainActivity;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = MainModule.class)
public interface MainComponent {
    void inject(MainActivity mainActivity);
}
