package com.knowlge.maneno.ui.reading.model;


public class PageData {
    private int chapterIndex;
    private String text; // todo - change to SpannableString ?
    private String imagePath; // Absolute path to the image.
    private Integer offsetInBook;

    public int getChapterIndex() {
        return chapterIndex;
    }

    public void setChapterIndex(int chapterIndex) {
        this.chapterIndex = chapterIndex;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getOffsetInBook() {
        return offsetInBook;
    }

    public void setOffsetInBook(Integer offsetInBook) {
        this.offsetInBook = offsetInBook;
    }
}
