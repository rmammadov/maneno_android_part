package com.knowlge.maneno.ui.animations;

import android.content.Context;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.TextureView;

import java.io.IOException;


public class VideoPlayer extends TextureView implements TextureView.SurfaceTextureListener {

    private boolean isMpPrepared;
    private boolean isLoaded;

    private boolean loop;
    private boolean autostart;

    private VideoPreparedListener videoPreparedListener;

    private MediaPlayer mp;
    private int rawResId;

    public VideoPlayer(Context context) {
        super(context);
    }

    public VideoPlayer(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public void loadVideo(int rawResId, boolean loop, boolean autostart) {
        if (isLoaded && isMpPrepared && this.rawResId == rawResId) {
            // Do not load the same video again.
            return;
        }

        this.rawResId = rawResId;
        this.loop = loop;
        this.autostart = autostart;
        this.isLoaded = true;

        if (this.isAvailable()) {
            prepareVideo(getSurfaceTexture());
        }

        setSurfaceTextureListener(this);
    }

    @Override
    public void onSurfaceTextureAvailable(final SurfaceTexture surface, int width, int height) {
        isMpPrepared = false;
        prepareVideo(surface);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {}

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        destroyMediaPlayer();

        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    public void prepareVideo(SurfaceTexture t) {
        Surface surface = new Surface(t);

        destroyMediaPlayer();

        mp = new MediaPlayer();
        mp.setSurface(surface);

        try {
            Uri uri = Uri.parse("android.resource://" + getContext().getPackageName() + "/" + rawResId);

            mp.setDataSource(getContext(), uri);
            mp.prepareAsync();

            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    isMpPrepared = true;
                    mp.setLooping(loop);
                    if (autostart) {
                        mp.start();
                    }
                    if (videoPreparedListener != null) {
                        videoPreparedListener.onVideoPrepared(rawResId);
                    }
                }


            });
        } catch (IllegalArgumentException | SecurityException | IllegalStateException | IOException e1) {
            e1.printStackTrace();
        }
    }

    public boolean startPlay()
    {
        if(mp!=null)
            if(!mp.isPlaying())
            {
                mp.start();
                return true;
            }

        return false;
    }

    private void destroyMediaPlayer() {
        this.isLoaded = false;
        this.isMpPrepared = false;

        if (mp!=null) {
            try {
                mp.stop();
            } catch (IllegalStateException e) {  /*Do nothing. TODO - log warning?*/ }

            mp.reset();
            mp.release();
            mp = null;
        }
    }

    public void pausePlay()
    {
        if (mp != null) {
            mp.pause();
        }
    }

    public void stopPlay()
    {
        if (mp != null) {
            mp.stop();
        }
    }

    public void changePlayState()
    {
        if(mp != null) {
            if(mp.isPlaying()) {
                mp.pause();
            } else {
                mp.start();
            }
        }

    }

    public void setOnVideoPreparedListener(VideoPreparedListener videoPreparedListener) {
        this.videoPreparedListener = videoPreparedListener;
    }
}
