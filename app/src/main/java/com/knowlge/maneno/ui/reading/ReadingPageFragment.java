package com.knowlge.maneno.ui.reading;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.knowlge.maneno.R;
import com.knowlge.maneno.ui.base.BaseFragment;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;



public class ReadingPageFragment extends BaseFragment {

    private static final String PARAM_PAGE_CONTENT = "PARAM_PAGE_CONTENT";
    private static final String PARAM_IMAGE_FILE_PATH = "PARAM_IMAGE_FILE_PATH";

    @BindView(R.id.fullPageContentTextView)
    TextView fullPageContentTextView;

    @BindView(R.id.withImagePageContentTextView)
    TextView withImagePageContentTextView;

    @BindView(R.id.pageImageView)
    ImageView pageImageView;

    @BindView(R.id.fullPageImageView)
    ImageView fullPageImageView;

    @BindView(R.id.imageTextLayout)
    ViewGroup imageTextLayout;

    private String pageContent;
    private String imagePath;

    public static ReadingPageFragment newInstance(String pageContent, String imagePath) {
        ReadingPageFragment readingPageFragment = new ReadingPageFragment();

        Bundle args = new Bundle();
        args.putString(PARAM_PAGE_CONTENT, pageContent);
        args.putString(PARAM_IMAGE_FILE_PATH, imagePath);

        readingPageFragment.setArguments(args);

        return readingPageFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pageContent = getArguments().getString(PARAM_PAGE_CONTENT);
        imagePath = getArguments().getString(PARAM_IMAGE_FILE_PATH);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book_page, container, false);

        ButterKnife.bind(this, view);
        setupUI();

        return view;
    }

    private void setupUI() {
        if (imagePath != null && pageContent != null) {
            fullPageContentTextView.setVisibility(View.INVISIBLE);
            fullPageImageView.setVisibility(View.INVISIBLE);

            withImagePageContentTextView.setText(pageContent);
            Picasso.with(getContext()).load(new File(imagePath)).into(pageImageView);
            imageTextLayout.setVisibility(View.VISIBLE);
        } else if (imagePath != null) {
            imageTextLayout.setVisibility(View.INVISIBLE);
            fullPageContentTextView.setVisibility(View.INVISIBLE);

            Picasso.with(getContext()).load(new File(imagePath)).into(fullPageImageView);
            fullPageImageView.setVisibility(View.VISIBLE);
        } else if (pageContent != null) {
            imageTextLayout.setVisibility(View.INVISIBLE);
            fullPageImageView.setVisibility(View.INVISIBLE);

            fullPageContentTextView.setText(pageContent);
            fullPageContentTextView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void setupFragmentComponent() {
        // todo
    }
}
