package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Util;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MNNProgress extends RelativeLayout {

    private static final String ORIENTATION_VERTICAL = "1";
    private static final String ORIENTATION_HORIZONTAL = "2";

    public static final String FILL_COLOR_BLUE = "1";
    public static final String FILL_COLOR_RED = "2";
    public static final String FILL_COLOR_GREEN = "3";

    private static final int EMPTY_1 = Color.parseColor("#f0f1f1");
    private static final int EMPTY_2 = Color.parseColor("#d5dbdb");

    private static final int GREEN_1 = Color.parseColor("#a6f785");
    private static final int GREEN_2 = Color.parseColor("#92d975");

    private static final int RED_1 = Color.parseColor("#ff785f");
    private static final int RED_2 = Color.parseColor("#e06953");

    private static final int BLUE_1 = Color.parseColor("#49b4e4");
    private static final int BLUE_2 = Color.parseColor("#41a4cf");

    @BindView(R.id.emptyFirstBackground)
    ImageView emptyFirstBackground;

    @BindView(R.id.emptySecondBackground)
    ImageView emptySecondBackground;

    @BindView(R.id.progressClipFirstBackground)
    ImageView progressClipFirstBackground;

    @BindView(R.id.progressClipSecondBackground)
    ImageView progressClipSecondBackground;

    @BindView(R.id.progressPercentTextView)
    TextView progressPercentTextView;

    private boolean showCheckMark = false; // Should we show check mark if progress is 100% ? False, by default.
    private String currentColor;
    private GradientDrawable firstFillDrawable = new GradientDrawable();
    private GradientDrawable secondFillDrawable = new GradientDrawable();

    public MNNProgress(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MNNProgress);
        String orientation = typedArray.getString(R.styleable.MNNProgress_orientation);
        String fillColor = typedArray.getString(R.styleable.MNNProgress_fillColor);
        String showCheckMarkAttr = typedArray.getString(R.styleable.MNNProgress_showCheckMark);
        String showPercentAttr = typedArray.getString(R.styleable.MNNProgress_showPercent);
        typedArray.recycle();

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;

        if (ORIENTATION_VERTICAL.equals(orientation)) {
            view = layoutInflater.inflate(R.layout.mnn_progress_vertical, this, true);
        } else {
            view = layoutInflater.inflate(R.layout.mnn_progress_horizontal, this, true);
        }
        ButterKnife.bind(this, view);

        setup(orientation, fillColor);

        if (Util.isSet(showPercentAttr) && Boolean.valueOf(showPercentAttr)) {
            progressPercentTextView.setVisibility(VISIBLE);
        } else {
            progressPercentTextView.setVisibility(INVISIBLE);
        }

        if (Util.isSet(showCheckMarkAttr)) {
            showCheckMark = Boolean.valueOf(showCheckMarkAttr);
        }
    }

    public void setLevel(int percent) {
        progressClipFirstBackground.getDrawable().setLevel(100 * percent);
        progressClipSecondBackground.getDrawable().setLevel(100 * percent);

        progressPercentTextView.setText(getContext().getString(R.string.percent, percent));

        if (percent == 100 && showCheckMark) {
            // todo - show check mark
        } else {
            // todo - hide check mark
        }
    }

    private void setup(String orientation, String fillColor) {
        float radiusPx = UITools.dpToPx(getContext(), getResources().getDimension(R.dimen._12sdp));

        int gravity;
        int clipOrientation;
        float[] firstRadius;
        float[] secondRadius;

        if (ORIENTATION_VERTICAL.equals(orientation)) {
            gravity = Gravity.BOTTOM;
            clipOrientation = ClipDrawable.VERTICAL;

            firstRadius = getLeftRadii(radiusPx);
            secondRadius = getRightRadii(radiusPx);
        } else {
            gravity = Gravity.LEFT;
            clipOrientation = ClipDrawable.HORIZONTAL;

            firstRadius = getTopRadii(radiusPx);
            secondRadius = getBottomRadii(radiusPx);
        }

        GradientDrawable firstEmptyBackground = new GradientDrawable();
        GradientDrawable secondEmptyBackground = new GradientDrawable();
        firstEmptyBackground.setColor(EMPTY_1);
        secondEmptyBackground.setColor(EMPTY_2);
        firstEmptyBackground.setCornerRadii(firstRadius);
        secondEmptyBackground.setCornerRadii(secondRadius);

        emptyFirstBackground.setImageDrawable(firstEmptyBackground);
        emptySecondBackground.setImageDrawable(secondEmptyBackground);

        firstFillDrawable.setCornerRadii(firstRadius);
        secondFillDrawable.setCornerRadii(secondRadius);

        ClipDrawable firstClipDrawable = new ClipDrawable(firstFillDrawable, gravity, clipOrientation);
        ClipDrawable secondClipDrawable = new ClipDrawable(secondFillDrawable, gravity, clipOrientation);

        progressClipFirstBackground.setImageDrawable(firstClipDrawable);
        progressClipSecondBackground.setImageDrawable(secondClipDrawable);

        setFillColor(fillColor);
    }

    public void setFillColor(String fillColor) {
        // Blue is default
        if (!Util.isSet(fillColor)) {
            if (Util.isSet(currentColor)) {
                return; // Do nothing if new color is empty and we already have something.
            }
            fillColor = FILL_COLOR_BLUE;
        }

        if (fillColor.equals(currentColor)) {
            // Do nothing if color is already set.
            return;
        }

        currentColor = fillColor;

        if (FILL_COLOR_GREEN.equals(currentColor)) {
            firstFillDrawable.setColor(GREEN_1);
            secondFillDrawable.setColor(GREEN_2);
        } else if (FILL_COLOR_RED.equals(currentColor)) {
            firstFillDrawable.setColor(RED_1);
            secondFillDrawable.setColor(RED_2);
        } else {
            firstFillDrawable.setColor(BLUE_1);
            secondFillDrawable.setColor(BLUE_2);
        }
    }

    private float[] getTopRadii(float radius) {
        return new float[] {radius, radius, radius, radius, 0, 0, 0, 0};
    }

    private float[] getBottomRadii(float radius) {
        return new float[] {0, 0, 0, 0, radius, radius, radius, radius};
    }

    private float[] getLeftRadii(float radius) {
        return new float[] {radius, radius, 0, 0, 0, 0, radius, radius};
    }

    private float[] getRightRadii(float radius) {
        return new float[] {0, 0, radius, radius, radius, radius, 0, 0};
    }
}
