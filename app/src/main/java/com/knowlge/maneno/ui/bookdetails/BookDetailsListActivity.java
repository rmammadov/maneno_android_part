package com.knowlge.maneno.ui.bookdetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Window;
import android.view.WindowManager;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Consts;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;



public class BookDetailsListActivity extends FragmentActivity {

    private static final String PARAM_BOOK_IDS = "PARAM_BOOK_IDS";
    private static final String PARAM_SELECTED_BOOK_POSITION = "PARAM_SELECTED_BOOK_POSITION";

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    private List<Integer> bookIds;
    private Integer selectedPosition;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Open in full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_book_details_list);
        ButterKnife.bind(this);

        bookIds = getIntent().getIntegerArrayListExtra(PARAM_BOOK_IDS);
        selectedPosition = getIntent().getIntExtra(PARAM_SELECTED_BOOK_POSITION, 0);

        setupUI();
    }

    private void setupUI() {
        viewPager.setAdapter(new BookDetailsPagerAdapter(getSupportFragmentManager(), bookIds, Consts.FULL_SCREEN_LAYOUT_TYPE));
        viewPager.setCurrentItem(selectedPosition);
    }

    public static Intent getCallingIntent(Context context, ArrayList<Integer> bookIds, Integer selectedPosition) {
        Intent intent = new Intent(context, BookDetailsListActivity.class);

        intent.putIntegerArrayListExtra(PARAM_BOOK_IDS, bookIds);
        intent.putExtra(PARAM_SELECTED_BOOK_POSITION, selectedPosition);

        return intent;
    }
}
