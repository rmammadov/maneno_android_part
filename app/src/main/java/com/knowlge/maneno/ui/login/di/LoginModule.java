package com.knowlge.maneno.ui.login.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.login.LoginActivity;
import com.knowlge.maneno.ui.login.LoginView;

import dagger.Module;
import dagger.Provides;


@Module
public class LoginModule {
    private final LoginActivity loginActivity;

    public LoginModule(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

    @Provides
    @ActivityScope
    LoginView provideLoginView() {
        return this.loginActivity;
    }
}
