package com.knowlge.maneno.ui.animations;

import android.net.Uri;


public interface VideoPreparedListener {
    void onVideoPrepared(int rawResId);
}
