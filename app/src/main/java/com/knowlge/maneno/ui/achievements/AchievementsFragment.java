package com.knowlge.maneno.ui.achievements;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.ui.achievements.di.AchievementsModule;
import com.knowlge.maneno.ui.base.BaseFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AchievementsFragment extends BaseFragment implements AchievementsView {

    @Inject
    AchievementsPresenter achievementsPresenter;

    @BindView(R.id.achievementsRecyclerView) RecyclerView achievementsRecyclerView;
    @BindView(R.id.noAchievementsInfoText) TextView noAchievementsInfoText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_achievements, container, false);

        ButterKnife.bind(this, view);
        setupUI();

        achievementsPresenter.open();

        return view;
    }

    private void setupUI() {
        achievementsRecyclerView.setHasFixedSize(true);
        int colsCount = 4;
        if (isLandscapeOrientation()) {
            colsCount++;
        }

        achievementsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), colsCount));
    }

    @Override
    protected void setupFragmentComponent() {
        getApplicationComponent()
                .createAchievementsComponent(new AchievementsModule(this))
                .inject(this);
    }

    @Override
    public void setAchievements(List<Achievement> achievements) {
        if (achievements != null && achievements.size() > 0) {
            noAchievementsInfoText.setVisibility(View.INVISIBLE);
            achievementsRecyclerView.setVisibility(View.VISIBLE);
            achievementsRecyclerView.setAdapter(new AchievementsAdapter(getContext(), achievements));
        } else {
            noAchievementsInfoText.setVisibility(View.VISIBLE);
            achievementsRecyclerView.setVisibility(View.INVISIBLE);
        }
    }
}
