package com.knowlge.maneno.ui.login;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.usecase.base.LogErrorSubscriber;
import com.knowlge.maneno.domain.usecase.login.LoginParams;
import com.knowlge.maneno.domain.usecase.login.LoginResult;
import com.knowlge.maneno.domain.usecase.login.LoginUseCase;
import com.knowlge.maneno.domain.usecase.reading.SendReadingStatisticsUseCase;
import com.knowlge.maneno.ui.base.BasePresenter;
import com.knowlge.maneno.ui.login.subscriber.LoginResultSubscriber;

import javax.inject.Inject;

import rx.observers.Subscribers;


@ActivityScope
public class LoginPresenter extends BasePresenter<LoginView> {

    @Inject
    LoginUseCase loginUseCase;

    @Inject
    SendReadingStatisticsUseCase sendReadingStatisticsUseCase;

    @Inject
    public LoginPresenter(LoginView loginView) {
        super(loginView);
    };

    void onLoginButtonClicked(String email, String password, boolean rememberMe) {
        //loginUseCase.execute(new LoginResultSubscriber(this), new LoginParams("bogumil.sikora@gmail.com", "aqqaqq", rememberMe));
        //loginUseCase.execute(new LoginResultSubscriber(this), new LoginParams("NoAh4548", "cfk66wtv", rememberMe));
        if (!Util.isSet(email)) {
            getView().animateEmailFieldError();
        } else if (!Util.isSet(password)) {
            getView().animatePasswordFieldError();
        } else {
            getView().showActivityIndicator();
            // todo - disable login button.

            loginUseCase.execute(new LoginResultSubscriber(this), new LoginParams(email, password, rememberMe));
        }
    }

    void onSignupButtonClicked() {
        // todo - open sign up screen.
    }

    public void onLoginResult(LoginResult loginResult) {
        getView().hideActivityIndicator();
        // todo - enable login button.

        if (loginResult.isSuccess()) {
            getView().openIntroductionScreen();

            // Initiate old unset reading statistics send for this user.
            sendReadingStatisticsUseCase.execute(new LogErrorSubscriber(log));

            getView().finish();
        } else {
            if (LoginResult.ERR_CODE_INCORRECT_USERNAME_PASSWORD == loginResult.getErrCode()) {
                getView().animatePasswordFieldError();
            }
            getView().openLoginErrorDialog(loginResult.getErrCode());
        }
    }

    public void onPasswordFieldFocusGained() {
        getView().startClosingEyesAnimation();
    }

    public void onPasswordFieldFocusLost() {
        getView().startOpenEyesAnimation();
    }

    public void onPasswordRecoveryButtonClicked() {
        // todo
    }

    public void onTryAgainButtonClicked() {
        getView().closeLoginErrorDialog();
    }

    public void onUniloginErrorOkButtonClicked() {
        getView().closeLoginErrorDialog();
    }
}
