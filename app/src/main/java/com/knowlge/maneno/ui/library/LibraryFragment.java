package com.knowlge.maneno.ui.library;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.Tag;
import com.knowlge.maneno.ui.base.BaseFragment;
import com.knowlge.maneno.ui.book.BooksAdapter;
import com.knowlge.maneno.ui.bookdetails.BookDetailsPagerAdapter;
import com.knowlge.maneno.ui.common.BottomNavigationItemsAdapter;
import com.knowlge.maneno.ui.common.model.BottomNavigationItemsModel;
import com.knowlge.maneno.ui.common.MNNBottomNavigation;
import com.knowlge.maneno.ui.common.MNNSelectionList;
import com.knowlge.maneno.ui.library.di.LibraryModule;
import com.knowlge.maneno.ui.reading.SelectionListAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LibraryFragment extends BaseFragment implements LibraryView, View.OnClickListener {

    @Inject LibraryPresenter libraryPresenter;

    @BindView(R.id.booksRecyclerView) RecyclerView booksRecyclerView;
    @BindView(R.id.booksViewPager) ViewPager booksViewPager;
    @BindView(R.id.fullViewImageView) ImageView fullViewImageView;
    @BindView(R.id.gridViewImageView) ImageView gridViewImageView;

    @BindView(R.id.colorFilterButton) ImageView colorFilterButton;
    @BindView(R.id.categoryFilterButton) ImageView categoryFilterButton;
    @BindView(R.id.languageFilterButton) ImageView languageFilterButton;
    @BindView(R.id.lengthFilterButton) ImageView lengthFilterButton;
    @BindView(R.id.difficultFilterButton) ImageView difficultFilterButton;

    @BindView(R.id.filtersLayout) ViewGroup filtersLayout;
    @BindView(R.id.filterValuesSelectionList) MNNSelectionList filterValuesSelectionList;

    @BindView(R.id.bottomNavigationList) MNNBottomNavigation bottomNavigationList;

    private BooksAdapter booksResultAdapter;
    private BookDetailsPagerAdapter booksResultDetailsPagerAdapter;

    private SelectionListAdapter colorCodesAdapter;
    private SelectionListAdapter categoriesAdapter;
    private SelectionListAdapter languagesAdapter;
    private SelectionListAdapter lengthsAdapter;
    private SelectionListAdapter difficultiesAdapter;

    private List<BottomNavigationItemsModel> listBottomNavigationItems = new ArrayList<>();
    private BottomNavigationItemsAdapter bottomNavigationItemsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_library, container, false);

        ButterKnife.bind(this, view);
        setupUI();

        libraryPresenter.open();

        return view;
    }

    private void setupUI() {
        filterValuesSelectionList.setVisibility(View.INVISIBLE);
        filtersLayout.bringToFront();

        colorFilterButton.setClickable(true);
        categoryFilterButton.setClickable(true);
        languageFilterButton.setClickable(true);
        lengthFilterButton.setClickable(true);
        difficultFilterButton.setClickable(true);

        fullViewImageView.setClickable(true);
        gridViewImageView.setClickable(true);


        colorFilterButton.setOnClickListener(this);
        categoryFilterButton.setOnClickListener(this);
        languageFilterButton.setOnClickListener(this);
        lengthFilterButton.setOnClickListener(this);
        difficultFilterButton.setOnClickListener(this);

        fullViewImageView.setOnClickListener(this);
        gridViewImageView.setOnClickListener(this);

        booksRecyclerView.setHasFixedSize(true);
        // todo - set initial columns count properly - for tablets it could be 3.
        int colsCount = 2;
        if (isLandscapeOrientation()) {
            colsCount++;
        }
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), colsCount);
        booksRecyclerView.setLayoutManager(gridLayoutManager);

        booksRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                libraryPresenter.onBooksResultListScrolled(gridLayoutManager.getItemCount(), gridLayoutManager.findLastVisibleItemPosition());
            }
        });

        filterValuesSelectionList.setOnHideClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                libraryPresenter.onHideFiltersSelectionListButtonClicked();
            }
        });

        bottomNavigationList.setOnToggleClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                libraryPresenter.onToggleBottomNavigationButtonClicked();
            }
        });

        setBottomNavigation();
    }

    @Override
    protected void setupFragmentComponent() {
        getApplicationComponent()
                .createLibraryComponent(new LibraryModule(this))
                .inject(this);
    }

    @Override
    public void setResultBooksList(List<BookInfo> manenosChoiceBooks, List<BookInfo> books, int currentViewType) {
        if (currentViewType == GRID_VIEW_TYPE) {
            booksResultAdapter = new BooksAdapter(getContext(), manenosChoiceBooks, books, true, booksRecyclerView);
            booksResultAdapter.setOnItemSelectedListener(new BooksAdapter.OnItemSelectedListener() {
                @Override
                public void onItemSelected(int position) {
                    libraryPresenter.onBookSelected(position);
                }
            });
            booksRecyclerView.setAdapter(booksResultAdapter);
            booksRecyclerView.setVisibility(View.VISIBLE);
        } else {
            List<Integer> bookIds = new ArrayList<>();
            if (manenosChoiceBooks != null) {
                for (BookInfo bookInfo : manenosChoiceBooks) {
                    bookIds.add(bookInfo.getId());
                }
            }

            if (books != null) {
                for (BookInfo bookInfo : books) {
                    bookIds.add(bookInfo.getId());
                }
            }

            // todo - when booksViewPager adapter was already set earlier, then no new views are created! Correct it:
            // http://stackoverflow.com/questions/7263291/viewpager-pageradapter-not-updating-the-view
            //
            // http://stackoverflow.com/questions/10396321/remove-fragment-page-from-viewpager-in-android/26944013#26944013
            // http://stackoverflow.com/questions/13664155/dynamically-add-and-remove-view-to-viewpager
            booksResultDetailsPagerAdapter = new BookDetailsPagerAdapter(getFragmentManager(), bookIds, Consts.PART_SCREEN_LAYOUT_TYPE);
            booksViewPager.setAdapter(booksResultDetailsPagerAdapter);
            booksViewPager.setCurrentItem(0);
            booksViewPager.addOnPageChangeListener(onPageChangeListener);
            booksViewPager.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void clearResultBooksList() {
        booksResultAdapter = null;
        booksResultDetailsPagerAdapter = null;

        booksRecyclerView.setVisibility(View.INVISIBLE);
        booksViewPager.setVisibility(View.INVISIBLE);
    }

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            libraryPresenter.onBookSelected(position);
        }
    };

    @Override
    public void setBookSelected(int position) {
        if (booksResultAdapter != null) {
            booksResultAdapter.setSelected(position);
        }
    }

    @Override
    public void showLoadingNextBooks() {
        if (booksResultAdapter != null) {
            booksResultAdapter.showLoadingNextBooks();
        }
    }

    @Override
    public void insertNextBooksToList(List<BookInfo> books, int currentViewType) {
        if (currentViewType == GRID_VIEW_TYPE) {
            if (booksResultAdapter == null) {
                setResultBooksList(null, books, currentViewType);
            } else {
                booksResultAdapter.insertMoreBooks(books);
            }
        } else if (currentViewType == FULL_VIEW_TYPE) {
            if (booksResultDetailsPagerAdapter == null) {
                setResultBooksList(null, books, currentViewType);
            } else {
                // todo - BUG - viewPager here has some memory leak! Getting OutOfMemory exception...
                booksResultDetailsPagerAdapter.insertMoreBooks(books);
            }
        }
    }

    @Override
    public void setViewType(int typeId) {
        if (typeId == FULL_VIEW_TYPE) {
            fullViewImageView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
            fullViewImageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.libraryResultGreenTab));

            gridViewImageView.setBackgroundDrawable(null);
            gridViewImageView.setColorFilter(null);

            booksRecyclerView.setVisibility(View.INVISIBLE);
        } else if (typeId == GRID_VIEW_TYPE) {
            gridViewImageView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
            gridViewImageView.setColorFilter(ContextCompat.getColor(getContext(), R.color.libraryResultGreenTab));

            fullViewImageView.setBackgroundDrawable(null);
            fullViewImageView.setColorFilter(null);

            booksViewPager.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setFiltersData(List<ColorCode> colorCodes, List<Tag> categories, List<Language> languages, List<Tag> lengths, List<Tag> difficulties) {
        colorCodesAdapter = createColorCodesAdapter(colorCodes);
        categoriesAdapter = createTagsAdapter(categories);
        languagesAdapter = createLanguagesAdapter(languages);
        lengthsAdapter = createTagsAdapter(lengths);
        difficultiesAdapter = createTagsAdapter(difficulties);

        colorCodesAdapter.setOnItemClickedListener(new OnFilterItemClickedListener(FILTER_COLORS));
        categoriesAdapter.setOnItemClickedListener(new OnFilterItemClickedListener(FILTER_CATEGORIES));
        languagesAdapter.setOnItemClickedListener(new OnFilterItemClickedListener(FILTER_LANGUAGES));
        lengthsAdapter.setOnItemClickedListener(new OnFilterItemClickedListener(FILTER_LENGTHS));
        difficultiesAdapter.setOnItemClickedListener(new OnFilterItemClickedListener(FILTER_DIFFICULTIES));
    }

    private class OnFilterItemClickedListener implements SelectionListAdapter.OnItemClickedListener {

        private int filterId;

        OnFilterItemClickedListener(int filterId) {
            this.filterId = filterId;
        }

        @Override
        public void onItemClicked(int position, SelectionListAdapter.ElementData element) {
            if (element != null) {
                libraryPresenter.onFilterItemClicked(filterId, position, element.isSelected(), element.getId());
            }
        }
    }


    @Override
    public void openFilterSelectionList(int filterId) {
        final int newTop = filtersLayout.getBottom();
        final int startTop = newTop - filterValuesSelectionList.getHeight();

        Runnable showFilterList = null;
        if (filterValuesSelectionList.getVisibility() == View.INVISIBLE) {
            showFilterList = new Runnable() {
                @Override
                public void run() {
                    filterValuesSelectionList.moveDownShow(startTop, newTop);
                }
            };
        }

        if (FILTER_COLORS == filterId && colorCodesAdapter != null) {
            filterValuesSelectionList.setAdapter(colorCodesAdapter, showFilterList);
        } else if (FILTER_CATEGORIES == filterId && categoriesAdapter != null) {
            filterValuesSelectionList.setAdapter(categoriesAdapter, showFilterList);
        } else if (FILTER_LANGUAGES == filterId && languagesAdapter != null) {
            filterValuesSelectionList.setAdapter(languagesAdapter, showFilterList);
        } else if (FILTER_LENGTHS == filterId && lengthsAdapter != null) {
            filterValuesSelectionList.setAdapter(lengthsAdapter, showFilterList);
        } else if (FILTER_DIFFICULTIES == filterId && difficultiesAdapter != null) {
            filterValuesSelectionList.setAdapter(difficultiesAdapter, showFilterList);
        }
    }


    @Override
    public void hideFilterSelectionList() {
        int startTop = filtersLayout.getBottom();
        int newTop = startTop - filterValuesSelectionList.getHeight();

        filterValuesSelectionList.moveUpHide(startTop, newTop);
    }

    @Override
    public void toggleSelection(int filterId, int position) {
        SelectionListAdapter adapter = getAdapter(filterId);
        if (adapter != null) {
            adapter.toggleSelection(position);
        }
    }

    private SelectionListAdapter getAdapter(int filterId) {
        switch (filterId) {
            case FILTER_COLORS:
                return colorCodesAdapter;
            case FILTER_CATEGORIES:
                return categoriesAdapter;
            case FILTER_LANGUAGES:
                return languagesAdapter;
            case FILTER_LENGTHS:
                return lengthsAdapter;
            case FILTER_DIFFICULTIES:
                return difficultiesAdapter;
        }

        return null;
    }

    private SelectionListAdapter createColorCodesAdapter(List<ColorCode> colorCodes) {
        List<SelectionListAdapter.ElementData> colorCodesElements = new ArrayList<>();
        for (ColorCode colorCode : colorCodes) {
            colorCodesElements.add(new SelectionListAdapter.ElementData(colorCode.getId(), colorCode.getIconUrl(), colorCode.getName()));
        }
        return new SelectionListAdapter(getContext(), colorCodesElements, true);
    }

    private SelectionListAdapter createTagsAdapter(List<Tag> tags) {
        List<SelectionListAdapter.ElementData> tagsElements = new ArrayList<>();
        for (Tag tag : tags) {
            tagsElements.add(new SelectionListAdapter.ElementData(tag.getId(), tag.getIconUrl(), tag.getName()));
        }

        return new SelectionListAdapter(getContext(), tagsElements, true);
    }

    private SelectionListAdapter createLanguagesAdapter(List<Language> languages) {
        List<SelectionListAdapter.ElementData> languagesElements = new ArrayList<>();
        for (Language language : languages) {
            SelectionListAdapter.ElementData element = new SelectionListAdapter.ElementData(language.getId(), language.getName());
            if (Consts.LangCode.BRITISH_ENGLISH.equals(language.getCode())) {
                element.setImageResId(R.drawable.filter_icons_english);
            } else if (Consts.LangCode.DANISH.equals(language.getCode())) {
                element.setImageResId(R.drawable.filter_icons_danish);
            }
            languagesElements.add(element);
        }

        return new SelectionListAdapter(getContext(), languagesElements, true);
    }

    private class OnBottomNavigationItemClickedListener implements BottomNavigationItemsAdapter.OnItemClickedListener {

        @Override
        public void onItemClicked(int position, BottomNavigationItemsModel item) {
            libraryPresenter.onBottomNavigationItemClicked(position, item.isSelected(), item.getItemKey());
        }
    }

    // todo - Should be improved

    @Override
    public void toggleBottomNavigation() {
        bottomNavigationList.toggle();
    }

    @Override
    public void setBottomNavigationSelection(int position, int itemKey) {
        switch (itemKey){
            case OFFLINE_LIBRARY:

                break;
            case ONLINE_LIBRARY:
                break;
            case CURENTLY_READING:
                break;
        }

        bottomNavigationItemsAdapter.setSelected(position);
    }

    private void setBottomNavigation() {
        Animation pulseAnim = AnimationUtils.loadAnimation(getContext(), R.anim.bottom_navigation_toggle_image_pulse_anim);
        bottomNavigationList.setToggleImageAnimation(pulseAnim);
        bottomNavigationItemsAdapter = new BottomNavigationItemsAdapter(getContext(), listBottomNavigationItems);
        bottomNavigationList.setNavigationItemHeight((int) getResources().getDimension(R.dimen.bottom_navigation_item_height));
        bottomNavigationList.setAdapter(bottomNavigationItemsAdapter);
        prepareBottomNavigation();
        bottomNavigationList.setStartAsHidden(true);
    }

    /**
     * Set bottom navigation list to recyclerView
     */
    private void prepareBottomNavigation() {
        BottomNavigationItemsModel bottomNavigationItem = new BottomNavigationItemsModel("Offline Library", R.drawable.ic_offlinebooks, false, OFFLINE_LIBRARY);
        listBottomNavigationItems.add(bottomNavigationItem);

        bottomNavigationItem = new BottomNavigationItemsModel("Online Library", R.drawable.ic_onlinelibrary, true, ONLINE_LIBRARY);
        listBottomNavigationItems.add(bottomNavigationItem);

        bottomNavigationItem = new BottomNavigationItemsModel("Currently Reading", R.drawable.ic_currentlyreading, false, CURENTLY_READING);
        listBottomNavigationItems.add(bottomNavigationItem);

        bottomNavigationItemsAdapter.notifyDataSetChanged();
        bottomNavigationItemsAdapter.setOnItemClickedListener(new OnBottomNavigationItemClickedListener());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.fullViewImageView) {
            libraryPresenter.onFullViewSelected();
        } else if (view.getId() == R.id.gridViewImageView) {
            libraryPresenter.onGridViewSelected();
        } else if (view.getId() == R.id.colorFilterButton) {
            libraryPresenter.onFilterButtonClicked(FILTER_COLORS);
        } else if (view.getId() == R.id.categoryFilterButton) {
            libraryPresenter.onFilterButtonClicked(FILTER_CATEGORIES);
        } else if (view.getId() == R.id.languageFilterButton) {
            libraryPresenter.onFilterButtonClicked(FILTER_LANGUAGES);
        } else if (view.getId() == R.id.lengthFilterButton) {
            libraryPresenter.onFilterButtonClicked(FILTER_LENGTHS);
        } else if (view.getId() == R.id.difficultFilterButton) {
            libraryPresenter.onFilterButtonClicked(FILTER_DIFFICULTIES);
        }
    }
}
