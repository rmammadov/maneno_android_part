package com.knowlge.maneno.ui.achievements;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.model.Achievement;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AchievementsAdapter extends RecyclerView.Adapter<AchievementsAdapter.ViewHolder> {

    private Context context;
    private List<Achievement> achievements;
    private final LayoutInflater layoutInflater;

    public AchievementsAdapter(Context context, List<Achievement> achievements) {
        this.context = context;
        this.achievements = achievements;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_achievement, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setAchievement(context, achievements.get(position));
    }

    @Override
    public int getItemCount() {
        if (achievements != null) {
            return achievements.size();
        }

        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.achievementImageView) ImageView achievementImageView;
        @BindView(R.id.achievementName) TextView achievementName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setAchievement(Context context, Achievement achievement) {
            Picasso.with(context)
                    .load(achievement.getIconUrl())
                    .into(achievementImageView);

            achievementName.setText(achievement.getName());
        }
    }
}
