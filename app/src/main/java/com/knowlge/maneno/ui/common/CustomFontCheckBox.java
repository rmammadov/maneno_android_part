package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

import com.knowlge.maneno.R;


public class CustomFontCheckBox extends AppCompatCheckBox {

    public CustomFontCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontCheckBox);

        setTypeface(Typefaces.get(context, typedArray.getString(R.styleable.CustomFontCheckBox_font)));

        typedArray.recycle();
    }
}
