package com.knowlge.maneno.ui.base;

import java.util.ArrayList;


public interface BaseView {

    void openBookDetails(ArrayList<Integer> bookListIds, Integer selectedPosition);
}
