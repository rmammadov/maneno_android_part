package com.knowlge.maneno.ui.reading.subscriber;

import com.knowlge.maneno.domain.usecase.book.GetBookContentUseCase;
import com.knowlge.maneno.ui.base.BaseSubscriber;
import com.knowlge.maneno.ui.reading.ReadingPresenter;



public class GetBookContentResultSubscriber extends BaseSubscriber<GetBookContentUseCase.Result> {

    private final ReadingPresenter readingPresenter;

    public GetBookContentResultSubscriber(ReadingPresenter readingPresenter) {
        super(readingPresenter.getLog());
        this.readingPresenter = readingPresenter;
    }

    @Override
    public void onNext(GetBookContentUseCase.Result result) {
        super.onNext(result);
        if (result != null) {
            if (result.getProgressPercent() != null) {
                readingPresenter.onGetBookContentProgress(result.getProgressPercent());
            }

            if (result.getBookInfo() != null) {
                readingPresenter.onBookContentReceived(result.getBookInfo());
            }
        }
    }
}
