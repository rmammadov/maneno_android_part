package com.knowlge.maneno.ui.library.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.library.LibraryFragment;
import com.knowlge.maneno.ui.library.LibraryView;

import dagger.Module;
import dagger.Provides;


@Module
public class LibraryModule {
    private final LibraryFragment libraryFragment;

    public LibraryModule(LibraryFragment libraryFragment) {
        this.libraryFragment = libraryFragment;
    }

    @Provides
    @ActivityScope
    LibraryView provideLibraryView() {
        return this.libraryFragment;
    }
}
