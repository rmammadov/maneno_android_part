package com.knowlge.maneno.ui.base;

import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.knowlge.maneno.ManenoApplication;
import com.knowlge.maneno.R;
import com.knowlge.maneno.di.components.ApplicationComponent;
import com.knowlge.maneno.ui.bookdetails.BookDetailsListActivity;
import com.knowlge.maneno.ui.login.LoginActivity;
import com.knowlge.maneno.ui.main.MainActivity;

import java.util.ArrayList;

import butterknife.BindView;


public abstract class BaseActivity extends AppCompatActivity implements BaseActivityView {

    @Nullable @BindView(R.id.activityIndicator)
    ProgressBar activityIndicator;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Open in full screen.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setupActivityComponent();
    }

    protected ApplicationComponent getApplicationComponent() {
        return ((ManenoApplication)getApplicationContext()).getApplicationComponent();
    }

    protected abstract void setupActivityComponent();

    @Override
    public void openLoginScreen() {
        // todo - move this to Navigator later.
        startActivity(LoginActivity.getCallingIntent(this));
    }

    @Override
    public void openIntroductionScreen() {
        // todo - move this to Navigator later.
        // todo - handle selected fragment arguments, so MainActivity is opened with correct content.
        startActivity(MainActivity.getCallingIntent(this));
    }

    @Override
    public void showActivityIndicator() {
        if (activityIndicator != null) {
            activityIndicator.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideActivityIndicator() {
        if (activityIndicator != null) {
            activityIndicator.setVisibility(View.INVISIBLE);
        }
    }

    protected void animateViewShake(View view) {
        // TODO - Change values to dp and calculate px based on given dp values?
        ObjectAnimator.ofFloat(view, "translationX", 0, 30, -20, 20, -10, 10, 0)
                .setDuration(600)
                .start();
    }

    @Override
    public void openBookDetails(ArrayList<Integer> bookListIds, Integer selectedPosition) {
        startActivity(BookDetailsListActivity.getCallingIntent(this, bookListIds, selectedPosition));
    }
}
