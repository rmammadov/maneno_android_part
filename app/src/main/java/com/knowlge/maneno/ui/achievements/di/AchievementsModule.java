package com.knowlge.maneno.ui.achievements.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.achievements.AchievementsFragment;
import com.knowlge.maneno.ui.achievements.AchievementsView;

import dagger.Module;
import dagger.Provides;


@Module
public class AchievementsModule {

    private final AchievementsFragment achievementsFragment;

    public AchievementsModule(AchievementsFragment achievementsFragment) {
        this.achievementsFragment = achievementsFragment;
    }

    @Provides
    @ActivityScope
    AchievementsView provideAchievementsView() {
        return this.achievementsFragment;
    }
}