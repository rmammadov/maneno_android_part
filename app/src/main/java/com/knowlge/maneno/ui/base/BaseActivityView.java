package com.knowlge.maneno.ui.base;


public interface BaseActivityView extends BaseView {
    void finish();

    void showActivityIndicator();
    void hideActivityIndicator();

    void openLoginScreen();
    void openIntroductionScreen();
}
