package com.knowlge.maneno.ui.achievements.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.achievements.AchievementsFragment;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = AchievementsModule.class)
public interface AchievementsComponent {
    void inject(AchievementsFragment achievementsFragment);
}