package com.knowlge.maneno.ui.introduction;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.devbrackets.android.exomedia.ui.widget.EMVideoView;
import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.DailyReading;
import com.knowlge.maneno.domain.model.Level;
import com.knowlge.maneno.domain.model.User;
import com.knowlge.maneno.ui.animations.VideoPlayer;
import com.knowlge.maneno.ui.animations.VideoPreparedListener;
import com.knowlge.maneno.ui.base.BaseFragment;
import com.knowlge.maneno.ui.book.BooksAdapter;
import com.knowlge.maneno.ui.common.MNNButton;
import com.knowlge.maneno.ui.common.MNNProgress;
import com.knowlge.maneno.ui.introduction.di.IntroductionModule;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class IntroductionFragment extends BaseFragment implements IntroductionView {

    private static final String LEVEL_UP_BUTTON_SI_ID = "level_up_button";
    private static final String DRAGON_STAGES_BUTTON_SI_ID = "dragon_stages_button";
    private static final String TAB_BAR_BOOKS_SI_ID = "tab_bar_books";

    @Inject
    IntroductionPresenter introductionPresenter;

    @BindView(R.id.mainScrollView) ScrollView mainScrollView;

    @BindView(R.id.progressHintLabel) TextView progressHintLabel;
    @BindView(R.id.mainLevelProgress) MNNProgress mainLevelProgress;

    @BindView(R.id.newBooksArrivalsTabHeader) TextView newBooksArrivalsTabHeader;
    @BindView(R.id.suggestedFriendsTabHeader) TextView suggestedFriendsTabHeader;
    @BindView(R.id.readingTabHeader) TextView readingTabHeader;

    @BindView(R.id.newBooksRecyclerView) RecyclerView newBooksRecyclerView;
    @BindView(R.id.suggestedFriendsRecyclerView) RecyclerView suggestedFriendsRecyclerView;
    @BindView(R.id.readingRecyclerView) RecyclerView readingRecyclerView;

    @BindView(R.id.dragonIntroPortraitVideo) VideoPlayer dragonIntroPortraitVideo;

    @BindView(R.id.levelUpButton) MNNButton levelUpButton;
    @BindView(R.id.dragonStagesButton) MNNButton dragonStagesButton;

    @BindView(R.id.levelUpLayout) ViewGroup levelUpLayout;
    @BindView(R.id.dragonPortraitLevelUpVideo) VideoPlayer dragonPortraitLevelUpVideo;
    @BindView(R.id.levelInfoTextView) TextView levelInfoTextView;
    @BindView(R.id.levelUpLevelProgress) MNNProgress levelUpLevelProgress;
    @BindView(R.id.nextLevelImageView) ImageView nextLevelImageView;
    @BindView(R.id.streakTextView) TextView streakTextView;
    @BindView(R.id.dailyReadingsRecyclerView) RecyclerView dailyReadingsRecyclerView;
    @BindView(R.id.dailyReadingsArrowLeft) ImageView dailyReadingsArrowLeft;
    @BindView(R.id.dailyReadingsArrowRight) ImageView dailyReadingsArrowRight;
    private LinearLayoutManager dailyReadingLinearLayoutManager;

    @BindView(R.id.dragonStagesLayout) ViewGroup dragonStagesLayout;
    @BindView(R.id.dragonStagesTextView) TextView dragonStagesTextView;
    @BindView(R.id.dragonStagesViewGroup) ViewGroup dragonStagesViewGroup;

    @BindView(R.id.emVideoView)  EMVideoView emVideoView;

    private SparseArray<View> dragonStagesViews;

    private BooksAdapter recommendedBooksAdapter;

    private boolean dragonStagesViewCreated = false;
    private boolean levelUpViewLoaded = false;

    private int selectedTabId = IntroductionView.NEW_BOOKS_ARRIVALS_TAB_ID;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_introduction, container, false);

        ButterKnife.bind(this, view);
        setupUI();

        Boolean levelUpSavedState = null;
        Boolean dragonStagesSavedState = null;
        Integer selectedTabSavedId = null;

        if (savedInstanceState != null) {
            levelUpSavedState = savedInstanceState.getBoolean(LEVEL_UP_BUTTON_SI_ID);
            dragonStagesSavedState = savedInstanceState.getBoolean(DRAGON_STAGES_BUTTON_SI_ID);
            selectedTabSavedId = savedInstanceState.getInt(TAB_BAR_BOOKS_SI_ID);
        }

        introductionPresenter.open(levelUpSavedState, dragonStagesSavedState, selectedTabSavedId);

        // By default open new books arrivals tab.
        if (selectedTabSavedId == null) {
            introductionPresenter.onTabSelected(IntroductionView.NEW_BOOKS_ARRIVALS_TAB_ID);
        }

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

        // todo - ask presenter to refresh user data.
    }

    private void setupUI() {
        newBooksArrivalsTabHeader.setClickable(true);
        suggestedFriendsTabHeader.setClickable(true);
        readingTabHeader.setClickable(true);

        levelUpButton.setOnClickListener(onClickListener);
        dragonStagesButton.setOnClickListener(onClickListener);
        newBooksArrivalsTabHeader.setOnClickListener(onClickListener);
        suggestedFriendsTabHeader.setOnClickListener(onClickListener);
        readingTabHeader.setOnClickListener(onClickListener);

        newBooksRecyclerView.setHasFixedSize(true);
        // todo - set initial columns count properly - for tablets it could be 3.
        int colsCount = 2;
        if (isLandscapeOrientation()) {
            colsCount++;
        }
        newBooksRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), colsCount));

        dailyReadingsArrowLeft.setClickable(true);
        dailyReadingsArrowRight.setClickable(true);
        dailyReadingsArrowLeft.setOnClickListener(onClickListener);
        dailyReadingsArrowRight.setOnClickListener(onClickListener);
        dailyReadingsRecyclerView.setHasFixedSize(true);
        dailyReadingLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        dailyReadingsRecyclerView.setLayoutManager(dailyReadingLinearLayoutManager);

        // todo - if any problems appear with ExoMedia (https://github.com/brianwernick/ExoMedia), try to use following:
        // - https://github.com/afollestad/easy-video-player
        // - https://github.com/Krupen/AutoplayVideos
        emVideoView.setVideoURI(Uri.parse(getString(R.string.intro_video_url)));
    }

    /**
     * Save states of the ui
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(LEVEL_UP_BUTTON_SI_ID, levelUpLayout.getVisibility() == View.VISIBLE);
        outState.putBoolean(DRAGON_STAGES_BUTTON_SI_ID, dragonStagesLayout.getVisibility() == View.VISIBLE);
        outState.putInt(TAB_BAR_BOOKS_SI_ID, selectedTabId);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.newBooksArrivalsTabHeader) {
                introductionPresenter.onTabSelected(IntroductionView.NEW_BOOKS_ARRIVALS_TAB_ID);
            } else if (view.getId() == R.id.suggestedFriendsTabHeader) {
                introductionPresenter.onTabSelected(IntroductionView.SUGGESTED_FRIENDS_TAB_ID);
            } else if (view.getId() == R.id.readingTabHeader) {
                introductionPresenter.onTabSelected(IntroductionView.READING_TAB_ID);
            } else if (view.getId() == R.id.levelUpButton) {
                introductionPresenter.onLevelUpButtonClicked();
            } else if (view.getId() == R.id.dragonStagesButton) {
                introductionPresenter.onDragonStagesButtonClicked();
            } else if (view.getId() == R.id.dailyReadingsArrowLeft) {
                introductionPresenter.onDailyReadingLeftArrowClicked();
            } else if (view.getId() == R.id.dailyReadingsArrowRight) {
                introductionPresenter.onDailyReadingRightArrowClicked();
            }
        }
    };

    @Override
    protected void setupFragmentComponent() {
        getApplicationComponent()
                .createIntroductionComponent(new IntroductionModule(this))
                .inject(this);
    }

    @Override
    public void setUserData(User user) {
        if (user != null) {
            if (user.getLevel() != null) {
                progressHintLabel.setText(getString(R.string.progress_level_hint_text,
                        user.getLevel().getFreeStatPoints(),
                        user.getLevel().getDaysRemaining(),
                        user.getLevel().getMinutesRemainingToday()));

                mainLevelProgress.setLevel(user.getLevel().getLevelCompletePercent());

                dragonIntroPortraitVideo.setOnVideoPreparedListener(new VideoPreparedListener() {
                    @Override
                    public void onVideoPrepared(int rawResId) {
                        // Hack: to start with scroll on top.
                        mainScrollView.fullScroll(ScrollView.FOCUS_UP);
                    }
                });
                dragonIntroPortraitVideo.loadVideo(getDragonIntroVideoResId(user.getLevel()), true, true);

                // Level up.
                levelUpViewLoaded = false;
                if (levelUpLayout.getVisibility() == View.VISIBLE) {
                    loadLevelUpView(user);
                }

                // Dragon stages.
                updateDragonStagesView(user);
            }
        }
    }

    private void setDailyReadingStreakText(List<DailyReading> dailyReadings) {
        int streak = 0;

        // Calculate latest daily streak.
        if (dailyReadings != null && dailyReadings.size() > 0) {
            for (int idx = dailyReadings.size() - 1; idx >= 0; idx--) {
                if (dailyReadings.get(idx).getMinutes() >= Consts.MIN_DAILY_READING_MINUTES) {
                    streak++;
                } else {
                    break;
                }
            }
        }

        if (streak <= 1) {
            streakTextView.setVisibility(View.INVISIBLE);
        } else {
            switch (streak) {
                case 2: streakTextView.setText(R.string.reading_streak_2_days); break;
                case 3: streakTextView.setText(R.string.reading_streak_3_days); break;
                case 4: streakTextView.setText(R.string.reading_streak_4_days); break;
                case 5: streakTextView.setText(R.string.reading_streak_5_days); break;
                case 6: streakTextView.setText(R.string.reading_streak_6_days); break;
                case 7: streakTextView.setText(R.string.reading_streak_7_days); break;
                case 8: streakTextView.setText(R.string.reading_streak_8_days); break;
                case 9: streakTextView.setText(R.string.reading_streak_9_days); break;
                default: streakTextView.setText(R.string.reading_streak_10_days_and_more); break;
            }
            streakTextView.setVisibility(View.VISIBLE);
        }
    }

    private int getDragonOffImageResId(int dragonStage) {
        switch (dragonStage) {
            case 2: return R.drawable.dragon_off_2;
            case 3: return R.drawable.dragon_off_3;
            case 4: return R.drawable.dragon_off_4;
            case 5: return R.drawable.dragon_off_5;
            case 6: return R.drawable.dragon_off_6;
            case 7: return R.drawable.dragon_off_7;
            case 8: return R.drawable.dragon_off_8;
            case 9: return R.drawable.dragon_off_9;
            case 10: return R.drawable.dragon_off_10;
        }

        return R.drawable.dragon_off_2;
    }

    private static int getDragonStageAnimationResId(int dragonStage) {
        switch (dragonStage) {
            case 1: return R.raw.progress_dragon1;
            case 2: return R.raw.progress_dragon2;
            case 3: return R.raw.progress_dragon3;
            case 4: return R.raw.progress_dragon4;
            case 5: return R.raw.progress_dragon5;
            case 6: return R.raw.progress_dragon6;
            case 7: return R.raw.progress_dragon7;
            case 8: return R.raw.progress_dragon8;
            case 9: return R.raw.progress_dragon9;
            case 10: return R.raw.progress_dragon10;
        }

        return R.raw.progress_dragon1;
    }

    private int getDragonIntroVideoResId(Level level) {
        if (level != null && level.getDragonLevel() != null) {
            switch (level.getDragonLevel()) {
                case 1: return R.raw.dragon_intro_portrait_1;
                case 2: return R.raw.dragon_intro_portrait_2;
                case 3: return R.raw.dragon_intro_portrait_3;
                case 4: return R.raw.dragon_intro_portrait_4;
                case 5: return R.raw.dragon_intro_portrait_5;
                case 6: return R.raw.dragon_intro_portrait_6;
                case 7: return R.raw.dragon_intro_portrait_7;
                case 8: return R.raw.dragon_intro_portrait_8;
                case 9: return R.raw.dragon_intro_portrait_9;
                case 10: return R.raw.dragon_intro_portrait_10;
            }
        }

        return  R.raw.dragon_intro_portrait_1;
    }

    @Override
    public void setTabSelected(int selectedTabId) {
        // Deselect all tabs.
        newBooksArrivalsTabHeader.setBackgroundResource(R.color.transparent);
        newBooksArrivalsTabHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        suggestedFriendsTabHeader.setBackgroundResource(R.color.transparent);
        suggestedFriendsTabHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        readingTabHeader.setBackgroundResource(R.color.transparent);
        readingTabHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
        newBooksRecyclerView.setVisibility(RecyclerView.INVISIBLE);
        suggestedFriendsRecyclerView.setVisibility(RecyclerView.INVISIBLE);
        readingRecyclerView.setVisibility(RecyclerView.INVISIBLE);

        if (IntroductionView.NEW_BOOKS_ARRIVALS_TAB_ID == selectedTabId) {
            newBooksArrivalsTabHeader.setBackgroundResource(R.drawable.background_rect_white_radius_top);
            newBooksArrivalsTabHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.greenBackground));
            newBooksRecyclerView.setVisibility(RecyclerView.VISIBLE);
        } else if (IntroductionView.SUGGESTED_FRIENDS_TAB_ID == selectedTabId) {
            suggestedFriendsTabHeader.setBackgroundResource(R.drawable.background_rect_white_radius_top);
            suggestedFriendsTabHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.greenBackground));
            suggestedFriendsRecyclerView.setVisibility(RecyclerView.VISIBLE);
        } else if (IntroductionView.READING_TAB_ID == selectedTabId) {
            readingTabHeader.setBackgroundResource(R.drawable.background_rect_white_radius_top);
            readingTabHeader.setTextColor(ContextCompat.getColor(getContext(), R.color.greenBackground));
            readingRecyclerView.setVisibility(RecyclerView.VISIBLE);
        }

        this.selectedTabId = selectedTabId;
    }

    @Override
    public void setBooksRecommendations(List<BookInfo> books) {
        recommendedBooksAdapter = new BooksAdapter(getContext(), books);
        recommendedBooksAdapter.setOnItemSelectedListener(new BooksAdapter.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int position) {
                introductionPresenter.onRecommendedBookSelected(position);
            }
        });
        newBooksRecyclerView.setAdapter(recommendedBooksAdapter);
    }

    @Override
    public void setRecommendedBookSelected(int position) {
        recommendedBooksAdapter.setSelected(position);
    }

    @Override
    public void showLevelUp(User user) {
        if (user != null) {
            loadLevelUpView(user);
        }
        levelUpLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void toggleLevelUpVisibility(User user) {
        if (levelUpLayout.getVisibility() == View.VISIBLE) {
            levelUpLayout.setVisibility(View.GONE);
        } else {
            showLevelUp(user);
        }
    }


    @Override
    public void hideLevelUp() {
        levelUpLayout.setVisibility(View.GONE);
    }

    @Override
    public void showDragonStages(User user) {
        if (!dragonStagesViewCreated && user != null) {
            createDragonStagesView(user);
        }

        dragonStagesLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void toggleDragonStagesVisibility(User user) {
        if (dragonStagesLayout.getVisibility() == View.VISIBLE) {
            dragonStagesLayout.setVisibility(View.GONE);
        } else {
            showDragonStages(user);
        }
    }

    @Override
    public void hideDragonStages() {
        dragonStagesLayout.setVisibility(View.GONE);
    }

    @Override
    public void scrollDailyReadingsLeft(int positionsCount) {
        //dailyReadingsRecyclerView.getLayoutManager().scrollToPosition(dailyReadingLinearLayoutManager.findFirstVisibleItemPosition() - 1);
        dailyReadingsRecyclerView.smoothScrollToPosition(dailyReadingLinearLayoutManager.findFirstVisibleItemPosition() - 1);
    }

    @Override
    public void scrollDailyReadingsRight(int positionsCount) {
        //dailyReadingsRecyclerView.getLayoutManager().scrollToPosition();
        dailyReadingsRecyclerView.smoothScrollToPosition(dailyReadingLinearLayoutManager.findLastVisibleItemPosition() + 1);
    }

    private void loadLevelUpView(User user) {
        if (!levelUpViewLoaded) {
            dragonPortraitLevelUpVideo.loadVideo(getDragonIntroVideoResId(user.getLevel()), true, true);
            levelInfoTextView.setText(getString(R.string.user_your_dragon_is_on_level_x, user.getName(), user.getLevel().getDragonLevel()));
            levelUpLevelProgress.setLevel(user.getLevel().getLevelCompletePercent());
            Picasso.with(getContext()).load(getDragonOffImageResId(user.getLevel().getDragonLevel() + 1)).into(nextLevelImageView);
            setDailyReadingStreakText(user.getLevel().getDailyReadings());
            dailyReadingsRecyclerView.setAdapter(new DailyReadingsAdapter(getContext(), user.getLevel().getDailyReadings()));
            // Scroll to the latest day.
            if (dailyReadingsRecyclerView.getAdapter().getItemCount() > 0) {
                dailyReadingsRecyclerView.getLayoutManager().scrollToPosition(dailyReadingsRecyclerView.getAdapter().getItemCount() - 1);
            }
            levelUpViewLoaded = true;
        }
    }

    private void updateDragonStagesView(User user) {
        String lastUpgraded = "";
        try {
            Date lastUpgradeDate = Util.parseDate(user.getLevel().getLastUpgradeDate(), Consts.DATE_TIME_FORMAT_SHORT);
            lastUpgraded = Util.formatDate(lastUpgradeDate, Consts.DATE_DAY_MONTH_YEAR_FORMAT);
        } catch (ParseException e) { /*Do nothing.*/ }

        String progressSummaryText = getResources().getQuantityString(R.plurals.progress_summary, user.getLevel().getDaysInLevel(),
                user.getName(), user.getLevel().getDragonLevel(), lastUpgraded, user.getLevel().getDaysInLevel());
        dragonStagesTextView.setText(progressSummaryText);

        if (dragonStagesViewCreated && dragonStagesViews != null) {
            for (int lev = 1; lev <= 10; lev++){
                updateDragonStageView(dragonStagesViews.get(lev), lev, user.getLevel().getDragonLevel());
            }
        }
    }

    private void createDragonStagesView(User user) {
        if (!dragonStagesViewCreated && user != null && user.getLevel() != null) {
            dragonStagesViews = new SparseArray<>();

            for (int lev = 1; lev <= 10; lev++){
                View dragonStage = LayoutInflater.from(getContext()).inflate(R.layout.item_dragon_stage, dragonStagesViewGroup, false);
                dragonStagesViews.put(lev, dragonStage);
                dragonStagesViewGroup.addView(dragonStage);

                updateDragonStageView(dragonStage, lev, user.getLevel().getDragonLevel());
            }

            dragonStagesViewCreated = true;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (emVideoView != null) {
            emVideoView.release();
        }
    }

    private void updateDragonStageView(View dragonStage, int lev, int userDragonLevel) {
        VideoPlayer videoPlayer = (VideoPlayer) dragonStage.findViewById(R.id.dragonLevelVideo);
        ImageView dragonLevelInactiveImageView = (ImageView) dragonStage.findViewById(R.id.dragonLevelInactiveImageView);

        TextView levelTextView = (TextView) dragonStage.findViewById(R.id.levelTextView);
        levelTextView.setText(String.valueOf(lev));

        if (lev <= userDragonLevel) {
            videoPlayer.setVisibility(View.VISIBLE);
            dragonLevelInactiveImageView.setVisibility(View.INVISIBLE);

            videoPlayer.loadVideo(getDragonStageAnimationResId(lev), true, true);
        } else {
            videoPlayer.setVisibility(View.INVISIBLE);
            dragonLevelInactiveImageView.setVisibility(View.VISIBLE);

            Picasso.with(getContext())
                    .load(getDragonOffImageResId(lev))
                    .into(dragonLevelInactiveImageView);
        }
    }
}
