package com.knowlge.maneno.ui.library.di;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.library.LibraryFragment;

import dagger.Subcomponent;


@ActivityScope
@Subcomponent(modules = LibraryModule.class)
public interface LibraryComponent {
    void inject(LibraryFragment libraryFragment);
}