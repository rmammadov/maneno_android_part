package com.knowlge.maneno.ui.reading;

import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.domain.model.Chapter;
import com.knowlge.maneno.ui.base.BaseActivityView;
import com.knowlge.maneno.ui.reading.model.PageData;

import java.util.List;



public interface ReadingView extends BaseActivityView {
    void setTitle(String title);

    List<PageData> loadPages(List<Chapter> pages);

    void showPage(Integer num);

    void setChapterTitle(String chapterTitle);

    void setPageNum(int currentPageNum, int pagesCount);

    void openReadingSessionSummaryView(String userName, Integer minutes, Integer pages, Integer words);

    void closeReadingSessionSummaryView();

    void updateReadingSessionSummaryView(Integer statPoints, Integer equipPoints, List<Achievement> achievements, Boolean levelCompleted);

    void updateBookDownloadProgress(int progressPercent);

    void showBookContent();

    void showChaptersMenu();

    void hideChaptersMenu();

    void setChapterSelected(int position);
}
