package com.knowlge.maneno.ui.base;


public interface BaseFragmentView extends BaseView {
    void openReadingScreen(Integer bookId);

    void showActivityIndicator();
    void hideActivityIndicator();
}
