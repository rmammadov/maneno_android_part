package com.knowlge.maneno.ui.book;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.knowlge.maneno.R;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.BookInfo;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BooksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_ITEM = 0;
    private static final int VIEW_TYPE_LOADING = 1;
    private static final int VIEW_TYPE_SECTION_HEADER = 2;

    public interface OnItemSelectedListener {
        void onItemSelected(int position);
    }

    private Context context;
    private List<Object> items; // Contains BookInfo for item, String for section title and null for loading spinner item.
    private final LayoutInflater layoutInflater;

    private OnItemSelectedListener onItemSelectedListener;
    private int selectedPosition = -1;

    private Set<Integer> sectionsIdx;

    public BooksAdapter(Context context, List<BookInfo> books) {
        this(context, null, books, false, null);
    }

    public BooksAdapter(Context context, List<BookInfo> manenoChoiceBooks, List<BookInfo> restBooks,
                        boolean showSectionTitles, RecyclerView recyclerView) {
        this.context = context;
        this.items = new ArrayList<>();
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (showSectionTitles) {
            sectionsIdx = new HashSet<>();
            sectionsIdx.add(0);
            this.items.add(context.getString(R.string.manenos_choice));

            if (manenoChoiceBooks != null && manenoChoiceBooks.size() > 0) {
                this.items.addAll(manenoChoiceBooks);
            }

            sectionsIdx.add(this.items.size());
            this.items.add(context.getString(R.string.most_popular));

            if (restBooks != null && restBooks.size() > 0) {
                this.items.addAll(restBooks);
            }

            if (recyclerView != null) {
                final GridLayoutManager gridLayoutManager = (GridLayoutManager) (recyclerView.getLayoutManager());
                gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        if (sectionsIdx.contains(position)) {
                            return gridLayoutManager.getSpanCount();
                        } else {
                            return 1;
                        }
                    }
                });
            }
        } else {
            if (manenoChoiceBooks != null && manenoChoiceBooks.size() > 0) {
                this.items.addAll(manenoChoiceBooks);
            }

            if (restBooks != null && restBooks.size() > 0) {
                this.items.addAll(restBooks);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) == null) {
            return VIEW_TYPE_LOADING;
        }

        if (items.get(position) instanceof BookInfo) {
            return VIEW_TYPE_ITEM;
        }

        return VIEW_TYPE_SECTION_HEADER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = layoutInflater.inflate(R.layout.item_book_info, parent, false);
            return new BookInfoViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = layoutInflater.inflate(R.layout.item_loading, parent, false);
            return new LoadingViewHolder(view);
        } else if (viewType == VIEW_TYPE_SECTION_HEADER) {
            View view = layoutInflater.inflate(R.layout.item_section_header, parent, false);
            return new SectionHeaderViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BookInfoViewHolder) {
            ((BookInfoViewHolder)holder).setBookInfo(context, (BookInfo) items.get(position), onItemSelectedListener, selectedPosition);
        } else if (holder instanceof LoadingViewHolder) {
            ((LoadingViewHolder)holder).setLoading();
        } else if (holder instanceof SectionHeaderViewHolder) {
            ((SectionHeaderViewHolder)holder).setTitle((String) items.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (items != null) {
            return items.size();
        }

        return 0;
    }

    public void setSelected(int newSelectedPosition) {
        if (newSelectedPosition >= 0 && newSelectedPosition != selectedPosition
                && newSelectedPosition < items.size()) {
            int oldSelectedPosition = selectedPosition;
            selectedPosition = newSelectedPosition;

            notifyItemChanged(oldSelectedPosition);
            notifyItemChanged(selectedPosition);
        }
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public void showLoadingNextBooks() {
        items.add(null);
        notifyItemInserted(items.size() - 1);
    }

    public void insertMoreBooks(List<BookInfo> newBooks) {
        // Remove loading element if exists.
        if (items != null && items.size() > 0
            && items.get(items.size() - 1) == null) {
            items.remove(items.size() - 1);
            notifyItemRemoved(items.size());
        }

        if (newBooks != null && newBooks.size() > 0) {
            if (items == null) {
                items = new ArrayList<>();
            }

            items.addAll(newBooks);
        }

        notifyDataSetChanged();
    }

    static class SectionHeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleTextView) TextView titleTextView;
        @BindView(R.id.horizontalLine) View horizontalLine;

        SectionHeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setTitle(String title) {
            if (getAdapterPosition() > 0) {
                horizontalLine.setVisibility(View.VISIBLE);
            } else {
                // Line is hidden for first section header.
                horizontalLine.setVisibility(View.GONE);
            }

            titleTextView.setText(title);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.progressBar) ProgressBar progressBar;

        LoadingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setLoading() {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setIndeterminate(true);
        }
    }

    static class BookInfoViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemBookInfoLayout) View itemBookInfoLayout;
        @BindView(R.id.coverImageView) ImageView coverImageView;
        @BindView(R.id.titleTextView) TextView titleTextView;
        @BindView(R.id.authorTextView) TextView authorTextView;
        @BindView(R.id.readStatsTextView) TextView readStatsTextView;
        @BindView(R.id.levelCircleImageView) ImageView levelCircleImageView;
        @BindView(R.id.icPagesImageView) ImageView icPagesImageView;
        @BindView(R.id.totalPagesTextView) TextView totalPagesTextView;
        @BindView(R.id.icLevelImageView) ImageView icLevelImageView;
        @BindView(R.id.levelTextView) TextView levelTextView;


        BookInfoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setBookInfo(Context context, final BookInfo bookInfo, final OnItemSelectedListener onItemSelectedListener, int selectedPosition) {
            final int position = getAdapterPosition();

            if (position == selectedPosition) {
                itemBookInfoLayout.setBackgroundResource(R.drawable.background_item_book_info_selected);
                titleTextView.setTextColor(ContextCompat.getColor(context, R.color.white));
                authorTextView.setTextColor(ContextCompat.getColor(context, R.color.white));
                readStatsTextView.setTextColor(ContextCompat.getColor(context, R.color.white));
                totalPagesTextView.setTextColor(ContextCompat.getColor(context, R.color.white));
                levelTextView.setTextColor(ContextCompat.getColor(context, R.color.white));
            } else {
                itemBookInfoLayout.setBackgroundResource(R.drawable.background_item_book_info);
                titleTextView.setTextColor(ContextCompat.getColor(context, R.color.defaultFontColor));
                authorTextView.setTextColor(ContextCompat.getColor(context, R.color.defaultFontColor));
                readStatsTextView.setTextColor(ContextCompat.getColor(context, R.color.defaultFontColor));
                totalPagesTextView.setTextColor(ContextCompat.getColor(context, R.color.defaultFontColor));
                levelTextView.setTextColor(ContextCompat.getColor(context, R.color.defaultFontColor));
            }

            // Load data.
            Picasso.with(context)
                    .load(bookInfo.getThumbnailUrl())
                    .into(coverImageView);

            titleTextView.setText(bookInfo.getName());
            authorTextView.setText(bookInfo.getAuthorName());
            if (bookInfo.getLevel() != null) {
                levelTextView.setText(bookInfo.getLevel());
                levelTextView.setVisibility(View.VISIBLE);
                icLevelImageView.setVisibility(View.VISIBLE);
            } else {
                levelTextView.setVisibility(View.INVISIBLE);
                icLevelImageView.setVisibility(View.INVISIBLE);
            }

            if (bookInfo.getColorCodeObj() != null
                    && Util.isSet(bookInfo.getColorCodeObj().getIconUrl())) {
                levelCircleImageView.setVisibility(View.VISIBLE);
                Picasso.with(context)
                        .load(bookInfo.getColorCodeObj().getIconUrl())
                        .into(levelCircleImageView);
            } else {
                levelCircleImageView.setVisibility(View.INVISIBLE);
            }

            Integer totalPages = bookInfo.getPagesCount();

            if (totalPages != null) {
                Integer pagesRead = bookInfo.getPagesRead();
                if (pagesRead == null) {
                    pagesRead = 0;
                }
                readStatsTextView.setVisibility(TextView.VISIBLE);
                readStatsTextView.setText(context.getString(R.string.read_pages, pagesRead, totalPages));
                totalPagesTextView.setText(String.valueOf(totalPages));
                icPagesImageView.setVisibility(View.VISIBLE);
                totalPagesTextView.setVisibility(View.VISIBLE);
            } else {
                readStatsTextView.setVisibility(TextView.INVISIBLE);
                icPagesImageView.setVisibility(View.GONE);
                totalPagesTextView.setVisibility(View.GONE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onItemSelectedListener != null && position != RecyclerView.NO_POSITION) {
                        onItemSelectedListener.onItemSelected(position);
                    }
                }
            });
        }
    }
}
