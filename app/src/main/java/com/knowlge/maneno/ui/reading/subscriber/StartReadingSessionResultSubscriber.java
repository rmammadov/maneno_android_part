package com.knowlge.maneno.ui.reading.subscriber;

import com.knowlge.maneno.domain.usecase.reading.StartReadingSessionUseCase;
import com.knowlge.maneno.ui.base.BaseSubscriber;
import com.knowlge.maneno.ui.reading.ReadingPresenter;



public class StartReadingSessionResultSubscriber extends BaseSubscriber<StartReadingSessionUseCase.Result> {

    private final ReadingPresenter readingPresenter;

    public StartReadingSessionResultSubscriber(ReadingPresenter readingPresenter) {
        super(readingPresenter.getLog());
        this.readingPresenter = readingPresenter;
    }

    @Override
    public void onNext(StartReadingSessionUseCase.Result result) {
        super.onNext(result);

        if (result != null) {
            readingPresenter.onReadingSessionStarted(result.getOffsetInBook());
        }
    }
}
