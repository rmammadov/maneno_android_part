package com.knowlge.maneno.ui.base;


public interface Presenter<V extends BaseView> {

    void attachView(V view);

    void detachView();
}