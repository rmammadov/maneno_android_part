package com.knowlge.maneno.ui.login.subscriber;

import com.knowlge.maneno.domain.usecase.login.LoginResult;
import com.knowlge.maneno.ui.base.BaseSubscriber;
import com.knowlge.maneno.ui.login.LoginPresenter;


public class LoginResultSubscriber extends BaseSubscriber<LoginResult> {

    private LoginPresenter loginPresenter;

    public LoginResultSubscriber(LoginPresenter loginPresenter) {
        super(loginPresenter.getLog());
        this.loginPresenter = loginPresenter;
    }

    @Override
    public void onNext(LoginResult loginResult) {
        super.onNext(loginResult);
        loginPresenter.onLoginResult(loginResult);
    }

    @Override
    public void onError(Throwable e) {
        super.onError(e);
        loginPresenter.onLoginResult(new LoginResult(false, LoginResult.ERR_CODE_UNILOGIN_USER_LOGIN_ERROR));
    }
}
