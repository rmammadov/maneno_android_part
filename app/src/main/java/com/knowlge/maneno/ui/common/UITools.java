package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.util.DisplayMetrics;



public class UITools {
    public static float dpToPx(Context context, float dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return dp * displayMetrics.density + 0.5f;
    }
}
