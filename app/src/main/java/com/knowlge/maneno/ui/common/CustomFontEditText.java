package com.knowlge.maneno.ui.common;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.knowlge.maneno.R;


public class CustomFontEditText extends AppCompatEditText {

    public CustomFontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontEditText);

        setTypeface(Typefaces.get(context, typedArray.getString(R.styleable.CustomFontEditText_font)));

        typedArray.recycle();
    }
}
