package com.knowlge.maneno.ui.animations;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.widget.VideoView;


public class Player {

    public static void playAnimation(Context context, final VideoView videoView, final View videoPlaceholder, int videoResId,
                                     final boolean looping, final PauseVideoRunnable pauseVideoRunnable, final Integer pauseTimeMillis) {
        Uri video = Uri.parse("android.resource://" + context.getPackageName() + "/" + videoResId);
        videoView.setVideoURI(video);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(looping);
                videoView.start();
                if (videoPlaceholder != null) {
                    mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
                            if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                                videoPlaceholder.setVisibility(View.INVISIBLE);
                                return true;
                            }
                            return false;
                        }
                    });
                }

                if (pauseTimeMillis != null && pauseTimeMillis > 0) {
                    if (pauseVideoRunnable != null) {
                        new Handler().postDelayed(pauseVideoRunnable, pauseTimeMillis);
                    } else {
                        new Handler().postDelayed(new PauseVideoRunnable(videoView), pauseTimeMillis);
                    }
                }
            }

        });
    }

    public static void playAnimation(Context context, VideoView videoView, View videoPlaceholder, int videoResId, boolean loop) {
        playAnimation(context, videoView, videoPlaceholder, videoResId, loop, null, null);
    }
}
