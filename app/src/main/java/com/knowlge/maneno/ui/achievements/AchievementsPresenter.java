package com.knowlge.maneno.ui.achievements;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.domain.usecase.achievements.GetAchievementsUseCase;
import com.knowlge.maneno.ui.base.BasePresenter;
import com.knowlge.maneno.ui.base.BaseSubscriber;

import java.util.List;

import javax.inject.Inject;


@ActivityScope
public class AchievementsPresenter extends BasePresenter<AchievementsView> {

    @Inject
    GetAchievementsUseCase getAchievementsUseCase;

    @Inject
    public AchievementsPresenter(AchievementsView view) {
        super(view);
    }

    public void open() {
        getView().showActivityIndicator();

        getAchievementsUseCase.execute(new BaseSubscriber<List<Achievement>>(getLog()) {
            @Override
            public void onNext(List<Achievement> achievements) {
                super.onNext(achievements);
                getView().setAchievements(achievements);
                getView().hideActivityIndicator();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                getView().setAchievements(null);
                getView().hideActivityIndicator();
            }
        }, new GetAchievementsUseCase.Params(false));
    }
}
