package com.knowlge.maneno.ui.main;

import com.knowlge.maneno.di.ActivityScope;
import com.knowlge.maneno.ui.base.BasePresenter;

import javax.inject.Inject;


@ActivityScope
public class MainPresenter extends BasePresenter<MainView> {

    @Inject
    public MainPresenter(MainView view) {
        super(view);
    }

    public void onMenuItemSelected(int menuItemId) {
        getView().setItemSelected(menuItemId);
        getView().openPage(menuItemId);
        getView().closeNavigation();
    }

    public void onLogoutClicked() {
        // todo - call logout usecase.

        // todo - ON RESULT: Close Main screen and open login screen.
        getView().finish();
        getView().openLoginScreen();
    }
}
