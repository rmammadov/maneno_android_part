package com.knowlge.maneno.ui.base;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.knowlge.maneno.ManenoApplication;
import com.knowlge.maneno.di.components.ApplicationComponent;
import com.knowlge.maneno.ui.bookdetails.BookDetailsListActivity;
import com.knowlge.maneno.ui.reading.ReadingActivity;

import java.util.ArrayList;


public abstract class BaseFragment extends Fragment implements BaseFragmentView {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupFragmentComponent();
    }

    protected BaseActivity getBaseActivity() {
        return (BaseActivity)getActivity();
    }

    protected abstract void setupFragmentComponent();

    protected ApplicationComponent getApplicationComponent() {
        return ((ManenoApplication)getActivity().getApplicationContext()).getApplicationComponent();
    }

    protected boolean isLandscapeOrientation() {
        return getContext().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    @Override
    public void openBookDetails(ArrayList<Integer> bookListIds, Integer selectedPosition) {
        startActivity(BookDetailsListActivity.getCallingIntent(getContext(), bookListIds, selectedPosition));
    }

    @Override
    public void openReadingScreen(Integer bookId) {
        startActivity(ReadingActivity.getCallingIntent(getContext(), bookId));
    }

    @Override
    public void showActivityIndicator() {
        getBaseActivity().showActivityIndicator();
    }

    @Override
    public void hideActivityIndicator() {
        getBaseActivity().hideActivityIndicator();
    }
}
