package com.knowlge.maneno.executor;

import com.knowlge.maneno.domain.executor.PostExecutionThread;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;


@Singleton
public class MainUIThread implements PostExecutionThread {

    @Inject
    public MainUIThread() {}

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
