package com.knowlge.maneno.executor;

import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class AppExecutor implements Executor {

    private static final int CORE_POOL_SIZE = 3;

    private static final int MAXIMUM_POOL_SIZE = 5;

    private static final int KEEP_ALIVE_TIME = 15;

    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;

    private final ThreadPoolExecutor threadPoolExecutor;

    @Inject
    public AppExecutor() {
        threadPoolExecutor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE,
                KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, new LinkedBlockingQueue<Runnable>(), new AppThreadFactory());
    }

    @Override
    public void execute(Runnable runnable) {
        if (runnable == null) {
            throw new IllegalArgumentException("Cannot execute null runnable.");
        }

        threadPoolExecutor.execute(runnable);
    }

    private static class AppThreadFactory implements ThreadFactory {
        private static final String PREFIX = "com_knowlge_maneno";
        private static int counter = 0;

        @Override
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, PREFIX + counter++);
        }
    }
}
