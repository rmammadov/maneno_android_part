package com.knowlge.maneno.data.db.dao.user;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.dao.Dao;
import com.knowlge.maneno.data.db.table.user.ChapterTable;
import com.knowlge.maneno.domain.model.Chapter;

import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.BODY_HTML;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.BOOK_ID;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.CONTENT_PATH;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.IMAGES;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.INDEX;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.TITLE;


public class ChapterDao extends Dao<Chapter, ChapterTable> {

    public ChapterDao(SQLiteDatabase db, ChapterTable table) {
        super(db);
        this.table = table;
    }

    @Override
    public long save(Chapter entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getBookId());
        bind(insertStatement, 2, entity.getContentPath());
        bind(insertStatement, 3, entity.getTitle());
        bind(insertStatement, 4, entity.getIndex());
        bind(insertStatement, 5, entity.getBodyHtml());
        bind(insertStatement, 6, entity.getImages());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(Chapter entity) {
        throw new UnsupportedOperationException();
    }

    @Override
    protected Chapter buildFromCursor(Cursor c) {
        Chapter chapter = null;

        if (c != null) {
            chapter = new Chapter();

            chapter.setBookId(getInt(BOOK_ID, c));
            chapter.setContentPath(getString(CONTENT_PATH, c));
            chapter.setTitle(getString(TITLE, c));
            chapter.setIndex(getInt(INDEX, c));
            chapter.setBodyHtml(getString(BODY_HTML, c));
            chapter.setImages(getMap(IMAGES, c));
        }

        return chapter;
    }
}
