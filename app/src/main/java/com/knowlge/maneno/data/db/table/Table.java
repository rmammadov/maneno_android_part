package com.knowlge.maneno.data.db.table;

import android.database.sqlite.SQLiteDatabase;



public abstract class Table {
    protected static final String TYPE_INT = "INTEGER";
    protected static final String TYPE_TEXT = "TEXT";

    public abstract String getTableName();
    public abstract String getPrimaryKey();
    public abstract String [] getColumns();
    protected abstract String getType(String columnName);

    // For now only one column indexes are supported.
    protected String [] getIndexedColumns() { return null; }

    public void onCreate(SQLiteDatabase db) {
        String createSql = "CREATE TABLE " + getTableName() + "(";

        String colName;
        for (int i = 0; i < getColumns().length; i++) {
            colName = getColumns()[i];

            createSql += colName + " " + getType(colName);
            if (colName.equals(getPrimaryKey())) {
                if (isPrimaryKeyAutoincrement()) {
                    createSql += " PRIMARY KEY AUTOINCREMENT";
                } else {
                    createSql += " PRIMARY KEY";
                }
            }

            // If not last column.
            if (i < getColumns().length-1) {
                createSql += ",";
            }
        }
        createSql += ");";

        db.execSQL(createSql);

        // Create indexes.
        if (getIndexedColumns() != null) {
            // CREATE INDEX name_idx ON table_name(id);
            for (String idxCol : getIndexedColumns()) {
                db.execSQL("CREATE INDEX " + idxCol +"_idx ON " + getTableName() + "(" + idxCol + ");");
            }
        }
    }

    protected boolean isPrimaryKeyAutoincrement() {
        // False by default.
        return false;
    }

    public void onUpgrade(SQLiteDatabase db) {
        // By default, recreate whole table.
        db.execSQL("DROP TABLE IF EXISTS " + getTableName());
        onCreate(db);
    }
}
