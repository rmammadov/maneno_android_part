package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.ReadingSessionTable;
import com.knowlge.maneno.domain.model.ReadingSession;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.BOOK_ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.CHAR_OFFSET;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.END_READING_DATE_MILLIS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.NUMBER_OF_WORDS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.READING_SESSION_ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.READING_STATISTICS_ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.READ_START_TIME_MILLIS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.SESSION_TIME_INTERVAL;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.STATUS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.TIME_INTERVAL;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.UNKNOWN_WORDS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.USER_ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.USER_READING_HISTORY_ID;


@Singleton
public class ReadingSessionDao extends Dao<ReadingSession, ReadingSessionTable> {

    @Inject
    public ReadingSessionDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(ReadingSession entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getId()); // Always null in entity - autoincrement column.
        bind(insertStatement, 2, entity.getUserId());
        bind(insertStatement, 3, entity.getStatus());
        bind(insertStatement, 4, entity.getEndReadingDateMillis());
        bind(insertStatement, 5, entity.getTimeInterval());
        bind(insertStatement, 6, entity.getSessionTimeInterval());
        bind(insertStatement, 7, entity.getCharOffset());
        bind(insertStatement, 8, entity.getUnknownWords());
        bind(insertStatement, 9, entity.getNumberOfWords());
        bind(insertStatement, 10, entity.getReadStartTimeMillis());
        bind(insertStatement, 11, entity.getBookId());
        bind(insertStatement, 12, entity.getReadingSessionId());
        bind(insertStatement, 13, entity.getUserReadingHistoryId());
        bind(insertStatement, 14, entity.getReadingStatisticsId());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(ReadingSession entity) {
        if (entity != null ) {
            final ContentValues values = new ContentValues();

            put(values, USER_ID, entity.getUserId());
            put(values, STATUS, entity.getStatus());
            put(values, END_READING_DATE_MILLIS, entity.getEndReadingDateMillis());
            put(values, TIME_INTERVAL, entity.getTimeInterval());
            put(values, SESSION_TIME_INTERVAL, entity.getSessionTimeInterval());
            put(values, CHAR_OFFSET, entity.getCharOffset());
            put(values, UNKNOWN_WORDS, entity.getUnknownWords());
            put(values, NUMBER_OF_WORDS, entity.getNumberOfWords());
            put(values, READ_START_TIME_MILLIS, entity.getReadStartTimeMillis());
            put(values, BOOK_ID, entity.getBookId());
            put(values, READING_SESSION_ID, entity.getReadingSessionId());
            put(values, USER_READING_HISTORY_ID, entity.getUserReadingHistoryId());
            put(values, READING_STATISTICS_ID, entity.getReadingStatisticsId());

            return update(values, String.valueOf(entity.getId()));
        }

        return 0;
    }

    @Override
    protected ReadingSession buildFromCursor(Cursor c) {
        ReadingSession readingSession = null;

        if (c != null) {
            readingSession = new ReadingSession();

            readingSession.setId(getLong(ID, c));
            readingSession.setUserId(getInt(USER_ID, c));
            readingSession.setStatus(getInt(STATUS, c));
            readingSession.setEndReadingDateMillis(getLong(END_READING_DATE_MILLIS, c));
            readingSession.setTimeInterval(getInt(TIME_INTERVAL, c));
            readingSession.setSessionTimeInterval(getInt(SESSION_TIME_INTERVAL, c));
            readingSession.setCharOffset(getInt(CHAR_OFFSET, c));
            readingSession.setUnknownWords(getString(UNKNOWN_WORDS, c));
            readingSession.setNumberOfWords(getInt(NUMBER_OF_WORDS, c));
            readingSession.setReadStartTimeMillis(getLong(READ_START_TIME_MILLIS, c));
            readingSession.setBookId(getInt(BOOK_ID, c));
            readingSession.setReadingSessionId(getInt(READING_SESSION_ID, c));
            readingSession.setUserReadingHistoryId(getString(USER_READING_HISTORY_ID, c));
            readingSession.setReadingStatisticsId(getString(READING_STATISTICS_ID, c));
        }

        return readingSession;
    }
}
