package com.knowlge.maneno.data.db;


public class DBConsts {
    public static final String DB_NAME = "maneno.db";
    public static final int DB_VERSION = 1;

    public static final int USER_DB_VERSION = 1;
    public static String getUserDbName(Integer userId) {
        return "maneno.user_" + userId + ".db";
    }
}
