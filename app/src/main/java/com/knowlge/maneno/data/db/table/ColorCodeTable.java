package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.ColorCodeTable.ColorCodeColumns.ICON_URL;
import static com.knowlge.maneno.data.db.table.ColorCodeTable.ColorCodeColumns.ID;
import static com.knowlge.maneno.data.db.table.ColorCodeTable.ColorCodeColumns.NAME;


@Singleton
public class ColorCodeTable extends Table {

    @Inject
    public ColorCodeTable() {}

    private static final String TABLE_NAME = "color_codes";
    private static final String[] COLUMNS = new String[] {ID, NAME, ICON_URL};

    public static class ColorCodeColumns {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String ICON_URL = "icon_url";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(ID, TYPE_INT);
        types.put(NAME, TYPE_TEXT);
        types.put(ICON_URL, TYPE_TEXT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return ID;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
