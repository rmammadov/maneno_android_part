package com.knowlge.maneno.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.knowlge.maneno.data.db.table.Table;
import com.knowlge.maneno.data.db.table.user.BookInfoTable;
import com.knowlge.maneno.data.db.table.user.ChapterTable;



public class UserDBOpenHelper extends SQLiteOpenHelper {

    private final Table[] TABLES;

    private BookInfoTable bookInfoTable;
    private ChapterTable chapterTable;

    public UserDBOpenHelper(Context context, Integer userId) {
        super(context, DBConsts.getUserDbName(userId), null, DBConsts.USER_DB_VERSION);

        this.bookInfoTable = new BookInfoTable();
        this.chapterTable = new ChapterTable();

        TABLES = new Table[] {bookInfoTable, chapterTable};
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (Table t: TABLES) {
            t.onCreate(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (Table t: TABLES) {
            t.onUpgrade(db);
        }
    }

    public BookInfoTable getBookInfoTable() {
        return bookInfoTable;
    }

    public ChapterTable getChapterTable() {
        return chapterTable;
    }
}
