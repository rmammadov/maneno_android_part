package com.knowlge.maneno.data.db.table.user;

import com.knowlge.maneno.data.db.table.Table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.AUTHOR_ID;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.AUTHOR_NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.BACKGROUND_COLOR;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.BOOK_LOAD_DATE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.COLOR_CODE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.DICTIONARY_FILE_PATH;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.HAS_ORIGINAL_NARRATION;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.HAS_TASKS;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.ID;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.IS_AVAILABLE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.IS_PRIVATE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LANGUAGE_ID;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LAST_TIME_OPENED;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LATEST_TEXT_OFFSET;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LEVEL;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LIKE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LIKES_COUNT;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LIX;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LIX_VALUE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.MANENOS_CHOICE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NARRATION_FILE_NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NARRATION_FILE_PATH;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NARRATION_JSON_PATH;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NARRATION_METADATA_FILE_NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NUMBER_OF_READS;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.PAGES_READ;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.PAGES_TOTAL;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.PURGE_DATE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.RELEASE_DATE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.SERIES_NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.SERIES_TAG_ID;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.SUMMARY;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TAG_LIST;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.THUMBNAIL_URL;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TIMESTAMP;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TOTAL_CHAPTERS;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TOTAL_CHARACTERS;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TOTAL_PAGES;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TOTAL_WORD_COUNT;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.UNIQUE_FOLDER_NAME;



public class BookInfoTable extends Table {

    private static final String TABLE_NAME = "book_info";
    private static final String[] COLUMNS = new String[] {ID, NAME, TOTAL_WORD_COUNT, TOTAL_CHAPTERS,
            TOTAL_PAGES, AUTHOR_ID, RELEASE_DATE, LIKE, LIKES_COUNT, AUTHOR_NAME, SUMMARY, HAS_ORIGINAL_NARRATION,
            NARRATION_FILE_NAME, NARRATION_METADATA_FILE_NAME, THUMBNAIL_URL, IS_PRIVATE, HAS_TASKS,
            LANGUAGE_ID, TOTAL_CHARACTERS, MANENOS_CHOICE, BACKGROUND_COLOR, TIMESTAMP, PURGE_DATE,
            NUMBER_OF_READS, IS_AVAILABLE, SERIES_NAME, SERIES_TAG_ID, LIX, LEVEL, LIX_VALUE,
            LAST_TIME_OPENED, PAGES_READ, PAGES_TOTAL, LATEST_TEXT_OFFSET, BOOK_LOAD_DATE, UNIQUE_FOLDER_NAME,
            DICTIONARY_FILE_PATH, NARRATION_FILE_PATH, NARRATION_JSON_PATH, TAG_LIST, COLOR_CODE};

    public static class BookInfoColumns {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String TOTAL_WORD_COUNT = "total_word_count";
        public static final String TOTAL_CHAPTERS = "total_chapters";
        public static final String TOTAL_PAGES = "total_pages";
        public static final String AUTHOR_ID = "author_id";
        public static final String RELEASE_DATE = "release_date";
        public static final String LIKE = "like";
        public static final String LIKES_COUNT = "likes_count";
        public static final String AUTHOR_NAME = "author_name";
        public static final String SUMMARY = "summary";
        public static final String HAS_ORIGINAL_NARRATION = "has_original_narration";
        public static final String NARRATION_FILE_NAME = "narration_file_name";
        public static final String NARRATION_METADATA_FILE_NAME = "narration_metadata_file_name";
        public static final String THUMBNAIL_URL = "thumbnail_url";
        public static final String IS_PRIVATE = "is_private";
        public static final String HAS_TASKS = "has_tasks";
        public static final String LANGUAGE_ID = "language_id";
        public static final String TOTAL_CHARACTERS = "total_characters";
        public static final String MANENOS_CHOICE = "manenos_choice";
        public static final String BACKGROUND_COLOR = "background_color";
        public static final String TIMESTAMP = "timestamp";
        public static final String PURGE_DATE = "purge_date";
        public static final String NUMBER_OF_READS = "number_of_reads";
        public static final String IS_AVAILABLE = "is_available";
        public static final String SERIES_NAME = "series_name";
        public static final String SERIES_TAG_ID = "series_tag_id";
        public static final String LIX = "lix";
        public static final String LEVEL = "level";
        public static final String LIX_VALUE = "lix_value";
        public static final String LAST_TIME_OPENED = "last_time_opened";
        public static final String PAGES_READ = "pages_read";
        public static final String PAGES_TOTAL = "pages_total";
        public static final String LATEST_TEXT_OFFSET = "latest_text_offset";
        public static final String BOOK_LOAD_DATE = "book_load_date";
        public static final String UNIQUE_FOLDER_NAME = "unique_folder_name";
        public static final String DICTIONARY_FILE_PATH = "dictionary_file_path";
        public static final String NARRATION_FILE_PATH = "narration_file_path";
        public static final String NARRATION_JSON_PATH = "narration_json_path";
        public static final String TAG_LIST = "tag_list";
        public static final String COLOR_CODE = "color_code";

        // One to many relations: todo - handle in separate tables.
//        private List<Chapter> chapters;
//        private List<BookFile> files = null;
//        private List<BaseUser> friendsReading = null;
//        private List<BaseUser> friendsRecommend = null;
//        private List<BookPackage> packages = null;
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(ID, TYPE_INT);
        types.put(NAME, TYPE_TEXT);
        types.put(TOTAL_WORD_COUNT, TYPE_INT);
        types.put(TOTAL_CHAPTERS, TYPE_INT);
        types.put(TOTAL_PAGES, TYPE_INT);
        types.put(AUTHOR_ID, TYPE_INT);
        types.put(RELEASE_DATE, TYPE_TEXT);
        types.put(LIKE, TYPE_INT);
        types.put(LIKES_COUNT, TYPE_INT);
        types.put(AUTHOR_NAME, TYPE_TEXT);
        types.put(SUMMARY, TYPE_TEXT);
        types.put(HAS_ORIGINAL_NARRATION, TYPE_INT);
        types.put(NARRATION_FILE_NAME, TYPE_TEXT);
        types.put(NARRATION_METADATA_FILE_NAME, TYPE_TEXT);
        types.put(THUMBNAIL_URL, TYPE_TEXT);
        types.put(IS_PRIVATE, TYPE_INT);
        types.put(HAS_TASKS, TYPE_INT);
        types.put(LANGUAGE_ID, TYPE_INT);
        types.put(TOTAL_CHARACTERS, TYPE_INT);
        types.put(MANENOS_CHOICE, TYPE_INT);
        types.put(BACKGROUND_COLOR, TYPE_TEXT);
        types.put(TIMESTAMP, TYPE_TEXT);
        types.put(PURGE_DATE, TYPE_TEXT);
        types.put(NUMBER_OF_READS, TYPE_INT);
        types.put(IS_AVAILABLE, TYPE_INT);
        types.put(SERIES_NAME, TYPE_TEXT);
        types.put(SERIES_TAG_ID, TYPE_INT);
        types.put(LIX, TYPE_TEXT);
        types.put(LEVEL, TYPE_TEXT);
        types.put(LIX_VALUE, TYPE_INT);
        types.put(LAST_TIME_OPENED, TYPE_INT);
        types.put(PAGES_READ, TYPE_INT);
        types.put(PAGES_TOTAL, TYPE_INT);
        types.put(LATEST_TEXT_OFFSET, TYPE_INT);
        types.put(BOOK_LOAD_DATE, TYPE_INT);
        types.put(UNIQUE_FOLDER_NAME, TYPE_TEXT);
        types.put(DICTIONARY_FILE_PATH, TYPE_TEXT);
        types.put(NARRATION_FILE_PATH, TYPE_TEXT);
        types.put(NARRATION_JSON_PATH, TYPE_TEXT);
        types.put(TAG_LIST, TYPE_TEXT);
        types.put(COLOR_CODE, TYPE_INT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return ID;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
