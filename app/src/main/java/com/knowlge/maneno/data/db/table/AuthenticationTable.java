package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.AuthenticationTable.AuthenticationColumns.ACCESS_TOKEN;
import static com.knowlge.maneno.data.db.table.AuthenticationTable.AuthenticationColumns.EXPIRES_IN;
import static com.knowlge.maneno.data.db.table.AuthenticationTable.AuthenticationColumns.TOKEN_TYPE;
import static com.knowlge.maneno.data.db.table.AuthenticationTable.AuthenticationColumns.USER_ID;


@Singleton
public class AuthenticationTable extends Table {

    @Inject
    public AuthenticationTable() {}

    private static final String TABLE_NAME = "authentication";
    private static final String[] COLUMNS = new String[] {USER_ID, ACCESS_TOKEN, TOKEN_TYPE, EXPIRES_IN};

    public static class AuthenticationColumns {
        public static final String USER_ID = "user_id";
        public static final String ACCESS_TOKEN = "access_token";
        public static final String TOKEN_TYPE = "token_type";
        public static final String EXPIRES_IN = "expires_in";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(USER_ID, TYPE_INT);
        types.put(ACCESS_TOKEN, TYPE_TEXT);
        types.put(TOKEN_TYPE, TYPE_TEXT);
        types.put(EXPIRES_IN, TYPE_INT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return USER_ID;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
