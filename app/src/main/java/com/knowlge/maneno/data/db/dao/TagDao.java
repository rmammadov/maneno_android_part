package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.TagTable;
import com.knowlge.maneno.domain.model.Tag;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.COLOR_CODE;
import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.ICON_URL;
import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.ID;
import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.NAME;
import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.TAG_TYPE_ID;


@Singleton
public class TagDao extends Dao<Tag, TagTable> {

    @Inject
    public TagDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(Tag entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getId());
        bind(insertStatement, 2, entity.getName());
        bind(insertStatement, 3, entity.getTagTypeId());
        bind(insertStatement, 4, entity.getIconUrl());
        bind(insertStatement, 5, entity.getColorCode());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(Tag entity) {
        if (entity != null ) {
            final ContentValues values = new ContentValues();

            put(values, NAME, entity.getName());
            put(values, TAG_TYPE_ID, entity.getTagTypeId());
            put(values, ICON_URL, entity.getIconUrl());
            put(values, COLOR_CODE, entity.getColorCode());

            return update(values, String.valueOf(entity.getId()));
        }

        return 0;
    }

    @Override
    protected Tag buildFromCursor(Cursor c) {
        Tag tag = null;

        if (c != null) {
            tag = new Tag();

            tag.setId(getInt(ID, c));
            tag.setName(getString(NAME, c));
            tag.setTagTypeId(getInt(TAG_TYPE_ID, c));
            tag.setIconUrl(getString(ICON_URL, c));
            tag.setColorCode(getInt(COLOR_CODE, c));
        }

        return tag;
    }
}
