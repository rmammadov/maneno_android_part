package com.knowlge.maneno.data;

import android.content.Context;
import android.util.SparseArray;

import com.knowlge.maneno.data.db.AppDBManager;
import com.knowlge.maneno.data.db.UserDBManager;
import com.knowlge.maneno.domain.log.Log;
import com.knowlge.maneno.domain.model.AppData;
import com.knowlge.maneno.domain.model.AuthenticationData;
import com.knowlge.maneno.domain.model.AuthenticationResult;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.Chapter;
import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Country;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.Level;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.model.School;
import com.knowlge.maneno.domain.model.Tag;
import com.knowlge.maneno.domain.model.User;
import com.knowlge.maneno.domain.model.UserLevel;
import com.knowlge.maneno.domain.repository.LocalRepository;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class LocalDataRepository implements LocalRepository {

    @Inject
    Context context;

    @Inject
    AppDBManager dbManager;

    @Inject
    Log log;

    private Integer currentUserId;
    private Long currentReadingSessionId;
    private Long currentReadingPageId;

    // Arrays with user related DB managers. Objects created dynamically when necessary.
    private SparseArray<UserDBManager> userDBManagers = new SparseArray<>();

    private SparseArray<ColorCode> colorCodes;

    @Inject
    LocalDataRepository() {}

    private UserDBManager getUserDBManager() {
        Integer currentUserId = getCurrentUserId();

        UserDBManager result = null;
        if (currentUserId != null) {
            synchronized (LocalRepository.class) {
                result = userDBManagers.get(currentUserId);

                if (result == null) {
                    result = new UserDBManager(context, currentUserId);
                    userDBManagers.put(currentUserId, result);
                }
            }
        }

        return result;
    }

    @Override
    public Integer getCurrentUserId() {
        if (currentUserId == null) {
            currentUserId = dbManager.getCurrentUserId();
        }

        return currentUserId;
    }

    @Override
    public User getCurrentUser() {
        Integer userId = dbManager.getCurrentUserId();
        if (userId != null) {
            return dbManager.findUserById(userId.toString());
        }

        return null;
    }

    @Override
    public User findUser(String username) {
        User u = dbManager.findUserByUsername(username);
        if (u == null) {
            u = dbManager.findUserByEmail(username);
        }

        return u;
    }

    @Override
    public void updateUser(User user) {
        dbManager.updateUser(user);
    }

    @Override
    public boolean saveAuthenticationData(AuthenticationResult authResult) {
        dbManager.beginTransaction();
        try {
            // Store current logged in user id.
            currentUserId = authResult.getUser().getId();
            dbManager.setAppData(AppData.KEY_CURRENT_USER_ID, currentUserId);

            dbManager.saveAuthenticationData(new AuthenticationData(authResult));
            dbManager.saveUser(authResult.getUser());

            // todo - update user tags.

            // todo - update user level, stats, abilities.
            dbManager.saveUserLevel(authResult.getUser().getId(), authResult.getUser().getLevel());

            dbManager.setTransactionSuccessful();

            return true;
        } catch (Exception e) {
            // todo - log exception properly.
            android.util.Log.e("DB", "Err", e);
        } finally {
            dbManager.endTransaction();
        }

        return false;
    }

    @Override
    public Level getUserLevel(Integer userId) {
        if (userId != null) {
            UserLevel userLevel = dbManager.findUserLevel(userId);
            if (userLevel != null) {
                return userLevel.getLevel();
            }
        }
        return null;
    }

    @Override
    public AuthenticationData getAuthenticationData() {
        return dbManager.getAuthenticationData(getCurrentUserId());
    }

    @Override
    public boolean updateDictionaryData(List<Tag> tags, List<Language> languages, List<Country> countries, List<School> schools) {
        dbManager.beginTransaction();
        try {
            // Update all dictionaries in one transaction, so whole operation is much faster.
            dbManager.saveTags(tags);
            dbManager.saveLanguages(languages);
            dbManager.saveCountries(countries);
            dbManager.saveSchools(schools);

            dbManager.setTransactionSuccessful();
            log.d("All dictionaries updated successfully!");

            return true;
        } catch (Exception e) {
            log.e(e);
        } finally {
            dbManager.endTransaction();
        }

        return false;
    }

    @Override
    public void updateTags(List<Tag> tags) {
        if (tags != null && tags.size() > 0) {
            dbManager.beginTransaction();
            try {
                // Update all tags in one transaction, so whole operation is much faster.
                dbManager.saveTags(tags);

                dbManager.setTransactionSuccessful();
            } catch (Exception e) {
                log.e(e);
            } finally {
                dbManager.endTransaction();
            }
        }
    }

    @Override
    public void updateLanguages(List<Language> languages) {
        if (languages != null && languages.size() > 0) {
            dbManager.beginTransaction();
            try {
                // Update all languages in one transaction, so whole operation is much faster.
                dbManager.saveLanguages(languages);

                dbManager.setTransactionSuccessful();
            } catch (Exception e) {
                log.e(e);
            } finally {
                dbManager.endTransaction();
            }
        }
    }

    @Override
    public void updateColorCodes(List<ColorCode> colorCodes) {
        if (colorCodes != null && colorCodes.size() > 0) {
            dbManager.beginTransaction();
            try {
                // Update all colorCodes in one transaction, so whole operation is much faster.
                dbManager.saveColorCodes(colorCodes);

                dbManager.setTransactionSuccessful();
            } catch (Exception e) {
                log.e(e);
            } finally {
                dbManager.endTransaction();
            }
        }
    }

    @Override
    public void updateCountries(List<Country> countries) {
        if (countries != null && countries.size() > 0) {
            dbManager.beginTransaction();
            try {
                // Update all countries in one transaction, so whole operation is much faster.
                dbManager.saveCountries(countries);

                dbManager.setTransactionSuccessful();
            } catch (Exception e) {
                log.e(e);
            } finally {
                dbManager.endTransaction();
            }
        }
    }

    @Override
    public void updateSchools(List<School> schools) {
        if (schools != null && schools.size() > 0) {
            dbManager.beginTransaction();
            try {
                // Update all schools in one transaction, so whole operation is much faster.
                dbManager.saveSchools(schools);

                dbManager.setTransactionSuccessful();
            } catch (Exception e) {
                log.e(e);
            } finally {
                dbManager.endTransaction();
            }
        }
    }

    @Override
    public List<Language> getLanguages() {
        return dbManager.getLanguages();
    }

    @Override
    public Tag getTag(Integer tagId) {
        // todo - Check memory cache when implemented.
        return dbManager.getTag(tagId);
    }

    @Override
    public List<Tag> getTags(List<Integer> tagIds) {
        // todo - check if all tags were put to memory cache. If not, create memory cache with all tags.
        return dbManager.getTags(tagIds);
    }

    @Override
    public List<Tag> getTagsByType(Integer typeId) {
        return dbManager.getTagsByType(typeId);
    }

    @Override
    public void setCurrentReadingSessionId(Long sessionId) {
        currentReadingSessionId = dbManager.setCurrentReadingSessionId(sessionId);
    }

    @Override
    public Long getCurrentReadingSessionId() {
        if (currentReadingSessionId == null) {
            currentReadingSessionId = dbManager.getCurrentReadingSessionId();
        }

        return currentReadingSessionId;
    }

    @Override
    public ReadingSession getReadingSession(Long sessionId) {
        return dbManager.getReadingSession(sessionId);
    }

    @Override
    public void updateReadingSession(ReadingSession readingSession) {
        dbManager.updateReadingSession(readingSession);
    }

    @Override
    public List<ReadingSession> getReadingSessions(Integer userId) {
        return dbManager.getReadingSessions(userId);
    }

    @Override
    public void deleteReadingSession(ReadingSession session) {
        dbManager.deleteReadingSession(session);
    }

    @Override
    public long saveReadingSession(ReadingSession readingSession) {
        return dbManager.saveReadingSession(readingSession);
    }

    @Override
    public void setCurrentReadingPageId(Long readingPageId) {
        //In memory only. No save in local storage is needed.
        currentReadingPageId = readingPageId;
    }

    @Override
    public Long getCurrentReadingPageId() {
        return currentReadingPageId;
    }

    @Override
    public ReadingPageStatistics getReadingPageStatistics(Long readingPageStatisticsId) {
        return dbManager.getReadingPageStatistics(readingPageStatisticsId);
    }

    @Override
    public List<ReadingPageStatistics> getReadingPagesStatistics(Long readingSessionId) {
        return dbManager.getReadingPagesStatistics(readingSessionId);
    }

    @Override
    public long saveReadingPageStatistics(ReadingPageStatistics readingPageStatistics) {
        return dbManager.saveReadingPageStatistics(readingPageStatistics);
    }

    @Override
    public void updateReadingPageStatistics(ReadingPageStatistics readingPageStatistics) {
        dbManager.updateReadingPageStatistics(readingPageStatistics);
    }

    @Override
    public int getDistinctPagesCount(Long readingSessionId) {
        return dbManager.getDistinctPagesCount(readingSessionId);
    }

    @Override
    public int getDistinctWordsReadCount(Long readingSessionId) {
        return dbManager.getDistinctWordsReadCount(readingSessionId);
    }

    @Override
    public void deleteReadingPagesStatistics(List<ReadingPageStatistics> readingPagesStatistics) {
        if (readingPagesStatistics != null && readingPagesStatistics.size() > 0) {
            String[] ids = new String[readingPagesStatistics.size()];

            for (int idx = 0; idx < readingPagesStatistics.size(); idx++) {
                ids[idx] = String.valueOf(readingPagesStatistics.get(idx).getId());
            }

            dbManager.deleteReadingPagesStatistics(ids);
        }
    }


    /*** UserDBManager related methods. ***/

    @Override
    public BookInfo getBookInfo(Integer bookId, boolean withChapters) {
        // return cachedBooks.get(bookId); // todo - create books cache but per user.

        UserDBManager userDBManager = getUserDBManager();

        if (userDBManager != null) {
            BookInfo bookInfo = userDBManager.getBookInfo(bookId);

            if (bookInfo != null) {
                if (withChapters) {
                    List<Chapter> bookChapters = getBookChapters(bookId);
                    if (bookChapters != null && bookChapters.size() > 0) {
                        bookInfo.setChapters(bookChapters);
                    }
                }

                setBookInfoRelations(bookInfo);

                // todo - get other related objects from other tables.

                return bookInfo;
            }
        }

        return null;
    }

    private void setBookInfoRelations(BookInfo bookInfo) {
        if (bookInfo != null) {
            bookInfo.setColorCodeObj(getColorCode(bookInfo.getColorCode()));

            // todo - others?
        }
    }

    @Override
    public ColorCode getColorCode(Integer colorCodeId) {
        if (colorCodes == null) {
            List<ColorCode> colors = getColorCodes();
            if (colors != null) {
                colorCodes = new SparseArray<>();
                for (ColorCode c: colors) {
                    colorCodes.put(c.getId(), c);
                }
            }
        }

        if (colorCodes != null) {
            return colorCodes.get(colorCodeId);
        }

        return null;
    }

    @Override
    public List<ColorCode> getColorCodes() {
        return dbManager.getColorCodes();
    }

    @Override
    public void saveBookInfo(BookInfo bookInfo) {
        UserDBManager userDBManager = getUserDBManager();

        if (userDBManager != null) {
            userDBManager.saveBookInfo(bookInfo);
        }
    }

    @Override
    public void updateBookInfo(BookInfo bookInfo) {
        // cachedBooks.put(bookInfo.getId(), bookInfo); // todo - create books cache but per user.

        UserDBManager userDBManager = getUserDBManager();

        if (userDBManager != null) {
            userDBManager.updateBookInfo(bookInfo);
        }
    }

    @Override
    public Map<Integer, BookInfo> getBookInfosMap(List<Integer> ids) {
        UserDBManager userDBManager = getUserDBManager();

        if (userDBManager != null) {
            Map<Integer, BookInfo> bookInfosMap = userDBManager.getBookInfosMap(ids);

            if (bookInfosMap != null) {
                for (BookInfo bookInfo: bookInfosMap.values()) {
                    setBookInfoRelations(bookInfo);
                }
            }
            return bookInfosMap;
        }

        return null;
    }

    private List<Chapter> getBookChapters(Integer bookId) {
        UserDBManager userDBManager = getUserDBManager();

        if (userDBManager != null) {
            return userDBManager.getBookChapters(bookId);
        }

        return null;
    }

    @Override
    public void saveBookChapters(Integer bookId, List<Chapter> chapters) {
        if (bookId != null) {
            deleteBookChapters(bookId);

            if (chapters != null) {
                UserDBManager userDBManager = getUserDBManager();

                if (userDBManager != null) {
                    userDBManager.saveBookChapters(chapters);
                }
            }
        }
    }

    @Override
    public void deleteBookChapters(Integer bookInfoId) {
        UserDBManager userDBManager = getUserDBManager();

        if (userDBManager != null) {
            userDBManager.deleteBookChapters(bookInfoId);
        }
    }

    @Override
    public void updateUserLevel(Integer userId, Level level) {
        dbManager.saveUserLevel(userId, level);
    }
}
