package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.CAN_ADD_NEW_ABILITY;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.CAN_IMPROVE_ABILITY;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DAYS_ACTIVE_IN_LEVEL;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DAYS_IN_LEVEL;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DAYS_REMAINING;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DAY_SINCE_UPGRADE;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DRAGON_COLOR;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DRAGON_LEVEL;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.FREE_EQUIP_POINTS;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.FREE_STAT_POINTS;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.LAST_UPGRADE_DATE;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.LEVEL_COMPLETE_PERCENT;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.MINUTES_REMAINING;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.MINUTES_REMAINING_TODAY;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.MOOD;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.MOOD_SCORE;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.PROGRESS_WAS_RESETED;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.STAT_POINTS_LIMIT_REACHED;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.USER_ID;


@Singleton
public class UserLevelTable extends Table {

    @Inject
    public UserLevelTable() {}

    private static final String TABLE_NAME = "user_level";
    private static final String[] COLUMNS = new String[] {USER_ID, LAST_UPGRADE_DATE, DAY_SINCE_UPGRADE, FREE_STAT_POINTS,
            FREE_EQUIP_POINTS, DAYS_ACTIVE_IN_LEVEL, DAYS_IN_LEVEL, LEVEL_COMPLETE_PERCENT, MINUTES_REMAINING, MINUTES_REMAINING_TODAY,
            DAYS_REMAINING, CAN_ADD_NEW_ABILITY, CAN_IMPROVE_ABILITY, STAT_POINTS_LIMIT_REACHED, MOOD, MOOD_SCORE,
            DRAGON_COLOR, PROGRESS_WAS_RESETED, DRAGON_LEVEL };

    public static class UserLevelColumns {
        public static final String USER_ID = "user_id";
        public static final String LAST_UPGRADE_DATE = "last_upgrade_date";
        public static final String DAY_SINCE_UPGRADE = "day_since_upgrade";
        public static final String FREE_STAT_POINTS = "free_stat_points";
        public static final String FREE_EQUIP_POINTS = "free_equip_points";
        public static final String DAYS_ACTIVE_IN_LEVEL = "days_active_in_level";
        public static final String DAYS_IN_LEVEL = "days_in_level";
        public static final String LEVEL_COMPLETE_PERCENT = "level_complete_percent";
        public static final String MINUTES_REMAINING = "minutes_remaining";
        public static final String MINUTES_REMAINING_TODAY = "minutes_remaining_today";
        public static final String DAYS_REMAINING = "days_remaining";
        public static final String CAN_ADD_NEW_ABILITY = "can_add_new_ability";
        public static final String CAN_IMPROVE_ABILITY = "can_improve_ability";
        public static final String STAT_POINTS_LIMIT_REACHED = "stat_points_limit_reached";
        public static final String MOOD = "mood";
        public static final String MOOD_SCORE = "mood_score";
        public static final String DRAGON_COLOR = "dragon_color";
        public static final String PROGRESS_WAS_RESETED = "progress_was_reseted";
        public static final String DRAGON_LEVEL = "dragon_level";


//        private List<Stat> stats = null;
//        private List<String> downgradeDates = null;
//        private List<DailyReading> dailyReadings = null;
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(USER_ID, TYPE_INT);
        types.put(LAST_UPGRADE_DATE, TYPE_TEXT);
        types.put(DAY_SINCE_UPGRADE, TYPE_INT);
        types.put(FREE_STAT_POINTS, TYPE_INT);
        types.put(FREE_EQUIP_POINTS, TYPE_INT);
        types.put(DAYS_ACTIVE_IN_LEVEL, TYPE_INT);
        types.put(DAYS_IN_LEVEL, TYPE_INT);
        types.put(LEVEL_COMPLETE_PERCENT, TYPE_INT);
        types.put(MINUTES_REMAINING, TYPE_INT);
        types.put(MINUTES_REMAINING_TODAY, TYPE_INT);
        types.put(DAYS_REMAINING, TYPE_INT);
        types.put(CAN_ADD_NEW_ABILITY, TYPE_INT);
        types.put(CAN_IMPROVE_ABILITY, TYPE_INT);
        types.put(STAT_POINTS_LIMIT_REACHED, TYPE_INT);
        types.put(MOOD, TYPE_INT);
        types.put(MOOD_SCORE, TYPE_INT);
        types.put(DRAGON_COLOR, TYPE_INT);
        types.put(PROGRESS_WAS_RESETED, TYPE_INT);
        types.put(DRAGON_LEVEL, TYPE_INT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return USER_ID;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
