package com.knowlge.maneno.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.knowlge.maneno.data.db.table.AppDataTable;
import com.knowlge.maneno.data.db.table.AuthenticationTable;
import com.knowlge.maneno.data.db.table.ColorCodeTable;
import com.knowlge.maneno.data.db.table.CountryTable;
import com.knowlge.maneno.data.db.table.LanguageTable;
import com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable;
import com.knowlge.maneno.data.db.table.ReadingSessionTable;
import com.knowlge.maneno.data.db.table.SchoolTable;
import com.knowlge.maneno.data.db.table.Table;
import com.knowlge.maneno.data.db.table.TagTable;
import com.knowlge.maneno.data.db.table.UserLevelTable;
import com.knowlge.maneno.data.db.table.UserTable;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class AppDBOpenHelper extends SQLiteOpenHelper {

    private final Table[] TABLES;

    @Inject
    public AppDBOpenHelper(Context context, UserTable userTable, AuthenticationTable authenticationTable,
                           AppDataTable appDataTable, UserLevelTable userLevelTable, TagTable tagTable, CountryTable countryTable,
                           LanguageTable languageTable, SchoolTable schoolTable, ReadingSessionTable readingSessionTable,
                           ReadingPageStatisticsTable readingPageStatisticsTable, ColorCodeTable colorCodeTable) {
        super(context, DBConsts.DB_NAME, null, DBConsts.DB_VERSION);

        TABLES = new Table[] {userTable, authenticationTable, appDataTable, userLevelTable, tagTable, countryTable,
                languageTable, schoolTable, readingSessionTable, readingPageStatisticsTable, colorCodeTable};
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (Table t: TABLES) {
            t.onCreate(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (Table t: TABLES) {
            t.onUpgrade(db);
        }
    }
}
