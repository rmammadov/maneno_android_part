package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.LanguageTable.LanguageColumns.CODE;
import static com.knowlge.maneno.data.db.table.LanguageTable.LanguageColumns.ID;
import static com.knowlge.maneno.data.db.table.LanguageTable.LanguageColumns.NAME;


@Singleton
public class LanguageTable extends Table {

    @Inject
    public LanguageTable() {}

    private static final String TABLE_NAME = "languages";
    private static final String[] COLUMNS = new String[] {ID, NAME, CODE};

    public static class LanguageColumns {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String CODE = "code";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(ID, TYPE_INT);
        types.put(NAME, TYPE_TEXT);
        types.put(CODE, TYPE_TEXT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return ID;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
