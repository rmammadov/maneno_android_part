package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.ADDRESS;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.CITY;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.COUNTRY_ID;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.ID;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.NAME;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.ZIP_CODE;


@Singleton
public class SchoolTable extends Table {

    @Inject
    public SchoolTable() {}

    private static final String TABLE_NAME = "schools";
    private static final String[] COLUMNS = new String[] {ID, NAME, COUNTRY_ID, ADDRESS, CITY, ZIP_CODE};

    public static class SchoolColumns {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String COUNTRY_ID = "country_id"; // todo - make index on this column.
        public static final String ADDRESS = "address";
        public static final String CITY = "city";
        public static final String ZIP_CODE = "zip_code";

    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(ID, TYPE_INT);
        types.put(NAME, TYPE_TEXT);
        types.put(COUNTRY_ID, TYPE_INT);
        types.put(ADDRESS, TYPE_TEXT);
        types.put(CITY, TYPE_TEXT);
        types.put(ZIP_CODE, TYPE_INT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    protected String[] getIndexedColumns() {
        return new String[] {COUNTRY_ID};
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return ID;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
