package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.LanguageTable;
import com.knowlge.maneno.domain.model.Language;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.LanguageTable.LanguageColumns.CODE;
import static com.knowlge.maneno.data.db.table.LanguageTable.LanguageColumns.ID;
import static com.knowlge.maneno.data.db.table.LanguageTable.LanguageColumns.NAME;


@Singleton
public class LanguageDao extends Dao<Language, LanguageTable> {

    @Inject
    public LanguageDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(Language entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getId());
        bind(insertStatement, 2, entity.getName());
        bind(insertStatement, 3, entity.getCode());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(Language entity) {
        if (entity != null ) {
            final ContentValues values = new ContentValues();

            put(values, NAME, entity.getName());
            put(values, CODE, entity.getCode());

            return update(values, String.valueOf(entity.getId()));
        }

        return 0;
    }

    @Override
    protected Language buildFromCursor(Cursor c) {
        Language language = null;

        if (c != null) {
            language = new Language();

            language.setId(getInt(ID, c));
            language.setName(getString(NAME, c));
            language.setCode(getString(CODE, c));
        }

        return language;
    }
}
