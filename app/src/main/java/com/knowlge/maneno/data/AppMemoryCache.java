package com.knowlge.maneno.data;

import android.util.SparseArray;

import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.ReadingProgress;
import com.knowlge.maneno.domain.repository.MemoryCache;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class AppMemoryCache implements MemoryCache {

    private Map<Long, ReadingProgress> readingSessionProgress = new HashMap<>();
    private SparseArray<BookInfo> bookInfos = new SparseArray<>();

    @Inject
    public AppMemoryCache() {}

    @Override
    public void putLatestReadingProgress(Long sessionId, ReadingProgress readingProgress) {
        if (sessionId != null) {
            readingSessionProgress.put(sessionId, readingProgress);
        }
    }

    @Override
    public ReadingProgress getLatestReadingProgress(Long sessionId) {
        if (sessionId != null) {
            return readingSessionProgress.get(sessionId);
        }
        return null;
    }

    @Override
    public BookInfo getBookInfo(Integer bookId, boolean withChapters) {
        // todo - handle with chapters?
        return bookInfos.get(bookId);
    }

    @Override
    public void put(BookInfo bookInfo) {
        if (bookInfo != null) {
            bookInfos.put(bookInfo.getId(), bookInfo);
        }
    }
}
