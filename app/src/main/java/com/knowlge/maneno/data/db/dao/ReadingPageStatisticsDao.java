package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.ID;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.PAGE_END_TIME;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.PAGE_NUMBER;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.PAGE_START_TIME;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.SESSION_ID;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.WORDS_READ;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.WORD_INDEX_END;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.WORD_INDEX_START;



@Singleton
public class ReadingPageStatisticsDao extends Dao<ReadingPageStatistics, ReadingPageStatisticsTable> {

    @Inject
    public ReadingPageStatisticsDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(ReadingPageStatistics entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getId()); // Always null in entity - autoincrement column.
        bind(insertStatement, 2, entity.getSessionId());
        bind(insertStatement, 3, entity.getPageNumber());
        bind(insertStatement, 4, entity.getWordIndexStart());
        bind(insertStatement, 5, entity.getWordIndexEnd());
        bind(insertStatement, 6, entity.getPageStartTime());
        bind(insertStatement, 7, entity.getPageEndTime());
        bind(insertStatement, 8, entity.getWordsRead());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(ReadingPageStatistics entity) {
        if (entity != null ) {
            final ContentValues values = new ContentValues();

            put(values, SESSION_ID, entity.getSessionId());
            put(values, PAGE_NUMBER, entity.getPageNumber());
            put(values, WORD_INDEX_START, entity.getWordIndexStart());
            put(values, WORD_INDEX_END, entity.getWordIndexEnd());
            put(values, PAGE_START_TIME, entity.getPageStartTime());
            put(values, PAGE_END_TIME, entity.getPageEndTime());
            put(values, WORDS_READ, entity.getWordsRead());

            return update(values, String.valueOf(entity.getId()));
        }

        return 0;
    }

    @Override
    protected ReadingPageStatistics buildFromCursor(Cursor c) {
        ReadingPageStatistics readingPageStatistics = null;

        if (c != null) {
            readingPageStatistics = new ReadingPageStatistics();

            readingPageStatistics.setId(getLong(ID, c));
            readingPageStatistics.setSessionId(getLong(SESSION_ID, c));
            readingPageStatistics.setPageNumber(getInt(PAGE_NUMBER, c));
            readingPageStatistics.setWordIndexStart(getInt(WORD_INDEX_START, c));
            readingPageStatistics.setWordIndexEnd(getInt(WORD_INDEX_END, c));
            readingPageStatistics.setPageStartTime(getString(PAGE_START_TIME, c));
            readingPageStatistics.setPageEndTime(getString(PAGE_END_TIME, c));
            readingPageStatistics.setWordsRead(getInt(WORDS_READ, c));
        }

        return readingPageStatistics;
    }

    public int getDistinctPagesCount(Long readingSessionId) {
        if (readingSessionId != null) {
            long result = DatabaseUtils.longForQuery(db, "select count(DISTINCT " + PAGE_NUMBER + ") FROM " + getTableName() +
                    " WHERE " + SESSION_ID + " = ?", new String[]{readingSessionId.toString()});

            return (int) result;
        }


        return 0;
    }

    public int getDistinctWordsReadCount(Long readingSessionId) {
        if (readingSessionId != null) {
            long result = DatabaseUtils.longForQuery(db, "select sum(" + WORDS_READ + ") FROM (SELECT " + WORDS_READ +
                    " FROM " + getTableName() +
                    " WHERE " + SESSION_ID + " = ? GROUP BY " + PAGE_NUMBER + ")", new String[]{readingSessionId.toString()});

            return (int) result;
        }
        return 0;
    }
}
