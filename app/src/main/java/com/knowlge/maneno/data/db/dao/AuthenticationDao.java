package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.AuthenticationTable;
import com.knowlge.maneno.data.db.table.AuthenticationTable.AuthenticationColumns;
import com.knowlge.maneno.domain.model.AuthenticationData;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class AuthenticationDao extends Dao<AuthenticationData, AuthenticationTable> {

    @Inject
    public AuthenticationDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(AuthenticationData entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getUserId());
        bind(insertStatement, 2, entity.getAccessToken());
        bind(insertStatement, 3, entity.getTokenType());
        bind(insertStatement, 4, entity.getExpiresIn());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(AuthenticationData entity) {
        if (entity != null) {
            final ContentValues values = new ContentValues();

            put(values, AuthenticationColumns.ACCESS_TOKEN, entity.getAccessToken());
            put(values, AuthenticationColumns.TOKEN_TYPE, entity.getTokenType());
            put(values, AuthenticationColumns.EXPIRES_IN, entity.getExpiresIn());

            return update(values, String.valueOf(entity.getUserId()));
        }

        return 0;
    }

    @Override
    protected AuthenticationData buildFromCursor(Cursor c) {
        AuthenticationData authData = null;

        if (c != null) {
            authData = new AuthenticationData();
            authData.setUserId(getInt(AuthenticationColumns.USER_ID, c));
            authData.setAccessToken(getString(AuthenticationColumns.ACCESS_TOKEN, c));
            authData.setTokenType(getString(AuthenticationColumns.TOKEN_TYPE, c));
            authData.setExpiresIn(getInt(AuthenticationColumns.EXPIRES_IN, c));
        }

        return authData;
    }
}
