package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.AppDataTable.AppDataColumns.KEY;
import static com.knowlge.maneno.data.db.table.AppDataTable.AppDataColumns.VALUE;


@Singleton
public class AppDataTable extends Table {

    @Inject
    public AppDataTable() {}

    private static final String TABLE_NAME = "app_data";
    private static final String[] COLUMNS = new String[] {KEY, VALUE};

    public static class AppDataColumns {
        public static final String KEY = "key";
        public static final String VALUE = "value";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(KEY, TYPE_TEXT);
        types.put(VALUE, TYPE_TEXT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return KEY;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
