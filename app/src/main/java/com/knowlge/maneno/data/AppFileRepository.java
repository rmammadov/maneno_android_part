package com.knowlge.maneno.data;

import android.content.Context;

import com.google.common.io.Files;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.repository.FileRepository;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class AppFileRepository implements FileRepository {

    @Inject
    Context context;

    @Inject
    public AppFileRepository() {}

    @Override
    public void saveFile(String fileName, InputStream inputStream) throws IOException {
        // Save file in internal storage.
        Files.asByteSink(new File(context.getFilesDir(), fileName)).writeFrom(inputStream);

        // todo - add possibility to save books on external storage.
    }

    @Override
    public void unzipFileInto(String zipFileName, String destinationDirectoryName) throws IOException {
        File zipFile = new File(context.getFilesDir(), zipFileName);
        if (!zipFile.exists()) {
            // todo - check external storage.
        }

        unzipFileInto(zipFile, destinationDirectoryName);
    }

    @Override
    public void unzipFileInto(File zipFile, String destinationDirectoryName) throws IOException {
        File destDir = new File(context.getFilesDir(), destinationDirectoryName);
        if (!destDir.exists()) {
            // todo - check external storage if zipFile is on external storage.
            destDir.mkdir();
        }

        Util.unzip(zipFile, destDir);
    }

    @Override
    public boolean delete(String fileName) throws IOException {
        File file = new File(context.getFilesDir(), fileName);
        if (!file.exists()) {
            // todo - check external storage.
        }

        return file.delete();
    }

    @Override
    public boolean delete(File file) throws IOException {
        if (file != null) {
            return file.delete();
        }
        return false;
    }

    @Override
    public File[] getFiles(String folderName) {
        File folder = new File(context.getFilesDir(), folderName);
        if (!folder.exists()) {
            // todo - check external storage.
        }

        return folder.listFiles();
    }

    @Override
    public boolean exists(String filePath) {
        File file = new File(context.getFilesDir(), filePath);
        if (!file.exists()) {
            // todo - check external storage.
        }

        return file.exists();
    }

    @Override
    public File getFile(String filePath) {
        File file = new File(context.getFilesDir(), filePath);
        if (!file.exists()) {
            // todo - check external storage.
            return null;
        }

        return file;
    }

    @Override
    public String readFile(String filePath) throws IOException {
        File file = new File(context.getFilesDir(), filePath);
        if (!file.exists()) {
            // todo - check external storage.
            return null;
        }

        return Files.toString(file, Charset.forName("UTF-8"));
    }

    @Override
    public String getParentAbsolutePath(String filePath) {
        File file = new File(context.getFilesDir(), filePath);
        if (!file.exists()) {
            // todo - check external storage.
            return null;
        }

        return file.getParent();
    }
}
