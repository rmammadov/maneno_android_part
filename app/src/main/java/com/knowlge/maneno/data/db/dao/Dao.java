package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.knowlge.maneno.data.db.table.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

public abstract class Dao<M, T extends Table> {

	@Inject
	protected T table;

	private static final int TRUE_VALUE = 1;
	private static final int FALSE_VALUE = 0;

	private static final String VALUES_SEPARATOR = "|";
	private static final String KEY_VALUE_SEPARATOR = ":=";

	protected final SQLiteDatabase db;

	private SQLiteStatement insertStatement;
	protected SQLiteStatement getInsertStatement() {
		if(insertStatement == null) {
			insertStatement = db.compileStatement(generateInsertStatement(getTableName(), getColumns()));
		}

		return insertStatement;
	}

	public abstract long save(M entity);
	public abstract int update(M entity);

	public String getTableName() {
		return table.getTableName();
	}

	public String[] getColumns() {
		return table.getColumns();
	}

	public String getPrimaryKey() {
		return table.getPrimaryKey();
	}

	protected abstract M buildFromCursor(Cursor c);

	public Dao(SQLiteDatabase db) {
		this.db = db;
	}

	public void insertOrUpdate(M entity) {
		int updatedRows = update(entity);
		if (updatedRows == 0) {
			save(entity);
		}
	}

	public M get(String id) {
		return findBy(getPrimaryKey(), id);
	}

	public List<M> getAll() {
		List<M> result = new ArrayList<>();

		Cursor c = db.query(getTableName(), getColumns(), null, null, null, null, null);

		if (c.moveToFirst()) {
			do {
				M item = this.buildFromCursor(c);
				if (item != null) {
					result.add(item);
				}
			} while (c.moveToNext());
		}

		if (!c.isClosed()) {
			c.close();
		}

		return result;
	}

	public M findBy(String columnName, String value) {
		return findBy(columnName, value, true);
	}

	public List<M> findBy(List<Integer> ids) {
		if (ids != null && ids.size() > 0) {
			String[] strIds = new String[ids.size()];
			for (int i = 0; i < ids.size(); i++) {
				strIds[i] = String.valueOf(ids.get(i));
			}

			return findBy(strIds);
		}
		return null;
	}

	public List<M> findBy(String[] ids) {
		List<M> result = new ArrayList<>();

		Cursor c = db.query(getTableName(), getColumns(), getPrimaryKey() + " IN (" + makePlaceholders(ids.length) + ")", ids, null, null, null);

		if (c.moveToFirst()) {
			do {
				M element = this.buildFromCursor(c);
				if (element != null) {
					result.add(element);
				}
			} while (c.moveToNext());
		}

		if (!c.isClosed()) {
			c.close();
		}

		return result;
	}

	public List<M> findAllBy(String columnName, String value) {
		List<M> result = new ArrayList<>();

		Cursor c = db.query(getTableName(), getColumns(), columnName + " = ?", new String[] {value}, null, null, null);

		if (c.moveToFirst()) {
			do {
				M item = this.buildFromCursor(c);
				if (item != null) {
					result.add(item);
				}
			} while (c.moveToNext());
		}

		if (!c.isClosed()) {
			c.close();
		}

		return result;
	}

	// Returns first find result.
	public M findBy(String columnName, String value, boolean ignoreCase) {
		M result = null;

		String selection;
		String [] selectionArgs;

		if (ignoreCase) {
			selection = "lower("+columnName+")" + " = ?";
			selectionArgs = new String[] { value.toLowerCase() };
		} else {
			selection = columnName + " = ?";
			selectionArgs = new String[] { value };
		}

		Cursor c = db.query(getTableName(), getColumns(), selection, selectionArgs, null, null, null, "1");

		if (c.moveToFirst()) {
			result = this.buildFromCursor(c);
		}

		if (!c.isClosed()) {
			c.close();
		}

		return result;
	}

	protected int update(ContentValues values, String primaryKeyValue) {
		return db.update(getTableName(), values, getPrimaryKey() + " = ?", new String[] {primaryKeyValue});
	}

	protected void bind(SQLiteStatement statement, int idx, Boolean value) {
		if (value != null) {
			if (value) {
				statement.bindLong(idx, TRUE_VALUE);
			} else {
				statement.bindLong(idx, FALSE_VALUE);
			}
		} else {
			statement.bindNull(idx);
		}
	}

	protected void bind(SQLiteStatement statement, int idx, String value) {
		if(value != null) {
			statement.bindString(idx, value);
		} else {
			statement.bindNull(idx);
		}
	}
	
	protected void bind(SQLiteStatement statement, int idx, Long value) {
		if(value != null) {
			statement.bindLong(idx, value);
		} else {
			statement.bindNull(idx);
		}
	}
	
	protected void bind(SQLiteStatement statement, int idx, Integer value) {
		if(value != null) {
			statement.bindLong(idx, value);
		} else {
			statement.bindNull(idx);
		}
	}
	
	protected void bind(SQLiteStatement statement, int idx, byte[] content) {
		if(content != null) {
			statement.bindBlob(idx, content);
		} else {
			statement.bindNull(idx);
		}
	}

	protected void bind(SQLiteStatement statement, int idx, Map<String, String> values) {
		if (values != null) {
			statement.bindString(idx, Joiner.on(VALUES_SEPARATOR).withKeyValueSeparator(KEY_VALUE_SEPARATOR).join(values));
		} else  {
			statement.bindNull(idx);
		}
	}

	// Bind list of integer as string comma separated values.
	protected void bind(SQLiteStatement statement, int idx, List<Integer> values) {
		if (values != null) {
			statement.bindString(idx, Joiner.on(VALUES_SEPARATOR).join(values));
		} else  {
			statement.bindNull(idx);
		}
	}

	// We assume that values in database are saved in text as comma separated integers.
	protected List<Integer> getIntList(String columnName, Cursor c) {
		int idx = c.getColumnIndexOrThrow(columnName);
		if (!c.isNull(idx)) {
			String values = c.getString(idx);

			if (values != null && values.length() > 0) {
				List<Integer> result = new ArrayList<>();

				for (String s : Splitter.on(VALUES_SEPARATOR).split(values)) {
					result.add(Integer.valueOf(s));
				}
				return result;
			}
		}

		return null;
	}

	protected Map<String, String> getMap(String columnName, Cursor c) {
		int idx = c.getColumnIndexOrThrow(columnName);
		if (!c.isNull(idx)) {
			String values = c.getString(idx);

			if (values != null && values.length() > 0) {
				return Splitter.on(VALUES_SEPARATOR).withKeyValueSeparator(KEY_VALUE_SEPARATOR).split(values);
			}
		}

		return null;
	}

	protected Integer getInt(String columnName, Cursor c) {
		int idx = c.getColumnIndexOrThrow(columnName);
		if (c.isNull(idx)) {
			return null;
		} else {
			return c.getInt(idx);
		}
	}

	protected Long getLong(String columnName, Cursor c) {
		int idx = c.getColumnIndexOrThrow(columnName);
		if (c.isNull(idx)) {
			return null;
		} else {
			return c.getLong(idx);
		}
	}

	protected String getString(String columnName, Cursor c) {
		int idx = c.getColumnIndexOrThrow(columnName);
		if (c.isNull(idx)) {
			return null;
		} else {
			return c.getString(idx);
		}
	}

	protected boolean getBoolean(String columnName, Cursor c) {
		return c.getInt(c.getColumnIndexOrThrow(columnName)) == TRUE_VALUE;
	}

	protected void put(ContentValues values, String key, List<Integer> value) {
		if (value != null) {
			values.put(key, Joiner.on(VALUES_SEPARATOR).join(value));
		} else {
			values.putNull(key);
		}
	}

	protected void put(ContentValues values, String key, Boolean value) {
		if (value != null) {
			if (value) {
				values.put(key, TRUE_VALUE);
			} else {
				values.put(key, FALSE_VALUE);
			}
		} else {
			values.putNull(key);
		}
	}

	protected void put(ContentValues values, String key, String value) {
		if (value != null) {
			values.put(key, value);
		} else {
			values.putNull(key);
		}
	}

	protected void put(ContentValues values, String key, Integer value) {
		if (value != null) {
			values.put(key, value);
		} else {
			values.putNull(key);
		}
	}


	protected void put(ContentValues values, String key, Long value) {
		if (value != null) {
			values.put(key, value);
		} else {
			values.putNull(key);
		}
	}

	private String generateInsertStatement(String tableName, String [] columns) {
		String stmt = "insert into " + tableName + "(";

		for (int i = 0; i < columns.length - 1; i++) {
			stmt += columns[i] + ",";
		}

		stmt += columns[columns.length - 1] + ") values (";

		for (int i = 0; i < columns.length - 1; i++) {
			stmt += "?,";
		}

		stmt += "?)";

		return  stmt;
	}

	private String makePlaceholders(int len) {
		if (len < 1) {
			throw new RuntimeException("No possibility to create query placeholders.");
		} else {
			StringBuilder sb = new StringBuilder(len * 2 - 1);
			sb.append("?");
			for (int i = 1; i < len; i++) {
				sb.append(",?");
			}
			return sb.toString();
		}
	}

	public int delete(String[] ids) {
		return db.delete(getTableName(), getPrimaryKey() + " IN (" + makePlaceholders(ids.length) + ")", ids);
	}

	public int deleteBy(String columnName, String value) {
		return db.delete(getTableName(), columnName + " = ? ", new String[] {value});
	}
}
