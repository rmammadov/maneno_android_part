package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.UserTable;
import com.knowlge.maneno.data.db.table.UserTable.UserColumns;
import com.knowlge.maneno.domain.model.User;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class UserDao extends Dao<User, UserTable> {

    @Inject
    public UserDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(User user) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, user.getId());
        bind(insertStatement, 2, user.getEmail());
        bind(insertStatement, 3, user.getSecurityStamp());
        bind(insertStatement, 4, user.getCity());
        bind(insertStatement, 5, user.getCountryId());
        bind(insertStatement, 6, user.getLanguageId());
        bind(insertStatement, 7, user.getState());
        bind(insertStatement, 8, user.getUserTypeId());
        bind(insertStatement, 9, user.getIsAuthor());
        bind(insertStatement, 10, user.getIsUser());
        bind(insertStatement, 11, user.getIsTeacher());
        bind(insertStatement, 12, user.getGender());
        bind(insertStatement, 13, user.getLastLoginDate());
        bind(insertStatement, 14, user.getSignupDate());
        bind(insertStatement, 15, user.getNumberOfLogins());
        bind(insertStatement, 16, user.getReadingLevel());
        bind(insertStatement, 17, user.getSchoolId());
        bind(insertStatement, 18, user.getName());
        bind(insertStatement, 19, user.getUserName());
        bind(insertStatement, 20, user.isUniloginUser());
        bind(insertStatement, 21, user.getPasswordHash());
        bind(insertStatement, 22, user.isRememberMe());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(User user) {
        if (user != null) {
            final ContentValues values = new ContentValues();

            put(values, UserColumns.EMAIL, user.getEmail());
            put(values, UserColumns.SECURITY_STAMP, user.getSecurityStamp());
            put(values, UserColumns.CITY, user.getCity());
            put(values, UserColumns.COUNTRY_ID, user.getCountryId());
            put(values, UserColumns.LANGUAGE_ID, user.getLanguageId());
            put(values, UserColumns.STATE, user.getState());
            put(values, UserColumns.USER_TYPE_ID, user.getUserTypeId());
            put(values, UserColumns.IS_AUTHOR, user.getIsAuthor());
            put(values, UserColumns.IS_USER, user.getIsUser());
            put(values, UserColumns.IS_TEACHER, user.getIsTeacher());
            put(values, UserColumns.GENDER, user.getGender());
            put(values, UserColumns.LAST_LOGIN_DATE, user.getLastLoginDate());
            put(values, UserColumns.SIGNUP_DATE, user.getSignupDate());
            put(values, UserColumns.NUMBER_OF_LOGINS, user.getNumberOfLogins());
            put(values, UserColumns.READING_LEVEL, user.getReadingLevel());
            put(values, UserColumns.SCHOOL_ID, user.getSchoolId());
            put(values, UserColumns.NAME, user.getName());
            put(values, UserColumns.USER_NAME, user.getUserName());
            put(values, UserColumns.IS_UNILOGIN_USER, user.isUniloginUser());
            put(values, UserColumns.PASSWORD_HASH, user.getPasswordHash());
            put(values, UserColumns.IS_REMEMBER_ME, user.isRememberMe());

            return update(values, String.valueOf(user.getId()));
        }

        return 0;
    }

    @Override
    protected User buildFromCursor(Cursor c) {
        User u = null;

        if (c != null) {
            u = new User();
            u.setId(getInt(UserColumns.ID, c));
            u.setEmail(getString(UserColumns.EMAIL, c));
            u.setSecurityStamp(getString(UserColumns.SECURITY_STAMP, c));
            u.setCity(getString(UserColumns.CITY, c));
            u.setCountryId(getInt(UserColumns.COUNTRY_ID, c));
            u.setLanguageId(getInt(UserColumns.LANGUAGE_ID, c));
            u.setState(getString(UserColumns.STATE, c));
            u.setUserTypeId(getInt(UserColumns.USER_TYPE_ID, c));
            u.setIsAuthor(getBoolean(UserColumns.IS_AUTHOR, c));
            u.setIsUser(getBoolean(UserColumns.IS_USER, c));
            u.setIsTeacher(getBoolean(UserColumns.IS_TEACHER, c));
            u.setGender(getInt(UserColumns.GENDER, c));
            u.setLastLoginDate(getString(UserColumns.LAST_LOGIN_DATE, c));
            u.setSignupDate(getString(UserColumns.SIGNUP_DATE, c));
            u.setNumberOfLogins(getInt(UserColumns.NUMBER_OF_LOGINS, c));
            u.setReadingLevel(getInt(UserColumns.READING_LEVEL, c));
            u.setSchoolId(getInt(UserColumns.SCHOOL_ID, c));
            u.setName(getString(UserColumns.NAME, c));
            u.setUserName(getString(UserColumns.USER_NAME, c));
            u.setIsUniloginUser(getBoolean(UserColumns.IS_UNILOGIN_USER, c));
            u.setPasswordHash(getString(UserColumns.PASSWORD_HASH, c));
            u.setRememberMe(getBoolean(UserColumns.IS_REMEMBER_ME, c));
        }
        return u;
    }
}
