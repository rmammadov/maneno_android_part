package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.AppDataTable;
import com.knowlge.maneno.data.db.table.AppDataTable.AppDataColumns;
import com.knowlge.maneno.domain.model.AppData;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class AppDataDao extends Dao<AppData, AppDataTable> {

    @Inject
    public AppDataDao(SQLiteDatabase db) {
        super(db);
    }

    public void put(String key, String value) {
        insertOrUpdate(new AppData(key, value));
    }

    public void put(String key, Integer value) {
        if (value !=  null) {
            put(key, value.toString());
        } else {
            put(key, "");
        }
    }
    public void putNull(String key) {
        insertOrUpdate(new AppData(key, null));
    }

    public String getValue(String key) {
        AppData appData = get(key);

        if (appData != null) {
            return appData.getValue();
        }

        return null;
    }

    @Override
    public long save(AppData entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getKey());
        bind(insertStatement, 2, entity.getValue());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(AppData entity) {
        if (entity != null ) {
            final ContentValues values = new ContentValues();

            put(values, AppDataColumns.KEY, entity.getKey());
            put(values, AppDataColumns.VALUE, entity.getValue());

            return update(values, String.valueOf(entity.getKey()));
        }

        return 0;
    }

    @Override
    protected AppData buildFromCursor(Cursor c) {
        AppData appData = null;

        if (c != null) {
            appData = new AppData();
            appData.setKey(getString(AppDataColumns.KEY, c));
            appData.setValue(getString(AppDataColumns.VALUE, c));
        }

        return appData;
    }
}
