package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.CITY;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.COUNTRY_ID;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.EMAIL;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.GENDER;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.ID;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.IS_AUTHOR;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.IS_REMEMBER_ME;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.IS_TEACHER;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.IS_UNILOGIN_USER;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.IS_USER;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.LANGUAGE_ID;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.LAST_LOGIN_DATE;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.NAME;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.NUMBER_OF_LOGINS;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.PASSWORD_HASH;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.READING_LEVEL;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.SCHOOL_ID;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.SECURITY_STAMP;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.SIGNUP_DATE;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.STATE;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.USER_NAME;
import static com.knowlge.maneno.data.db.table.UserTable.UserColumns.USER_TYPE_ID;


@Singleton
public class UserTable extends Table {

    @Inject
    public UserTable() {}

    private static final String TABLE_NAME = "user";
    private static final String[] COLUMNS = new String[] {ID, EMAIL, SECURITY_STAMP, CITY, COUNTRY_ID,
            LANGUAGE_ID, STATE, USER_TYPE_ID, IS_AUTHOR, IS_USER, IS_TEACHER, GENDER, LAST_LOGIN_DATE,
            SIGNUP_DATE, NUMBER_OF_LOGINS, READING_LEVEL, SCHOOL_ID, NAME, USER_NAME, IS_UNILOGIN_USER,
            PASSWORD_HASH, IS_REMEMBER_ME };

    public static class UserColumns {
        public static final String ID = "id";
        public static final String EMAIL = "email";
        public static final String SECURITY_STAMP = "security_stamp";
        public static final String CITY = "city";
        public static final String COUNTRY_ID = "country_id";
        public static final String LANGUAGE_ID = "language_id";
        public static final String STATE = "state";
        public static final String USER_TYPE_ID = "user_type_id";
        public static final String IS_AUTHOR = "is_author";
        public static final String IS_USER = "is_user";
        public static final String IS_TEACHER = "is_teacher";
        public static final String GENDER = "gender";
        public static final String LAST_LOGIN_DATE = "last_login_date";
        public static final String SIGNUP_DATE = "signup_date";
        public static final String NUMBER_OF_LOGINS = "number_of_logins";
        public static final String READING_LEVEL = "reading_level";
        public static final String SCHOOL_ID = "school_id";
        public static final String NAME = "name";
        public static final String USER_NAME = "user_name";
        public static final String IS_UNILOGIN_USER = "is_unilogin_user";
        public static final String PASSWORD_HASH = "password_hash";
        public static final String IS_REMEMBER_ME = "is_remember_me";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(ID, TYPE_INT);
        types.put(EMAIL, TYPE_TEXT);
        types.put(SECURITY_STAMP, TYPE_TEXT);
        types.put(CITY, TYPE_TEXT);
        types.put(COUNTRY_ID, TYPE_INT);
        types.put(LANGUAGE_ID, TYPE_INT);
        types.put(STATE, TYPE_TEXT);
        types.put(USER_TYPE_ID, TYPE_INT);
        types.put(IS_AUTHOR, TYPE_INT);
        types.put(IS_USER, TYPE_INT);
        types.put(IS_TEACHER, TYPE_INT);
        types.put(GENDER, TYPE_INT);
        types.put(LAST_LOGIN_DATE, TYPE_TEXT);
        types.put(SIGNUP_DATE, TYPE_TEXT);
        types.put(NUMBER_OF_LOGINS, TYPE_INT);
        types.put(READING_LEVEL, TYPE_INT);
        types.put(SCHOOL_ID, TYPE_INT);
        types.put(NAME, TYPE_TEXT);
        types.put(USER_NAME, TYPE_TEXT);
        types.put(IS_UNILOGIN_USER, TYPE_INT);
        types.put(PASSWORD_HASH, TYPE_TEXT);
        types.put(IS_REMEMBER_ME, TYPE_INT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return ID;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
