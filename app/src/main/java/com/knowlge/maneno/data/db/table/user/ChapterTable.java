package com.knowlge.maneno.data.db.table.user;

import com.knowlge.maneno.data.db.table.Table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.BODY_HTML;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.BOOK_ID;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.CONTENT_PATH;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.IMAGES;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.INDEX;
import static com.knowlge.maneno.data.db.table.user.ChapterTable.ChapterColumns.TITLE;



public class ChapterTable extends Table {

    private static final String TABLE_NAME = "chapters";
    private static final String[] COLUMNS = new String[] {BOOK_ID, CONTENT_PATH, TITLE, INDEX, BODY_HTML, IMAGES};

    public static class ChapterColumns {
        public static final String BOOK_ID = "book_id";
        public static final String CONTENT_PATH = "content_path";
        public static final String TITLE = "title";
        public static final String INDEX = "chapter_index"; // "index" is SQLite restricted word.
        public static final String BODY_HTML = "body_html";
        public static final String IMAGES = "images";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(BOOK_ID, TYPE_INT);
        types.put(CONTENT_PATH, TYPE_TEXT);
        types.put(TITLE, TYPE_TEXT);
        types.put(INDEX, TYPE_INT);
        types.put(BODY_HTML, TYPE_TEXT);
        types.put(IMAGES, TYPE_TEXT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return null;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
