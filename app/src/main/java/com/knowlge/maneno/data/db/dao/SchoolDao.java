package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.SchoolTable;
import com.knowlge.maneno.domain.model.School;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.ADDRESS;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.CITY;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.COUNTRY_ID;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.ID;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.NAME;
import static com.knowlge.maneno.data.db.table.SchoolTable.SchoolColumns.ZIP_CODE;


@Singleton
public class SchoolDao extends Dao<School, SchoolTable> {

    @Inject
    public SchoolDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(School entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getSchoolId());
        bind(insertStatement, 2, entity.getName());
        bind(insertStatement, 3, entity.getCountryId());
        bind(insertStatement, 4, entity.getAddress());
        bind(insertStatement, 5, entity.getCity());
        bind(insertStatement, 6, entity.getZipCode());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(School entity) {
        if (entity != null ) {
            final ContentValues values = new ContentValues();

            put(values, NAME, entity.getName());
            put(values, COUNTRY_ID, entity.getCountryId());
            put(values, ADDRESS, entity.getAddress());
            put(values, CITY, entity.getCity());
            put(values, ZIP_CODE, entity.getZipCode());

            return update(values, String.valueOf(entity.getSchoolId()));
        }

        return 0;
    }

    @Override
    protected School buildFromCursor(Cursor c) {
        School school = null;

        if (c != null) {
            school = new School();

            school.setSchoolId(getInt(ID, c));
            school.setName(getString(NAME, c));
            school.setCountryId(getInt(COUNTRY_ID, c));
            school.setAddress(getString(ADDRESS, c));
            school.setCity(getString(CITY, c));
            school.setZipCode(getInt(ZIP_CODE, c));
        }

        return school;
    }
}
