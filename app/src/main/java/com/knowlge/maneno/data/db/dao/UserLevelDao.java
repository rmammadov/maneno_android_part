package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.AuthenticationTable;
import com.knowlge.maneno.data.db.table.UserLevelTable;
import com.knowlge.maneno.domain.model.Level;
import com.knowlge.maneno.domain.model.UserLevel;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.CAN_ADD_NEW_ABILITY;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.CAN_IMPROVE_ABILITY;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DAYS_ACTIVE_IN_LEVEL;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DAYS_IN_LEVEL;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DAYS_REMAINING;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DAY_SINCE_UPGRADE;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DRAGON_COLOR;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.DRAGON_LEVEL;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.FREE_EQUIP_POINTS;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.FREE_STAT_POINTS;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.LAST_UPGRADE_DATE;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.LEVEL_COMPLETE_PERCENT;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.MINUTES_REMAINING;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.MINUTES_REMAINING_TODAY;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.MOOD;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.MOOD_SCORE;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.PROGRESS_WAS_RESETED;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.STAT_POINTS_LIMIT_REACHED;
import static com.knowlge.maneno.data.db.table.UserLevelTable.UserLevelColumns.USER_ID;


@Singleton
public class UserLevelDao extends Dao <UserLevel, UserLevelTable> {

    @Inject
    public UserLevelDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(UserLevel entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getUserId());
        bind(insertStatement, 2, entity.getLevel().getLastUpgradeDate());
        bind(insertStatement, 3, entity.getLevel().getDaySinceUpgrade());
        bind(insertStatement, 4, entity.getLevel().getFreeStatPoints());
        bind(insertStatement, 5, entity.getLevel().getFreeEquipPoints());
        bind(insertStatement, 6, entity.getLevel().getDaysActiveInLevel());
        bind(insertStatement, 7, entity.getLevel().getDaysInLevel());
        bind(insertStatement, 8, entity.getLevel().getLevelCompletePercent());
        bind(insertStatement, 9, entity.getLevel().getMinutesRemaining());
        bind(insertStatement, 10, entity.getLevel().getMinutesRemainingToday());
        bind(insertStatement, 11, entity.getLevel().getDaysRemaining());
        bind(insertStatement, 12, entity.getLevel().getCanAddNewAbility());
        bind(insertStatement, 13, entity.getLevel().getCanImproveAbility());
        bind(insertStatement, 14, entity.getLevel().getStatPointsLimitReached());
        bind(insertStatement, 15, entity.getLevel().getMood());
        bind(insertStatement, 16, entity.getLevel().getMoodScore());
        bind(insertStatement, 17, entity.getLevel().getDragonColor());
        bind(insertStatement, 18, entity.getLevel().getProgressWasReseted());
        bind(insertStatement, 19, entity.getLevel().getDragonLevel());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(UserLevel entity) {
        if (entity != null) {
            final ContentValues values = new ContentValues();

            put(values, USER_ID, entity.getUserId());
            put(values, LAST_UPGRADE_DATE, entity.getLevel().getLastUpgradeDate());
            put(values, DAY_SINCE_UPGRADE, entity.getLevel().getDaySinceUpgrade());
            put(values, FREE_STAT_POINTS, entity.getLevel().getFreeStatPoints());
            put(values, FREE_EQUIP_POINTS, entity.getLevel().getFreeEquipPoints());
            put(values, DAYS_ACTIVE_IN_LEVEL, entity.getLevel().getDaysActiveInLevel());
            put(values, DAYS_IN_LEVEL, entity.getLevel().getDaysInLevel());
            put(values, LEVEL_COMPLETE_PERCENT, entity.getLevel().getLevelCompletePercent());
            put(values, MINUTES_REMAINING, entity.getLevel().getMinutesRemaining());
            put(values, MINUTES_REMAINING_TODAY, entity.getLevel().getMinutesRemainingToday());
            put(values, DAYS_REMAINING, entity.getLevel().getDaysRemaining());
            put(values, CAN_ADD_NEW_ABILITY, entity.getLevel().getCanAddNewAbility());
            put(values, CAN_IMPROVE_ABILITY, entity.getLevel().getCanImproveAbility());
            put(values, STAT_POINTS_LIMIT_REACHED, entity.getLevel().getStatPointsLimitReached());
            put(values, MOOD, entity.getLevel().getMood());
            put(values, MOOD_SCORE, entity.getLevel().getMoodScore());
            put(values, DRAGON_COLOR, entity.getLevel().getDragonColor());
            put(values, PROGRESS_WAS_RESETED, entity.getLevel().getProgressWasReseted());
            put(values, DRAGON_LEVEL, entity.getLevel().getDragonLevel());

            return update(values, String.valueOf(entity.getUserId()));
        }

        return 0;
    }

    @Override
    protected UserLevel buildFromCursor(Cursor c) {
        UserLevel userLevel = null;

        if (c != null) {
            userLevel = new UserLevel();
            Level level = new Level();

            userLevel.setUserId(getInt(AuthenticationTable.AuthenticationColumns.USER_ID, c));
            userLevel.setLevel(level);

            level.setLastUpgradeDate(getString(LAST_UPGRADE_DATE, c));
            level.setDaySinceUpgrade(getInt(DAY_SINCE_UPGRADE, c));
            level.setFreeStatPoints(getInt(FREE_STAT_POINTS, c));
            level.setFreeEquipPoints(getInt(FREE_EQUIP_POINTS, c));
            level.setDaysInLevel(getInt(DAYS_ACTIVE_IN_LEVEL, c));
            level.setDaysInLevel(getInt(DAYS_IN_LEVEL, c));
            level.setLevelCompletePercent(getInt(LEVEL_COMPLETE_PERCENT, c));
            level.setMinutesRemaining(getInt(MINUTES_REMAINING, c));
            level.setMinutesRemainingToday(getInt(MINUTES_REMAINING_TODAY, c));
            level.setDaysRemaining(getInt(DAYS_REMAINING, c));
            level.setCanAddNewAbility(getBoolean(CAN_ADD_NEW_ABILITY, c));
            level.setCanImproveAbility(getBoolean(CAN_IMPROVE_ABILITY, c));
            level.setStatPointsLimitReached(getBoolean(STAT_POINTS_LIMIT_REACHED, c));
            level.setMood(getInt(MOOD, c));
            level.setMoodScore(getInt(MOOD_SCORE, c));
            level.setDragonColor(getInt(DRAGON_COLOR, c));
            level.setProgressWasReseted(getBoolean(PROGRESS_WAS_RESETED, c));
            level.setDragonLevel(getInt(DRAGON_LEVEL, c));
        }

        return userLevel;
    }
}
