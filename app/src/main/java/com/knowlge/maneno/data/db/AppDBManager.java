package com.knowlge.maneno.data.db;

import android.database.sqlite.SQLiteDatabase;

import com.knowlge.maneno.data.db.dao.AppDataDao;
import com.knowlge.maneno.data.db.dao.AuthenticationDao;
import com.knowlge.maneno.data.db.dao.ColorCodeDao;
import com.knowlge.maneno.data.db.dao.CountryDao;
import com.knowlge.maneno.data.db.dao.LanguageDao;
import com.knowlge.maneno.data.db.dao.ReadingPageStatisticsDao;
import com.knowlge.maneno.data.db.dao.ReadingSessionDao;
import com.knowlge.maneno.data.db.dao.SchoolDao;
import com.knowlge.maneno.data.db.dao.TagDao;
import com.knowlge.maneno.data.db.dao.UserDao;
import com.knowlge.maneno.data.db.dao.UserLevelDao;
import com.knowlge.maneno.data.db.table.AuthenticationTable;
import com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable;
import com.knowlge.maneno.data.db.table.ReadingSessionTable;
import com.knowlge.maneno.data.db.table.TagTable;
import com.knowlge.maneno.data.db.table.UserLevelTable;
import com.knowlge.maneno.data.db.table.UserTable;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.log.Log;
import com.knowlge.maneno.domain.model.AppData;
import com.knowlge.maneno.domain.model.AuthenticationData;
import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Country;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.Level;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingSession;
import com.knowlge.maneno.domain.model.School;
import com.knowlge.maneno.domain.model.Tag;
import com.knowlge.maneno.domain.model.User;
import com.knowlge.maneno.domain.model.UserLevel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class AppDBManager {

    @Inject
    Log log;

    @Inject
    SQLiteDatabase db;

    @Inject
    UserDao userDao;

    @Inject
    AuthenticationDao authenticationDao;

    @Inject
    AppDataDao appDataDao;

    @Inject
    UserLevelDao userLevelDao;

    @Inject
    TagDao tagDao;

    @Inject
    LanguageDao languageDao;

    @Inject
    CountryDao countryDao;

    @Inject
    SchoolDao schoolDao;

    @Inject
    ReadingSessionDao readingSessionDao;

    @Inject
    ReadingPageStatisticsDao readingPageStatisticsDao;

    @Inject
    ColorCodeDao colorCodeDao;

    @Inject
    public AppDBManager() {}

    public void saveAuthenticationData(AuthenticationData authenticationData) {
        authenticationDao.insertOrUpdate(authenticationData);
    }

    public User findUserByEmail(String email) {
        return userDao.findBy(UserTable.UserColumns.EMAIL, email, true);
    }

    public User findUserByUsername(String username) {
        return userDao.findBy(UserTable.UserColumns.USER_NAME, username, true);
    }

    public User findUserById(String userId) {
        return userDao.get(userId);
    }

    public void updateUser(User user) {
        userDao.update(user);
    }

    public Integer getCurrentUserId() {
        String result = appDataDao.getValue(AppData.KEY_CURRENT_USER_ID);
        if (Util.isSet(result)) {
            return Integer.parseInt(result);
        }

        return null;
    }

    public void beginTransaction() {
        db.beginTransaction();
    }

    public void setTransactionSuccessful() {
        db.setTransactionSuccessful();
    }

    public void endTransaction() {
        db.endTransaction();
    }

    public void setAppData(String key, Integer value) {
        if (Util.isSet(key)) {
            appDataDao.put(key, value);
        }
    }

    public void saveUser(User user) {
        if (user != null) {
            userDao.insertOrUpdate(user);
        }
    }

    public void saveUserLevel(Integer userId, Level level) {
        if (userId != null && level != null) {
            userLevelDao.insertOrUpdate(new UserLevel(userId, level));
        }
    }

    public UserLevel findUserLevel(Integer userId) {
        return userLevelDao.findBy(UserLevelTable.UserLevelColumns.USER_ID, String.valueOf(userId));
    }

    public AuthenticationData getAuthenticationData(Integer userId) {
        if (userId != null) {
            return authenticationDao.findBy(AuthenticationTable.AuthenticationColumns.USER_ID, String.valueOf(userId));
        }

        return null;
    }

    public void saveTags(List<Tag> tags) {
        if (tags != null) {
            for (Tag tag: tags) {
                tagDao.insertOrUpdate(tag);
            }

            log.d("Saved " + tags.size() + " tags.");
        }
    }

    public void saveLanguages(List<Language> languages) {
        if (languages != null) {
            for (Language language: languages) {
                languageDao.insertOrUpdate(language);
            }

            log.d("Saved " + languages.size() + " languages.");
        }
    }


    public void saveColorCodes(List<ColorCode> colorCodes) {
        if (colorCodes != null) {
            for (ColorCode colorCode: colorCodes) {
                colorCodeDao.insertOrUpdate(colorCode);
            }

            log.d("Saved " + colorCodes.size() + " color codes.");
        }
    }

    public void saveCountries(List<Country> countries) {
        if (countries != null) {
            for (Country country: countries) {
                countryDao.insertOrUpdate(country);
            }

            log.d("Saved " + countries.size() + " countries.");
        }
    }

    public void saveSchools(List<School> schools) {
        if (schools != null) {
            for (School school: schools) {
                schoolDao.insertOrUpdate(school);
            }

            log.d("Saved " + schools.size() + " schools.");
        }
    }

    public Tag getTag(Integer tagId) {
        if (tagId != null) {
            return tagDao.get(String.valueOf(tagId));
        }
        return null;
    }

    public List<Tag> getTags(List<Integer> tagIds) {
        List<Tag> tags = new ArrayList<>();
        if (tagIds != null && tagIds.size() > 0) {
            return tagDao.findBy(tagIds);
        }
        return tags;
    }

    public List<Tag> getTagsByType(Integer typeId) {
        if (typeId != null) {
            return tagDao.findAllBy(TagTable.TagColumns.TAG_TYPE_ID, String.valueOf(typeId));
        }

        return null;
    }

    public long saveReadingSession(ReadingSession readingSession) {
        return readingSessionDao.save(readingSession);
    }

    public Long setCurrentReadingSessionId(Long sessionId) {
        if (sessionId != null) {
            appDataDao.put(AppData.KEY_CURRENT_READING_SESSION_ID, sessionId.toString());
        } else {
            appDataDao.putNull(AppData.KEY_CURRENT_READING_SESSION_ID);
        }

        return sessionId;
    }

    public Long getCurrentReadingSessionId() {
        String result = appDataDao.getValue(AppData.KEY_CURRENT_READING_SESSION_ID);
        if (Util.isSet(result)) {
            return Long.parseLong(result);
        }

        return null;
    }

    public ReadingSession getReadingSession(Long sessionId) {
        if (sessionId != null) {
            return readingSessionDao.get(sessionId.toString());
        }

        return null;
    }

    public void updateReadingSession(ReadingSession readingSession) {
        readingSessionDao.update(readingSession);
    }

    public ReadingPageStatistics getReadingPageStatistics(Long readingPageStatisticsId) {
        if (readingPageStatisticsId != null) {
            return readingPageStatisticsDao.get(readingPageStatisticsId.toString());
        }

        return null;
    }

    public long saveReadingPageStatistics(ReadingPageStatistics readingPageStatistics) {
        return readingPageStatisticsDao.save(readingPageStatistics);
    }

    public void updateReadingPageStatistics(ReadingPageStatistics readingPageStatistics) {
        readingPageStatisticsDao.update(readingPageStatistics);
    }

    public int getDistinctPagesCount(Long readingSessionId) {
        return readingPageStatisticsDao.getDistinctPagesCount(readingSessionId);
    }

    public int getDistinctWordsReadCount(Long readingSessionId) {
        return readingPageStatisticsDao.getDistinctWordsReadCount(readingSessionId);
    }

    public List<ReadingSession> getReadingSessions(Integer userId) {
        if (userId != null) {
            return readingSessionDao.findAllBy(ReadingSessionTable.ReadingSessionColumns.USER_ID, String.valueOf(userId));
        }

        return null;
    }

    public List<ReadingPageStatistics> getReadingPagesStatistics(Long readingSessionId) {
        if (readingSessionId != null) {
            return readingPageStatisticsDao.findAllBy(ReadingPageStatisticsTable.ReadingPageStatisticsColumns.SESSION_ID, String.valueOf(readingSessionId));
        }

        return null;
    }

    public void deleteReadingPagesStatistics(String[] readingPageStatisticsIds) {
        if (readingPageStatisticsIds != null && readingPageStatisticsIds.length > 0) {
            readingPageStatisticsDao.delete(readingPageStatisticsIds);
        }
    }

    public void deleteReadingSession(ReadingSession session) {
        if (session != null) {
            readingSessionDao.delete(new String[]{String.valueOf(session.getBookId())});
        }
    }

    public List<ColorCode> getColorCodes() {
        return colorCodeDao.getAll();
    }

    public List<Language> getLanguages() {
        return languageDao.getAll();
    }
}
