package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.COLOR_CODE;
import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.ICON_URL;
import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.ID;
import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.NAME;
import static com.knowlge.maneno.data.db.table.TagTable.TagColumns.TAG_TYPE_ID;



@Singleton
public class TagTable extends Table {

    @Inject
    public TagTable() {}

    private static final String TABLE_NAME = "tags";
    private static final String[] COLUMNS = new String[] {ID, NAME, TAG_TYPE_ID, ICON_URL, COLOR_CODE};

    public static class TagColumns {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String TAG_TYPE_ID = "tag_type_id";
        public static final String ICON_URL = "icon_url";
        public static final String COLOR_CODE = "color_code";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(ID, TYPE_INT);
        types.put(NAME, TYPE_TEXT);
        types.put(TAG_TYPE_ID, TYPE_INT);
        types.put(ICON_URL, TYPE_TEXT);
        types.put(COLOR_CODE, TYPE_INT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return ID;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
