package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.CountryTable;
import com.knowlge.maneno.domain.model.Country;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.CountryTable.CountryColumns.COUNTRY_ISO_CODE;
import static com.knowlge.maneno.data.db.table.CountryTable.CountryColumns.ID;
import static com.knowlge.maneno.data.db.table.CountryTable.CountryColumns.NAME;



@Singleton
public class CountryDao extends Dao<Country, CountryTable> {

    @Inject
    public CountryDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(Country entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getCountryId());
        bind(insertStatement, 2, entity.getName());
        bind(insertStatement, 3, entity.getCountryIsoCode());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(Country entity) {
        if (entity != null ) {
            final ContentValues values = new ContentValues();

            put(values, NAME, entity.getName());
            put(values, COUNTRY_ISO_CODE, entity.getCountryIsoCode());

            return update(values, String.valueOf(entity.getCountryId()));
        }

        return 0;
    }

    @Override
    protected Country buildFromCursor(Cursor c) {
        Country country = null;

        if (c != null) {
            country = new Country();

            country.setCountryId(getInt(ID, c));
            country.setName(getString(NAME, c));
            country.setCountryIsoCode(getString(COUNTRY_ISO_CODE, c));
        }

        return country;
    }
}
