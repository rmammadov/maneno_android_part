package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.ID;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.PAGE_END_TIME;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.PAGE_NUMBER;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.PAGE_START_TIME;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.SESSION_ID;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.WORDS_READ;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.WORD_INDEX_END;
import static com.knowlge.maneno.data.db.table.ReadingPageStatisticsTable.ReadingPageStatisticsColumns.WORD_INDEX_START;



@Singleton
public class ReadingPageStatisticsTable extends Table {

    @Inject
    public ReadingPageStatisticsTable() {}

    private static final String TABLE_NAME = "reading_page_statistics";
    private static final String[] COLUMNS = new String[] {ID, SESSION_ID, PAGE_NUMBER, WORD_INDEX_START,
            WORD_INDEX_END, PAGE_START_TIME, PAGE_END_TIME, WORDS_READ};

    public static class ReadingPageStatisticsColumns {
        public static final String ID = "id";
        public static final String SESSION_ID = "session_id";
        public static final String PAGE_NUMBER = "page_number";
        public static final String WORD_INDEX_START = "word_index_start";
        public static final String WORD_INDEX_END = "word_index_end";
        public static final String PAGE_START_TIME = "page_start_time";
        public static final String PAGE_END_TIME = "page_end_time";
        public static final String WORDS_READ = "words_read";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(ID, TYPE_INT);
        types.put(SESSION_ID, TYPE_INT);
        types.put(PAGE_NUMBER, TYPE_INT);
        types.put(WORD_INDEX_START, TYPE_INT);
        types.put(WORD_INDEX_END, TYPE_INT);
        types.put(PAGE_START_TIME, TYPE_TEXT);
        types.put(PAGE_END_TIME, TYPE_TEXT);
        types.put(WORDS_READ, TYPE_INT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return ID;
    }

    @Override
    protected boolean isPrimaryKeyAutoincrement() {
        return true;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
