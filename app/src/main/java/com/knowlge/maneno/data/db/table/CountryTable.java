package com.knowlge.maneno.data.db.table;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.CountryTable.CountryColumns.COUNTRY_ISO_CODE;
import static com.knowlge.maneno.data.db.table.CountryTable.CountryColumns.ID;
import static com.knowlge.maneno.data.db.table.CountryTable.CountryColumns.NAME;


@Singleton
public class CountryTable extends Table {

    @Inject
    public CountryTable() {}

    private static final String TABLE_NAME = "countries";
    private static final String[] COLUMNS = new String[] {ID, NAME, COUNTRY_ISO_CODE};

    public static class CountryColumns {
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String COUNTRY_ISO_CODE = "country_iso_code";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(ID, TYPE_INT);
        types.put(NAME, TYPE_TEXT);
        types.put(COUNTRY_ISO_CODE, TYPE_TEXT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return ID;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
