package com.knowlge.maneno.data.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.knowlge.maneno.data.db.dao.user.BookInfoDao;
import com.knowlge.maneno.data.db.dao.user.ChapterDao;
import com.knowlge.maneno.data.db.table.user.ChapterTable;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.Chapter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UserDBManager {

    private BookInfoDao bookInfoDao;
    private ChapterDao chapterDao;

    public UserDBManager(Context context, Integer userId) {
        UserDBOpenHelper userDBOpenHelper = new UserDBOpenHelper(context, userId);
        SQLiteDatabase db = userDBOpenHelper.getWritableDatabase();

        this.bookInfoDao = new BookInfoDao(db, userDBOpenHelper.getBookInfoTable());
        this.chapterDao = new ChapterDao(db, userDBOpenHelper.getChapterTable());
    }

    public BookInfo getBookInfo(Integer bookId) {
        if (bookId != null) {
            return bookInfoDao.get(bookId.toString());
        }
        return null;
    }

    public void saveBookInfo(BookInfo bookInfo) {
        if (bookInfo != null) {
            bookInfoDao.save(bookInfo);
        }
    }

    public void updateBookInfo(BookInfo bookInfo) {
        if (bookInfo != null) {
            bookInfoDao.insertOrUpdate(bookInfo);
        }
    }

    public void saveBookChapters(List<Chapter> chapters) {
        if (chapters != null) {
            for (Chapter ch: chapters) {
                chapterDao.save(ch);
            }
        }
    }

    public int deleteBookChapters(Integer bookInfoId) {
        if (bookInfoId != null) {
            return chapterDao.deleteBy(ChapterTable.ChapterColumns.BOOK_ID, bookInfoId.toString());
        }

        return 0;
    }

    public List<Chapter> getBookChapters(Integer bookId) {
        if (bookId != null) {
            return chapterDao.findAllBy(ChapterTable.ChapterColumns.BOOK_ID, bookId.toString());
        }
        return null;
    }

    public Map<Integer, BookInfo> getBookInfosMap(List<Integer> ids) {
        if (ids != null && ids.size() > 0) {
            List<BookInfo> bookInfos = bookInfoDao.findBy(ids);

            if (bookInfos != null && bookInfos.size() > 0) {
                Map<Integer, BookInfo> result = new HashMap<>();

                for (BookInfo bi: bookInfos) {
                    result.put(bi.getId(), bi);
                }

                return result;
            }
        }

        return null;
    }
}
