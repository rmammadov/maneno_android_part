package com.knowlge.maneno.data.db.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.table.ColorCodeTable;
import com.knowlge.maneno.domain.model.ColorCode;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.ColorCodeTable.ColorCodeColumns.ICON_URL;
import static com.knowlge.maneno.data.db.table.ColorCodeTable.ColorCodeColumns.ID;
import static com.knowlge.maneno.data.db.table.ColorCodeTable.ColorCodeColumns.NAME;


@Singleton
public class ColorCodeDao extends Dao<ColorCode, ColorCodeTable> {

    @Inject
    public ColorCodeDao(SQLiteDatabase db) {
        super(db);
    }

    @Override
    public long save(ColorCode entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getId());
        bind(insertStatement, 2, entity.getName());
        bind(insertStatement, 3, entity.getIconUrl());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(ColorCode entity) {
        if (entity != null ) {
            final ContentValues values = new ContentValues();

            put(values, NAME, entity.getName());
            put(values, ICON_URL, entity.getIconUrl());

            return update(values, String.valueOf(entity.getId()));
        }

        return 0;
    }

    @Override
    protected ColorCode buildFromCursor(Cursor c) {
        ColorCode colorCode = null;

        if (c != null) {
            colorCode = new ColorCode();

            colorCode.setId(getInt(ID, c));
            colorCode.setName(getString(NAME, c));
            colorCode.setIconUrl(getString(ICON_URL, c));
        }

        return colorCode;
    }
}
