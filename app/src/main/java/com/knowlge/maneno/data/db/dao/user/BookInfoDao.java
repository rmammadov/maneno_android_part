package com.knowlge.maneno.data.db.dao.user;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.knowlge.maneno.data.db.dao.Dao;
import com.knowlge.maneno.data.db.table.user.BookInfoTable;
import com.knowlge.maneno.domain.model.BookInfo;

import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.AUTHOR_ID;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.AUTHOR_NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.BACKGROUND_COLOR;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.BOOK_LOAD_DATE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.COLOR_CODE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.DICTIONARY_FILE_PATH;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.HAS_ORIGINAL_NARRATION;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.HAS_TASKS;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.ID;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.IS_AVAILABLE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.IS_PRIVATE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LANGUAGE_ID;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LAST_TIME_OPENED;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LATEST_TEXT_OFFSET;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LEVEL;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LIKE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LIKES_COUNT;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LIX;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.LIX_VALUE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.MANENOS_CHOICE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NARRATION_FILE_NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NARRATION_FILE_PATH;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NARRATION_JSON_PATH;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NARRATION_METADATA_FILE_NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.NUMBER_OF_READS;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.PAGES_READ;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.PAGES_TOTAL;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.PURGE_DATE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.RELEASE_DATE;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.SERIES_NAME;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.SERIES_TAG_ID;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.SUMMARY;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TAG_LIST;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.THUMBNAIL_URL;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TIMESTAMP;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TOTAL_CHAPTERS;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TOTAL_CHARACTERS;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TOTAL_PAGES;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.TOTAL_WORD_COUNT;
import static com.knowlge.maneno.data.db.table.user.BookInfoTable.BookInfoColumns.UNIQUE_FOLDER_NAME;


public class BookInfoDao extends Dao<BookInfo, BookInfoTable> {

    public BookInfoDao(SQLiteDatabase db, BookInfoTable table) {
        super(db);
        this.table = table;
    }

    @Override
    public long save(BookInfo entity) {
        SQLiteStatement insertStatement = getInsertStatement();

        insertStatement.clearBindings();

        bind(insertStatement, 1, entity.getId());
        bind(insertStatement, 2, entity.getName());
        bind(insertStatement, 3, entity.getTotalWordCount());
        bind(insertStatement, 4, entity.getTotalChapters());
        bind(insertStatement, 5, entity.getTotalPages());
        bind(insertStatement, 6, entity.getAuthorId());
        bind(insertStatement, 7, entity.getReleaseDate());
        bind(insertStatement, 8, entity.getLike());
        bind(insertStatement, 9, entity.getLikesCount());
        bind(insertStatement, 10, entity.getAuthorName());
        bind(insertStatement, 11, entity.getSummary());
        bind(insertStatement, 12, entity.getHasOriginalNarration());
        bind(insertStatement, 13, entity.getNarrationFileName());
        bind(insertStatement, 14, entity.getNarrationMetadataFileName());
        bind(insertStatement, 15, entity.getThumbnailUrl());
        bind(insertStatement, 16, entity.getIsPrivate());
        bind(insertStatement, 17, entity.getHasTasks());
        bind(insertStatement, 18, entity.getLanguageId());
        bind(insertStatement, 19, entity.getTotalCharacters());
        bind(insertStatement, 20, entity.getManenosChoice());
        bind(insertStatement, 21, entity.getBackgroundColor());
        bind(insertStatement, 22, entity.getTimestamp());
        bind(insertStatement, 23, entity.getPurgeDate());
        bind(insertStatement, 24, entity.getNumberOfReads());
        bind(insertStatement, 25, entity.getIsAvailable());
        bind(insertStatement, 26, entity.getSeriesName());
        bind(insertStatement, 27, entity.getSeriesTagId());
        bind(insertStatement, 28, entity.getLix());
        bind(insertStatement, 29, entity.getLevel());
        bind(insertStatement, 30, entity.getLixValue());
        bind(insertStatement, 31, entity.getLastTimeOpened());
        bind(insertStatement, 32, entity.getPagesRead());
        bind(insertStatement, 33, entity.getPagesTotal());
        bind(insertStatement, 34, entity.getLatestTextOffset());
        bind(insertStatement, 35, entity.getBookLoadDate());
        bind(insertStatement, 36, entity.getUniqueFolderName());
        bind(insertStatement, 37, entity.getDictionaryFilePath());
        bind(insertStatement, 38, entity.getNarrationFilePath());
        bind(insertStatement, 39, entity.getNarrationJsonPath());
        bind(insertStatement, 40, entity.getTagList());
        bind(insertStatement, 41, entity.getColorCode());

        return insertStatement.executeInsert();
    }

    @Override
    public int update(BookInfo entity) {
        if (entity != null ) {
            final ContentValues values = new ContentValues();

            put(values, NAME, entity.getName());
            put(values, TOTAL_WORD_COUNT, entity.getTotalWordCount());
            put(values, TOTAL_CHAPTERS, entity.getTotalChapters());
            put(values, TOTAL_PAGES, entity.getTotalPages());
            put(values, AUTHOR_ID, entity.getAuthorId());
            put(values, RELEASE_DATE, entity.getReleaseDate());
            put(values, LIKE, entity.getLike());
            put(values, LIKES_COUNT, entity.getLikesCount());
            put(values, AUTHOR_NAME, entity.getAuthorName());
            put(values, SUMMARY, entity.getSummary());
            put(values, HAS_ORIGINAL_NARRATION, entity.getHasOriginalNarration());
            put(values, NARRATION_FILE_NAME, entity.getNarrationFileName());
            put(values, NARRATION_METADATA_FILE_NAME, entity.getNarrationMetadataFileName());
            put(values, THUMBNAIL_URL, entity.getThumbnailUrl());
            put(values, IS_PRIVATE, entity.getIsPrivate());
            put(values, HAS_TASKS, entity.getHasTasks());
            put(values, LANGUAGE_ID, entity.getLanguageId());
            put(values, TOTAL_CHARACTERS, entity.getTotalCharacters());
            put(values, MANENOS_CHOICE, entity.getManenosChoice());
            put(values, BACKGROUND_COLOR, entity.getBackgroundColor());
            put(values, TIMESTAMP, entity.getTimestamp());
            put(values, PURGE_DATE, entity.getPurgeDate());
            put(values, NUMBER_OF_READS, entity.getNumberOfReads());
            put(values, IS_AVAILABLE, entity.getIsAvailable());
            put(values, SERIES_NAME, entity.getSeriesName());
            put(values, SERIES_TAG_ID, entity.getSeriesTagId());
            put(values, LIX, entity.getLix());
            put(values, LEVEL, entity.getLevel());
            put(values, LIX_VALUE, entity.getLixValue());
            put(values, LAST_TIME_OPENED, entity.getLastTimeOpened());
            put(values, PAGES_READ, entity.getPagesRead());
            put(values, PAGES_TOTAL, entity.getPagesTotal());
            put(values, LATEST_TEXT_OFFSET, entity.getLatestTextOffset());
            put(values, BOOK_LOAD_DATE, entity.getBookLoadDate());
            put(values, UNIQUE_FOLDER_NAME, entity.getUniqueFolderName());
            put(values, DICTIONARY_FILE_PATH, entity.getDictionaryFilePath());
            put(values, NARRATION_FILE_PATH, entity.getNarrationFilePath());
            put(values, NARRATION_JSON_PATH, entity.getNarrationJsonPath());
            put(values, TAG_LIST, entity.getTagList());
            put(values, COLOR_CODE, entity.getColorCode());

            return update(values, String.valueOf(entity.getId()));
        }

        return 0;
    }

    @Override
    protected BookInfo buildFromCursor(Cursor c) {
        BookInfo bookInfo = null;

        if (c != null) {
            bookInfo = new BookInfo();

            bookInfo.setId(getInt(ID, c));
            bookInfo.setName(getString(NAME, c));
            bookInfo.setTotalWordCount(getInt(TOTAL_WORD_COUNT, c));
            bookInfo.setTotalChapters(getInt(TOTAL_CHAPTERS, c));
            bookInfo.setTotalPages(getInt(TOTAL_PAGES, c));
            bookInfo.setAuthorId(getInt(AUTHOR_ID, c));
            bookInfo.setReleaseDate(getString(RELEASE_DATE, c));
            bookInfo.setLike(getBoolean(LIKE, c));
            bookInfo.setLikesCount(getInt(LIKES_COUNT, c));
            bookInfo.setAuthorName(getString(AUTHOR_NAME, c));
            bookInfo.setSummary(getString(SUMMARY, c));
            bookInfo.setHasOriginalNarration(getBoolean(HAS_ORIGINAL_NARRATION, c));
            bookInfo.setNarrationFileName(getString(NARRATION_FILE_NAME, c));
            bookInfo.setNarrationMetadataFileName(getString(NARRATION_METADATA_FILE_NAME, c));
            bookInfo.setThumbnailUrl(getString(THUMBNAIL_URL, c));
            bookInfo.setIsPrivate(getBoolean(IS_PRIVATE, c));
            bookInfo.setHasTasks(getBoolean(HAS_TASKS, c));
            bookInfo.setLanguageId(getInt(LANGUAGE_ID, c));
            bookInfo.setTotalCharacters(getInt(TOTAL_CHARACTERS, c));
            bookInfo.setManenosChoice(getBoolean(MANENOS_CHOICE, c));
            bookInfo.setBackgroundColor(getString(BACKGROUND_COLOR, c));
            bookInfo.setTimestamp(getString(TIMESTAMP, c));
            bookInfo.setPurgeDate(getString(PURGE_DATE, c));
            bookInfo.setNumberOfReads(getInt(NUMBER_OF_READS, c));
            bookInfo.setIsAvailable(getBoolean(IS_AVAILABLE, c));
            bookInfo.setSeriesName(getString(SERIES_NAME, c));
            bookInfo.setSeriesTagId(getInt(SERIES_TAG_ID, c));
            bookInfo.setLix(getString(LIX, c));
            bookInfo.setLevel(getString(LEVEL, c));
            bookInfo.setLixValue(getInt(LIX_VALUE, c));
            bookInfo.setLastTimeOpened(getLong(LAST_TIME_OPENED, c));
            bookInfo.setPagesRead(getInt(PAGES_READ, c));
            bookInfo.setPagesTotal(getInt(PAGES_TOTAL, c));
            bookInfo.setLatestTextOffset(getInt(LATEST_TEXT_OFFSET, c));
            bookInfo.setBookLoadDate(getLong(BOOK_LOAD_DATE, c));
            bookInfo.setUniqueFolderName(getString(UNIQUE_FOLDER_NAME, c));
            bookInfo.setDictionaryFilePath(getString(DICTIONARY_FILE_PATH, c));
            bookInfo.setNarrationFilePath(getString(NARRATION_FILE_PATH, c));
            bookInfo.setNarrationJsonPath(getString(NARRATION_JSON_PATH, c));
            bookInfo.setTagList(getIntList(TAG_LIST, c));
            bookInfo.setColorCode(getInt(COLOR_CODE, c));
        }

        return bookInfo;
    }
}
