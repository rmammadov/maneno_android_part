package com.knowlge.maneno.data.db.table;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.BOOK_ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.CHAR_OFFSET;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.END_READING_DATE_MILLIS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.NUMBER_OF_WORDS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.READING_SESSION_ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.READING_STATISTICS_ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.READ_START_TIME_MILLIS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.SESSION_TIME_INTERVAL;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.STATUS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.TIME_INTERVAL;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.UNKNOWN_WORDS;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.USER_ID;
import static com.knowlge.maneno.data.db.table.ReadingSessionTable.ReadingSessionColumns.USER_READING_HISTORY_ID;


@Singleton
public class ReadingSessionTable extends Table {

    @Inject
    public ReadingSessionTable() {}

    private static final String TABLE_NAME = "reading_session";
    private static final String[] COLUMNS = new String[] {ID, USER_ID, STATUS, END_READING_DATE_MILLIS, TIME_INTERVAL,
            SESSION_TIME_INTERVAL, CHAR_OFFSET, UNKNOWN_WORDS, NUMBER_OF_WORDS, READ_START_TIME_MILLIS, BOOK_ID,
            READING_SESSION_ID, USER_READING_HISTORY_ID, READING_STATISTICS_ID};


    public static class ReadingSessionColumns {
        public static final String ID = "id";
        public static final String USER_ID = "user_id";
        public static final String STATUS = "status";
        public static final String END_READING_DATE_MILLIS = "end_reading_date_millis";
        public static final String TIME_INTERVAL = "time_interval";
        public static final String SESSION_TIME_INTERVAL = "session_time_interval";
        public static final String CHAR_OFFSET = "char_offset";
        public static final String UNKNOWN_WORDS = "unknown_words";
        public static final String NUMBER_OF_WORDS = "number_of_words";
        public static final String READ_START_TIME_MILLIS = "read_start_time_millis";
        public static final String BOOK_ID = "book_id";
        public static final String READING_SESSION_ID = "reading_session_id";
        public static final String USER_READING_HISTORY_ID = "user_reading_history_id";
        public static final String READING_STATISTICS_ID = "reading_statistics_id";
    }

    private static Map<String, String> COLUMN_TYPES;
    static {
        Map<String, String> types = new HashMap<>();
        types.put(ID, TYPE_INT);
        types.put(USER_ID, TYPE_INT);
        types.put(STATUS, TYPE_INT);
        types.put(END_READING_DATE_MILLIS, TYPE_INT);
        types.put(TIME_INTERVAL, TYPE_INT);
        types.put(SESSION_TIME_INTERVAL, TYPE_INT);
        types.put(CHAR_OFFSET, TYPE_INT);
        types.put(UNKNOWN_WORDS, TYPE_TEXT);
        types.put(NUMBER_OF_WORDS, TYPE_INT);
        types.put(READ_START_TIME_MILLIS, TYPE_INT);
        types.put(BOOK_ID, TYPE_INT);
        types.put(READING_SESSION_ID, TYPE_INT);
        types.put(USER_READING_HISTORY_ID, TYPE_TEXT);
        types.put(READING_STATISTICS_ID, TYPE_TEXT);

        COLUMN_TYPES = Collections.unmodifiableMap(types);
    }


    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String getPrimaryKey() {
        return ID;
    }

    @Override
    protected boolean isPrimaryKeyAutoincrement() {
        return true;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    protected String getType(String columnName) {
        return COLUMN_TYPES.get(columnName);
    }
}
