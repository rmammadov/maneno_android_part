package com.knowlge.maneno.log;

import com.knowlge.maneno.domain.log.Log;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
public class AppLog implements Log {
    public static final String TAG = "MNN";

    @Inject
    public AppLog() {}

    @Override
    public void d(String message) {
        android.util.Log.d(TAG, getCurrentThread() + message);
    }

    @Override
    public void e(String message) {
        android.util.Log.e(TAG, getCurrentThread() + message);
        // todo - add Crashlytics logException after integration.
    }

    @Override
    public void e(String message, Throwable t) {
        android.util.Log.e(TAG, getCurrentThread() + message, t);
        // todo - add Crashlytics logException after integration.

    }

    @Override
    public void e(Throwable t) {
        android.util.Log.e(TAG, getCurrentThread() + t.getMessage(), t);
        // todo - add Crashlytics logException after integration.
    }

    private String getCurrentThread() {
        return "[" + Thread.currentThread().getName() + "]";
    }
}
