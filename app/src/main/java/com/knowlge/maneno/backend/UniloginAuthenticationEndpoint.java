package com.knowlge.maneno.backend;

import com.knowlge.maneno.domain.model.AuthenticationResult;
import com.knowlge.maneno.domain.model.UniloginAuthenticationResult;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;


public interface UniloginAuthenticationEndpoint {

    @FormUrlEncoded
    @POST("mauth/")
    Call<UniloginAuthenticationResult> authenticate(@Field("wsBrugerid") String wsBrugerid,
                                                    @Field("UTCtime") String UTCtime,
                                                    @Field("kontekst") String kontekst,
                                                    @Field("projekt") String projekt,
                                                    @Field("BrugerId") String brugerId,
                                                    @Field("Auth") String auth);
}
