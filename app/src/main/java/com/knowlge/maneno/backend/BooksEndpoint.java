package com.knowlge.maneno.backend;

import com.knowlge.maneno.domain.Consts;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.BookInfoList;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingProgress;
import com.knowlge.maneno.domain.model.ReadingStatisticsParams;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface BooksEndpoint {

    @GET("books/recommendations")
    Call<List<Integer>> getRecommendations(@Query("userid") Integer userId,
                                           @Header("Authorization") String authenticationHeader);

    @GET("books/{bookId}/info")
    Call<BookInfo> getBookInfo(@Path("bookId") Integer bookId,
                               @Header("Authorization") String authorizationHeader);

    @GET("book")
    Call<ResponseBody> getBookContent(@Query("encr") String encr,
                                      @Query("hash") String hash,
                                      @Query("encrypt") Boolean encrypt,
                                      @Header("Authorization") String authorizationHeader,
                                      @Header(Consts.PROGRESS_ID_HEADER) String progressId);

    @GET("book")
    Call<ResponseBody> getBookContent(@Query("encr") String encr,
                                      @Query("hash") String hash,
                                      @Query("encrypt") Boolean encrypt,
                                      @Header("Authorization") String authorizationHeader);

    @Headers({"Content-Type: application/json"})
    @POST("books/{bookId}/readingstatistics")
    Call<ReadingProgress> sendReadingStatistics(@Path("bookId") Integer bookId,
                                                @Body ReadingStatisticsParams readingStatisticsParams,
                                                @Header("Authorization") String authorizationHeader);

    @POST("books/{bookId}/readingPagesStatistics")
    Call<Boolean> sendReadingPagesStatistics(@Path("bookId") Integer bookId,
                                             @Query("readingHistoryId") String userReadingHistoryId,
                                             @Body List<ReadingPageStatistics> readingPageStatisticsList,
                                             @Header("Authorization") String authorizationHeader);

    @GET("books")
    Call<BookInfoList> getBooks(@Query("authorId") Integer authorId,
                                @Query("colorCodes") String colorCodes,
                                @Query("tags") String tags,
                                @Query("languages") String languages,
                                @Query("liked") Boolean liked,
                                @Query("offset") Integer offset,
                                @Query("limit") Integer limit,
                                @Header("Authorization") String authorizationHeader);
}
