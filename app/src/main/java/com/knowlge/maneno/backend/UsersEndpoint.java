package com.knowlge.maneno.backend;

import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.domain.model.Level;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface UsersEndpoint {

    @GET("users/{userId}/level")
    Call<Level> getUserLevel(@Path("userId") Integer userId,
                             @Header("Authorization") String authenticationHeader);

    @GET("achievements")
    Call<List<Achievement>> getAchievements(@Query("onlyAchieved") boolean onlyAchieved,
                                            @Header("Authorization") String authorizationHeader);
}
