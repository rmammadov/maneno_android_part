package com.knowlge.maneno.backend;

import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Country;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.School;
import com.knowlge.maneno.domain.model.Tag;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;


public interface DictionariesEndpoint {

    @GET("tags")
    Call<List<Tag>> getTags(@Header("StaticToken") String staticToken);

    @GET("languages")
    Call<List<Language>> getLanguages(@Header("StaticToken") String staticToken);

    @GET("colorcodes")
    Call<List<ColorCode>> getColorCodes(@Header("StaticToken") String staticToken);

    @GET("countries")
    Call<List<Country>> getCountries(@Header("StaticToken") String staticToken);

    @GET("schools")
    Call<List<School>> getSchools(@Header("StaticToken") String staticToken);
}
