package com.knowlge.maneno.backend;

import com.knowlge.maneno.domain.model.AuthenticationResult;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface AuthenticationEndpoint {

    @FormUrlEncoded
    @POST("token")
    Call<AuthenticationResult> getToken(@Field("grant_type") String grantType,
                                        @Field("username") String username,
                                        @Field("password") String password);

    @GET("checkForUnilogin")
    Call<Boolean> checkForUnilogin(@Query("username") String username,
                                   @Query("token") String uniloginToken,
                                   @Header("StaticToken") String staticToken);

    @GET("obtaintoken?unilogin=true")
    Call<AuthenticationResult> obtainToken(@Query("username") String username,
                                           @Query("token") String uniloginToken);

    @GET("assignTempLicense")
    Call<Boolean> assignTempLicense(@Query("username") String username,
                                    @Query("token") String uniloginToken,
                                    @Header("StaticToken") String staticToken);
}
