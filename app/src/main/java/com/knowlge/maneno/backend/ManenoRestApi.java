package com.knowlge.maneno.backend;

import com.knowlge.maneno.ManenoApplication;
import com.knowlge.maneno.domain.Util;
import com.knowlge.maneno.domain.model.Achievement;
import com.knowlge.maneno.domain.model.AuthenticationData;
import com.knowlge.maneno.domain.model.AuthenticationResult;
import com.knowlge.maneno.domain.model.BookInfo;
import com.knowlge.maneno.domain.model.BookInfoList;
import com.knowlge.maneno.domain.model.ColorCode;
import com.knowlge.maneno.domain.model.Country;
import com.knowlge.maneno.domain.model.Language;
import com.knowlge.maneno.domain.model.Level;
import com.knowlge.maneno.domain.model.ReadingPageStatistics;
import com.knowlge.maneno.domain.model.ReadingProgress;
import com.knowlge.maneno.domain.model.ReadingStatisticsParams;
import com.knowlge.maneno.domain.model.School;
import com.knowlge.maneno.domain.model.Tag;
import com.knowlge.maneno.domain.model.UniloginAuthenticationResult;
import com.knowlge.maneno.domain.repository.RestApi;
import com.knowlge.maneno.domain.repository.RestApiRequestProgressListener;
import com.knowlge.maneno.domain.usecase.login.UniloginResult;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.inject.Singleton;

import okhttp3.ResponseBody;
import retrofit2.Call;


@Singleton
public class ManenoRestApi implements RestApi {

    private static final String GRANT_TYPE_PASSWORD = "password";
    private static final String UNILOGIN_TOKEN = "e133ae40480f4897882225235461c03e";
    private static final String STATIC_TOKEN = "U09qkYTaNuKxPfyfwr9GgrLlcrzgAt28eBFopjSRSOMKPEYr5S8ulsCNZxh5SlRQfE8RnaGcln8FkmO0DWqx46";

    private static final String UNILOGIN_WS_USER_ID = "wsknowlge";
    private static final String UNILOGIN_WS_PASSWORD = "9S5mc6hYgmNh";
    private static final String UNILOGIN_CONTEXT = "A04333";
    private static final String UNILOGIN_PROJECT = "Knowlge";

    private static final String UNILOGIN_RESULT_SUCCESS_CODE = "1";
    private static final String UNILOGIN_RESULT_TEMP_LICENSE_CODE = "-1";

    private ManenoApplication application;
    private AuthenticationData authenticationData;

    private final AuthenticationEndpoint authenticationEndpoint;
    private final UniloginAuthenticationEndpoint uniloginAuthenticationEndpoint;
    private final BooksEndpoint booksEndpoint;
    private final DictionariesEndpoint dictionariesEndpoint;
    private final UsersEndpoint usersEndpoint;


    public ManenoRestApi(ManenoApplication application, AuthenticationData authenticationData, AuthenticationEndpoint authenticationEndpoint,
                         UniloginAuthenticationEndpoint uniloginAuthenticationEndpoint, BooksEndpoint booksEndpoint,
                         DictionariesEndpoint dictionariesEndpoint, UsersEndpoint usersEndpoint) {
        this.application = application;
        this.authenticationData = authenticationData;

        this.authenticationEndpoint = authenticationEndpoint;
        this.uniloginAuthenticationEndpoint = uniloginAuthenticationEndpoint;
        this.booksEndpoint = booksEndpoint;
        this.dictionariesEndpoint = dictionariesEndpoint;
        this.usersEndpoint = usersEndpoint;
    }

    @Override
    public void setAuthenticationData(AuthenticationData authenticationData) {
        this.authenticationData = authenticationData;
    }

    public AuthenticationResult getToken(String username, String password) throws IOException {
        Call<AuthenticationResult> resCall = authenticationEndpoint.getToken(GRANT_TYPE_PASSWORD, username, password);
        return resCall.execute().body();
    }

    public Boolean checkForUnilogin(String username) throws IOException {
        Call<Boolean> resCall = authenticationEndpoint.checkForUnilogin(username, UNILOGIN_TOKEN, STATIC_TOKEN);
        return resCall.execute().body();
    }

    @Override
    public AuthenticationResult getTokenForUniloginUser(String username) throws IOException {
        Call<AuthenticationResult> resCall = authenticationEndpoint.obtainToken(username, UNILOGIN_TOKEN);
        return resCall.execute().body();
    }

    @Override
    public int authenticateWithUniloginServer(String username, String password) throws IOException {
        String utcTime = String.valueOf(new Date().getTime()/1000); // In seconds, so divide by 1000.
        String k0 = Util.sha1(UNILOGIN_WS_PASSWORD) + Util.sha1(password);
        String m0 = utcTime + UNILOGIN_CONTEXT + UNILOGIN_PROJECT + username;
        String auth = Util.hmacsha1(m0, k0);

        Call<UniloginAuthenticationResult> call = uniloginAuthenticationEndpoint.authenticate(UNILOGIN_WS_USER_ID, utcTime, UNILOGIN_CONTEXT, UNILOGIN_PROJECT, username, auth);
        UniloginAuthenticationResult result = call.execute().body();

        if (result != null) {
            if (UNILOGIN_RESULT_SUCCESS_CODE.equals(result.getValid())) {
                return UniloginResult.SUCCESS;
            } else if (UNILOGIN_RESULT_TEMP_LICENSE_CODE.equals(result.getValid())) {
                return UniloginResult.TEMP_LICENSE;
            }
        }

        return UniloginResult.FAILED;
    }

    @Override
    public boolean assignTempLicense(String username) throws IOException {
        Call<Boolean> resCall = authenticationEndpoint.assignTempLicense(username, UNILOGIN_TOKEN, STATIC_TOKEN);
        return resCall.execute().body();
    }

    @Override
    public Level getUserLevel(Integer userId) throws IOException {
        Call<Level> call = usersEndpoint.getUserLevel(userId, getAuthorizationHeader());
        return call.execute().body();
    }

    @Override
    public List<Achievement> getAchievements(boolean onlyAchieved) throws IOException {
        Call<List<Achievement>> call = usersEndpoint.getAchievements(onlyAchieved, getAuthorizationHeader());
        return call.execute().body();
    }

    @Override
    public BookInfoList getBooks(Integer authorId, String colorCodes, String tags, String languages,
                                 Boolean liked, Integer offset, Integer limit) throws IOException {
        Call<BookInfoList> call = booksEndpoint.getBooks(authorId, colorCodes, tags, languages, liked, offset, limit, getAuthorizationHeader());
        return call.execute().body();
    }

    @Override
    public List<Tag> getTags() throws IOException {
        Call<List<Tag>> resCall = dictionariesEndpoint.getTags(STATIC_TOKEN);
        return resCall.execute().body();
    }

    @Override
    public List<Language> getLanguages() throws IOException {
        Call<List<Language>> resCall = dictionariesEndpoint.getLanguages(STATIC_TOKEN);
        return resCall.execute().body();
    }

    @Override
    public List<ColorCode> getColorCodes() throws IOException {
        Call<List<ColorCode>> call = dictionariesEndpoint.getColorCodes(STATIC_TOKEN);
        return call.execute().body();
    }

    @Override
    public List<Country> getCountries() throws IOException {
        Call<List<Country>> resCall = dictionariesEndpoint.getCountries(STATIC_TOKEN);
        return resCall.execute().body();
    }

    @Override
    public List<School> getSchools() throws IOException {
        Call<List<School>> resCall = dictionariesEndpoint.getSchools(STATIC_TOKEN);
        return resCall.execute().body();
    }

    @Override
    public List<Integer> getBookRecommendations() throws IOException {
        if (authenticationData != null) {
            Call<List<Integer>> recommendationsCall = booksEndpoint.getRecommendations(authenticationData.getUserId(), getAuthorizationHeader());
            return recommendationsCall.execute().body();
        }

        return null;
    }

    @Override
    public BookInfo getBookInfo(Integer bookId) throws IOException {
        Call<BookInfo> resCall = booksEndpoint.getBookInfo(bookId, getAuthorizationHeader());
        return resCall.execute().body();
    }

    @Override
    public InputStream getBookContent(String encr, String hash, boolean encrypt, final RestApiRequestProgressListener progressListener) throws IOException {
        try {
            Call<ResponseBody> bookContent;

            if (progressListener != null) {
                application.setRequestProgressListener(progressListener.getProgressId(), progressListener);
                bookContent = booksEndpoint.getBookContent(encr, hash, encrypt, getAuthorizationHeader(), progressListener.getProgressId());
            } else {
                bookContent = booksEndpoint.getBookContent(encr, hash, encrypt, getAuthorizationHeader());
            }

            return bookContent.execute().body().byteStream();
        } finally {
            // Remove request progress listener.
            if (progressListener != null) {
                application.setRequestProgressListener(progressListener.getProgressId(), null);
            }
        }
    }

    @Override
    public ReadingProgress sendReadingStatistics(Integer bookId, ReadingStatisticsParams readingStatisticsParams) throws IOException {
        Call<ReadingProgress> call = booksEndpoint.sendReadingStatistics(bookId, readingStatisticsParams, getAuthorizationHeader());

        return call.execute().body();
    }

    @Override
    public boolean sendReadingPagesStatistics(Integer bookId, String userReadingHistoryId, List<ReadingPageStatistics> readingPageStatisticsList) throws IOException {
        Call<Boolean> call = booksEndpoint.sendReadingPagesStatistics(bookId, userReadingHistoryId, readingPageStatisticsList, getAuthorizationHeader());

        return call.execute().body();
    }

    private String getAuthorizationHeader() {
        if (authenticationData != null) {
            return authenticationData.getTokenType() + " " + authenticationData.getAccessToken();
        }

        return "";
    }
}